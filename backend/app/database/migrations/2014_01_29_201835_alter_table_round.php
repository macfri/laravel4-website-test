<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableRound extends Migration {

	public function up()
    {
        Schema::table('game_games_round', function($t) {
            
            $t->integer('multi')
                ->unsigned()
                ->nullable()
                ->index();
            $t->string('full_name')
                ->nullable();
            $t->dropColumn('guess');
            $t->dropColumn('was_anonymous');
            $t->dropColumn('share');
            $t->dropColumn('enabled');
        });
	}

	public function down()
    {

        Schema::table('game_games_round', function($t) {

            $t->boolean('guess')
                ->index()
                ->default(0);
            $t->integer('was_anonymous')
                ->default(0);
            $t->integer('share')
                ->boolean()
                ->default(0);
            $t->boolean('enabled')
                ->index()
                ->default(0);
            $t->dropColumn('multi');
        });
	}
}
