<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGamesIsNew extends Migration {


	public function up()
    {
        Schema::table('game_players', function($t) {
            $t->boolean('is_new')
                ->index();
        });


        Schema::table('game_games_chulls', function($t) {
            $t->boolean('is_new')
                ->index();
       });

        Schema::table('game_games_round', function($t) {
            $t->boolean('is_new')
                ->index();
       });

	}


	public function down()
    {
        Schema::table('game_players', function($t) {
            $t->dropColumn('is_new');
        });

        Schema::table('game_games_chulls', function($t) {
            $t->dropColumn('is_new');
        });

        Schema::table('game_games_round', function($t) {
            $t->dropColumn('is_new');
        });

	}

}
