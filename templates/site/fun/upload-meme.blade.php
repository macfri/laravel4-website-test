    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('freetime') }}">Pasa el rato</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <!-- HEADER -->
        @include('site.fun.menu')
        <!-- HEADER -->
      </div>

      <div class="content">
        <div class="row fun">
          <div class="fun-upload-meme">
            <h2>sube tu meme</h2>
            <div class="app-meme">
              <div class="app-memes-list">
                @foreach ($default_memes as $meme)
                    <img src="{{ asset($meme->image->path) }}" data-image-id="{{ $meme->id }}" class="app-meme-img">
                @endforeach</div>
              <div class="app-meme-generator">
                @if(Session::has('meme'))
                <div class="finish-meme">
                  <h3>¡Tu Meme ha sido creado exitosamente!</h3>
                    <a href="{{ URL::route('freetime_category', 'sube-tu-meme') }}" class="btn btn-create-meme">crear otro</a>
                    <img src="{{ asset(Session::get('meme')->image->path) }}" class="img-preview">
                        <a href="{{ URL::route('freetime_download', Session::get('meme')->id) }}" class="btn-download-meme">descarga tu meme</a>
                </div>
                @else
                  <div class="meme-generator-bg" data-default="{{ $default_meme }}" data-static="{{ URL::asset('static') }}">
                    @if (!Session::has('status_code'))
                    <div id="memegame"></div>
                    <form method="post" action="{{ URL::route('freetime_do_upload') }}" class="app-meme-form">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
                      <div class="build-meme">
                        <input id="image_str" type="hidden" value="" name="image">
                        <label>Texto 1</label>
                        <input type="text" name="text1" value="TEXTO 1" maxlength="32" class="top-line">
                        <label>Texto 2</label>
                        <input type="text" name="text2" value="TEXTO 2" maxlength="20" class="bottom-line">
                        <select name="category">
                          <option value="bravazo">Bravazos</option>
                          <option value="lol">LOL</option>
                        </select>
                        <button class="btn btn-create-meme">crear</button>
                      </div>
                    </form>
                      @else
                    @if($errors->has())
                      <div class="finish-meme"> 
                        @foreach($errors->all() as $m)<h3>{{ $m}}</h3>@endforeach 
                        <a href="{{ URL::route('freetime_category', 'sube-tu-meme') }}" class="btn btn-create-meme">volver</a>
                      </div>
                    @endif
                @endif
                </div>
                 @endif
              </div>
              <div class="upload-your-meme">
                <label>Sube una foto</label>
                <form id="upload-image" action="{{ URL::route('freetime_do_upload_me') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
                  <input id="file-root" type="text" disabled class="left"><a class="btn-search-meme">Buscar</a>
                  <input id="upload-file" type="file" name="photo" style="width:0; height:0; display:block">
                </form>
                <p>*Solo las imágenes en .JPG y .PNG están permitidas.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'upload-meme'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'upload-meme'))
    @include('site.register-model')
    <script>
      window.app = window.app || {}
      
    </script>
    <script src="{{ URL::asset('static/site/scripts/libs/swfobject.js') }}"></script>
    <script data-main="{{ asset('static/site/scripts/site/upload-meme') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
