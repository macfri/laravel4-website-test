<?php

class AdminPermissionController extends BaseController {

    private $viewpath = 'admin/permission/';

    public function index() {

        if (!SystemUser::has_perm('permissions-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $items = Permission::where('id', '>', 0);
        $total = $items->count();
        $items = $items->paginate(15);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('permissions-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        return View::make(
                        $this->viewpath . __function__, array(
                        )
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('permissions-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Permission::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('permissions-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = new Permission();
        $item->name = Input::get('name');
        $item->slug = Permission::get_slug($item->name);

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_permission_add')
                            ->with('failure', $exception->getMessage());
        }

        return Redirect::route('admin_permission')
                        ->with('success', sprintf(
                                        'Permission %s Created Successfully.', $item->title));
    }

    public function update($id) {

        if (!SystemUser::has_perm('permissions-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Permission::find($id);
        $item->slug = Input::get('slug');
        $item->name = Input::get('title');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_permission_edit', $id)
                            ->with('failure', $exception->getMessage());
        }

        return Redirect::route('admin_permission')
                        ->with('success', 'Permission Updated Successfully.');
    }

    public function destroy($id) {
        if (!SystemUser::has_perm('permissions-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        Permission::find($id)->delete();
        return Redirect::route('admin_permission')
                        ->with('success', 'Permission Deleted Successfully.');
    }

}
