require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'modal': '../modules/modal',
    'funvote': '../modules/funvote',
    'register': '../modules/register',
    'video': '../modules/video',
    'auth': '../modules/auth'


  },
  shim: {
    'modal': ['jquery'],
    'register': ['jquery'],
    'video': ['jquery'],
    'auth': ['jquery'],
    'funvote': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'register',
  'video',
  'auth',
  'funvote'
],
function ( $, modal,register, video, auth, funvote  ) {
  modal();

  register($('#register'));
  auth($('#login'));


  funvote({
    like: '.like-large',
    dislike: '.dislike-large'
  });

  $('.facebook-share').on('click', function(event){
    event.preventDefault();
    var $el = $(this);
    var link = $el.attr('data-post-link');
    var picture = $el.attr('data-post-image');

    FB.ui({
      method: 'feed',
      name: 'Pasa el rato - Milo',
      link: link,
      picture: picture,
      caption: '',
      description: 'Pásala chévere y crea tus memes. Entra Aquí.'
    });

  });

  $('.twitter-share').on('click', function(event){
    event.preventDefault();
    $el = $(this);
    var link = $el.attr('data-post-link');
    var popWwidth = 570;
    var popHeight = 280;
    var left = (screen.width/2)-(popWwidth/2);
    var top = (screen.height/2)-(popHeight/2);
    var setText = 'Si buscas crear memes chéveres, entra aquí ';

    var url = 'https://twitter.com/intent/tweet?text='+setText+'&url=' + link;

    window.open(url, 'Twitter', "width="+popWwidth+",height="+popHeight+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+left+",top="+top+"");
  });

});
