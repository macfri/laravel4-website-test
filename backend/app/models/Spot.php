<?php

class Spot extends Eloquent {

    protected $table = 'spots';
    protected $fillable = array(
        'title',
        'description',
        'slug',
        'brief',
        'enabled',
        'author_id',
        'block_size',
        'image_id',
        'image_home_id'
    );

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public function imageHome() {
        return $this->belongsTo('Photo');
    }

    public function imageSlider() {
        return $this->belongsTo('Photo');
    }

    public function gallery() {
        return $this->hasMany('EventPhoto');
    }

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw(
                        "slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public static function validate($input) {
        $rules = array(
            'title' => 'Required|Min:1|Max:100',
            'code' => 'Required',
            'photo' => 'Required|mimes:jpeg,png'
        );
        
        $messages = array(
            'required' => 'El campo :attribute es obligatorio',
        );

        return Validator::make($input, $rules, $messages);
    }
    
    
    public static function max_position() {
        return Spot::max('order');
    }    

}
