<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SystemUserRoles extends Migration {

	public function up()
    {
		Schema::create('system_users_role', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
			$table->timestamps();
        });

		Schema::create('system_users_permissions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('slug', 100)->index();
			$table->timestamps();
        });

		Schema::create('system_users_role_permissions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->integer('role_id')
                ->unsigned()
                ->index();
            $table->foreign('role_id')
                ->references('id')
                ->on('system_users_role')
                ->on_update('cascade');
            $table->integer('permission_id')
                ->unsigned()
                ->index();
            $table->foreign('permission_id')
                ->references('id')
                ->on('system_users_permissions')
                ->on_update('cascade');
        });
        Schema::table('system_users', function($t) {
            $t->integer('role_id')
                ->unsigned()
                ->nullable()
                ->index();
            $t->foreign('role_id')
                ->references('id')
                ->on('system_users_role')
                ->on_update('cascade');
        });
	}

	public function down()
    {

        Schema::table('system_users', function($t) {
            $t->dropForeign('system_users_role_id_foreign');
            $t->dropColumn('role_id');
        });

        Schema::drop('system_users_role_permissions');
		Schema::drop('system_users_role');
		Schema::drop('system_users_permissions');

	}
}
