    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.users.menu', array('section'=>''))
        <!-- MENU -->
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div id="register-page">
          <div class="row modal-session myform">
            <div class="columns large-12 large-centered">
              <form id="register" action="{{ URL::route('user_add_simple') }}" method="post" class="custom form-reg">

                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                <h3>Regístrate</h3>
                <div class="row">
                  <div class="columns large-6">
                    <label>Tu Nombre</label>
                    <input type="text" name="first_name" id="first_name" maxlength="50" value="{{ Form::old('first_name') }}" class="text">
                      @if($errors->has('first_name'))<label for="email" class="error">{{ $errors->first('first_name') }}</label>@endif
                  </div>
                  <div class="columns large-6">
                    <label>Tu Apellido</label>
                    <input type="text" name="last_name" id="last_name" maxlength="50" value="{{ Form::old('last_name') }}" class="text">
                      @if($errors->has('last_name'))<label for="email" class="error">{{ $errors->first('last_name') }}</label>@endif
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-6">
                    <label>Tu Correo electronico</label>
                    <input type="text" name="email" id="email" value="{{ Form::old('email') }}">
                      @if($errors->has('email'))<label for="email" class="error">{{ $errors->first('email') }}</label>@endif
                  </div>
                  <div class="columns large-6">
                    <label>¿Qué edad tienes?</label>
                    <div class="row">
                      <div class="columns large-4">
                          <select name="day" id="day"  class="text">
                            <option value="">Día</option>
                            @foreach ($days as $day=>$row)
                            <option value="{{ $day }}" @if($day == Form::old('day')) selected @endif>{{ $row }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="columns large-4">
                          <select name="month" id="month" class="text">
                            <option value="">Mes</option>
                            @foreach ($months as $month=>$row)
                            <option value="{{ $month }}" @if($month == Form::old('month')) selected @endif>{{ $row }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="columns large-4">
                          <select name="year" id="year" class="text">
                            <option value="">Año</option>
                            @foreach ($years as $year=>$row)
                            <option value="{{ $year }}" @if($year == Form::old('year')) selected @endif>{{ $row }}</option>
                            @endforeach
                          </select>
                      </div>
                      @if($errors->has('date_birth'))<label for="email" class="error">{{ $errors->first('date_birth') }}</label>@endif
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="columns large-6">
                    <label>DNI  de mamá, papá, apoderado o el tuyo</label>
                    <input type="text" name="dni" id="dni" maxlength="8" value="{{ Form::old('dni') }}" class="numeric">
                      @if($errors->has('dni'))<label for="email" class="error">{{ $errors->first('dni') }}</label>@endif
                  </div>
                  <!-- <div class="columns large-6"><span class="text-help" style="color: #066427">DNI  de mamá, papá, apoderado o el tuyo</span></div> -->
                </div>




                <div class="row">
                  <div class="columns large-6">
                    <label>Crea tu Contrase&ntilde;a</label>
                    <input type="password" name="password" id="password" maxlength="70" value="">
                      @if($errors->has('password'))<label for="email" class="error">{{ $errors->first('password') }}</label>@endif
                  </div>
                  <div class="columns large-6" style="color: #066427">
                    <span class="text-help">
                      .Mínimo de 8 caracteres<br/>
                      .Incluir un número<br/>
                      .Incluir 1 letra mayúscula y minúscula
                    </span>
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-6">
                    <label>Tu Teléfono</label>
                    <input type="text" name="phone" class="number" id="phone" maxlength="9" value="{{ Form::old('phone') }}">
                      @if($errors->has('phone'))<label for="phone" class="error">{{ $errors->first('phone') }}</label>@endif
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-12 h-30">
                    <label class="check">
                      <input type="checkbox" name="reply_email" id="reply_email" value="1">
                      <p>Deseo recibir notificaciones y promociones sobre las novedades de Milo.</p>
                    </label>
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-12 h-45">
                    <label class="check">
                      <input type="checkbox" name="authorize_father" id="authorize_father" value="1">
                      <p>Mis padres autorizan que participe de las promociones y concursos.</p>
                    </label>
                    <div class="authorize-error"></div><br>
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-12 h-45">
                    <label class="check">
                      <input type="checkbox" name="is_lima" id="is_lima" value="1">
                      <p>Eres de lima.</p>
                    </label>
                    <div class="authorize-error"></div><br>
                  </div>
                </div>

                <div class="row">
                  <div class="columns large-12">
                    <button class="right btn-continue">Registrarse</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>''))
        <!-- FOOTER -->
      </div>
    </div>

    <script data-main="{{ asset('static/site/scripts/site/register-simple') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
    <script>
      window.app = window.app || {};
      app.validate_profile_email = "{{ URL::route('validate_form_email') }}";
      app.validate_profile_dni = "{{ URL::route('validate_form_dni') }}";

    </script>


  </body>
</html>
