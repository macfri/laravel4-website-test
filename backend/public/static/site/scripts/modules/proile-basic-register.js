define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){


  function register($form){

    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        first_name: {
          required: true
        },
        last_name: {
          required: true
        },
        dni: {
          required: true
        },
        phone: {
          required: true,
          minlength: 7
        }
      },
      messages: {
        email: {
          required: 'Olvidaste  ingresar tu correo electrónico.',
          email: 'Necesitas un correo válido'
        },
        first_name: {
          required: 'Olvidaste ingresar tu nombre'
        },
        last_name: {
          required: 'Olvidaste ingresar tus apellidos'
        },
        dni: {
          required: 'Olvidaste ingresar el DNI'
        },
        phone: {
          required: 'Olvidaste ingresar tu teléfono'
        },        
        authorize_father: {
          required: 'campo requerido'
        }
      }
    });

    $form.find('#year').on('change', function(){
      $el = $(this);
      $authorize = $form.find('#authorize_father');
      var year = parseInt( $el.val(), 10 );
      var current_year = new Date().getFullYear();

      if( ( current_year - year ) < 18 ) {

        $authorize.rules('add', {required: true });

      } else {
        $authorize.rules('add',{required: false });

      }
    });

  }
      function setupRegisterForm(settings) {
        var $form = $(settings.form),
            day = $('#day'),
            month = $('#month'),
            year = $('#year');
        register($form);

        $form.on('submit', {
          settings: settings
        }, onSubmitRegisterForm);
        
       
      };

    function onSubmitRegisterForm(e) {
      e.preventDefault();
      var $form = $(this),
        settings = e.data.settings,
        tracking = settings.tracking;

      if ($form.valid()) {
          var payload = {};
          payload.first_name = $('#first_name').val();
          payload.last_name = $('#last_name').val();
          payload.email = $('#email').val();
          payload.dni = $('#dni').val();
          payload.day = $('#day').val();
          payload.month = $('#month').val();
          payload.year = $('#year').val();
          payload.authorize_father = $('#authorize_father').val();
          payload.csrf_token = $('#csrf_token').val();
 
          $.ajax({
              type: "POST",
              url: $form.attr('action'),
              data: $form.serialize(),
              success: function(msg){
                if(!msg.success){
                  for(var index in msg.errors){
                    $('#'+index+'_error').html(msg.errors[index]).show();
                  }
                }else{
                  $('#register-facebook-modal').foundation('reveal', 'close');
                  var flash=getFlashMovieObject('main');
                  flash.changeCopy();
                }
              },
              error: function(msg){

              }
          });

          return false;

        if (tracking) {
          trackEventSubmitForm(tracking.category, tracking.action, tracking.label);
        }
      } else {
        return false;
      }
    }

  return {
    setupRegisterForm : setupRegisterForm
  }
});
