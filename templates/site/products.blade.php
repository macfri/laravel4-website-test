    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'productos'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('products') }}">Productos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner">
          <h3>productos</h3><img src="{{ asset('static/site/images/banners/products.png') }}" style="background:#E3AC00">
          <div class="description-banner"></div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div data-magellan-expedition="fixed" class="cont-filter">
        <div class="cont-center">
          <div class="left"><a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Volver-Home','productos'])" class="btn-logo hide"><span class="logo-fixed"></span></a></div>
          <div class="cont-buttons center"><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','LATA 1 Kg.'])" data-magellan-arrival='1' href="#1" class="btn-next bubble">LATA 1 Kg.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','LATA 400 g.'])" data-magellan-arrival='2' href="#2" class="btn-next bubble">LATA 400 g.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','DOYPACK 200 g.'])" data-magellan-arrival='3' href="#3" class="btn-next bubble">DOYPACK 200 g.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','SOBRE 18 g.'])" data-magellan-arrival='4' href="#4" class="btn-next bubble">SOBRE 18 g.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','DOYPACK 50 g.'])" data-magellan-arrival='5' href="#5" class="btn-next bubble">DOYPACK 50 g.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','RECARGA 400 g.'])" data-magellan-arrival='6' href="#6" class="btn-next bubble">RECARGA 400 g.</a><a onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Producto','MILO FREE'])" data-magellan-arrival='7' href="#7" class="btn-next bubble">MILO FREE</a>
          </div><a href="#" class="btn-top"><span>Arriba</span></a>
        </div>
      </div>
      <div class="content">
        <div class="products">
          <div id="1" data-magellan-destination="1" class="product-01 product">
            <div id="lata-de-1kg" class="row">
              <div class="columns large-6 push-6"></div>
              <div class="columns large-6 pull-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3 class="red-title">Lata de</h3><b class="orange-title">1Kg.</b>
                    <p>No hay ni un solo hombre en la tierra que haya logrado levantar un peso de 1000 kg, no importa lo fuerte que sea...</p>
                    <p>Como nosotros no queremos ponértelo complicado, te dejamos otro gran peso pesado que puedes llevar a casa mientras te ejercitas... ¡Nuestra lata de 1Kg!.<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-1kg.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','lata de 1Kg'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="2" data-magellan-destination="2" class="product-02 product">
            <div id="lata-de-400g" class="row">
              <div class="columns large-6 pull-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3>Lata de</h3><b>400 gr</b>
                    <p>En las olimpiadas de Londres 2012, la medalla de oro para los mejores atletas tiene el peso ideal: 400 gramos de felicidad</p>
                    <p>
                       
                      ¡Por eso, esta lata de Milo puede ser <br>levantada solo por los campeones!<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-400g.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','lata de 400 gr'])" class="btn-products">Ver información<span>nutricional</span></a>
                    </p>
                  </div>
                </div>
              </div>
              <div class="columns large-6 push-6"></div>
            </div>
          </div>
          <div id="3" data-magellan-destination="3" class="product-03 product">
            <div id="doy-pack-200g" class="row">
              <div class="columns large-6 push-6"></div>
              <div class="columns large-6 pull-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3>Doypack de</h3><b>200 gr</b>
                    <p>Se creía que era imposible, pero la NASA encontró una especie similar a un camarón… ¡A 200 metros por debajo de la capa de hielo de la Antártida! </p>
                    <p>¡Un gramito de Milo por cada metro, para que ese camaroncito salga a la superficie!<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-200g.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','Doypack de 200 gr'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="4" data-magellan-destination="4" class="product-04 product">
            <div id="sobre-18g" class="row">
              <div class="columns large-6 pull-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3>Sobre de</h3><b>18 gr</b>
                    <p>
                       
                      Matemáticamente hablando,<br>el 18 no es un número perfecto...
                    </p>
                    <p>Pero si hablamos en términos de energéticos...¡Claro que lo es! ¿Por qué? Simple: con 18 gramos de MILO, obtienes suficiente energía para arrancar bien tu día.</p>
                    <p>Lo sé, es fascinante...<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-18g.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','Sobre de 18 gr'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
              <div class="columns large-6 push-6"></div>
            </div>
          </div>
          <div id="5" data-magellan-destination="5" class="product-05 product">
            <div id="sobre-de-50g" class="row">
              <div class="columns large-6 pull-6"></div>
              <div class="columns large-6 push-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3> <span class="yellow">Nuevo</span><br>Doypack de</h3><b>50 gr</b>
                    <p>Un filósofo loco dijo alguna vez: 47, 48, 49... 50. Si yo puedo contar hasta cincuenta, ¿por qué el número se llama "sin cuenta"?</p>
                    <p>¡Pero tú no te hagas tantos problemas! El sobre de 50g de MILO definitivamente cuenta con suficiente energía para tus desayunos.<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-50g.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','Sobre 50 gr'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="6" data-magellan-destination="6" class="product-06 product">
            <div id="milo-free-400g" class="row">
              <div class="columns large-6 push-6">
                <div class="middle-cont">
                  <div class="cont-product">
                    <h3 class="purple"><span class="yellow">Nuevo</span><br>Recarga</h3><b class="purple">400 gr</b>
                    <p class="purple">Si compras 400 latas de MILO, tu casa estaría llena de latas verdes. ¡Eso sería genial! ¿No crees? Pero probablemente necesitarías más espacio para tu próxima lata.</p>
                    <p class="purple">Por eso te recomendamos llenar la última que compraste con la Recarga de 400g.<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-400g-refil.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','Recarga 400 gr'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
              <div class="columns large-6 pull-6"></div>
            </div>
          </div>
          <div id="7" data-magellan-destination="7" class="product-07 product">
            <div id="milo-free-400g" class="row">
              <div class="columns large-6 push-6"></div>
              <div class="columns large-6 pull-6">
                <div class="middle-cont">
                  <div class="cont-product"><b class="brown-title">Milo</b><b class="brown-title">free</b>
                    <h3 class="brown">400 gr</h3>
                    <p class="black">La carrera de atletismo de 400 metros es una de las más difíciles de todas, requiere mucha energía y fuerza increíble. </p>
                    <p class="black">Con 55% menos azúcar y calorías, ¡Milo Free puso un gramo por cada metro que un atleta recorre para terminar la carrera!<a href="#" data-table="{{ asset('static/site/images/tabla-nutricional-Milo-free.png') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-Productos-InfoNutri','Milo 400 gr'])" class="btn-products">Ver información<span>nutricional</span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'productos'))
        <!-- FOOTER -->
      </div>
    </div>

    @include('site.login-model', array('page'=>'productos'))
    @include('site.register-model')

    <script data-main="{{ asset('static/site/scripts/site/products') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
