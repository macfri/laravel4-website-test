<?php

class AdminMemeDefaultController extends BaseController {

    private $viewpath = 'admin/meme_default/';

    public function index() {
        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $memes = MemeDefault::where('id', '>', 0);

        if (strlen($search_status)) {
            $memes->where('status', '=', $search_status);
        }

        $memes = $memes->orderby('created_at', 'desc');

        $total = $memes->count();
        $memes = $memes->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $memes,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status
                        )
        );
    }

    public function add() {
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => array()
                        )
        );
    }

    public function edit($id) {
        $meme = MemeDefault::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $meme
                        )
        );
    }

    public function save() {
        $v = MemeDefault::validate(Input::all());
        if ($v->passes()) {
            $meme = new MemeDefault();
            $meme->status = Input::get('status');
            $meme->author_id = Session::get('session_user');

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_meme_default_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'memes_default');

                $slug = uniqid();

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes_default', $slug, $ext
                );
                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $slug, $ext)
                );

                $meme->image_id = $image->id;

                try {
                    $meme->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_meme_default_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            return Redirect::route('admin_meme_default')
                            ->with('success', 'MemeDefault Created Successfully.');
        } else {
            return Redirect::back()->withErrors($v);
        }
    }

    public function update($id) {
        $v = MemeDefault::validate(Input::all());
        if ($v->passes()) {

            $meme = MemeDefault::find($id);
            $meme->status = Input::get('status');
            $meme->author_id = Session::get('session_user');

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_meme_default_edit', $id)
                                ->with('failure', $exception->getMessage());
            }


            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'memes_default');

                $image = Photo::find($meme->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $slug = uniqid();

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes_default', $slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_meme_default_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $slug, $ext)
                );

                if (isset($is_new)) {
                    $meme->image_id = $image->id;

                    try {
                        $meme->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_meme_default_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            return Redirect::route('admin_meme_default')
                            ->with('success', 'MemeDefault %s Updated Successfully.');
        } else {
            return Redirect::back()->withErrors($v);
        }
    }

    public function destroy($id) {
        MemeDefault::find($id)->delete();
        return Redirect::back();
    }

}
