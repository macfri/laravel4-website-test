define([
  'jquery',
  'foundation'
], function($){

  function magellan(){
    $(document).foundation('magellan',{
      activeClass:'on'
    });
  }
  return magellan;
});
