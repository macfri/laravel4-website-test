<?php

use Illuminate\Console\Command;

class GetLastPostFacebookCommand extends Command {

    protected $name = 'get_last_post_facebook';
    protected $description = 'get_last_post_facebook';

    public function __construct() {
        parent::__construct();
    }

    private function get_posts($limit) {
        $data = array();
        $app_facebook = Config::get('app.facebook');
        $source_id = $app_facebook['page_id'];
        $access_token = $app_facebook['page_access_token'];
        $url = $app_facebook['url_fql'];
        $params = array(
            'q' => sprintf(
                    "select attachment, post_id, message, type from stream where source_id='%s' and type=247 limit %d", $source_id, $limit),
            'access_token' => $access_token
        );
        $data_last_post = get_data_fb($url, $params);
        
        //print_r($data_last_post);
        
        
        $data_last_post = json_decode($data_last_post);
        
        foreach ($data_last_post->data as $row) 
        {
            $pid = $row->attachment->media[0]->photo->pid;
            $params = array(
                'q' => sprintf("SELECT caption, images, link FROM photo WHERE pid='%s'", $pid),
                'access_token' => $access_token
            );
            $detail_data_last_post = get_data_fb($url, $params);
            $detail_data_last_post = json_decode($detail_data_last_post);
            
            $detail_data_last_post = $detail_data_last_post->data[0];
            $message = $detail_data_last_post->caption;
            $detail_link = $detail_data_last_post->link;
            $picture = $detail_data_last_post->images[2]->source;
            $image_id = (string) uniqid();
            $pic_facebook = sprintf(
                    '%s/%s/%s/%s.jpg', public_path(), 'upload', 'facebook', $image_id);
            
            $file_handler = fopen($pic_facebook, 'w');
            $curl = curl_init($picture);
            curl_setopt($curl, CURLOPT_FILE, $file_handler);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($curl);
            curl_close($curl);
            fclose($file_handler);
            //$_url = secure_asset('upload/facebook/' . $image_id . '.jpg');
            //$_url =Config::get('app.url').'upload/facebook/' . $image_id . '.jpg';
            $_url = 'upload/facebook/' . $image_id . '.jpg';
            
            $data[] = array(
                'title' => $message,
                'url' => $_url,
                'detail_link' => $detail_link);
        }
        return $data;
    }

    public function fire() {
        DB::table('grid')->delete();

        $products = Product::where('status', 'active')
                ->orderby('created_at')
                ->take(3)
                ->get();

        $games = Game::where('status', 'active')
                ->orderby('created_at')
                ->take(3)
                ->get();

        $videos = Video::where('status', 'active')
                ->orderby('created_at')
                ->take(2)
                ->get();

        $events = Event::where('status', 'active')
                ->orderby(DB::raw('RAND()'))
                ->take(2)
                ->get();


        foreach ($games as $row) {
            if (isset($row->image_home->path)) {
                $grid = new Grid();
                $grid->title = $row->title;
                $grid->w = get_size($row->block_size, 'w');
                $grid->h = get_size($row->block_size, 'h');
                $grid->url = $row->image_home->path;
                $grid->detail_link = Config::get('app.url') . '/juegos/'. $row->slug;;
                $grid->section_name = 'games';
                $grid->section_id = 4;
                try {
                    $grid->save();
                } catch (Exception $exception) {
                    Log::error($exception->getMessage());
                    print $exception->getMessage();
                }
            }
        }

        foreach ($events as $row) {
            if (isset($row->image_home->path)) {
                $grid = new Grid();
                $grid->title = $row->title;
                $grid->w = get_size($row->block_size, 'w');
                $grid->h = get_size($row->block_size, 'h');
                $grid->url = $row->image_home->path;
                $grid->detail_link = Config::get('app.url') . '/eventos/'. $row->slug;;
                $grid->section_name = 'events';
                $grid->section_id = 5;
                try {
                    $grid->save();
                } catch (Exception $exception) {
                    Log::error($exception->getMessage());
                    print $exception->getMessage();
                }
            }
        }

        foreach ($products as $row) {
            if (isset($row->image_home->path)) {
                $grid = new Grid();
                $grid->title = $row->title;
                $grid->w = get_size($row->block_size, 'w');
                $grid->h = get_size($row->block_size, 'h');
                $grid->url = $row->image_home->path;
                $grid->detail_link = Config::get('app.url') . '/productos';
                $grid->section_name = 'products';
                $grid->section_id = 1;
                try {
                    $grid->save();
                } catch (Exception $exception) {
                    Log::error($exception->getMessage());
                    print $exception->getMessage();
                }
            }
        }

        foreach ($videos as $row) {
            if (isset($row->image_home->path)) {
                $grid = new Grid();
                $grid->title = $row->title;
                $grid->w = get_size($row->block_size, 'w');
                $grid->h = get_size($row->block_size, 'h');
                $grid->url = $row->image_home->path;
                $grid->detail_link = Config::get('app.url') . '/spots-de-tv';
                $grid->section_name = 'videos';
                $grid->section_id = 3;
                try {
                    $grid->save();
                } catch (Exception $exception) {
                    Log::error($exception->getMessage());
                    print $exception->getMessage();
                }
            }
        }

        $memes = $this->get_posts(2);
        foreach ($memes as $row) {
            $grid = new Grid();
            $grid->title = $row['title'];
            $grid->w = 2;
            $grid->h = 2;
            $grid->url = $row['url'];
            $grid->detail_link = $row['detail_link'];
            $grid->section_name = 'facebook';
            $grid->section_id = 6;
            try {
                $grid->save();
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
                print $exception->getMessage();
            }
        }
    }

}
