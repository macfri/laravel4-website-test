<?php

Route::pattern('id', '[0-9]+');
Route::pattern('slug', '[a-zA-Z0-9\-]+');
Route::pattern('category', '[a-zA-Z0-9\-]+');
Route::pattern('token', '[a-z0-9]+');

/* * ******** site ************ */
Route::get('/', array(
    'before' => 'seorules.before',
    'as' => 'home',
    'uses' => 'SiteController@home'));


Route::get('/ses', array(
    'uses' => 'SiteController@ses'));


Route::get('/spots-de-tv', array(
    'before' => 'seorules.before',
    'as' => 'spots',
    'uses' => 'SiteController@spots'));

Route::get('/historia', array(
    'before' => 'seorules.before',
    'as' => 'history',
    'uses' => 'SiteController@history'));

Route::get('/contactanos', array(
    'before' => 'seorules.before',
    'as' => 'contact',
    'uses' => 'SiteController@contact'));

Route::get('/terminos-y-condiciones', array(
    'before' => 'seorules.before',
    'as' => 'terms',
    'uses' => 'SiteController@terms'));

Route::get('/privacidad', array(
    'before' => 'seorules.before',
    'as' => 'privacy',
    'uses' => 'SiteController@privacy'));

Route::get('404', array(
    'before' => 'seorules.before',
    'as' => '_404',
    'uses' => 'SiteController@_404'));

/* * ******** freetime ************ */
Route::group(array('before' => 'seorules.before', 'prefix' => 'pasa-el-rato'), function() {

    Route::get('/', array(
        'as' => 'freetime',
        'uses' => 'FreeTimeController@home'));

    Route::post('/votar', array(
        #'before' => 'csrf|auth',
        #'before' => 'auth',
        'as' => 'freetime_rank',
        'uses' => 'FreeTimeController@rank'));

    Route::post('/sube-tu-video', array(
        #'before' => 'csrf|auth',
        'before' => 'csrf',
        'as' => 'freetime_do_upload_video',
        'uses' => 'FreeTimeController@do_upload_video'));

    Route::group(array('prefix' => 'sube-tu-meme'), function() {
        Route::get('/', array(
            'before' => 'auth',
            'as' => 'freetime_upload',
            'uses' => 'FreeTimeController@upload'));

        Route::post('/', array(
            'before' => 'csrf|auth',
            'as' => 'freetime_do_upload',
            'uses' => 'FreeTimeController@do_upload_meme'));

        Route::post('/me', array(
            'before' => 'csrf|auth',
            'as' => 'freetime_do_upload_me',
            'uses' => 'FreeTimeController@do_upload_me'));
    });

    Route::get('/descarga/{id}', array(
        'as' => 'freetime_download',
        'uses' => 'FreeTimeController@download'));

    Route::get('/{category}/{slug}', array(
        'as' => 'freetime_detail',
        'uses' => 'FreeTimeController@detail'));

    Route::get('/{category}', array(
        'as' => 'freetime_category',
        'uses' => 'FreeTimeController@category'));
});



/* * ******** events ************ */
Route::group(array('before' => 'seorules.before', 'prefix' => 'eventos'), function() {
    Route::get('/', array(
        'as' => 'events',
        'uses' => 'EventController@index'));

    Route::get('/todos', array(
        'as' => 'events_all',
        'uses' => 'EventController@show'));

    Route::get('/{slug}/', array(
        'as' => 'events_detail',
        'uses' => 'EventController@detail'));

    Route::get('/{slug}/galeria', array(
        'as' => 'events_gallery',
        'uses' => 'EventController@gallery'));
});


/* * ******** products ************ */
Route::group(array('before' => 'seorules.before', 'prefix' => 'productos'), function() {
    Route::get('/', array(
        'as' => 'products',
        'uses' => 'ProductController@index'));
});


/* * ******** games ************ */
Route::group(array('before' => 'seorules.before', 'prefix' => 'juegos'), function() {
    Route::get('/', array(
        'as' => 'games',
        'uses' => 'GameController@games'));

    Route::get('/crea-tu-camiseta/galeria', array(
        'as' => 'game_mundial_gallery',
        'uses' => 'GameController@game_mundial_gallery'));

    Route::post('/rank', array(
        'before' => 'csrf',
        'as' => 'games_rank',
        'uses' => 'GameController@rank'));

    Route::get('/{slug}', array(
        'as' => 'games_detail',
        'uses' => 'GameController@detail'));
});


/* * ******** ubigeo ************ */
Route::group(array('prefix' => 'ubigeo'), function() {
    Route::get('/departments', array(
        'as' => 'ubigeo_departments',
        'uses' => 'UbigeoController@departments'));

    Route::get('/provinces/{cod_dpto}', array(
                'as' => 'ubigeo_provinces',
                'uses' => 'UbigeoController@provinces'))
            ->where('cod_dpto', '[0-9]+');

    Route::get('/districts/{cod_dpto}/{cod_prov}', array(
                'as' => 'ubigeo_districts',
                'uses' => 'UbigeoController@districts'))
            ->where('cod_dpto', '[0-9]+')
            ->where('cod_prov', '[0-9]+');
});


/* * ******** api game ************ */
Route::group(array('prefix' => 'api/v1/game'), function() {
    Route::post('/save', array(
        //'before' => 'csrf',
        'as' => 'api_game',
        'uses' => 'ApiGameController@save'));

    Route::post('/save_chulls', array(
        //'before' => 'csrf',
        'as' => 'api_game_chulls',
        'uses' => 'ApiGameController@save_chulls'));

    Route::post('/save-user-facebook', array(
        //'before' => 'csrf',
        'as' => 'save_user_facebook',
        'uses' => 'ApiGameController@save_facebook_user'));

    /*
      Route::post('/save_round',array(
      //'before' => 'csrf',
      'as' =>'api_game_round',
      'uses' => 'ApiGameController@save_round'));
     */

    Route::get('/round-ranking', array(
        //'before' => 'csrf',
        'as' => 'round_ranking',
        'uses' => 'ApiGameController@round_ranking'));

    /*
      Route::get('/round-get-multi',array(
      //'before' => 'csrf',
      'as' =>'round_get_multi',
      'uses' => 'ApiGameController@round_get_multi'));
     */

    Route::post('/save_chulls_start', array(
        //'before' => 'csrf',
        'as' => 'api_game_chulls_start',
        'uses' => 'ApiGameController@save_chulls_start'));

    Route::post('/save_chulls_choose', array(
        //'before' => 'csrf',
        'as' => 'api_game_chulls_choose',
        'uses' => 'ApiGameController@save_chulls_choose'));

    Route::post('/save_chulls_share', array(
        //'before' => 'csrf',
        'as' => 'api_game_chulls_share',
        'uses' => 'ApiGameController@save_chulls_share'));


    /*     * ******** api game MUNDIAL ************ */

    Route::post('/mundial/save-undershirt', array(
        #'before' => 'csrf',
        'as' => 'mundial_api_save_undershirt',
        'uses' => 'ApiGameController@mundial_api_save_undershirt'));

    Route::get('/mundial/get-team-from-friend', array(
        'as' => 'mundial_api_get_team_from_friend',
        'uses' => 'ApiGameController@mundial_api_get_team_from_friend'));

    Route::get('/mundial/bad-words', array(
        'as' => 'mundial_api_bad_words',
        'uses' => 'ApiGameController@mundial_api_bad_words'));
});


/* * ******** user ************ */
require_once 'routes_site_user.php';


/* * ******** admin ************ */
require_once 'routes_admin.php';
