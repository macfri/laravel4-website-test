define([], function() {

    function initialize () {
      $('body').prepend(
        $('<div class="custom-overlay"></div>'),
        $('<div class="box-modal"></div>')
      );   

      $('.custom-overlay').on('click',function(){
        var $overlay    = $('.custom-overlay'),
            $box_modal  = $(".box-modal");
        
        $overlay.fadeOut(200);
        $box_modal.fadeOut(200);
      });

      window.document.onkeydown = function (e)
      {
        if(e.keyCode == 27){
          var $overlay    = $('.custom-overlay'),
              $box_modal  = $(".box-modal");
          $overlay.fadeOut(200);
          $box_modal.fadeOut(200);
        }
      }
    }

    function initClose(){
      $('.box-modal-close').bind('click',function(){
        var $overlay    = $('.custom-overlay'),
            $box_modal  = $(".box-modal");
        $overlay.fadeOut(200);
        $box_modal.fadeOut(200);
      });
    }

    function hideModal(){
      var $overlay    = $('.custom-overlay'),
          $box_modal  = $(".box-modal");
      $overlay.fadeOut(200);
      $box_modal.fadeOut(200);
    }

    function setupModal(settings){
      var box_modal     = $('.box-modal'),
          box_default   = {
            'padding' : 40,
            'width'   : 450,
            'height'  : 320,
            'content' : "This is an example."
          },
          box_settings  = $.extend(box_default, settings),
          $doc          = $(document);  
      showModal($doc, box_modal, box_settings);
    }

    function showModal($doc, modal, settings){
      var $overlay    = $('.custom-overlay'),
          $box_modal = modal,
          $container = $('body'),
          $left = parseInt(($container.width() - settings.width) / 2);
      $overlay.css({
        'cursor' : 'pointer'
      });
      $box_modal.html(settings.content);
      initClose();
      $box_modal.css({
        "padding" : settings.padding+"px",
        "width" : settings.width+"px",
        "height" : settings.height+"px",
        "left" : $left + "px"
      })

      $overlay.css({"display":"block", "opacity":"0.8"}); 
      $box_modal.fadeIn(600);
    }  

    return {
      setupModal : setupModal,
      hideModal : hideModal,
      init : initialize
    };
});