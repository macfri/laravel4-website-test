<?php

class GameMundialPlayer extends Eloquent {

    protected $table = 'game_games_mundial_player';

    public function team() {
        return $this->belongsTo('GameMundialTeam');
    }

    public function user() {
        return FacebookUser::where('fb_id', $this->fb_id)->first();
    }

}
