    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'terminos-condiciones'))
        <!-- MENU -->
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="row privacy-tems">
        <div class="columns large-12">
          <h3>Terminos y Condiciones</h3>
          <p>Gracias por visitar esta página web. Por favor, lee atentamente los Términos y Condiciones contenidos en este documento, ya que cada vez que se usa esta página web se están aceptando los Términos y Condiciones que aquí se exponen.</p>
          <p>En toda esta página, los términos "nosotros", "nos", "nuestro" "Nestlé" y "Grupo Nestlé" se refieren a Nestlé S.A. y cualquiera de sus filiales, y se usan de la forma más apropiada según el contexto Nestlé. “Tú” se refiere a cualquier persona que accede y/o utiliza esta página web.</p>
          <h4>Política de Privacidad de la página web</h4>
          <p>Cualquier material o dato personal enviado a las páginas web del Grupo Nestlé queda amparado por la política de privacidad de dicho Grupo y por la protección de datos personales expuesta en el Comunicado de Privacidad que se encuentra en esta misma página web.</p>
          <h4>Exactitud, carácter completo y puntualidad de la información</h4>
          <p>Aunque usamos todos los medios a nuestro alcance para asegurar la exactitud y el carácter completo de la información contenida en esta página web, no asumimos responsabilidad alguna en el caso de que la información que se encuentra disponible en la misma no sea exacta o completa. Cualquier modificación que hagas sobre el material disponible en esta página web se realizará por tu cuenta y riesgo. Asimismo, aceptas que será responsabilidad tuya comprobar los eventuales cambios que se puedan producir.</p>
          <h4>Transmisión</h4>
          <p>Cualquier material o comunicación no personal que transmitas a esta página web por e-mail o cualquier otro medio; incluyendo datos, preguntas, comentarios, sugerencias o similares; es y será tratada como no confidencial y sin derechos de propiedad. Cualquier información que transmitas o que coloques en la página pasa a ser propiedad del Grupo Nestlé y se puede utilizar para cualquier finalidad, incluyendo, sin carácter limitativo, la reproducción, acceso, transmisión, publicación, emisión y colocación de contenidos en la web. Aún más, el Grupo Nestlé es totalmente libre de usar, para cualquier finalidad que estime oportuna (incluyendo, sin carácter limitativo, el desarrollo, fabricación, publicidad y marketing de productos), las ideas, material gráfico, invenciones, desarrollos, sugerencias o conceptos en general contenidos en cualquier comunicación que envíes a esta página web. Dicho uso no tendrá compensación para la parte que ha facilitado la información. Facilitando información, también estás garantizando que eres el propietario del material/contenido proporcionado, que no es de carácter difamatorio y que su uso por parte del Grupo Nestlé no violará derechos de terceras personas ni comportará un incumplimiento por nuestra parte de la legislación aplicable. El Grupo Nestlé no tiene ninguna obligación de utilizar la información facilitada.</p>
          <h4>Derechos de propiedad intelectual</h4>
          <p>® Marca registrada por Société des Produits Nestlé S.A. Todos los derechos reservados.</p>
          <p>Todos los derechos de autor y demás derechos de propiedad intelectual referentes a los textos, imágenes y materiales en general contenidos en esta página web son propiedad del Grupo Nestlé o han sido incluidos con la autorización de su correspondiente propietario.</p>
          <p>Puedes navegar por esta página web, reproducir extractos por medio de impresión y realizar descargas a un disco duro o a los efectos de distribución a otras personas físicas. Todo esto se puede realizar únicamente bajo la condición de mantener intactas todas las notas relativas a derechos de autor y a la propiedad de la página y que la mención mostrada anteriormente de marca registrada aparezca en tales reproducciones. No se debe vender o distribuir ninguna reproducción de parte alguna de esta página web con fines de lucro comercial, ni tampoco podrá ser modificada o incorporada a ningún otro trabajo, publicación o página web.</p>
          <p>Las marcas registradas, logotipos, caracteres y marcas de servicio (todo ello denominado, en conjunto, “marcas registradas”) incorporadas en esta página web pertenecen a la Société des Produits Nestlé S.A., que forma parte del Grupo Nestlé. No se debe interpretar, en ningún caso, que alguno de los contenidos de esta página web pueda significar la concesión de alguna, autorización o derecho a utilizar alguna de las marcas comerciales incorporadas en la página. El uso o el abuso de las marcas registradas mostradas tanto en esta página web como en cualquier otro contenido de la misma, salvo lo previsto en estos Términos y Condiciones, queda estrictamente prohibido. Asimismo, advertimos que el Grupo Nestlé defenderá sus derechos de propiedad intelectual con el máximo rigor que permita la ley.</p>
          <h4>Enlaces con otras páginas web</h4>
          <p>Los enlaces existentes en las páginas web del Grupo Nestlé te pueden llevar fuera de la red y sistemas del Grupo. En ese caso, el Grupo Nestlé no aceptará ninguna responsabilidad en relación al contenido, exactitud y funcionamiento de las páginas web de terceras personas. Los enlaces se proporcionan para brindar mayor información al usuario y el Grupo Nestlé no puede ser responsable por las modificaciones que posteriormente se produzcan en páginas web de terceros para las cuales proporcionamos un enlace. La inclusión de un enlace para conectarse con otras páginas web no implica asunción de responsabilidades en relación a esas páginas por parte del Grupo Nestlé. Te recomendamos encarecidamente que estés informado y que leas atentamente las notas legales y de privacidad de todas las páginas web que visites.</p>
          <h4>Garantías y exenciones de responsabilidad</h4>
          <p>El uso de esta página web es exclusivamente de tu cuenta y riesgo.</p>
          <h4>Garantías</h4>
          <p>Esta página web se ofrece "cuando es posible" y "tal cual está" y, por tanto, el Grupo Nestlé no da garantías de ningún tipo, ya sean explícitas, implícitas, legales o de otro tipo (incluso las garantías implícitas de comerciabilidad o satisfacción del nivel de calidad y de adecuación a un propósito particular), incluyendo garantías o compromisos en relación a que el material de esta página web será completo, exacto, fiable, puesto al día, respetuoso con los derechos de terceros, que el acceso a esta página web no sufrirá interrupciones, no presentará errores y estará exento de virus, que esta página web será segura y que cualquier opinión o consejo del Grupo Nestlé obtenido a través de esta página web será exacto o fiable. Por todo lo cual se indica expresamente que no se asumirá ningún compromiso o garantía sobre los aspectos enumerados.</p>
          <p>Por favor, ten en cuenta que, en algunas jurisdicciones, es posible que no se permita la exclusión de garantías implícitas, por lo que algunas de estas exclusiones pueden no ser de aplicación para ti. Por ello, te rogamos que consultes la legislación local.</p>
          <p>Nos reservamos el derecho de restringir, suspender o cancelar, sin previo aviso y en cualquier momento, tu acceso a esta página web o a una parte de la misma y a cualquier funcionalidad incorporada en la página.</p>
          <h4>Responsabilidad</h4>
          <p>Tanto el Grupo Nestlé como cualquier tercero implicado en la creación, producción o distribución de esta página web no tendrán ninguna responsabilidad derivada de mantener el material y los servicios que se encuentran disponibles en esta página o de realizar correcciones, actualizaciones o eliminaciones de los mismos. Cualquier material contenido en esta página web está sujeto a cambios sin previo aviso.</p>
          <p>Aún más, el Grupo Nestlé no tendrá ningún tipo de responsabilidad, en ningún caso, por cualquier daño sufrido a consecuencia de algún virus que pueda haber infectado tu ordenador u otros aparatos de tu propiedad por causa de tu acceso, uso o descargas de cualquier material de esta página web. Si optas por descargar material desde esta página web, esa acción se realiza por tu exclusiva cuenta y riesgo.</p>
          <p>Con la máxima amplitud permitida por la legislación aplicable, declaras renunciar expresamente a efectuar cualquier reclamación contra el Grupo Nestlé y sus responsables, directores, empleados, proveedores y programadores que se pueda derivar de tu uso o acceso a esta página web.</p>
          <h4>Actividades prohibidas</h4>
          <p>Tienes prohibido realizar cualquier acto que, a juicio del Grupo Nestlé, se pueda considerar inapropiado y/o llegar a considerarse como un acto ilegal o que esté prohibido de acuerdo con la legislación aplicable a esta página web, incluyendo, sin carácter limitativo, los siguientes:</p>
          <ul>
            <li>Cualquier acto que constituiría un incumplimiento de la privacidad (incluyendo transferencias a la página web de información privada sin la autorización de la persona afectada) o de cualquier otro de los derechos legales de las personas físicas.</li>
            <li>Usar esta página web para difamar o calumniar al Grupo Nestlé, sus empleados u otras personas físicas, o actuar de tal manera que se desacredite el buen nombre del Grupo Nestlé.</li>
            <li>Transferir a la página web archivos que contengan virus que puedan causar daños a la propiedad del Grupo Nestlé o a la propiedad de personas físicas.</li>
            <li>Colocar o transmitir a esta página web cualquier material no autorizado, incluyendo, sin carácter limitativo, material que probablemente cause, a juicio nuestro, molestias o perjuicios, o bien, violando los sistemas o la seguridad de la red del Grupo Nestlé o de terceras personas, material que sea difamatorio, racista, obsceno, amenazador, de contenido pornográfico o que presente cualquier forma de ilegalidad en general.</li>
          </ul>
          <h4>Jurisdicción y legislación aplicable</h4>
          <h4>PARA PÁGINAS WEB NACIONALES ESPECÍFICAS</h4>
          <h4>Para páginas web que ofrecen productos</h4>
          <p>Los productos, materiales, ofertas e información del Grupo Nestlé que aparecen en esta página web están pensados únicamente para los usuarios y/o clientes residentes en el territorio de la República del Perú, El Grupo Nestlé no se compromete a que los productos y el contenido de esta página web sean apropiados o se encuentren disponibles en zonas distintas a Perú. Por favor, ponte en contacto con nuestro distribuidor local para conseguir más información sobre cuál es la disponibilidad de productos en tu país. Los productos que aparecen en esta página web son meras representaciones visuales y, por consiguiente, no aparecen a tamaño real ni con los colores exactos de sus envases, etc.</p>
          <p>El usuario de esta página web y el Grupo Nestlé aceptan que cualquier controversia o reclamación derivada o referente al uso de esta página web se resolverá según las leyes de Perú y se someterá exclusivamente a la jurisdicción de los Juzgados de la ciudad de Lima, Perú.</p>
          <h4>Para páginas web informativas</h4>
          <p>El material y la información del Grupo Nestlé que aparecen en esta página web han sido incluidos únicamente para los usuarios de Perú. El Grupo Nestlé no asume ningún compromiso en relación a que el material y la información de esta página web sean apropiados o se encuentren disponibles en zonas distintas a Perú.</p>
          <p>El usuario de esta página web y el Grupo Nestlé aceptan que cualquier controversia o reclamación derivada o referente al uso de esta página web se resolverá según las leyes de Perú, y se someterá exclusivamente a la jurisdicción de los Juzgados y Tribunales de Perú.</p>
          <h4>PARA PÁGINAS WEB GLOBALES</h4>
          <h4>Para páginas web que ofrecen productos</h4>
          <p>El Grupo Nestlé no asume ningún compromiso en relación a que los productos y el contenido de esta página web sean apropiados o se encuentren disponibles en todos los países desde los cuales se puede acceder a esta página web. Por favor, ponte en contacto con nuestro distribuidor local para conseguir más información sobre cuál es la disponibilidad de productos en tu país. Los productos que aparecen en esta página web son meras representaciones visuales y, por consiguiente, no son un reflejo exacto de su tamaño real ni de los colores de sus envases, etc.</p>
          <p>El usuario de esta página web y el Grupo Nestlé aceptan que cualquier controversia o reclamación derivada o referente al uso de esta página web se resolverá con la máxima amplitud posible según las leyes suizas y se someterá exclusivamente a la jurisdicción de los Juzgados y Tribunales de Suiza.</p>
          <h4>Para páginas web informativas</h4>
          <p>El Grupo Nestlé no asume ningún compromiso en relación a que el material y la información contenidos en esta página web sean apropiados o se encuentren disponibles en todas las zonas o lenguas nacionales.</p>
          <p>El usuario de esta página web y el Grupo Nestlé aceptan que cualquier controversia o reclamación derivada o referente al uso de esta página web se resolverá según las leyes suizas y se someterá exclusivamente a la jurisdicción de los Juzgados y Tribunales de Suiza.</p>
          <h4>Cookies</h4>
          <p>Nestlé utiliza la tecnología tracking ("cookies"). La política del Grupo Nestlé en relación al uso de cookies se expone en el documento de política de privacidad y datos personales del Grupo Nestlé disponible en esta página web.</p>
          <h4>Actualización de notas legales</h4>
          <p>Nos reservamos el derecho de realizar cambios y correcciones en esta nota. Por favor, consulta esta página periódicamente para revisar estas y nuevas informaciones adicionales.</p>
          <p>Copyright © Société des Produits Nestlé S.A. 2004</p>
        </div>
      </div>
      <div class="content">
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'terminos-condiciones'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'terminos-condiciones'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/home') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
