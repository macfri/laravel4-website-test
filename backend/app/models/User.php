<?php

use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

    protected $table = 'users';
    protected $hidden = array('password');
    protected $guarded = array('id');
    protected $fillable = array(
        'username',
        'password',
        'first_name',
        'last_name',
        'email',
        'dni',
        'phone',
        'status',
        'activation',
        'activation_password',
        'facebook_id',
        'google_id',
        'token_game',
        'level_score',
        'is_new',
        'picture',
        'password'
    );

    public static function validate_login($input) {
        $rules = array(
            //'email'      => 'Required|Min:3|Max:64|Email',
            //'password'   => 'Required|Min:4|Max:120',
            'recaptcha_response_field' => 'required|recaptcha'
        );

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'email' => 'Necesitas un correo válido',
            'recaptcha' => 'Recaptcha es incorrecto',
        );

        return Validator::make($input, $rules, $messages);
    }

    public static function validate($input) {
        Validator::extend('exists_email', function($attribute, $value, $parameters) {
            $exists = User::where('email', $value)
                    ->where('id', '<>', Auth::user()->id)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('exists_dni', function($attribute, $value, $parameters) {
            $exists = User::where('dni', $value)
                    ->where('id', '<>', Auth::user()->id)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('dni', function($attribute, $value, $parameters) {
            if (strlen($value) == 8 and is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        });

        $rules = array(
            'first_name' => 'Required|Min:3|Max:80',
            'last_name' => 'Required|Min:3|Max:80',
            'email' => 'Required|Min:3|Max:64|exists_email',
            'date_birth' => 'date',
            'dni' => 'dni|exists_dni',
            'phone' => 'Required'
        );

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'email' => 'Necesitas un correo válido',
            'date' => 'La fecha de nacimiento no es vàlida',
            'exists_email' => 'El correo ya existe',
            'exists_dni' => 'El dni ya existe',
            'dni' => 'El dni es invàlido',
        );

        return Validator::make($input, $rules, $messages);
    }

    public static function validate_simple($input) {
        Validator::extend('exists_email', function($attribute, $value, $parameters) {
            $exists = User::where('email', $value)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('exists_dni', function($attribute, $value, $parameters) {
            $exists = User::where('dni', $value)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('check_dni', function($attribute, $value, $parameters) {
            if (strlen($value) == 8 and is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        });

        Validator::extend('strong_password', function($attribute, $value, $parameters) {
            if (preg_match(
                            "#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $value)) {
                return true;
            } else {
                return false;
            }
        });

        $rules = array(
            'first_name' => 'Required|Min:3|Max:80',
            'last_name' => 'Required|Min:3|Max:80',
            'email' => 'Required|Min:3|Max:64|email|exists_email',
            'date_birth' => 'date',
            'dni' => 'Required|check_dni|exists_dni',
            'password' => 'Required|Min:8|Max:120|strong_password',
        );

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'strong_password' => 'La clave no es segura, (Crea tu clave a partir de una combinación de números, letras mayúsculas y minúsculas)',
            'email' => 'Necesitas un correo válido',
            'date' => 'La fecha de nacimiento no es vàlida',
            'exists_email' => 'El correo ya existe',
            'exists_dni' => 'El dni ya existe',
            'check_dni' => 'El dni es invàlido',
        );

        return Validator::make($input, $rules, $messages);
    }

    public static function validate_simple_facebook($input) {

        Validator::extend('exists_email', function($attribute, $value, $parameters) {
            $user_id = Auth::user()->id;
            $exists = User::where('email', $value)
                    ->where('id', '<>', $user_id)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('exists_dni', function($attribute, $value, $parameters) {
            $user_id = Auth::user()->id;
            $exists = User::where('dni', $value)
                    ->where('id', '<>', $user_id)
                    ->count();
            return $exists > 0 ? false : true;
        });

        Validator::extend('check_dni', function($attribute, $value, $parameters) {
            if (strlen($value) == 8 and is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        });

        $rules = array(
            'first_name' => 'Required|Min:3|Max:80',
            'last_name' => 'Required|Min:3|Max:80',
            'email' => 'Required|Min:3|Max:64|email|exists_email',
            'date_birth' => 'date',
            'dni' => 'Required|check_dni|exists_dni',
        );

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'email' => 'Necesitas un correo válido',
            'date' => 'La fecha de nacimiento no es vàlida',
            'exists_email' => 'El correo ya existe',
            'exists_dni' => 'El dni ya existe',
            'check_dni' => 'El dni es invàlido',
        );

        return Validator::make($input, $rules, $messages);
    }

    public function getRememberToken() {
        return $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function facebook() {
        return $this->belongsTo('FacebookUser');
    }

    public function google() {
        return $this->belongsTo('GoogleUser');
    }

    public function player() {
        return $this->hasOne('Player');
    }

    public function players() {
        return $this->hasMany('Player');
    }

    public function profile_picture() {
        if ($this->facebook_id != null) {
            return str_replace('=small', '=large', $this->picture);
        } else if ($this->google_id != null) {
            return str_replace('sz=50', 'sz=150', $this->picture);
        } else {
            return $this->picture;
        }
    }

    public function level() {
        $total = DB::table('game_badges')->sum('points');
        $each_level = $total / 10;

        $user_score = DB::table('game_players_badges')
                ->join('game_badges', 'game_players_badges.badge_id', '=', 'game_badges.id')
                ->where('user_id', $this->id)
                ->sum('game_badges.points');

        $total_badges = DB::table('game_badges')->count('id');
        $user_total_badges = DB::table('game_players_badges')
                ->join('game_badges', 'game_players_badges.badge_id', '=', 'game_badges.id')
                ->where('user_id', $this->id)
                ->count('game_badges.id');

        $pc = intval(($user_score * 100) / $total);
        $level = intval($user_score / $each_level);
        $max_level = ($level + 1) * $each_level;
        $left = sprintf('%d LOGROS DE %d', $user_total_badges, $total_badges);
        $right = sprintf('%d puntos para el siguiente nivel', $max_level - $user_score);

        return array(
            'level' => $level,
            'pc' => $pc,
            'left' => $left,
            'right' => $right);
    }

    public function has_trophies($game_id) {
        $sql = "select 
                    t1.id,
                    t1.title,
                    t1.description,
                    c.path,
                    IF(t2.badge_id>0, 1, 0) as active 
                from
                    website_game_badges t1 inner join 
                    website_files as c on (t1.image_id = c.id) left join 
                    (select * from website_game_players_badges 
                        where user_id=$this->id and 
                            game_id=$game_id) as t2 on (t1.id = t2.badge_id) 
                where 
                t1.game_id=$game_id;";
        $data = DB::select(DB::raw($sql));
        return $data;
    }

    public function total_points() {
        return (int) Player::where('user_id', $this->id)
                        ->sum('total_score');
    }

    public function most_played() {
        $x = Point::select(DB::raw('
                website_game_games.title,
                website_files.path,
                count(*) as tot'))
                ->join('game_games', 'game_games.id', '=', 'game_points.game_id')
                ->join('files', 'files.id', '=', 'game_games.image_id')
                ->where('user_id', $this->id)
                ->orderBy('tot', 'desc')
                ->groupBy('game_id')
                ->take(3)
                ->get();
        //$queries = DB::getQueryLog();
        //$last_query = end($queries);
        return $x;
    }

    public function voted($meme_id) {
        $voted = UserMemeLike::where('user_id', $this->id)
                ->where('meme_id', $meme_id)
                ->first();
        return $voted;
    }

}
