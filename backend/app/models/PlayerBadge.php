<?php

class PLayerBadge extends Eloquent {

    protected $table = 'game_players_badges';

    public function game() {
        return $this->belongsTo('Game');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function Badge() {
        return $this->belongsTo('Badge');
    }

}
