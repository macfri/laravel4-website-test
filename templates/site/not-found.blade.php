    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU -->
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="not-found"><a href="{{ URL::route('home') }}"><img src="{{ asset('static/site/images/404.png') }}"></a>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>''))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>''))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/contact') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
