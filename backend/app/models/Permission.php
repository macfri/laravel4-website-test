<?php

class Permission extends Eloquent {

    protected $table = 'system_users_permissions';

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw(
                        "slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

}
