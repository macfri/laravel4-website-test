<?php

class AdminSpotController extends BaseController {

    private $viewpath = 'admin/spot/';

    public function index() {

        if (!SystemUser::has_perm('spots-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = Spot::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'items2' => Spot::all()
                        )
        );
    }

    public function add() {
        if (!SystemUser::has_perm('spots-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $max_position =  Spot::max_position();

        return View::make(
                        $this->viewpath . __function__, array(
                            'max_position' => $max_position
                        )
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('spots-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }



        $item = Spot::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('spots-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }



        $v = Spot::validate(Input::all());
        if ($v->passes()) {
            try {
                $item = Spot::create(array(
                            'title' => Input::get('title'),
                            'slug' => Spot::get_slug(Input::get('title')),
                            'description' => Input::get('title'),
                            'code' => Input::get('code'),
                            'status' => 'active',
                            'author_id' => Session::get('session_user'),
                            'order' => Input::get('order'),
                ));
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_spot_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'spots');

                $image = Photo::create(array(
                            'title' => $name,
                            'extension' => $ext,
                            'size' => $size,
                            'mime' => $mime
                ));

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'spots', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                $item->image_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_spot_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'spots');

                $image = Photo::create(array(
                            'title' => $name,
                            'extension' => $ext,
                            'size' => $size,
                            'mime' => $mime,
                ));

                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'spots', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $image->id, $ext)
                );

                $item->image_home_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_spot_add')
                                    ->with('failure', $exception->getMessage());
                }
            }


            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s', public_path(), 'upload', 'spots', 'gallery');

                        $image = SpotPhoto::create(array(
                                    'title' => $name,
                                    'extension' => $ext,
                                    'size' => $size,
                                    'mime' => $mime,
                                    'spot_id' => $item->id
                        ));

                        $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'spots', 'gallery', $image->id, $ext
                        );

                        $image->save();

                        $file->move(
                                $upload_path, sprintf('%s.%s', $image->id, $ext)
                        );
                    }
                }
            }

            return Redirect::route('admin_spot')
                            ->with('success', sprintf(
                                            'Spot %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {

        if (!SystemUser::has_perm('spots-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }



        $v = Spot::validate(Input::all());
        if ($v->passes()) {

            $item = Spot::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->code = Input::get('code');
            //$item->show_home =Input::get('show_home');
            //$item->block_size =Input::get('block_size');
            $item->status = Input::get('status');
            $item->order = Input::get('order');
            $item->author_id = Session::get('session_user');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_spot_edit', $id)
                                ->with('failure', $exception->getMessage());
            }


            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'spots');

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_spot_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'spots', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );


                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_spot_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'spots');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_spot_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'spots', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                if (isset($is_new)) {
                    $item->image_home_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_spot_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }


            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s', public_path(), 'upload', 'spots', 'gallery');

                        $image = SpotPhoto::create(array(
                                    'title' => $name,
                                    'extension' => $ext,
                                    'size' => $size,
                                    'mime' => $mime,
                                    'spot_id' => $item->id
                        ));

                        $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'spots', 'gallery', $image->id, $ext
                        );

                        $image->save();

                        $file->move(
                                $upload_path, sprintf('%s.%s', $image->id, $ext)
                        );
                    }
                }
            }


            return Redirect::route('admin_spot')
                            ->with('success', 'Spot Created Successfully.');
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('spots-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }



        Spot::find($id)->delete();

        return Redirect::route('admin_spot')
                        ->with('success', 'Spot Deleted Successfully.');
    }

    public function gallery_destroy() {
        $id = Input::get('id');
        SpotPhoto::find($id)->delete();
        return Response::json(array(
                    'status_code' => 0,
                    'message' => 'delete photo'), 200
        );
    }

}
