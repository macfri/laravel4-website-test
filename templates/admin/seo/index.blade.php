@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>

   @if(SystemUser::has_perm('seo-add'))

    <li><a href="{{ URL::route('admin_seo_add') }}">Create</a></li>

    @endif



</ul>

<form id="filter_form" method="GET" action="{{ URL::route('admin_seo') }}">
    <div class="pull-right"><button type="submit" class="btn btn-primary" style="">Apply</button></div>
    <table class="filters">
        <tbody>
            <tr>
                <td>Title</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>
        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
        <th class="span1">&nbsp;</th>
        <th class="span1">route</th>
        <th class="span1">title</th>
    <!--    <th class="span1">alias</th>
        <th class="span1">priority</th> -->

    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <td>
            <a class="icon" href="{{ URL::route('admin_seo_edit', $p->id) }}"><i class="icon-pencil"></i></a>
            <form class="icon" method="POST" action="{{ URL::route('admin_seo_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form>
        </td>
        <td><a href="{{ URL::route('admin_seo_edit', $p->id) }}">{{ $p->route }}</a></td>
        <td>{{ $p->title }}</td>
     <!--   <td>{{ $p->alias }}</td>
        <td>{{ $p->priority }}</td> -->

    </tr>
    @endforeach
    @else
    <tr><td colspan="6">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>
@stop
