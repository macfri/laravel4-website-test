define([
  'jquery'
], function($){

  function scrollArrow(){


    var positem = $('.gallery-photo').find(".figure").eq(1).offset().top;

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    $(window).scroll(function() {

        delay(function(){
          //detectHeight($item);

          var scrolltop = $('body').scrollTop();
          if(scrolltop > positem){
          }

        }, 150);
    });

    var index = 1;
    var total = $(".figure").length;

    $(".btn-top-gallery").click(function(event){
      event.preventDefault();
      if(index>0)
      {
        index -= 1;
        $(this).attr("href","#gallery"+index);
      }
    });

    $(".btn-bottom-gallery").click(function(event){
      event.preventDefault();
      if(index <= total-1)
      {
        index += 1;
        $(this).attr("href","#gallery"+index);
      }
    });


  }
  return scrollArrow;
});
