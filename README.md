Generate database
-----------------

mysql -u <username> -p<password>
drop database <databasename>; create database <databasename>;
exit mysql

cd /backend

--create tables into database:
php artisan migrate --env=local

--seed data into database
php artisan db:seed --env=local


Generate serve
---------------

create folder "local" into backend/app/config/
copy files from config to config/local to overrite configs like 'database, app and so on'

--run server
php artisan serve


mysql -u root -e "drop database milo_db; create database milo_db" && php backend/artisan migrate &&  backend/artisan db:seed

