    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'eventos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events') }}">Eventos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events_detail', $item->slug) }}">{{ $item->title }}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="row gallery-detail">
          <div class="columns large-2"> 
            <div class="social-share">
              <a href="{{ URL::route('events') }}" class="shape-grid-gallery"> </a><a data-href="https://milo.com.pe/" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false" class="fb-like"></a><a href="https://twitter.com/share" data-counturl="https://milo.no2.pe/website/" data-lang="en" data-count="vertical" target="_blank" class="twitter-share-button">Twitter
                <sctipt>.<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></sctipt></a>
            </div>
          </div>
          <div class="columns large-7">
            <h2>{{ $item->title }}</h2>
            <div class="date">{{ $item->when_where }}</div>
            <div class="row gallery-photo">
              <div class="columns large-12">@if(isset($item->image_detail_id))<img src="{{ asset($item->image_detail->path) }}">@else<img src="http://placehold.it/620x402">@endif
                <p>{{ $item->description }}</p>
              </div>
            </div>
            <div data-colorscheme="light" data-width="420" class="fb-comments"></div>
          </div>
          <div class="columns large-3">@if($item->gallery)<a href="{{ URL::route('events_gallery', $item->slug) }}" class="btn-view-gallery"><span>Ver galeria </span></a>@endif</div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'eventos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'eventos-detalle'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/events') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
