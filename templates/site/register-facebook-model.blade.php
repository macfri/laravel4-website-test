<div id="register-facebook-modal" class="reveal-modal" style="top:30px">
    <div class="row modal-session myform">
        <div class="columns large-12 large-centered">

              <form id="basic-register" action="{{ URL::route('save_user_facebook') }}" method="post" class="custom form-reg">

                <input type="hidden" name="_token" id="csrf_token" value="{{ $token_facebook_user }}">

                <h3>Regístrate</h3>
                <div class="row">
                    <div class="columns large-6">
                        <label>Tu Nombre</label>
                        <input type="text" name="first_name" id="first_name" maxlength="50" value="{{ $user_data->first_name }}" class="text">
                        <label for="first_name" id="first_name_error" class="error"></label>
                    </div>
                    <div class="columns large-6">
                        <label>Tu Apellido</label>
                        <input type="text" name="last_name" id="last_name" maxlength="50" value="{{ $user_data->last_name }}" class="text">
                        <label for="last_name" id="last_name_error" class="error"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-6">
                        <label>Tu Correo electronico</label>
                        <input type="text" name="email" id="email" value="{{ $user_data->email }}">
                       <label for="email" id="email_error" class="error"></label>
                    </div>
                    <div class="columns large-6">
                        <label>¿Qué edad tienes?</label>
                        <div class="row">
                            <div class="columns large-4">
                                <select name="day" id="day"  class="text">
                                    <option value="">Día</option>
                                    <option value="0">1</option>
                                    <option value="1" >2</option>
                                    <option value="2" >3</option>
                                    <option value="3" >4</option>
                                    <option value="4" >5</option>
                                    <option value="5" >6</option>
                                    <option value="6" >7</option>
                                    <option value="7" >8</option>
                                    <option value="8" >9</option>
                                    <option value="9" >10</option>
                                    <option value="10" >11</option>
                                    <option value="11" >12</option>
                                    <option value="12" >13</option>
                                    <option value="13" >14</option>
                                    <option value="14" >15</option>
                                    <option value="15" >16</option>
                                    <option value="16" >17</option>
                                    <option value="17" >18</option>
                                    <option value="18" >19</option>
                                    <option value="19" >20</option>
                                    <option value="20" >21</option>
                                    <option value="21" >22</option>
                                    <option value="22" >23</option>
                                    <option value="23" >24</option>
                                    <option value="24" >25</option>
                                    <option value="25" >26</option>
                                    <option value="26" >27</option>
                                    <option value="27" >28</option>
                                    <option value="28" >29</option>
                                    <option value="29" >30</option>
                                    <option value="30" >31</option>
                                </select>
                            </div>
                            <div class="columns large-4">
                                <select name="month" id="month" class="text">
                                    <option value="">Mes</option>
                                    <option value="1" >Ene</option>
                                    <option value="2" >Feb</option>
                                    <option value="3" >Mar</option>
                                    <option value="4" >Abr</option>
                                    <option value="5" >May</option>
                                    <option value="6" >Jun</option>
                                    <option value="7" >Jul</option>
                                    <option value="8" >Ago</option>
                                    <option value="9" >Set</option>
                                    <option value="10" >Oct</option>
                                    <option value="11" >Nov</option>
                                    <option value="12" >Dic</option>
                                </select>
                            </div>
                            <div class="columns large-4">
                                <select name="year" id="year" class="text">
                                    <option value="">Año</option>
                                    <option value="2007" >2007</option>
                                    <option value="2006" >2006</option>
                                    <option value="2005" >2005</option>
                                    <option value="2004" >2004</option>
                                    <option value="2003" >2003</option>
                                    <option value="2002" >2002</option>
                                    <option value="2001" >2001</option>
                                    <option value="2000" >2000</option>
                                    <option value="1999" >1999</option>
                                    <option value="1998" >1998</option>
                                    <option value="1997" >1997</option>
                                    <option value="1996" >1996</option>
                                    <option value="1995" >1995</option>
                                    <option value="1994" >1994</option>
                                    <option value="1993" >1993</option>
                                    <option value="1992" >1992</option>
                                    <option value="1991" >1991</option>
                                    <option value="1990" >1990</option>
                                    <option value="1989" >1989</option>
                                    <option value="1988" >1988</option>
                                    <option value="1987" >1987</option>
                                    <option value="1986" >1986</option>
                                    <option value="1985" >1985</option>
                                    <option value="1984" >1984</option>
                                    <option value="1983" >1983</option>
                                    <option value="1982" >1982</option>
                                    <option value="1981" >1981</option>
                                    <option value="1980" >1980</option>
                                    <option value="1979" >1979</option>
                                    <option value="1978" >1978</option>
                                    <option value="1977" >1977</option>
                                    <option value="1976" >1976</option>
                                    <option value="1975" >1975</option>
                                    <option value="1974" >1974</option>
                                    <option value="1973" >1973</option>
                                    <option value="1972" >1972</option>
                                    <option value="1971" >1971</option>
                                    <option value="1970" >1970</option>
                                    <option value="1969" >1969</option>
                                    <option value="1968" >1968</option>
                                    <option value="1967" >1967</option>
                                    <option value="1966" >1966</option>
                                    <option value="1965" >1965</option>
                                    <option value="1964" >1964</option>
                                    <option value="1963" >1963</option>
                                </select>
                            </div>
                            <label for="date_birth" id="date_birth_error" class="error"></label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-6">
                        <label>DNI de mamá, papá, apoderado o el tuyo</label>
                        <input type="text" name="dni" id="dni" maxlength="8" value="" class="numeric">
                        <label for="dni" id="dni_error" class="error"></label>
                    </div>
                    <!-- <div class="columns large-6" style="color: #066427"><span class="text-help">DNI  de mamá, papá, apoderado o el tuyo</span></div> -->
                </div>

                <div class="row">
                    <div class="columns large-6">
                        <label>Tu Teléfono</label>
                        <input type="text" name="phone" class="number" id="phone" maxlength="9" value="">
                        <label for="phone" id="phone_error" class="error"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-12 h-30">
                        <label class="check">
                            <input type="checkbox" name="reply_email" id="reply_email" value="1">
                            <p>Deseo recibir notificaciones y promociones sobre las novedades de Milo.</p>
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-12 h-45">
                        <label class="check">
                            <input type="checkbox" name="authorize_father" id="authorize_father" value="1">
                            <p>Mis padres autorizan que participe de las promociones y concursos.</p>
                        </label>
                        <div class="authorize-error" id="authorize_parent_error"></div><br>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-12 h-45">
                        <label class="check">
                            <input type="checkbox" name="is_lima" id="is_lima" value="1">
                            <p>Eres de lima.</p>
                        </label>
                        <div class="authorize-error"></div>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="columns large-12">
                        <button onclick="_gaq.push(['_trackEvent','Web-Conversion','Paso2-Guardardatos',''])" class="right btn-continue">Actualizar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
