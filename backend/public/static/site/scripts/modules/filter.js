define([
  'jquery'
], function($){
  $nav_fixed = $('.nav-fixed');
  function filter($container, $blocks){
    var $links = $nav_fixed.find('li a');
    var num_filters = $links.length;
    var blocks = serialize($blocks);
    var _blocks = JSON.parse(JSON.stringify(blocks));
    packing(_blocks, $blocks, sortByRandom());

    $links.on('click', sortBlocks);

    function sortBlocks(event){
      event.preventDefault();
      var $target = $(this);
      _blocks = JSON.parse(JSON.stringify(blocks));
      var criteria = [];


      $target.find('i').toggleClass('check-on').toggleClass('check-off');

      $target.closest('li').toggleClass('selected');


      var $lis = $nav_fixed.find('.selected');
      $lis.each(function(){
        $el = $(this);
        criteria.push(parseInt($el.attr('data-id'), 10));
      });

      _blocks.sort(function (a, b) { return Math.random() - 0.5; });

      var len = criteria.length;

      if( len === 0 || len === num_filters){
        packing(_blocks, $blocks, sortByRandom());
      }
      else{


        packing(_blocks, $blocks, sortByCriteria(criteria));

      }
    }
  }

  function Packer(w, h) {
    var self = this;
    self.root = { x: 0, y: 0, w: w, h: h };

    self.fit = function (blocks){
      var n, node, block;
      for (n = 0; n < blocks.length; n++) {
        block = blocks[n];
        if ((node = self.findNode(self.root, block.w, block.h)))
          block.fit = self.splitNode(node, block.w, block.h);
      }
    };

    self.findNode = function (root, w, h) {
      if (root.used)
        return self.findNode(root.right, w, h) || self.findNode(root.down, w, h);
      else if ((w <= root.w) && (h <= root.h))
        return root;
      else
        return null;
    };

    self.splitNode = function (node, w, h) {
      node.used = true;
      node.down  = { x: node.x,     y: node.y + h, w: node.w,     h: node.h - h };
      node.right = { x: node.x + w, y: node.y,     w: node.w - w, h: h          };
      return node;
    };
    return self;
  }

  function serialize($blocks){
    var blocks = [];

    $blocks.each(function(i){
      var $el = $(this);
      blocks[i] = {
        w: parseInt($el.attr('data-w'), 10),
        h: parseInt($el.attr('data-h'), 10),
        eq: i,
        id: parseInt($el.attr('data-id'), 10)
      };
    });
    return blocks;
  }

  function packing(blocks, $blocks, fnSort){
    var packer = new Packer(6, 100);
    var col = 160;
    var max_height = 0;
    blocks.sort(fnSort);
    packer.fit(blocks);

    for(var n = 0 ; n < blocks.length ; n++) {
      var block = blocks[n];
      if (block.fit) {
        $blocks.eq(block.eq).css({
          width: block.w*col,
          height: block.h*col,
        }).animate({
          top: block.fit.y*col,
          left: block.fit.x*col
        }, {duration: 1000, queue: false});
        var tmp_max_height = block.fit.y*col + block.h * col;
        if(max_height < tmp_max_height){
          max_height = tmp_max_height;
        }
      }
    }
    $('.blocks-grid').animate({
      height: max_height
    },  {duration: 1000, queue: false});
  }
//filters
  function sortByCriteria(criteria){
    return function (a, b){
      if(criteria.indexOf(a.id)=== -1){
          return 1;
      }else {
        return -1;
      }
    };
  }

  function sortByRandom(){
    return function (a, b) { return Math.random() - 0.5; };
  }

  return filter;

});
