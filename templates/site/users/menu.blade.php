  <div class="nav-side">
    <div class="inner row">
      <div class="columns small-10">
        <ul>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-inicio', ''])">Inicio</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('history') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-historia', ''])">Historia</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('products') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-productos', ''])">Productos</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('spots') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-spots-de-tv', ''])">Spots de TV</a></li>
          <li></li>
        </ul>
      </div>
      <div class="columns small-2">
        <div class="social-icons right"><a href="https://www.facebook.com/MiloPeru" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-FP',''])" class="icon-facebook"></a>
          <div class="div"></div><a href="http://www.youtube.com/channel/UC054ysyX5RH2FF7UHKIcXKw/feed" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-YT',''])" class="icon-youtube"></a>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-main">
    <div class="inner row">
      <div class="columns large-8 patt-left"> <a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Volver-Home',''])" class="logo">
          <h1>Milo</h1></a>
        <ul>
          <li><a href="{{ URL::route('games') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-juegos', ''])">Juegos</a></li>
          <li><a href="{{ URL::route('events') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-eventos', ''])">Eventos</a></li>
          <li><a href="{{ URL::route('freetime') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-meme', ''])">Pasa el rato</a></li>
        </ul>
      </div>
      <div class="columns large-4 patt-right">
      </div>
    </div>
  </div>
