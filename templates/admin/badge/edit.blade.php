@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif


@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<fieldset>

    <legend class="scheduler-border">@yield('title_module', 'Edit Badge')</legend>
    <div class="control-group">
        <div class="control-label">Game<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <select name="game_id" id="game_id">
                @foreach($games as $x)
                    <option value="{{ $x->id }}" @if(isset($item)) @if($item->game_id==$x->id) selected @endif @endif>{{ $x->title }}</option>
                @endforeach
                </select>

            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">id<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="id" id="id" value="@if(isset($item)){{$item->id}}@else{{ Form::old('id')}}@endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="title" id="title" value="@if(isset($item)){{$item->title}}@else{{ Form::old('title')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">description<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <textarea id="description" name="description" cols="80" rows="4">@if(isset($item)){{$item->description}}@else{{Form::old('description')}}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">points<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="points" id="points" value="@if(isset($item)){{$item->points}}@else{{ Form::old('points')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Badge</div>
        <div class="controls">
           <div>
                <select name="badge">
                    <option value="oro"  @if(isset($item->badge))  @if($item->badge == 'oro') selected @endif @endif  >oro</option>
                    <option value="plata"  @if(isset($item->badge))  @if($item->badge == 'plata') selected @endif @endif >plata</option>
                    <option value="bronce"  @if(isset($item->badge))  @if($item->badge == 'bronce') selected @endif @endif >bronce</option>
                </select >
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">Photo&nbsp;</div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>

            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#demoLightbox">{{ $item->image->title  }}</a>
            <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }} ">
                </div>
            </div>
            @endif

        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
        <input type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div>

</fieldset>
{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>
@stop
