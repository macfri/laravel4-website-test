<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableMemesAddFacebookPid extends Migration {

	public function up()
	{
        Schema::table('section_memes', function($t) {
            $t->string('pid', 500)
                ->nullable()
                ->index();
        });
	}

	public function down()
	{
       Schema::table('section_memes', function($t) {
            $t->dropColumn('pid');
       });
	}

}
