<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableMemes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		        Schema::table('section_memes', function($t) {
                //$t->integer('type')
                    //->default(0);
                $t->string('link', 500)
                    ->default('');
                });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	       Schema::table('section_memes', function($t) {
               //$t->dropColumn('type');
               $t->dropColumn('link');

           });
	}

}
