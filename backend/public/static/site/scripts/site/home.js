require.config({
  paths: {
    'jquery':'../libs/jquery',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'foundation': '../libs/plugins/foundation',

    'navfixed': '../modules/navfixed',
    'modal': '../modules/modal',
    'filter': '../modules/filter',
    'scrolltop': '../modules/scrolltop',
    'slider': '../modules/slider',
    'hover': '../modules/hover',
    'auth': '../modules/auth',
    'register': '../modules/register',
  },
  shim: {
    'ifhidden' : ['jquery'],
    'filter': ['jquery'],
    'jquery.scrolltop': ['jquery'],
    'scrolltop': ['jquery.scrolltop'],
    'foundation' : ['jquery'],
    'slider' : ['foundation'],
    'modal': ['foundation'],
    'hover': ['jquery']
  }
});

require([
  'jquery',
  'slider',
  'navfixed',
  'modal',
  'filter',
  'scrolltop',
  'hover',
  'auth',
  'register'
], function ($, slider, navFixed, modal, filter, scrollTop, hover, auth, register) {
  slider();
  navFixed($('.nav-fixed'), 'fixed');
  modal();
  filter($('.blocks-grid'), $('.block'));
  scrollTop($('.nav-fixed'));
  hover($('.block-inner'));
  auth($('#login'));
  register($('#register'));
});
