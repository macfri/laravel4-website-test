<?php
class ProductsTableSeeder extends Seeder {

    public function run()
    {

        $file = Photo::create(array('title' => 'image 1', 'path'  => 'upload/home/products/1.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Lata de 1Kg',
            'slug' => 'lata-1kg',
            'description' => 'Si compras tu lata de MILO de 1 kilo en el sol, necesitarás ayuda para 
                cargarla, ¡allá pesa 27 kilos!. Pero si te das una vuelta por la luna, podrás cargar sus 
                100 gramitos con un solo dedo.',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x2'
        ));

        $file = Photo::create(array('title' => 'image 2', 'path'  => 'upload/home/products/2.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Lata de 400g',
            'slug'  => 'lata-de-400g',
            'description' => 'En las olimpiadas de Londres 2012, la medalla de oro para los mejores atletas 
                tiene el peso ideal: 400 gramos de felicidad. ¡Por eso, esta lata de Milo puede ser
                levantada solo por los campeones!',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x2'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/products/3.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Zipper de 200g',
            'slug' => 'zipper-de-200g',
            'description' => 'Se creía que era imposible, pero la NASA encontró una especie similar a un camarón… 
                ¡A 200 metros por debajo de la capa de hielo de la Antártida! ¡Un gramito de Milo por cada metro, 
                para que ese camaroncito salga a la superficie!',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x3'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/products/4.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Sobre de 18g',
            'slug' => 'sobre-de-18g',
            'description' => 'No puedes llegar al cielo con 14 globos… Pero, por el mismo peso, ¡puedes llevar tu 
                potencia más allá de las nubes con el sobrecito que Milo tiene para ti! A volar se ha dicho…',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x2'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/products/5.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Sobre de 50g',
            'slug' => 'sobre-de-50g',
            'description' => 'Un filósofo loco dijo alguna vez: 47, 48, 49... 50. Si yo puedo contar hasta cincuenta,
                ¿por qué el número se llama "sin cuenta"? ¡Pero tú no te hagas tantos problemas! 
                El sobre de 50g de MILO definitivamente cuenta con suficiente energía para tus desayunos.',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x2'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/products/6.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Sobre de 400g',
            'slug' => 'sobre-de-400g',
            'description' => 'Si compras 400 latas de MILO, tu casa estaría llena de latas verdes. 
                ¡Eso sería genial! ¿No crees? Pero probablemente necesitarías más espacio para tu próxima lata. Por eso te 
                recomendamos llenar la última que compraste con la Recarga de 400g.',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '2x3'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/products/7.jpg'));
        DB::table('section_products')->insert(array(
            'title' => 'Milo Free',
            'slug' => 'milo-free',
            'description' => 'La carrera de atletismo de 400 metros es una de las más difíciles de todas, 
                requiere mucha energía y fuerza increíble. Con menos azúcar y calorías, ¡Milo Free puso un 
                gramo por cada metro que un atleta recorre para terminar la carrera!',
            'status' => 'active',
            'author_id' => 1,
             'image_home_id' => $file->id,
            'block_size' => '2x3'
        ));
    }
}
