    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'eventos-galleria'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events') }}">Eventos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events_detail', $item->slug) }}">{{ $item->title }}</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events_gallery', $item->slug) }}">Galería</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="row gallery-detail">
          <div class="columns large-2"> 
            <div class="social-share">
              <div class="shape-grid-gallery"> </div><a data-href="https://milo.no2.pe/website/" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false" class="fb-like"></a><a href="https://twitter.com/share" data-counturl="http://groups.google.com/group/twitter-api-announce" data-lang="en" data-count="vertical" target="_blank" class="twitter-share-button">Twitter
                <sctipt>.<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></sctipt></a>
            </div>
          </div>
          <div class="columns large-8">
            <h2>{{ $item->title }}</h2>
            <div class="date">{{ $item->when_where }}</div>
            <div class="row gallery-photo">
              <div class="columns large-12 large-centered"></div>@foreach($item->gallery as $key => $item)
              <div id="gallery{{ $key + 1 }}" class="figure @if($key === 0) current @endif"><img src="{{ asset($item->path) }}">
                <div class="social-buttons">
                </div>
                <div class="figcaption">{{ $item->description }}</div>
              </div>@endforeach
            </div>
          </div>
          <div class="columns large-2">
            <div class="arrows left"><a href="#gallery1" class="btn-top-gallery"> </a><a href="#gallery1" class="btn-bottom-gallery"> </a></div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'eventos-galleria'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'eventos-galeria'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/events') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
