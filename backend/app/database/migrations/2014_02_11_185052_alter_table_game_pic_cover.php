<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGamePicCover extends Migration {

	public function up()
    {
        Schema::table('game_games', function($t) {
            $t->integer('image_cover_id')
                ->unsigned()
                ->nullable()
                ->index();
            $t->foreign('image_cover_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
        });
	}

	public function down()
    {
        Schema::table('game_games', function($t) {
            $t->dropColumn('image_cover_id');
        });
	}
}
