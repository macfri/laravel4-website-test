    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('freetime') }}">Pasa el rato</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div style="posision:relative;display:block;height:364px" class="fun-banner"><img style="margin:auto;posision:relative;z-index:20;display:block" src="{{ asset('static/site/images/banners/pasa-el-rato.jpg') }}">
          <div style="position:absolute;color: #fff; display: block; z-index: 770; margin-left: -501px; left: 52.5%;  top: 140px; line-height:1" class="title-fun">
            <h3 style="font-family:gotham_ultraregular;font-size:52px;">Pasa el rato </h3>
            <p style="line-height:18px;font-size:15px;">Elegimos lo mejor de toda la web por ti. Pasa el rato y<br>diviértete con nosotros.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <!-- HEADER -->
        @include('site.fun.menu')
        <!-- HEADER -->
      </div>
      <div class="content">
        <div class="row fun">@foreach ($items as $item)
          <div class="columns large-4 left">
            <div class="fun-thumbnail">
              <h2>{{ $item->title }}</h2><a href="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}"><img src="@if( isset( $item->image->path ) ){{ asset($item->image->path) }}@endif"></a>
              <div class="votes">
                <div class="count">
                  <div class="like icon-fun-like"></div><span>{{ $item->vote_total_likes }}</span>
                </div>
                <div class="count">
                  <div class="dislike icon-fun-dislike"></div><span>{{ $item->vote_total_dislikes }}</span>
                </div>
              </div>
            </div>
          </div>@endforeach
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'memes'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'memes'))
    @include('site.register-model')
    <script>
      window.app = window.app || {};
      app.freetime_rank = "{{ URL::route('freetime_rank') }}";
      app.auth = '@if(Auth::check()) auth_true @endif';
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/fun') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
