<?php

class AdminBadgeController extends BaseController {

    private $viewpath = 'admin/badge/';

    public function index() {
        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');
        $search_game = Input::get('search_game');

        $games = Game::lists('title', 'id');
        $items = Badge::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        if (strlen($search_game)) {
            $items->where('game_id', '=', $search_game);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'games' => $games,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'search_game' => $search_game,
                    'items2' => Badge::all()
                        )
        );
    }

    public function add() {
        $games = Game::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'games' => $games
                        )
        );
    }

    public function edit($id) {
        $item = Badge::find($id);
        $games = Game::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                    'games' => $games
                        )
        );
    }

    public function save() {
        $v = Badge::validate(Input::all());
        if ($v->passes()) {
            $item = new Badge();
            $item->id = Input::get('id');
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->status = Input::get('status');
            $item->points = Input::get('points');
            $item->badge = Input::get('badge');
            $item->author_id = Session::get('session_user');
            $item->game_id = Input::get('game_id');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_badge_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'badges');

                $image = Photo::create(array(
                            'title' => $name,
                            'extension' => $ext,
                            'size' => $size,
                            'mime' => $mime
                ));

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'badges', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                $item->image_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            return Redirect::route('admin_badge')
                            ->with('success', sprintf(
                                            'Badge %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {
        $v = Badge::validate(Input::all());
        if ($v->passes()) {
            $item = Badge::find($id);
            $item->id = Input::get('id');
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->status = Input::get('status');
            $item->points = Input::get('points');
            $item->badge = Input::get('badge');
            $item->author_id = Session::get('session_user');
            $item->game_id = Input::get('game_id');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_badge_add', $id)
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'badges');

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'badges', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                    }
                }
            }

            return Redirect::route('admin_badge')
                            ->with('success', 'Badge Created Successfully.');
        } else {
            return Redirect::back()
                            ->withErrors($v)
                            ->withInput();
        }
    }

    public function destroy($id) {
        Badge::find($id)->delete();
        return Redirect::route('admin_badge')
                        ->with('success', 'Badge Deleted Successfully.');
    }

}
