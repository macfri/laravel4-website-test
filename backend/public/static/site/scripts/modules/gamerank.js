define([
  'jquery',
], function($){

  function gameRank($rateGame, $form){
    var $items = $rateGame.find('.face-rank');
    $items.one('click',setVote);

    function setVote(event){
      event.preventDefault();
      var $target = $(this);

      var id = $items.index($target)+1;

      $('#face_id').val('face'+ id);

      var data = $form.serialize();
      if ($form.find('#token_rank').val() !== 'null'){
        $.ajax({
          type:'post',
          data: data,
          url: game.gamerank_url
        }).done(function(response){
          if(response.status_code === 0){
            $target.addClass('face'+id+'-active');

          }else if(response.status_code === 1){


          }
        });
      }else{
        $('.js-login').trigger('click');
      }
    }
  }

  return gameRank;
});
