<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMundial extends Migration {

	public function up()
    {
        Schema::create('game_games_mundial_badwords', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 500);
            $table->boolean('enabled')
                ->index()
                ->default(1);
            $table->timestamps();
        });

        Schema::create('game_games_mundial_team', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->boolean('enabled')
                ->index()
                ->default(0);
            $table->timestamps();
        });

        Schema::create('game_games_mundial_player', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('team_id')
                ->unsigned()->unique();
            $table->foreign('team_id')
                ->references('id')->on('game_games_mundial_team')
                ->on_update('cascade')
                ->onDelete('cascade');
            $table->bigInteger('fb_id')
                ->unsigned()
                ->index()->unique();
            $table->string('tshirt_name', 20);
            $table->string('tshirt_path_front', 1000);
            $table->string('tshirt_path_front_ext', 4);
            $table->string('tshirt_path_back', 1000);
            $table->string('tshirt_path_back_ext', 4);
            $table->smallInteger('tshirt_number');
            $table->timestamps();
        });

        Schema::create('game_games_mundial_player_team', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('player_id')
                ->unsigned();
            $table->bigInteger('fb_id_to')
                ->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('game_games_mundial_badwords');
        Schema::dropIfExists('game_games_mundial_player_team');
        Schema::dropIfExists('game_games_mundial_player');
        Schema::dropIfExists('game_games_mundial_team');
    }
}
