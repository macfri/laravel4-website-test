<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSystemUsers extends Migration {

	public function up()
	{
		Schema::create('system_users', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username', 100)->unique('username');
            $table->string('password', 100);
            $table->string('email', 100)->unique('email');
            $table->enum('status', array('active', 'inactive'))
                ->default('inactive');
            $table->datetime('last_login_at');
			$table->timestamps();
        });
	}

	public function down()
    {
		Schema::dropIfExists('system_users');
	}
}
