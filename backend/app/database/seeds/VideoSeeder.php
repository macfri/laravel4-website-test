<?php
class VideosTableSeeder extends Seeder {

    public function run()
    {

        $file = Photo::create(array('title' => 'image 1', 'path'  => 'upload/home/videos/1.jpg'));
        DB::table('section_videos')->insert(array(
            'title' => 'Recarga tu energía todos los días y ningún reto te vencerá',
            'slug' => 'video-1',
            'brief' => 'brief 1',
            'description' => 'description 1',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '4x2',
            'code' => 'BCC4WtE2aGI'
        ));

        $file = Photo::create(array('title' => 'image 2', 'path'  => 'upload/home/videos/2.jpg'));
        DB::table('section_videos')->insert(array(
            'title' => 'Cree en ti mismo y busca nuevos retos… ¡Nosotros ponemos la energía!',
            'slug' => 'video-2',
            'brief' => 'brief 2',
            'description' => 'description 2',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '4x2',
            'code' => 'BCC4WtE2aGI'
        ));

        $file = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/videos/3.jpg'));
        DB::table('section_videos')->insert(array(
            'title' => 'Si creías que un skatepark de hielo era una buena idea, mejor mira este video…',
            'slug' => 'video-3',
            'brief' => 'brief 3',
            'description' => 'description 3',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file->id,
            'block_size' => '4x2',
            'code' => 'BCC4WtE2aGI'
        ));

        DB::table('section_videos')->insert(array(
            'title' => 'No le tengas miedo a las cosas gigantes… ¡Supéralas con Milo!',
            'slug' => 'video-4',
            'brief' => 'brief 1',
            'description' => 'description 1',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'BCC4WtE2aGI'
        ));

        DB::table('section_videos')->insert(array(
            'title' => 'Chapa tu skate y supera todas las barandas del camino. ¡Juntos lo lograremos!',
            'slug' => 'video-5',
            'brief' => 'brief 2',
            'description' => 'description 2',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'BCC4WtE2aGI'
        ));

        DB::table('section_videos')->insert(array(
            'title' => '¿Eres creativo en todo lo que haces? ¡Este video es para ti!',
            'slug' => 'video-6',
            'brief' => 'brief 3',
            'description' => 'description 3',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'BCC4WtE2aGI'
        ));
    }
}
