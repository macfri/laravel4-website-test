<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGameRoundPicture extends Migration {


	public function up()
	{
        Schema::table('game_games_round', function($t) {
            $t->string('picture', 500)
                ->nullable();
        });
	}

	public function down()
    {
        Schema::table('game_games_round', function($t) {
            $t->dropColumn('picture');
        });
	}
}
