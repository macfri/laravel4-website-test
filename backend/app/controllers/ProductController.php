<?php

class ProductController extends BaseController {

    public function index() {

        Session::set('current_route', URL::current());
        $items = Product::all();
        return View::make(
                        'site/products', array(
                    'items' => $items
                        )
        );
    }

}
