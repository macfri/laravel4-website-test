
    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games_detail', 'crea-tu-camiseta') }}">crea tu camiseta</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="game-content">
          <div id="game-vars">
          </div>
          <div class="row">
            <div class="columns large-12 pt-27">
              <div class="">
                <div class="cont-messages js-game-modal hide">
                  <div class="int-msj-modal chulls-modal">
                    <div class="chulls-stick"></div>
                    <div class="chulls-date"></div>
                    <a class="btn-register-foot btn-color-green js-trigger-register">registrate para participar</a>
                    <p class="line-middle">o</p>
                    <a class="btn-register-foot js-trigger-login">inicia sesión para participar</a>
                    <a href="{{ URL::route('games') }}" class="btn-register-foot">ver otros juegos</a>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div id="gallery-crea-tu-camiseta">
          <div class="gallery-inner-crea-tu-camiseta">
            <div class="header-crea-tu-camiseta">
              <h3 class="logo-crea-tu-camiseta">Crea tu camiseta</h3>
              <div class="sub-header-crea-tu-camiseta"><a onclick="_gaq.push(['_trackEvent','Web-Conversion','Paso1.3-Regresarahome',''])" href="{{ URL::route('games_detail', 'crea-tu-camiseta') }}" class="back"></a>
                <h4 class="gallery-crea-tu-camiseta-txt"></h4>
              </div>
            </div>
            <div class="body-crea-tu-camiseta">
              @foreach ($items as $k=>$p)
                <div class="col col-{{ $k%8 }}"><img src="{{ asset($p->tshirt_path_front.''.$p->tshirt_path_front_ext) }}" 
                  data-img="{{ asset($p->tshirt_path_front.$p->tshirt_path_front_ext) }}">
                  <div class="overlay-camiseta">
                    <div class="user-overlay">{{ $p->user()->fb_first_name }}</div>
                  </div>
                </div>
              @endforeach
         
            </div>
            <div class="footer-crea-tu-camiseta">
              <div class="left">
                <ul class="links-inner">
                  <?php  if($current_page > 1){ ?>
                  <li class="left">
                      <a href="galeria?page=<?php echo $current_page - 1; ?>" class="prev"><</a>
                  </li>
                  <?php }else{ ?>
                  <li class="disabled left">
                      <a><</a>
                  </li>
                  <?php } ?>

                  <?php foreach($pages as $page){ ?>
                      <?php if($page == $current_page){ ?>
                          <li class="active left">
                              <a class="current-page-crea-tu-camiseta"><?php echo $page ?></a>
                          </li>
                      <?php }else{ ?>
                          <li class="left">
                              <a class="disabled" href="galeria?page=<?php echo $page; ?>"><?php echo $page; ?></a>
                          </li>
                      <?php } ?>
                  <?php } ?>

                  <?php if($current_page < $total_pages){ ?>
                      <li class="left">
                          <a class="" href="galeria?page=<?php echo $current_page - 1; ?>">></a>
                      </li>
                  <?php } else { ?>
                      <li class="disabled left">
                          <a>></a>
                      </li>
                  <?php } ?>
                </ul>
              </div>
              <div class="right">{{ $offset }} de {{ $total_items }} FOTOS</div>
            </div>
          </div>
        </div>
        <script id="modal-template" type="text/template">
          <div class="modal-crea-tu-camiseta">
            <div class="left">
              <div class="back-content">
                <div class="back-1"></div>
              </div>
            </div>
            <div class="main-modal-crea-tu-camiseta">
              <div class="close"></div>
              <div class="inner-modal-crea-tu-camiseta"><img src="% data-img %" width="342" height="413"></div>
            </div>
            <div class="left">
              <div class="next-content">
                <div class="next"></div>
              </div>
            </div>
          </div>
        </script>
          <div class="mb-50"></div>
        </div>
      </div>
      </div>

      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos-detalle'))
    @include('site.register-model')
<script data-main="{{ asset('static/site/scripts/site/crea-tu-camiseta') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>

