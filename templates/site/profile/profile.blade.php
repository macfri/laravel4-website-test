    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'perfil'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('profile') }}">Mi perfil</a></li>
              </ul>
            </div>
          </div>
        </div>

        <div class="banner header-profile">
        <!-- HEADER PROFILE -->
        @include('site.profile.menu')
        <!-- HEADER PROFILE -->
        </div>

      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="row profile">
          <div class="columns large-8 progress">
            <h2>ULTIMOS LOGROS CONSEGUIDOS</h2>@if( count( $last_badges ) > 0 )
            @foreach ($last_badges as $last_badge)
            <div class="item-game clearfix"><img src="{{ $last_badge->badge->image->path }}" class="left">
              <div class="description left">
                <p class="middle">{{ $last_badge->badge->title }}, <strong>{{ $last_badge->game->title }}</strong><b>{{ $last_badge->created_at }}</b></p>
              </div>
              <div class="points left"><span>{{ $last_badge->badge->points }}<b>puntos</b></span></div>
            </div>@endforeach
            @else
            <p style="margin: 40px">Aún no has obtenido logros.</p>@endif
          </div>
          <div class="columns large-4 top-games">
            <h2>TUS MÁS JUGADOS</h2>@if( count ( $most_played ) > 0 )
            @foreach ( $most_played as $game )
            <div class="item-game clearfix"><img src="{{ $game->path }}" class="left">
              <div class="name-game left">
                <p class="middle">{{ $game->title }}</p>
              </div>
            </div>@endforeach
            @else
            <p style="margin: 20px ">Aún no has jugado</p>@endif
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'perfil'))
        <!-- FOOTER -->
      </div>
    </div>

    <script>
      window.app = window.app || {};
      app.user_update_cover = "{{ URL::route('user_update_cover') }}";
      app.validate_profile_email = "{{ URL::route('validate_form_email') }}";
      app.validate_profile_dni = "{{ URL::route('validate_form_dni') }}";
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/profile') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
