<?php

use Illuminate\Database\Migrations\Migration;

class MakeSeo extends Migration {


	public function up()
    {

        Schema::table('game_games', function($t) {
            $t->string('seo_meta_title', 70)
                ->nullable();
            $t->string('seo_meta_description', 160)
                ->nullable();
            $t->string('seo_meta_keywords', 160)
                ->nullable();
        });


        Schema::table('section_memes', function($t) {
            $t->string('seo_meta_title', 70)
                ->nullable();
            $t->string('seo_meta_description', 160)
                ->nullable();
            $t->string('seo_meta_keywords', 160)
                ->nullable();
        });

        Schema::table('section_events', function($t) {
            $t->string('seo_meta_title', 70)
                ->nullable();
            $t->string('seo_meta_description', 160)
                ->nullable();
            $t->string('seo_meta_keywords', 160)
                ->nullable();
        });

	}


	public function down()
    {

       Schema::table('game_games', function($t) {
            $t->dropColumn('seo_meta_title');
            $t->dropColumn('seo_meta_description');
            $t->dropColumn('seo_meta_keywords');
       });

       Schema::table('section_memes', function($t) {
            $t->dropColumn('seo_meta_title');
            $t->dropColumn('seo_meta_description');
            $t->dropColumn('seo_meta_keywords');
       });

       Schema::table('section_events', function($t) {
            $t->dropColumn('seo_meta_title');
            $t->dropColumn('seo_meta_description');
            $t->dropColumn('seo_meta_keywords');
       });

	}

}
