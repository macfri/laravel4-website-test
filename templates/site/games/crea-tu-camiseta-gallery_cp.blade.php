<table border="1">
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->slug }}</td>
    </tr>
    @endforeach
</table>

<div class="pagination">
    <ul>
        <?php  if($current_page > 1){ ?>
        <li>
            <a href="<?php echo $current_page - 1; ?>" class="prev">&laquo; anterior</a>
        </li>
        <?php }else{ ?>
        <li class="disabled">
            <a>&laquo; anterior</a>
        </li>
        <?php } ?>  

        <?php foreach($pages as $page){ ?>
            <?php if($page == $current_page){ ?>
                <li class="active">
                    <a><?php echo $page ?></a>
                </li>
            <?php }else{ ?>
                <li>
                    <a class="disabled" href="galeria?page=<?php echo $page; ?>"><?php echo $page; ?></a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php if($current_page < $total_pages){ ?>
            <li>
                <a class="next" href="<?php echo $current_page - 1; ?>">siguiente &raquo;</a>
            </li>
        <?php }else{ ?>
            <li class="disabled">
                <a>siguiente &raquo;</a>
            </li>
        <?php } ?>
    </ul>
</div>
