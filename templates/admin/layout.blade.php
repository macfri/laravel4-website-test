<!DOCTYPE html>
<html>
  <head>
    <title>ADMIN-MILO-WEBSITE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    <script src="{{ asset('static/admin/js/jquery-1.8.3.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('static/admin/jstree/_lib/jquery.js') }}" type="text/javascript"></script>
        <script src="{{ asset('static/admin/jstree/jquery.jstree.js') }}" type="text/javascript"></script>
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap-lightbox.min.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap-responsive.css') }}"rel="stylesheet">
        <link href="{{ asset('static/admin/css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/select2/select2.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/datepicker/css/datepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/colorpicker/css/colorpicker.css') }}" rel="stylesheet">
  </head>
  <body>

<div class="container">
  <div class="navbar">
    <div class="navbar-inner">
        <span class="brand">ADMIN-MILO-WEBSITE</span>
        <ul class="nav">
            <li><a href="{{ URL::route('admin_logout') }}">Logout</a></li>
        </ul>
        <ul class="nav pull-right"></ul>
     </div>
  </div>

  <div class="row-fluid">
    <div class="span2">@include('admin.menu')</div>
    <div class="span10">@yield('content')</div>
  </div>
</div>

    <script src="{{ asset('static/admin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script> 
    <script src="{{ asset('static/admin/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('static/admin/js/filters.js') }}" type="text/javascript"></script>
    <script src="{{ asset('static/admin/js/actions.js') }}" type="text/javascript"></script>
   <script src="{{ asset('static/admin/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    @yield('scripts')
  </body>
</html>
