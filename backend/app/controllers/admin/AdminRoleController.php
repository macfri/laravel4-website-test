<?php

class AdminRoleController extends BaseController {

    private $viewpath = 'admin/role/';

    public function index() {

        if (!SystemUser::has_perm('roles-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $items = Role::where('id', '>', 0);
        $total = $items->count();
        $items = $items->paginate(15);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('roles-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $permissions = Permission::all();

        return View::make(
                        $this->viewpath . __function__, array('permissions' => $permissions)
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('roles-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Role::find($id);
        $sql = sprintf('select id, name, IF(permission_id > 0, 1, 0) as state from 
                website_system_users_permissions as a left join 
                website_system_users_role_permissions b on (a.id = b.permission_id) and b.role_id = %d;', $id);
        $permissions = DB::select(DB::raw($sql));

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                    'permissions' => $permissions
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('roles-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = new Role();
        $item->name = Input::get('name');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_role_add')
                            ->with('failure', $exception->getMessage());
        }

        $permissions = Input::get('permissions');
        $data_insert = array();
        foreach ($permissions as $row => $vid) {
            $data_insert[] = array('role_id' => $item->id, 'permission_id' => $vid);
        }
        DB::table('system_users_role_permissions')->insert($data_insert);

        return Redirect::route('admin_role')
                        ->with('success', sprintf(
                                        'Role %s Created Successfully.', $item->title));
    }

    public function update($id) {

        if (!SystemUser::has_perm('roles-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Role::find($id);
        $item->name = Input::get('name');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_role_edit', $id)
                            ->with('failure', $exception->getMessage());
        }

        $permissions = Input::get('permissions');

        DB::table('system_users_role_permissions')
                ->where('role_id', '=', $item->id)
                ->delete();


        $permissions = Input::get('permissions');
        $data_insert = array();
        foreach ($permissions as $row => $vid) {
            $data_insert[] = array('role_id' => $item->id, 'permission_id' => $vid);
        }
        DB::table('system_users_role_permissions')->insert($data_insert);

        return Redirect::route('admin_role')
                        ->with('success', 'Role Updated Successfully.');
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('roles-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }


        DB::table('system_users_role_permissions')
                ->where('role_id', '=', $id)
                ->delete();

        Role::find($id)->delete();
        return Redirect::route('admin_role')
                        ->with('success', 'Role Deleted Successfully.');
    }

}
