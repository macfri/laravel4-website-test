define([
  'jquery',
  '../libs/plugins/jquery.validate'
], function($){
  function auth($form){
    //config
    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true
        }
      },
      messages: {
        email: {
          required: '*Olvidaste  ingresar tu correo electrónico.',
          email: 'Necesitas un correo válido'
        },
        password: {
          required: 'Olvidaste ingresar tu contraseña'
        }
      },
      submitHandler: function(form){
        form.submit();
      }
    });
  }
  return auth;
});
