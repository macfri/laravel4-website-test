<?php
class SpotTableSeeder extends Seeder {

    public function run()
    {
        DB::table('spots')->insert(array(
            'title' => 'Skateball',
            'slug' => 'skateball',
            'description' => 'Una de las mejores combinaciones del universo: ¡Skateboard + Basketball!',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'IHBpofhBdYQ'
        ));

        DB::table('spots')->insert(array(
            'title' => '¿Un skatepark de hielo?',
            'slug' => 'un-skate-park-de-hielo',
            'description' => 'Si creías que un skatepark de hielo era una buena idea, mejor mira este video…',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'Sllhq0qpKdE'
        ));

        DB::table('spots')->insert(array(
            'title' => '¿Listo para ganar?',
            'slug' => 'listo-para-ganar',
            'description' => 'Con un vaso de Milo, ¡serás más grande que cualquiera!',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'QlPxHo6vOsc'
        ));

        DB::table('spots')->insert(array(
            'title' => 'Si lo alucinaste, ¡hazlo!',
            'slug' => 'si-lo-alucinaste-hazlo',
            'description' => '¿Eres creativo en todo lo que haces? ¡Este video es para ti!',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'jzg3lawrzvg'
        ));

        DB::table('spots')->insert(array(
            'title' => '¡Milo te recarga de energía!',
            'slug' => 'milo-te-recarga-de-energia',
            'description' => 'Recarga tu energía todos los días y ningún reto te vencerá',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'kWVuSOlkAqc'
        ));

        DB::table('spots')->insert(array(
            'title' => '¡Recárgate con Milo!',
            'slug' => 'recarga-con-milo',
            'description' => 'Chapa tu skate y supera todas las barandas del camino. ¡Juntos lo lograremos!',
            'status' => 'active',
            'author_id' => 1,
            'code' => '7mPoirCY3NQ'
        ));

        DB::table('spots')->insert(array(
            'title' => '¡Tú pones el reto, Milo pone la energía!',
            'slug' => 'tu-pones-el-reto-milo-para-la energia',
            'description' => 'Cree en ti mismo y busca nuevos retos… ¡Nosotros ponemos la energía',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'KMccgKw1zkg'
        ));

        DB::table('spots')->insert(array(
            'title' => '¡Cosas gigantes!',
            'slug' => 'cosas-gigantes',
            'description' => 'No le tengas miedo a las cosas gigantes… ¡Supéralas con Milo!',
            'status' => 'active',
            'author_id' => 1,
            'code' => 'EA3lEdy4n9Y'
        ));
    }
}
