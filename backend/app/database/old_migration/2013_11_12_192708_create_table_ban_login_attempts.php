<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBanLoginAttempts extends Migration {

	public function up()
	{
		Schema::create('login_attempts', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->string('ip', 20);
            $table->integer('attempts');
            $table->datetime('last_login');
        });
	}

	public function down()
    {
        Schema::dropIfExists('login_attempts');
	}
}
