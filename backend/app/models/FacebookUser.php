<?php

class FacebookUser extends Eloquent {

    protected $table = 'users_facebook';
    protected $guarded = array('id');
    protected $fillable = array(
        'fb_id',
        'fb_first_name',
        'fb_last_name',
        'fb_email',
        'fb_gender',
        'fb_oauth_token',
        'fb_expires',
        'fb_username'
    );

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public static function validate($input) {
        $rules = array(
            'username' => 'Required|Min:3|Max:80|Alpha|Unique:users',
            'first_name' => 'Required|Min:3|Max:80|Alpha',
            'last_name' => 'Required|Min:3|Max:80|Alpha',
            'email' => 'Required|Between:3,64|Email|Unique:users',
            'dni' => 'Required|Size:8|Unique:users',
            'phone' => 'Max:18',
            'password' => 'Required|AlphaNum|Between:4,8|Confirmed',
            'password_confirmation' => 'Required|AlphaNum|Between:4,8'
        );
        return Validator::make($input, $rules);
    }

    public static function validate_simple($input) {
        $rules = array(
            'first_name' => 'Required|Min:3|Max:80|Alpha',
            'last_name' => 'Required|Min:3|Max:80|Alpha',
            'email' => 'Required|Between:3,64|Email|Unique:users',
            'password' => 'Required|AlphaNum|Between:4,8',
        );
        return Validator::make($input, $rules);
    }

}
