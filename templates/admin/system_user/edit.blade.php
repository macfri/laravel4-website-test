@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'files' => true)) }}
<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit User')</legend>

    <div class="control-group">
        <div class="control-label">username<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="username" id="username" value="@if(isset($item)){{$item->username}}@else{{ Form::old('username')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">password<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="password" id="password" value="@if(isset($item)){{$item->password}}@else{{ Form::old('password')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">email<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="email" id="email" value="@if(isset($item)){{$item->email}}@else{{ Form::old('email')}}@endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">role<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>

                <select name="role_id" id="role_id">
                    <option value="">[Select]</option>
                    @foreach($roles as $row)
                        <option value="{{ $row->id }}" @if(isset($item)) @if($item->role_id == $row->id) selected @endif @endif" >{{ $row->name }}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>



</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="controls"> 
        <input id="btn_submit" type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div> 

</fieldset>

{{ Form::close() }}
@stop
