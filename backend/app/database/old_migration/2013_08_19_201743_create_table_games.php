<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGames extends Migration {

	public function up()
    {

		Schema::create('game_games', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('description');
            $table->string('slug', 100)->index();
            $table->string('path_swf');
            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive');
            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->text('block_size', 20);
            $table->boolean('show_home')
                ->default(0);
            $table->integer('image_home_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_home_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->integer('image_ctrl_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_ctrl_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade')
                ->onDelete('cascade');

			$table->timestamps();
        });

		Schema::create('game_players', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->on_update('cascade');

            $table->integer('game_id')->unsigned()->index();
            $table->foreign('game_id')
                ->references('id')->on('game_games')
                ->on_update('cascade');
            
            $table->string('trophies');
            $table->integer('max_score');
            $table->integer('total_score');
            $table->integer('attempts');
            $table->boolean('was_guest');
			$table->timestamps();
        });

		Schema::create('game_points', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->on_update('cascade');

            $table->boolean('winner');
            $table->integer('game_id')->unsigned()->index();
            $table->foreign('game_id')
                ->references('id')->on('game_games')
                ->on_update('cascade');

            $table->integer('score');
			$table->timestamps();
        });

	}

	public function down()
    {
        Schema::dropIfExists('game_games');
        Schema::dropIfExists('game_players');
        Schema::dropIfExists('game_points');
	}

}
