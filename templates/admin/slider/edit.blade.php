@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif


@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<fieldset>



    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" style="width:420px;" name="title" id="title" value="@if(isset($item->title)) {{$item->title}} @else {{ Form::old('title')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">description<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <textarea id="description" name="description" cols="80" rows="4">@if(isset($item->description)) {{$item->description}} @else {{Form::old('description')}} @endif</textarea>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Photo&nbsp;</div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>

            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#demoLightbox">{{ $item->image->title  }}</a>
            <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }}">
                </div>
            </div>
            @endif

        </div>
    </div>

    <div class="control-group">
        <div class="control-label">backgroung_color<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="backgroung_color" id="backgroung_color" value="@if(isset($item->backgroung_color)) {{ $item->backgroung_color}} @else {{ Form::old('backgroung_color') }} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">button_title<strong style="color: red ">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="button_title" id="button_title" value="@if(isset($item->button_title)) {{$item->button_title }} @else {{Form::old('button_title')}} @endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">button_backgroung_color<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="button_backgroung_color" id="button_backgroung_color" value="@if(isset($item->button_backgroung_color)) {{$item->button_backgroung_color}} @else {{Form::old('button_backgroung_color') }} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">link<strong style="color: red">&#42;</strong></div> 
        <div class="controls">
           <div>
                <input type="text" style="width:420px;"   name="link" id="link" value="@if(isset($item->link)) {{$item->link}} @else {{Form::old('link')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">order<strong style="color: red">&#42;</strong></div> 
        <div class="controls">
           <div>
                <input type="text" name="order" id="order" value="@if(isset($item->order)) {{$item->order}} @else {{Form::old('order')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
        <input type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div>

</fieldset>
{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $('#backgroung_color').colorpicker();
        $('#button_backgroung_color').colorpicker();
    });
</script>


    <script src="{{ asset('static/admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('static/admin/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document ).ready( function() {
            $( 'textarea#description' ).ckeditor();
        });
    </script>


@stop
