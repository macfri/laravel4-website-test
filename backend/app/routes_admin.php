<?php

Route::group(array('prefix' => 'admin'), function()
{
    Route::get('/',array('as' => 'admin', 'uses' => 'AdminSystemUserController@login'));
    Route::get('/login',array('as' => 'admin_login', 'uses' => 'AdminSystemUserController@login'));
    Route::post('/login',array('as' => 'admin_dologin', 'uses' => 'AdminSystemUserController@dologin'));
    Route::get('/logout',array('as' => 'admin_logout', 'uses' => 'AdminSystemUserController@logout'));
    
    Route::get('/base',array('as' => 'admin_base', 'uses' => 'AdminSystemUserController@base'));

    Route::group(array('prefix' => 'seo'), function()
    {
        Route::get('/',array('as' => 'admin_seo', 'uses' => 'AdminSeoController@index'));
        Route::get('/add', array('as' => 'admin_seo_add', 'uses' => 'AdminSeoController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_seo_save', 'uses' => 'AdminSeoController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_seo_edit', 'uses' => 'AdminSeoController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_seo_update', 'uses' => 'AdminSeoController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_seo_destroy', 'uses' => 'AdminSeoController@destroy'));
    });

    Route::group(array('prefix' => 'products'), function()
    {
        Route::get('/',array('as' => 'admin_product', 'uses' => 'AdminProductController@index'));
        Route::get('/add', array('as' => 'admin_product_add', 'uses' => 'AdminProductController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_product_save', 'uses' => 'AdminProductController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_product_edit', 'uses' => 'AdminProductController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_product_update', 'uses' => 'AdminProductController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_product_destroy', 'uses' => 'AdminProductController@destroy'));
    });

    Route::group(array('prefix' => 'users'), function()
    {
        Route::get('/',array('as' => 'admin_user', 'uses' => 'AdminUserController@index'));
        Route::get('/export',array('as' => 'admin_user_export', 'uses' => 'AdminUserController@export'));
        Route::get('/report',array('as' => 'admin_user_report', 'uses' => 'AdminUserController@report'));
        Route::get('/report2',array('as' => 'admin_user_report2', 'uses' => 'AdminUserController@report2'));
        Route::get('/report3',array('as' => 'admin_user_report3', 'uses' => 'AdminUserController@report3'));
        Route::get('/camisetas',array('as' => 'admin_user_camiseta', 'uses' => 'AdminUserController@camisetas'));
        Route::get('/reports',array('as' => 'admin_user_reports', 'uses' => 'AdminUserController@reports'));
        Route::get('/add', array('as' => 'admin_user_add', 'uses' => 'AdminUserController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_user_save', 'uses' => 'AdminUserController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_user_edit', 'uses' => 'AdminUserController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_user_update', 'uses' => 'AdminUserController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_user_destroy', 'uses' => 'AdminUserController@destroy'));
    });

    Route::group(array('prefix' => 'system_users'), function()
    {
        Route::get('/',array('as' => 'admin_system_user', 'uses' => 'AdminSystemUserController@index'));
        Route::get('/add', array('as' => 'admin_system_user_add', 'uses' => 'AdminSystemUserController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_system_user_save', 'uses' => 'AdminSystemUserController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_system_user_edit', 'uses' => 'AdminSystemUserController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_system_user_update', 'uses' => 'AdminSystemUserController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_system_user_destroy', 'uses' => 'AdminSystemUserController@destroy'));
    });

    Route::group(array('prefix' => 'permissions'), function()
    {
        Route::get('/',array('as' => 'admin_permission', 'uses' => 'AdminPermissionController@index'));
        Route::get('/add', array('as' => 'admin_permission_add', 'uses' => 'AdminPermissionController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_permission_save', 'uses' => 'AdminPermissionController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_permission_edit', 'uses' => 'AdminPermissionController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_permission_update', 'uses' => 'AdminPermissionController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_permission_destroy', 'uses' => 'AdminPermissionController@destroy'));
    });

    Route::group(array('prefix' => 'roles'), function()
    {
        Route::get('/',array('as' => 'admin_role', 'uses' => 'AdminRoleController@index'));
        Route::get('/add', array('as' => 'admin_role_add', 'uses' => 'AdminRoleController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_role_save', 'uses' => 'AdminRoleController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_role_edit', 'uses' => 'AdminRoleController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_role_update', 'uses' => 'AdminRoleController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_role_destroy', 'uses' => 'AdminRoleController@destroy'));
    });


    Route::group(array('prefix' => 'players'), function()
    {
        Route::get('/',array('as' => 'admin_player', 'uses' => 'AdminUserController@index'));
        Route::get('/export',array('as' => 'admin_player_export', 'uses' => 'AdminPlayerController@export'));
        Route::get('/report',array('as' => 'admin_player_report', 'uses' => 'AdminPlayerController@report'));
        Route::get('/report2',array('as' => 'admin_player_report2', 'uses' => 'AdminPlayerController@report2'));
        Route::get('/report3',array('as' => 'admin_player_report3', 'uses' => 'AdminPLayerController@report3'));
        Route::get('/reports',array('as' => 'admin_player_reports', 'uses' => 'AdminPlayerController@reports'));
        Route::get('/add', array('as' => 'admin_player_add', 'uses' => 'AdminPlayerController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_player_save', 'uses' => 'AdminPlayerController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_player_edit', 'uses' => 'AdminPLayerController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_player_update', 'uses' => 'AdminPlayerController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_player_destroy', 'uses' => 'AdminPlayerController@destroy'));
    });

    Route::group(array('prefix' => 'memes'), function()
    {
        Route::get('/',array('as' => 'admin_meme', 'uses' => 'AdminMemeController@index'));
        Route::get('/add', array('as' => 'admin_meme_add', 'uses' => 'AdminMemeController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_meme_save', 'uses' => 'AdminMemeController@save'));
        Route::post('/change_status', array('before' => 'csrf', 'as' => 'admin_meme_change_status', 'uses' => 'AdminMemeController@change_status'));
        Route::get('/{id}/edit', array('as' => 'admin_meme_edit', 'uses' => 'AdminMemeController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_meme_update', 'uses' => 'AdminMemeController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_meme_destroy', 'uses' => 'AdminMemeController@destroy'));
        Route::post('/import_facebook',array('before' => 'csrf', 'as' => 'admin_meme_import_facebook', 'uses' => 'AdminMemeController@import_facebook'));
    });

    Route::group(array('prefix' => 'memes_default'), function()
    {
        Route::get('/',array('as' => 'admin_meme_default', 'uses' => 'AdminMemeDefaultController@index'));
        Route::get('/add', array('as' => 'admin_meme_default_add', 'uses' => 'AdminMemeDefaultController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_meme_default_save', 'uses' => 'AdminMemeDefaultController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_meme_default_edit', 'uses' => 'AdminMemeDefaultController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_meme_default_update', 'uses' => 'AdminMemeDefaultController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_meme_default_destroy', 'uses' => 'AdminMemeDefaultController@destroy'));
    });

    Route::group(array('prefix' => 'games'), function()
    {
        Route::get('/',array('as' => 'admin_game', 'uses' => 'AdminGameController@index'));
        Route::get('/chulls',array('as' => 'admin_game_chulls', 'uses' => 'AdminGameController@index_chulls'));
        Route::get('/players',array('as' => 'admin_game_players', 'uses' => 'AdminGameController@report_players'));
        Route::get('/add', array('as' => 'admin_game_add', 'uses' => 'AdminGameController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_game_save', 'uses' => 'AdminGameController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_game_edit', 'uses' => 'AdminGameController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_game_update', 'uses' => 'AdminGameController@update'));
        Route::get('/{slug}/report', array('as' => 'admin_game_report', 'uses' => 'AdminGameController@report'));
        Route::get('/{slug}/export', array('as' => 'admin_game_export', 'uses' => 'AdminGameController@export'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_game_destroy', 'uses' => 'AdminGameController@destroy'));
    });

    Route::group(array('prefix' => 'videos'), function()
    {
        Route::get('/',array('as' => 'admin_video', 'uses' => 'AdminVideoController@index'));
        Route::get('/add', array('as' => 'admin_video_add', 'uses' => 'AdminVideoController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_video_save', 'uses' => 'AdminVideoController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_video_edit', 'uses' => 'AdminVideoController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_video_update', 'uses' => 'AdminVideoController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_video_destroy', 'uses' => 'AdminVideoController@destroy'));
    });

    Route::group(array('prefix' => 'events'), function()
    {
        Route::get('/',array('as' => 'admin_event', 'uses' => 'AdminEventController@index'));
        Route::get('/add', array('as' => 'admin_event_add', 'uses' => 'AdminEventController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_event_save', 'uses' => 'AdminEventController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_event_edit', 'uses' => 'AdminEventController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_event_update', 'uses' => 'AdminEventController@update'));
        Route::post('/gallery/destroy',array('before' => 'csrf', 'as' => 'admin_event_gallery_destroy', 'uses' => 'AdminEventController@gallery_destroy')); 
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_event_destroy', 'uses' => 'AdminEventController@destroy'));
    });

    Route::group(array('prefix' => 'sliders'), function()
    {
        Route::get('/',array('as' => 'admin_slider', 'uses' => 'AdminSliderController@index'));
        Route::get('/add', array('as' => 'admin_slider_add', 'uses' => 'AdminSliderController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_slider_save', 'uses' => 'AdminSliderController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_slider_edit', 'uses' => 'AdminSliderController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_slider_update', 'uses' => 'AdminSliderController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_slider_destroy', 'uses' => 'AdminSliderController@destroy'));
    });

    Route::group(array('prefix' => 'spots'), function()
    {
        Route::get('/',array('as' => 'admin_spot', 'uses' => 'AdminSpotController@index'));
        Route::get('/add', array('as' => 'admin_spot_add', 'uses' => 'AdminSpotController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_spot_save', 'uses' => 'AdminSpotController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_spot_edit', 'uses' => 'AdminSpotController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_spot_update', 'uses' => 'AdminSpotController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_spot_destroy', 'uses' => 'AdminSpotController@destroy'));
    });

    Route::group(array('prefix' => 'badges'), function()
    {
        Route::get('/',array('as' => 'admin_badge', 'uses' => 'AdminBadgeController@index'));
        Route::get('/add', array('as' => 'admin_badge_add', 'uses' => 'AdminBadgeController@add'));
        Route::post('/add', array('before' => 'csrf', 'as' => 'admin_badge_save', 'uses' => 'AdminBadgeController@save'));
        Route::get('/{id}/edit', array('as' => 'admin_badge_edit', 'uses' => 'AdminBadgeController@edit'));
        Route::post('/{id}/edit', array('before' => 'csrf', 'as' => 'admin_badge_update', 'uses' => 'AdminBadgeController@update'));
        Route::post('/{id}/destroy',array('before' => 'csrf', 'as' => 'admin_badge_destroy', 'uses' => 'AdminBadgeController@destroy'));
    });

});
