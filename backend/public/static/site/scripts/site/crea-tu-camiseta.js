require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'tinyscroll':['../libs/plugins/jquery.tinyscrollbar'],
    'modal': '../modules/modal',
    'closeAlert': '../modules/closeAlert',
    'acordeon': '../modules/acordeon',
    'register': '../modules/register',
    'auth': '../modules/auth',
    'slider': '../modules/slider',
    'gamerank': '../modules/gamerank',
    'modal2': '../modules/custom_modal'
  },
  shim: {
    'modal': ['jquery'],
    'modal2': ['jquery'],
    'closeAlert': ['jquery'],
    'acordeon': ['jquery'],
    'register': ['jquery'],
    'auth': ['jquery'],
    'foundation' : ['jquery'],
    'slider' : ['foundation'],
    'gamerank': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'closeAlert',
  'acordeon',
  'register',
  'auth',
  'slider',
  'gamerank',
  'modal2'
],
function ($, modal, closeAlert, acordeon, register, auth, slider, gameRank, modal2) {


    var $doc = $(document);

    function init () {
      $('.body-crea-tu-camiseta .col').on('click', detailCamiseta);
      $('body').on('click', '.modal-crea-tu-camiseta .close', hideCamiseta);
      $('body').on('click', '.modal-crea-tu-camiseta .next', next);
      $('body').on('click', '.modal-crea-tu-camiseta .back-1', back);
      modal2.init();

      modal();
      closeAlert();
      acordeon($('.cont-badget a'));
      register($('#register'));
      auth($('#login'));
      gameRank($('.rate-game'), $('#game-vars form'));
      slider();

      $('.btn-close-x').on('click', function(event){
        event.preventDefault();
        var $target = $(this);
        $target.parent().hide(400).remove();

      });

    }

    function detailCamiseta () {
      var template  = $('#modal-template').html();
      template = template.split('% data-img %').join($(this).find('img').attr('data-img'));
      modal2.setupModal({
        'padding' : 0,
        'width'   : 630,
        'height'  : 520,
        'content' : template
      });

      $('.modal-crea-tu-camiseta .back').removeClass('active');
      $(this).addClass('active');
    };

    function hideCamiseta () {
      modal2.hideModal();
    };

    function next () {
      var active = $('.body-crea-tu-camiseta .col.active'),
          next = active.next();

      if (next.length > 0) {
        $('.modal-crea-tu-camiseta').find('img').attr('src', next.find('img').attr('data-img'))
        active.removeClass('active');
        next.addClass('active');
      }
      

    };

    function back () {
      var active = $('.body-crea-tu-camiseta .col.active'),
          prev = active.prev();

      if (prev.length > 0) {
        $('.modal-crea-tu-camiseta').find('img').attr('src', prev.find('img').attr('data-img'))
        active.removeClass('active');
        prev.addClass('active');
      }

      

    }

    $doc.ready(init);
});