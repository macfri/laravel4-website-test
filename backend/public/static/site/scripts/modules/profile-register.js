define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){
  function editProfileRegister($form){
    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        dni: {
          required: true
        }
        
      },
      messages: {
        email: {
          required: 'Olvidaste  ingresar tu correo electrónico.',
          email: 'Necesitas un correo válido'
        },
        dni: {
          required: 'Olvidaste ingresar tu DNI'
        }
      },
      submitHandler: function(form){
        form.submit();
      }
    });
  }
  return editProfileRegister;
});
