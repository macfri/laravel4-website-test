<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUbigeo extends Migration {

	public function up()
	{
		Schema::create('ubigeo', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cod_dpto', 2)->index();
            $table->string('cod_prov', 2)->index();
            $table->string('cod_dist', 2)->index();
            $table->string('name', 100);
        });
	}

	public function down()
	{
		Schema::drop('ubigeo');
	}
}
