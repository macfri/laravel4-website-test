define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){
  function editProfileFb($form){
    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {

        first_name: {
          required: true,
        },
        last_name: {
          required: true
        },
        email: {
          required: true,
          email: true,

          remote:{
            url: app.validate_profile_email,
            data: {
              value: function(){
                return $('#email').val();
              }
            }
          }

        },
        age: {
          required: true
        },
        dni: {
          required: true,
          minlength: 8,

          remote:{
            url: app.validate_profile_dni,
            data: {
              value: function(){
                return $('#dni').val();
              }
            }
          }

        },
        auth: {
          required: false
        },
        day: {
          required: true
        },
        month: {
          required: true
        },
        year: {
          required: true
        },
        phone: {
          required: true,
          minlength: 7
        }
      },
      messages: {
        first_name: {
          required: 'Olvidaste ingresar tu nombre'
        },
        last_name: {
          required: 'Olvidaste ingresar tus apellidos'
        },
        email: {
          required: 'Olvidaste  ingresar tu correo electrónico.',
          email: 'Necesitas un correo válido',
          remote: 'Este correo electrónico ya existe'
        },
        age: {
          required: 'Campo requerido'
        },
        dni: {
          required: 'Campo requerido',
          remote: 'Este DNI ya existe',
          minlength: '8 caracteres'
        },
        auth: {
          required: 'Campo requerido'
        },
        info: {
          required: 'Campo requerido'
        },
        day: {
          required: 'Campo requerido'
        },
        month: {
          required: 'Campo requerido'
        },
        year: {
          required: 'Campo requerido'
        },
        authorize_father: {
          required: 'Campo requerido'
        },
        phone: {
          required: 'Olvidaste ingresar tu teléfono'
        },         
      },
      submitHandler: function(form){
        form.submit();
      },
      errorPlacement: function(error, element) {
        var el_name = element.attr("name");
        if ( el_name == "day" || el_name == "month" || el_name == "year") {
          $(".error-birthday").html(error);

        }
        else if ( el_name == "authorize_father" ) {
          $('.authorize-error').html(error);

        }

        else {
          error.insertAfter(element);
        }

      }


    });

    $form.find('#year').on('change', function(){
      $el = $(this);
      $authorize = $form.find('#authorize_father');
      var year = parseInt( $el.val(), 10 );
      var current_year = new Date().getFullYear();

      if( ( current_year - year ) < 18 ) {

        $authorize.rules('add', {required: true });

      } else {
        $authorize.rules('add',{required: false });

      console.log('requerido false');
      }
    });
  }
  return editProfileFb;
});
