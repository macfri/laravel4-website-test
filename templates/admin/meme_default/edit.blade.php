@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

<fieldset>

    <legend class="scheduler-border">@yield('title_module', 'Edit Default Meme')</legend>


    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Photo&nbsp;</div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>
            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#demoLightbox">{{ $item->image->title  }}</a>
            <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
        <input type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div>


</fieldset>

{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('static/admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('static/admin/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document ).ready( function() {
            $( 'textarea#description' ).ckeditor();
        });
    </script>


@stop
