    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'inicio'))
        <!-- MENU -->        
        </div>
        <div class="orbit slideshow-wrapper">
          <div class="preloader"></div>
          <ul data-orbit>@foreach ($slides as $item)
            <li>@if(isset($item->image->id))<img src="{{ asset($item->image->path) }}" alt="{{ $item->image->title }}" width="1500" height="750">@endif
              <div class="caption">
                <h2>{{ $item->title }}</h2>
                <p>{{ $item->description }}</p>@if($item->id == 3)<a href="{{ $item->link }}" target="_blank" class="btn-slider">{{ $item->button_title }}</a>@else<a href="{{ $item->link }}" class="btn-slider">{{ $item->button_title }}</a>@endif
              </div>
            </li>@endforeach
          </ul>
        </div>
        <div class="nav-fixed">
          <div class="inner row">
            <div class="left"><a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Volver-Home','inicio'])" class="btn-logo hide"><span class="logo-fixed"></span></a></div>
            <div class="cont-menu-fixed">
              <ul>
                <li data-id='1' class="selected"><a href="{{ asset('static/site/products') }}"><i class="check-on"></i><span>Productos</span></a></li>
                <li data-id='2' class="selected"><a href="{{ asset('static/site/#') }}"><i class="check-on"></i><span>Facebook</span></a></li>
                <li data-id='3' class="selected"><a href="{{ asset('static/site/videos') }}"><i class="check-on"></i><span>Videos</span></a></li>
                <li data-id='4' class="selected"><a href="{{ asset('static/site/games') }}"><i class="check-on"></i><span>Juegos</span></a></li>
                <li data-id='5' class="selected"><a href="{{ asset('static/site/events') }}"><i class="check-on"></i><span>Eventos y Xtreme</span></a></li>
              </ul>
            </div><a href="#" class="btn-top"><span>Arriba</span></a>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="home">
          <div class="home-grid blocks-grid">@foreach ($items as $item)
            <div data-id="{{ $item['section_id'] }}" data-w="{{ $item['w'] }}" data-h="{{ $item['h'] }}" class="block b{{ $item['w'] }}x{{ $item['h'] }}">
              <div data-category="{{$item['section_name']}}" class="block-inner"><img src="{{ asset($item['url']) }}" class="block-image"><img src="{{ asset('static/site/images/overlay.png') }}" class="block-overlay">
                <div class="block-over">
                  <div class="icon-category"><img src="{{  sprintf(asset('static/site/images/icon-%s.png'), $item['section_name']) }}"></div>
                  <div class="icon-category-small"><img src="{{  sprintf(asset('static/site/images/icon-%s.png'), $item['section_name']) }}"></div><b>{{ $item['title'] }}</b>@if($item['section_name'] === 'social')<a href="{{ $item['detail_link'] }}" target="_blank" class="btn-view-more fb">ir a facebook</a>@else<a href="{{ $item['detail_link'] }}" class="btn-view-more">Ver más</a>@endif
                </div>
              </div>
            </div>@endforeach
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'inicio'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'inicio'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/home') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
