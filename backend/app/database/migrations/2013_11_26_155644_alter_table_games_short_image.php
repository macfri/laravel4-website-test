<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableGamesShortImage extends Migration {

	public function up()
	{
        Schema::table('game_games', function($t) {
            $t->integer('image_short_id')
                ->unsigned()
                ->nullable()
                ->index();
            $t->foreign('image_short_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
        });
	}


	public function down()
    {
       Schema::table('game_games', function($t) {
            $t->dropColumn('image_short_id');
       });
	}

}
