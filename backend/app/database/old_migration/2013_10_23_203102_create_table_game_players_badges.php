<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGamePlayersBadges extends Migration {

	public function up()
	{
		Schema::create('game_players_badges', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('game_id')->unsigned()->index();
            $table->foreign('game_id')
                ->references('id')->on('game_games')
                ->on_update('cascade');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->on_update('cascade');


            $table->integer('badge_id')->unsigned()->index();
            $table->foreign('badge_id')
                ->references('id')->on('game_badges')
                ->on_update('cascade');
        });
	}

	public function down()
    {
        Schema::dropIfExists('game_players_badges');
	}

}
