define([
  'jquery',
], function($){
  function funvote(arg){
    var url = app.freetime_rank;

    var $likes = $(arg.like);
    var $dislikes = $(arg.dislike);

    $likes.on('click', setLike);
    $dislikes.on('click', setDislike);

    function setLike(event){
      event.preventDefault();

      if(app.auth === ""){
        console.log('trigger login');
        $('.js-login').trigger("click");
        return false;
      }

      var $btn_like = $(this);
      var $post = $btn_like.parents('.fun-post-detail');

      var $btn_dislike = $post.find('.dislike-large');

      var meme_id = $post.attr('data-meme-id');

      var $likes = $post.find('.var-like');
      var $dislikes = $post.find('.var-dislike');

      var likes = parseInt( $likes.text(), 10 );
      var dislikes =  parseInt( $dislikes.text(), 10 );

      if ( $btn_like.is('.active-vote') ) {
        likes -= 1;
      } else {
        likes += 1;
      }

      $btn_like.toggleClass('active-vote');
      $likes.text(likes);

      if ( $btn_dislike.is('.active-vote') ) {
        $btn_dislike.removeClass('active-vote');
        $dislikes.text(dislikes - 1);
      }

      $.ajax({
        type: 'post',
        url: url,
        data: {
          vote_id: 1,
          meme_id: meme_id,
          _token: $('.fun-buttons-vote #_token').val()
        }
      }).done(function(response){
        if(response.status_code === 0){

        }
      });
    }

    function setDislike(event){
      event.preventDefault();
      if(app.auth === ""){
        $('.js-login').trigger("click");
        return false;
      }


      var $btn_dislike = $(this);
      var $post = $btn_dislike.parents('.fun-post-detail');

      var $btn_like = $post.find('.like-large');

      var meme_id = $post.attr('data-meme-id');

      var $likes = $post.find('.var-like');
      var $dislikes = $post.find('.var-dislike');

      var likes = parseInt( $likes.text(), 10 );
      var dislikes =  parseInt( $dislikes.text(), 10 );

      if ( $btn_dislike.is('.active-vote') ) {
        dislikes -= 1;
      } else {
        dislikes += 1;
      }

      $btn_dislike.toggleClass('active-vote');
      $dislikes.text(dislikes);

      if ( $btn_like.is('.active-vote') ) {
        $btn_like.removeClass('active-vote');
        $likes.text(likes - 1);
      }

      $.ajax({
        type: 'post',
        url: url,
        data: {
          vote_id: 0,
          meme_id: meme_id,
          _token: $('.fun-buttons-vote #_token').val()
        }
      }).done(function(response){
        if(response.status_code === 0){

        }
      });

    }
  }
  return funvote;
});
