<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBanners extends Migration {

	public function up()
	{
		Schema::create('section_banners', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('slug', 100);
            $table->string('brief', 300);
            $table->text('description');

            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('section_banners');
	}

}
