
<table border="1" width="100%">
    <thead>
        <th class="span1">name_rg</th>
        <th class="span1">lastname_rg</th>
        <th class="span1">email_rg</th>
        <th class="span1">dni_rg</th>
        <th class="span1">phone_rg</th>
        <th class="span1">date_birth</th>
        <th class="span1">email_sent</th>
    </tr>
    </thead>
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->name_rg }}</td>
        <td>{{ $p->lastname_rg }}</td>
        <td>{{ $p->email_rg }}</td>
        <td>{{ $p->dni_rg }}</td>
        <td>{{ $p->phone_rg }}</td>
        <td>{{ $p->date_birth }}</td>
        <td>{{ $p->email_sent }}</td>
    </tr>
    @endforeach
</table>
