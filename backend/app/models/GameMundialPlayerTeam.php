<?php

class GameMundialPlayerTeam extends Eloquent {

    protected $table = 'game_games_mundial_player_team';

    public function player() {
        return $this->belongsTo('GameMundialPlayer');
    }

}
