<?php

class AdminSystemUserController extends BaseController {

    private $viewpath = 'admin/system_user/';
    private $sessionk = 'session_user';

    public function index() {

        if (!SystemUser::has_perm('system-users-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $items = SystemUser::where('id', '>', 0);
        $total = $items->count();
        $items = $items->paginate(15);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                        )
        );
    }

    public function base() {
        return View::make($this->viewpath . __function__);
    }

    public function add() {
        if (!SystemUser::has_perm('system-users-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }
        $roles = Role::all();

        return View::make(
                        $this->viewpath . __function__, array('roles' => $roles
                        )
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('system-users-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = SystemUser::find($id);

        $id = Session::get('session_user');
        $user = SystemUser::where('id', $id)->first();
        
        if ($user->username != 'admin'){
        	
        	if($item->username == 'admin')
        	{
        		return Response::make(
        				'You are not authorized', '403');
        		
        	}
        }
        
        $roles = Role::all();
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                    'roles' => $roles
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('system-users-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = new SystemUser();
        $item->username = Input::get('username');
        $item->password = Input::get('password');
        $item->email = Input::get('email');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_system_user_add')
                            ->with('failure', $exception->getMessage());
        }

        return Redirect::route('admin_system_user')
                        ->with('success', sprintf(
                                        'User %s Created Successfully.', $item->title));
    }

    public function update($id) {

        if (!SystemUser::has_perm('system-users-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = SystemUser::find($id);
        $item->username = Input::get('username');
        $item->email = Input::get('email');
        $item->role_id = Input::get('role_id');
        $item->password = Input::get('password');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_system_user_edit', $id)
                            ->with('failure', $exception->getMessage());
        }

        return Redirect::route('admin_system_user')
                        ->with('success', 'User Created Successfully.');
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('system-users-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        SystemUser::find($id)->delete();
        return Redirect::route('admin_system_user')
                        ->with('success', 'SystemUser Deleted Successfully.');
    }

    public function login() {
        if (Session::get($this->sessionk)) {
            return Redirect::route('admin_base');
        }

        return View::make(
                        $this->viewpath . __function__
        );
    }

    public function logout() {
        Session::forget($this->sessionk);
        return Redirect::route('admin_login');
    }

    public function dologin() {
        $username = Input::get('username');
        $password = Input::get('password');

        $system_user = SystemUser::where('username', '=', $username)
                ->where('password', '=', $password)
                ->first();

        if ($system_user) {
            Session::put($this->sessionk, $system_user->id);
            return Redirect::route('admin_base');
        } else {
            return Redirect::back()
                            ->withInput()
                            ->with(array(
                                'failure' => 'Incorrect username o password'));
        }
    }

}
