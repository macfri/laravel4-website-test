<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSocialRed extends Migration {

	public function up()
	{
		Schema:: create('social_red', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('caption');
            $table->text('picture');
            $table->text('detail_link');
            $table->string('type', 20);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('social_red');
	}
}
