<?php
class SliderTableSeeder extends Seeder {

    public function run()
    {
        $file = Photo::create(array('title' => 'slider 1', 'path'  => 'upload/sliders/1.jpg'));
        DB::table('sliders')->insert(array(
            'title' => '¡Llénate de energía para ganar cada día!',
            'description' => 'El día está lleno de retos… 
                ¡Toma Milo todas las mañanas y la victoria será tuya!',
            'backgroung_color' => '#FFFFFF',
            'button_backgroung_color' => '#FFFFFF',
            'button_title' => 'Entra aquí',
            'link' => 'https://milo.no2.pe/website/spots-de-tv',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $file->id,
        ));

        $file = Photo::create(array('title' => 'slider 2', 'path'  => 'upload/sliders/2.jpg'));
        DB::table('sliders')->insert(array(
            'title' => '¡Una gran aventura te está esperando!',
            'description' => 'Mañana es un gran día... ¡Tu gran día!
                Demuestra que los retos no son un problema para ti en este divertido juego',
            'backgroung_color' => '#FFFFFF',
            'button_backgroung_color' => '#FFFFFF',
            'button_title' => '¡A jugar!',
            'link' => 'https://milo.no2.pe/website/juegos',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $file->id,
        ));

        $file = Photo::create(array('title' => 'slider 3', 'path'  => 'upload/sliders/3.jpg'));
        DB::table('sliders')->insert(array(
            'title' => 'Preparar un vaso de Milo es cosa seria...',
            'description' => '¿Crees tener lo necesario para preparar el vaso de Milo perfecto? 
                Confiamos en ti, ¡acepta el reto!',
            'backgroung_color' => '#FFFFFF',
            'button_backgroung_color' => '#FFFFFF',
            'button_title' => 'Vamos',
            'link' => 'https://www.facebook.com/MiloPeru/app_177029575788544',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $file->id,
        ));
    }
}
