<?php

class Point extends Eloquent {

    protected $table = 'game_points';
    protected $guarded = array('id');
    protected $fillable = array(
        'user_id',
        'score',
        'game_id',
    );

    public static function validate($input) {
        $rules = array(
            'username' => 'Required|Min:3|Max:80|Alpha|Unique:users',
            'first_name' => 'Required|Min:3|Max:80|Alpha',
            'last_name' => 'Required|Min:3|Max:80|Alpha',
            'email' => 'Required|Between:3,64|Email|Unique:users',
            'dni' => 'Required|Size:8|Unique:users',
            'phone' => 'Max:18',
            'password' => 'Required|AlphaNum|Between:4,8|Confirmed',
            'password_confirmation' => 'Required|AlphaNum|Between:4,8'
        );
        return Validator::make($input, $rules);
    }

}
