<?php

class MemeDefault extends Eloquent {

    protected $table = 'section_memes_default';

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public static function validate($input) {
        $rules = array(
            'photo' => 'mimes:jpeg,bmp,png'
        );
        return Validator::make($input, $rules);
    }

}
