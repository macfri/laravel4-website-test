$(document).one('ready', main);

function main(){
  var $nav_fixed = $('.nav-fixed');
  var nav_pos = $nav_fixed.position().top;
  var $window = $(window);

  $window.on('scroll', function() {
    if ($window.scrollTop() > nav_pos) {
      $nav_fixed.addClass('fixed');
    }
    else {
      $nav_fixed.removeClass('fixed');
    }
  });
}
