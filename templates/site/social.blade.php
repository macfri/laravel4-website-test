    <div id="fb-root"></div>
    <script type="text/javascript">
      function feedDialog(name, image, link){
        FB.ui({
          method: 'feed',
          name: 'Eres un digno rival... ¡Te reto a vencerme!',
          link: link,
          picture: 'https://www.milo.com.pe/static/site/images/share-games/' + image + '.jpg',
          description: 'Crees que tienes lo necesario para sacar más puntos que yo? ¡Pues inténtalo! Pero te advierto... ¡Nadie me gana jugando ' + name,
          message: ''
        });   
        return false;
      }
      
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId={{ Config::get('app.facebook')['client_id'] }}";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
      
      function popTwitter(name){
        var popWwidth = 570;
        var popHeight = 280;
        var left = (screen.width/2)-(popWwidth/2);
        var top = (screen.height/2)-(popHeight/2);
        var setText = '¡Soy invencible en el juego ' + name + '! Te reto a sacar más puntos que yo... Juega aquí: ' ;
        var setURL = "http://bit.ly/1bGxmLY";
        var url = encodeURI('https://twitter.com/intent/tweet?text='+setText+'&url=https://bit.ly/1bGxmLY');
        
        window.open(url, 'Twitter', "width="+popWwidth+",height="+popHeight+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+left+",top="+top+"");
      }
    </script>
