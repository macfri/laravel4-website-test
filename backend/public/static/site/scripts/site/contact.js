require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',

    'modal': '../modules/modal'
  },
  shim: {
    'jquery.scrolltop': ['jquery']
  }
});

require([
  'jquery',
  'modal'
], function ($, modal) {
  modal();
});
