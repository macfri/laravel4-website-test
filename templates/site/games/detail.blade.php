    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games_detail', $item->slug) }}">{{ $item->title }}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="game-detail">
          <div id="game-vars">
            <form>
              <input type="hidden" value="{{ $item->id }}" name="game_id">
              <input type="hidden" value="@if($token_rank){{$token_rank}} @else null @endif" name="token_rank" id="token_rank">
              <input type="hidden" value="" name="face_id" id="face_id">
            </form>
            <script>
              window.game = window.game || {};
              game.gamerank_url = "{{ URL::route('games_rank') }}";

            </script>
          </div>
          <div class="row">
            <div class="columns large-12 pt-27">@if(!Auth::check())
              <div class="alert-bar"><i class="icon-warning left"></i>
                <p>
                  ¡Espera! Estás jugando sin entrar a tu cuenta. Recuerda que si quieres grabar tus puntos,
                  recolectar “logros” y entrar a los sorteos que realicemos, debes estar conectado. Si aún no tienes cuenta,  <a href="#" data-reveal-id="register-modal" onclick="_gaq.push(['_trackEvent','Web-Conversion', 'Paso1-Iniciar', 'juegos-detalle'])">regístrate aquí</a>
                </p><a class="close-alert right"><i class="icon-close-game"></i></a>
              </div>@endif
              <div class="badget">
                <div class="left top-arrow-game"> <span>

                    SUPERA LOS RETOS<br>Y SUBE TU NIVEL</span></div>
                <h3 class="title-game">{{ $item->title }}</h3>
                <div class="cont-badget">@foreach($badges as $item2)<a class="left">
                    <div class="info-badge"><img src="{{ asset($item2->path) }}" class="left @if(!$item2->active) inactive @endif trophies-{{ $item2->id }}"><span class="left"><b>{{ $item2->title }}</b>
                        <p>{{ $item2->description }}</p></span></div></a>@endforeach
                  <div class="right top-detail-game">
                    <div>TU MAYOR PUNTAJE<b>{{ $max_score }}</b></div>
                  </div>
                </div>
              </div>
              <div class="@if($item->slug === 'tu-gran-dia') cont-game @else cont-game-full @endif">@if($item->slug === 'tu-gran-dia') @endif
                <div class="cont-messages js-game-modal hide">
                  <div class="int-msj-modal">
                    <a class="btn-register-foot btn-color-green js-trigger-register">registrate para participar</a>

                    <p class="line-middle">o</p>

                    <a class="btn-register-foot js-trigger-login">inicia sesión para participar</a>

                    <a href="{{ URL::route('games') }}" class="btn-register-foot">ver otros juegos</a>

                  </div>
                </div>{{ $game_content }}
              </div>@if($item->id==6)
              <div class="rd">R.D.Nº 4801-2013-ONAGI-DGAE</div>@endif
              @if($item->image_ctrl_id)
              <div class="bg-controls">
                <h3>controles</h3>
                <div class="int-controls"><img src="{{ asset($item->image_ctrl->path) }}"></div>
              </div>@endif
              <div class="rate-game">
                <div class="left">
                  <h3>rankea el juego</h3>
                  <ul class="star">
                    <li class="face-rank face1"></li>
                    <li class="face-rank face2"></li>
                    <li class="face-rank face3"></li>
                    <li class="face-rank face4"></li>
                    <li class="face-rank face5"></li>
                  </ul>
                </div>
                <div class="right social-icons">
                  <h3>reta <small>a TUS PATAS</small></h3>
                  <ul>
                    <li><a onclick="feedDialog('{{$item->title}}','{{ $item->slug }}','{{ URL::route('games_detail', $item->slug) }}'); _gaq.push(['_trackEvent','Web-Informativo', 'Compartir-Puntaje', ''])" class="shape-facebook"></a></li>
                    <li><a onclick="popTwitter('{{$item->title}}'); _gaq.push(['_trackEvent','Web-Informativo', 'Publicar-Puntaje', ''])" class="shape-twitter"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row ratings">
            <div class="columns large-6 cont-face">
              <h3>comenta</h3>
              <div data-colorscheme="light" data-width="380" data-href="{{ URL::route('games_detail', $item->slug) }}" class="fb-comments"></div>
            </div>
            <div class="columns large-6">
              <h3>mejores puntajes</h3>@foreach ($best_points as $key => $item)
              <div class="item @if($key%2 === 0)even @elseodd @endif">
                <ul>
                  <li class="number">#</li>
                  <li>
                    <div class="avatar-large">@if($item->user->picture) <img src="{{$item->user->picture}}"> @endif</div>
                  </li>
                  <li class="name">{{ $item->user->first_name }}</li>
                  <li class="points">{{ $item->max_score }}       </li>
                </ul>
              </div>@endforeach
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos-detalle'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/games') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
