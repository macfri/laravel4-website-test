@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>
  <!--  <li><a href="{{ URL::route('admin_user_add') }}">Create</a></li> -->

    <li><a href="{{ URL::route('admin_system_user_add') }}">Create</a></li>

</ul>



<table class="table table-striped table-bordered model-list">
    <thead>
        <!-- <th class="span1"><input type="checkbox" name="rowtoggle" class="action-rowtoggle" /></th> -->
        <th class="span1">&nbsp;</th>
        <th class="span1">username</th>
        <th class="span1">email</th>
        <th class="span1">created_at</th>
      <!--  <th class="span1">status</th> -->
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <!--<td><input type="checkbox" name="rowid" class="action-checkbox" value="{{ $p->id }}" /></td>-->
        <td>
<!--            <a class="icon" href="{{ URL::route('admin_system_user_edit', $p->id) }}"><i class="icon-pencil"></i></a> -->

            @if($p->username != 'admin')
            <form class="icon" method="POST" action="{{ URL::route('admin_system_user_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form>
            @endif


         <!--   <form class="icon" method="POST" action="{{ URL::route('admin_user_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form> -->
        </td>
         <td><a href="{{ URL::route('admin_system_user_edit', $p->id) }}">{{ $p->username }}</a></td> 
  <!--      <td><a href="#">{{ $p->username }}</a></td> -->
        <td>{{ $p->email }}</td>
        <td>{{ $p->created_at }}</td>

<!--
        <td>
            <i class="@if($p->status == 'inactive') text-error @else text-success @endif">
                {{ $p->status }}
            </i>
        </td>
-->

    </tr>
    @endforeach
    @else
    <tr><td colspan="6">No results</td></tr> 
    @endif
</table>



    

<?php echo $items->links(); ?>
@stop
