<?php

class ApiGameController extends BaseController {

    public function save() {

        if (!Request::ajax()) {
            return Response::make('You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        $token = Input::get('token', '');
        $trophies = Input::get('trophies', '');
        $level = (int) Input::get('level', 0);
        $game_id = (int) Input::get('game_id');
        $time_game = (int) Input::get('time_game');
        $score_level = (int) Input::get('score_level', 0);
        $score_total = (int) Input::get('score_total', 0);
        $event_id = (bool) Input::get('event_id');

        $games = Game::all();
        $games_ids = array();
        foreach ($games as $row) {
            $games_ids[] = $row->id;
        }

        if ($score_level < 1 or $score_total < 1 or ! in_array(
                        $game_id, $games_ids)) {
            return Response::make('Not Found', '404');
        }

        if (Auth::check()) {
            $user = Auth::user();

            if ($token != $user->token_game) {
                return Response::make('You are not authorized', '403');
            }

            $point = new Point();
            $point->user_id = $user->id;
            $point->score = $score_level;
            $point->game_id = $game_id;
            $point->winner = $event_id;

            try {
                $point->save();
                $is_save = True;
            } catch (Exception $exception) {
                $status_code = 1;
                Log::error($exception->getMessage());
            }

            if (isset($is_save)) {
                $player = Player::where('user_id', '=', $user->id)
                                ->where('game_id', '=', $game_id)->first();

                if (!$player) {
                    $player = new Player();
                    $player->game_id = $game_id;
                    $player->user_id = $user->id;
                    $player->max_score = $score_total;
                    $player->total_score = $score_total;
                    $player->was_guest = false;
                    $player->attempts = 1;
                    $player->trophies = '';
                    $player->is_new = 0;

                    if (is_array($trophies)) {
                        $player->trophies = implode(',', $trophies);
                    }

                    /*                     * ***************************************************** */

                    if (is_array($trophies)) {
                        $player_badge = PlayerBadge::where('game_id', $game_id)
                                ->where('user_id', $user->id);
                        if ($player_badge) {
                            $player_badge->delete();
                        }
                        $inser_data = array();
                        foreach ($trophies as $row) {
                            $player_badge = new PlayerBadge();
                            $player_badge->game_id = $game_id;
                            $player_badge->user_id = $user->id;
                            $player_badge->badge_id = $row;
                            $player_badge->save();
                        }
                    }
                    /*                     * ****************************************************** */

                    $message = 'new player';
                } else {

                    if ($score_total > $player->max_score) {
                        $player->max_score = $score_total;

                        if (is_array($trophies)) {
                            $player->trophies = implode(',', $trophies);
                        }
                    }

                    /*                     * ***************************************************** */

                    if (is_array($trophies)) {
                        $player_badge = PlayerBadge::where('game_id', $game_id)
                                ->where('user_id', $user->id);
                        if ($player_badge) {
                            $player_badge->delete();
                        }
                        $inser_data = array();
                        foreach ($trophies as $row) {
                            $player_badge = new PlayerBadge();
                            $player_badge->game_id = $game_id;
                            $player_badge->user_id = $user->id;
                            $player_badge->badge_id = $row;
                            $player_badge->save();
                        }
                    }
                    /*                     * ***************************************************** */

                    $player->total_score += $score_total;
                    $player->attempts += 1;
                    $message = 'update player';
                }

                try {
                    $player->save();
                    $is_player_save = true;
                } catch (Exception $exception) {
                    $status_code = 1;
                    Log::error($exception);
                    $message = $exception->getMessage();
                }

                if (isset($is_player_save)) {
                    $user->level_score = $player->max_score;

                    try {
                        $user->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        $message = $exception->getMessage();
                    }
                }
            }
        } else {

            Session::put('guest_token_post', $token);
            Session::put('guest_game_id', $game_id);
            Session::put('guest_score', $score_total);
            Session::put('guest_trophies', $trophies);
            Session::push('guest_points', array(
                'trophies' => $trophies,
                'level' => $level,
                'score' => $score_level,
                'time_game' => $time_game,
                'winner' => $event_id
                    )
            );
            $message = 'Login!';
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200);
    }

    function save_chulls() {

        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        $token = Input::get('token', '');
        $guess = Input::get('guess', '');
        $item1 = (int) Input::get('item1', '');
        $item2 = (int) Input::get('item2', '');
        $item3 = (int) Input::get('item3', '');
        $game_id = (int) Input::get('game_id');
        $lifetime = (int) Input::get('lifetime');
        $tries = (int) Input::get('tries');

        if (Auth::check()) {
            $user = Auth::user();

            if (is_null($user->dni) or $user->dni == '') {
                Log::info('complete regiser');
                $message = 'Complete Register!';

                Session::put('guest_token_post', $token);
                Session::put('guest_game_id', $game_id);
                Session::put('guest_game_item1', $item1);
                Session::put('guest_game_item2', $item2);
                Session::put('guest_game_item3', $item3);
                Session::put('guest_game_guess', $guess);
                Session::put('guest_game_tries', $tries);
                Session::put('guest_game_lifetime', $lifetime);
                Session::put('guest_game_chulls', 1);
                $message = 'Login!';
            } else {
                if ($token != $user->token_game) {
                    return Response::make(
                                    'You are not authorized', '403');
                }

                $point = GameChulls::where('user_id', $user->id)->first();

                if ($point) {
                    $point->item1 = $item1;
                    $point->item2 = $item2;
                    $point->item3 = $item3;
                    $point->guess = 1;
                    $point->tries = $tries;
                    $point->lifetime = $lifetime;
                    $point->enabled = true;

                    try {
                        $point->save();
                    } catch (Exception $exception) {
                        $status_code = 1;
                        Log::error($exception->getMessage());
                    }
                } else {
                    $message = 'not point';
                }
            }
        } else {
            Session::put('guest_token_post', $token);
            Session::put('guest_game_id', $game_id);
            Session::put('guest_game_item1', $item1);
            Session::put('guest_game_item2', $item2);
            Session::put('guest_game_item3', $item3);
            Session::put('guest_game_guess', $guess);
            Session::put('guest_game_tries', $tries);
            Session::put('guest_game_lifetime', $lifetime);
            Session::put('guest_game_chulls', 1);
            $message = 'Login!';
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    function save_chulls_start() {

        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        if (Auth::check()) {
            $token = Input::get('token', '');
            $user = Auth::user();

            if (is_null($user->dni) or $user->dni == '') {
                $message = 'Complete user data';
            } else {
                if ($token != $user->token_game) {
                    return Response::make(
                                    'You are not authorized', '403');
                }

                $point = GameChulls::where('user_id', $user->id)->first();

                if (!$point) {
                    $point = new GameChulls();
                    $point->user_id = $user->id;

                    try {
                        $point->save();
                    } catch (Exception $exception) {
                        $status_code = 1;
                        Log::error($exception->getMessage());
                    }
                } else {
                    $message = 'exists point';
                }
            }
        } else {
            $message = 'Login';
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    function save_chulls_choose() {

        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        if (Auth::check()) {

            $user = Auth::user();

            if (is_null($user->dni) or $user->dni == '') {
                $message = 'Complete user data';
            } else {
                $token = Input::get('token', '');
                $lifetime = Input::get('lifetime', '');

                if ($token != $user->token_game) {
                    return Response::make(
                                    'You are not authorized', '403');
                }

                $guess = Input::get('guess', '');
                $tries = Input::get('tries', '');
                $point = GameChulls::where('user_id', $user->id)->first();

                if ($point) {
                    $point->tries = $tries;
                    $point->guess = $guess;
                    $point->lifetime = $lifetime;

                    try {
                        $point->save();
                    } catch (Exception $exception) {
                        $status_code = 1;
                        Log::error($exception->getMessage());
                    }
                } else {
                    $message = 'not point';
                }
            }
        } else {
            $message = 'Login';
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    function save_chulls_share() {

        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        if (Auth::check()) {

            $user = Auth::user();

            if (is_null($user->dni) or $user->dni == '') {
                $message = 'Complete user data';
            } else {
                $token = Input::get('token', '');

                if ($token != $user->token_game) {
                    return Response::make(
                                    'You are not authorized', '403');
                }

                $point = GameChulls::where('user_id', $user->id)->first();

                if ($point) {
                    $point->share = 1;
                    try {
                        $point->save();
                    } catch (Exception $exception) {
                        $status_code = 1;
                        Log::error($exception->getMessage());
                    }
                } else {
                    $message = 'not point';
                }
            }
        } else {
            $message = 'Login';
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    function save_round() {

        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $message = null;
        $status_code = 0;

        $token = Input::get('token', '');
        $round = (int) Input::get('round');
        $lUser = (int) Input::get('lUser');
        $lEnemy = (int) Input::get('lEnemy');
        $totalScore = (int) Input::get('totalScore');
        $roundScore = (int) Input::get('roundScore');
        $kos = (int) Input::get('kos');
        $koP = (int) Input::get('koP');
        $spe = (int) Input::get('spe');
        $multi = (int) Input::get('multi');
        $hEnemy = (int) Input::get('hEnemy');
        $hEnemyBlock = (int) Input::get('hEnemyBlock');
        $bodyBlock = (int) Input::get('bodyBlock');
        $bodyAccert = (int) Input::get('bodyAccert');
        $bodyGiven = (int) Input::get('bodyGiven');
        $headAccert = (int) Input::get('headAccert');
        $headBlock = (int) Input::get('headBlock');
        $headGiven = (int) Input::get('headGiven');
        $lifetime = (int) Input::get('lifetime');

        if (in_range() == 0) {
            $message = 'block_time';
            $status_code = 1;
            if (Config::get('app.round_block_time')) {
                $status_code = 1;
            } else {
                $status_code = 0;
            }
        } else if ($totalScore < 1 or $totalScore < 1 or $totalScore > 210000) {
            $message = 'score-negative';
            $status_code = 1;
        } else if (!in_array($multi, array(5, 3, 2, 1))) {
            $message = 'not-multi';
            $status_code = 1;
        } else if (!in_array($round, array(1, 2, 3))) {
            $message = 'not-round';
            $status_code = 1;
        } else if (!round_formula($totalScore, $roundScore, $koP, $spe, $multi, $round, $lUser)) {
            $message = 'bad-formula';
            $status_code = 1;
        }

        if ($status_code == 0) {
            if (Auth::check()) {
                $user = Auth::user();
                $dni_is_null = is_null($user->dni) or ( $user->dni == '');
            } else {
                $dni_is_null = true;
            }

            if ($dni_is_null) {
                Session::put('guest_token_post', $token);
                Session::put('guest_game_round', 1);
                Session::put('guest_game_round_totalScore', $totalScore);
                Session::put('guest_game_round_lifetime', $lifetime);
                Session::put('guest_game_round_round', $round);
                Session::put('guest_game_round_lUser', $lUser);
                Session::put('guest_game_round_lEnemy', $lEnemy);
                Session::put('guest_game_round_roundScore', $roundScore);
                Session::put('guest_game_round_kos', $kos);
                Session::put('guest_game_round_koP', $koP);
                Session::put('guest_game_round_spe', $spe);
                Session::put('guest_game_round_multi', $multi);
                Session::put('guest_game_round_hEnemy', $hEnemy);
                Session::put('guest_game_round_hEnemyBlock', $hEnemyBlock);
                Session::put('guest_game_round_bodyBlock', $bodyBlock);
                Session::put('guest_game_round_bodyAccert', $bodyAccert);
                Session::put('guest_game_round_bodyGiven', $bodyGiven);
                Session::put('guest_game_round_headAccert', $headAccert);
                Session::put('guest_game_round_headBlock', $headBlock);
                Session::put('guest_game_round_headGiven', $headGiven);
                $message = 'Login Round!';
            } else {
                if ($token != $user->token_game) {
                    return Response::make(
                                    'You are not authorized', '403');
                }

                $point = GameRound::where('user_id', $user->id)
                        ->where('week', 4)
                        ->first();

                if (!$point) {
                    $point = new GameRound();
                    $point->user_id = $user->id;
                    $point->full_name = $user->first_name . ' ' . $user->last_name;
                    $point->picture = $user->picture;
                    $point->score = $totalScore;
                    $point->tries = 1;
                    $point->week = 4;
                } else {
                    if ($totalScore > $point->score) {
                        $point->score = $totalScore;
                    }
                    $point->tries += 1;
                }

                $point->multi = $multi;
                $point->lifetime = $lifetime;
                try {
                    $point->save();
                    $message = 'save round';
                } catch (Exception $exception) {
                    $message = $exception->getMessage();
                    Log::error($message);
                }

                $point2 = new GameRoundDetail();
                $point2->user_id = $user->id;
                $point2->totalScore = $totalScore;
                $point2->lifetime = $lifetime;
                $point2->round = $round;
                $point2->lUser = $lUser;
                $point2->lEnemy = $lEnemy;
                $point2->roundScore = $roundScore;
                $point2->kos = $kos;
                $point2->koP = $koP;
                $point2->spe = $spe;
                $point2->multi = $multi;
                $point2->hEnemy = $hEnemy;
                $point2->hEnemyBlock = $hEnemyBlock;
                $point2->bodyBlock = $bodyBlock;
                $point2->bodyAccert = $bodyAccert;
                $point2->bodyGiven = $bodyGiven;
                $point2->headAccert = $headAccert;
                $point2->headBlock = $headBlock;
                $point2->headGiven = $headGiven;
                $point2->user_type = Session::get('user_type');

                try {
                    $point2->save();
                    $message = 'save round';
                } catch (Exception $exception) {
                    $message = $exception->getMessage();
                    Log::error($message);
                }
            }
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    function round_ranking() {

        $users = GameRound::select('full_name', 'score', 'picture')
                ->where('week', 4)
                ->orderby('score', 'desc')
                ->take(10)
                ->get();

        $_users = array();
        foreach ($users as $k) {
            $_users[] = array(
                'name' => $k->full_name,
                'score' => $k->score,
                'picture' => $k->picture
            );
        }

        $user = array();
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $sql = sprintf('SELECT full_name, rank, score FROM (
                SELECT t.user_id, t.score, t.full_name, 
                    @rownum := @rownum + 1 AS rank
                FROM website_game_games_round t, 
                    (SELECT @rownum := 0) r where week=%d ORDER BY score DESC
            ) as t2 WHERE user_id=%d', $user_id, 4);
            $result = DB::select($sql);
            if (count($result) > 0) {
                $row = $result[0];
                $user['full_name'] = $row->full_name;
                $user['rank'] = $row->rank;
                $user['score'] = $row->score;
            }
        }

        return Response::json(array(
                    'users' => $_users,
                    'user' => $user), 200
        );
    }

    function round_get_multi() {

        $in_range = in_range();
        return response::json(array(
                    'multi' => $in_range), 200
        );
    }

    function mundial_api_save_undershirt() {

        function get_tshirt_image($fb_id, $type) {
            return asset(sprintf(
                            '%s/%s/%s/%s/%s', 'upload', 'games', 'mundial', $fb_id, $type
            ));
        }

        function __create_thumbnail($stream, $filename, $id, $ext = 'jpg') {
            $upload_path = sprintf(
                    '%s/%s/%s/%s/%s', public_path(), 'upload', 'games', 'mundial', $id
            );

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            $success = file_put_contents(sprintf(
                            '%s/%s.%s', $upload_path, $filename, $ext), get_img_base64($stream));
            return $success;
        }

        if (!Auth::check()) {
            Log::info('Mundial: no-auth');
            return Response::make(
                            'You are not authorized', '403');
        }

        $user = Auth::user();
        $user_fb_id = $user->facebook->fb_id;

        $message = null;
        $status_code = 0;
        $picture = null;
        $used_ids = null;
        $mundial_token = Input::get('mundial_token', '');

        $mundial_recipients = Input::get('mundial_recipients', '');
        $mundial_team_name = Input::get('mundial_team_name', '');
        $mundial_tshirt_name = Input::get('mundial_tshirt_name', '');
        $mundial_tshirt_number = (int) Input::get('mundial_tshirt_number', 0);
        $mundial_tshirt_image_back = Input::get('mundial_tshirt_image_back', '');
        $mundial_tshirt_image_front = Input::get('mundial_tshirt_image_front', '');
        $mundial_tshirt_image_thumb = Input::get('mundial_tshirt_image_thumb', '');
        $mundial_tshirt_image_share = Input::get('mundial_tshirt_image_share', '');

        if ($mundial_token != Session::get('mundial_token')) {
            Log::info('Mundial: no-tokenr');
            return Response::make(
                            'You are not authorized', '403');
        }

        if (strlen($mundial_tshirt_name) < 1 or strlen($mundial_tshirt_name) > 20) {
            $status_code = 1;
            $message = 'La longitud del nombre de la camiseta es incorrecta, Intente otro por favor.';
        } else {
            if ($mundial_tshirt_number < 1 or $mundial_tshirt_number > 99) {
                $status_code = 1;
                $message = 'La número de la camiseta es incorrecta, Intente otro por favor.';
            } else {
                if (strlen($mundial_team_name) < 1 or strlen($mundial_team_name) > 20) {
                    $status_code = 1;
                    $message = 'La longitud del nombre de el equipo es incorrecta, Intente otro por favor.';
                } else {
                    if (strlen($mundial_tshirt_image_back) < 1 or strlen($mundial_tshirt_image_front) < 1) {
                        $status_code = 1;
                        $message = 'La Imagen es requerida.';
                    } else {
                        if (strlen($mundial_recipients) < 1) {
                            $status_code = 1;
                            $message = 'Seleccione amigos por favor.';
                        } else {
                            $mundial_recipients = explode(',', $mundial_recipients);
                            $used_ids = '';
                            foreach ($mundial_recipients as $row) {
                                if (GameMundialPlayerTeam::where('fb_id_to', '=', $row)->count()) {
                                    $used_ids .= $row . ',';
                                }
                            }
                            if (strlen($used_ids) > 0) {
                                $status_code = 1;
                                $message = 'invitados usados';
                                $used_ids = substr($used_ids, 0, strlen($used_ids) - 1);
                            }
                        }
                    }
                }
            }
        }

        if ($status_code == 0) {
            $team_exists = GameMundialTeam::where('name', $mundial_team_name)->first();
            if ($team_exists) {
                $status_code = 1;
                $message = 'El nombre del equipo ya existe, Intente otro por favor.';
            } else {
                $player_exists = GameMundialPlayer::where('fb_id', $user_fb_id)->first();
                if ($player_exists) {
                    $status_code = 1;
                    $message = 'Este usuario ya creo un equipo.';
                } else {

                    $sucess_image_back = __create_thumbnail($mundial_tshirt_image_back, 'back', $user_fb_id, 'jpg'
                    );

                    $sucess_image_front = __create_thumbnail($mundial_tshirt_image_front, 'front', $user_fb_id, 'png'
                    );

                    $sucess_image_share = __create_thumbnail($mundial_tshirt_image_share, 'front_share', $user_fb_id, 'jpg'
                    );

                    if (!$sucess_image_back or ! $sucess_image_front) {
                        $message = 'Ha ocurrido un error, por favor intente de nuevo más tarde.';
                        Log::error($message);
                        $status_code = 1;
                    } else {
                        $upload_path = sprintf(
                                '%s/%s/%s/%s/%s', public_path(), 'upload', 'games', 'mundial', $user_fb_id
                        );

                        $source_filename = sprintf('%s/front.png', $upload_path);
                        $source_filename_dest = sprintf('%s/front_thumb.png', $upload_path);
                        resize($source_filename, 108, 130, $source_filename_dest);

                        $team = new GameMundialTeam();
                        $team->name = $mundial_team_name;
                        $team->enabled = 1;
                        try {
                            $team->save();
                            $is_save_team = true;
                        } catch (Exception $exception) {
                            Log::error($exception->getMessage());
                            $status_code = 1;
                            $message = 'Ha ocurrido un error, por favor intente de nuevo más tarde.1';
                        }

                        if (isset($is_save_team)) {
                            $player = new GameMundialPlayer();
                            $player->tshirt_name = $mundial_tshirt_name;
                            $player->tshirt_number = $mundial_tshirt_number;
                            $player->team_id = $team->id;
                            $player->fb_id = $user_fb_id;
                            $player->tshirt_path_front = get_tshirt_image($user_fb_id, 'front');
                            $player->tshirt_path_front_ext = '.png';
                            $player->tshirt_path_back = get_tshirt_image($user_fb_id, 'back');
                            $player->tshirt_path_back_ext = '.jpg';
                            $player->save();

                            try {
                                $player->save();
                                $is_save_player = true;
                            } catch (Exception $exception) {
                                Log::error($exception);
                                $status_code = 1;
                                $message = 'Ha ocurrido un error, por favor intente de nuevo más tarde.2';
                            }

                            if (isset($is_save_player)) {
                                $total_players = 0;
                                foreach ($mundial_recipients as $fb_id_to) {
                                    $player_team = new GameMundialPlayerTeam();
                                    $player_team->player_id = $player->id;
                                    $player_team->fb_id_to = $fb_id_to;

                                    try {
                                        $player_team->save();
                                        $total_players += 1;
                                    } catch (Exception $exception) {
                                        Log::error($exception);
                                    }
                                }

                                if ($total_players < 1) {
                                    $message = 'Ha ocurrido un error, por favor intente de nuevo más tarde.3';
                                    Log::error($message);
                                    $status_code = 1;
                                } else {
                                    $picture = asset($player->tshirt_path_front . '_share.jpg');
                                }
                            }
                        }
                    }
                }
            }
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message, 'picture' => $picture, 'used_ids' => $used_ids), 200
        );
    }

    function mundial_api_get_team_from_friend() {

        $fb_id = Input::get('fb_id');
        if (!$fb_id) {
            return Response::make(
                            'You are not authorized1', '403');
        }

        $_player_team = GameMundialPlayerTeam::where('fb_id_to', $fb_id);

        $player_team1 = $_player_team->first();
        if (!$player_team1) {
            return Response::make(
                            'You are not authorized2', '403');
        }

        $player = GameMundialPlayer::where('id', $player_team1->player_id)->first();
        if (!$player) {
            return Response::make(
                            'You are not authorized3', '403');
        }

        $facebook_user_invited = FacebookUser::where('fb_id', $player->fb_id)->first();
        if (!$facebook_user_invited) {
            return Response::make(
                            'You are not authorized4', '403');
        }

        $user = User::where('facebook_id', $facebook_user_invited->id)->first();
        if (!$user) {
            return Response::make(
                            'You are not authorized5', '403');
        }

        $player_team = $_player_team->get();
        if (!$player_team) {
            return Response::make(
                            'You are not authorized6', '403');
        }

        $data['team_name'] = $player->team->name;
        $data['tshirt_name'] = $player->tshirt_name;
        $data['tshirt_image'] = $player->tshirt_path_front . '.png';
        $data['first_name'] = $user->first_name . ' ' . $user->last_name;

        $player_id = $player->id;
        $ids_friends = GameMundialPlayerTeam::where('player_id', $player_id)->get();
        $friends_ids = '';
        foreach ($ids_friends as $row) {
            $friends_ids .= $row->fb_id_to . ',';
        }
        $friends_ids.=$player->fb_id;
        $data['friends_ids'] = $friends_ids;
        return Response::json($data, 200);
    }

    function mundial_api_bad_words() {

        $data = GameMundialBadword::get(array('name'))->toArray();
        return Response::json($data, 200);
    }

    public function save_facebook_user() {

        if (!Request::ajax()) {
            return Response::make('You are not authorized', '403');
        }

        $day = Input::get('day');
        $month = Input::get('month');
        $year = Input::get('year');
        $reply_email = Input::get('reply_email') == null ? 0 : 1;
        $authorize_father = Input::get('authorize_father') == null ? 0 : 1;

        Input::merge(array(
            'date_birth' => sprintf('%s-%s-%s', $year, $month, $day),
            'reply_email' => $reply_email,
            'authorize_father' => $authorize_father
        ));

        $v = User::validate_simple_facebook(Input::all());

        if (!Session::get('token_facebook_user')) {
            return Response::json([
                        'success' => false,
                        'errors' => array('csrf' => 'invalid token')]);
        }

        if (!$v->passes()) {
            return Response::json([
                        'success' => false,
                        'errors' => $v->errors()->toArray()]);
        }

        $user = Auth::user();
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->dni = Input::get('dni');
        $user->phone = Input::get('phone');
        $user->authorize_father = Input::get('authorize_father');
        $user->reply_email = Input::get('reply_email');
        $user->date_birth = Input::get('date_birth');
        $user->is_lima = (bool) Input::get('is_lima');

        try {
            $user->save();
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return Response::json([
                        'success' => false,
                        'errors' => array(
                            'database' => 'Intenta de nuevo porfavor')]);
        }
        return Response::json(['success' => true, 'errors' => []]);
    }

}
