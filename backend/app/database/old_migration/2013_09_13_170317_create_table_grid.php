<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGrid extends Migration {

	public function up()
    {
		Schema::create('grid', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('url', 500);
            $table->string('w', 10);
            $table->string('h', 10);
            $table->string('section_name', 20);
            $table->integer('section_id');
            $table->string('detail_link', 100);
			$table->timestamps();
        });
	}

	public function down()
    {
        Schema::dropIfExists('grid');
	}
}
