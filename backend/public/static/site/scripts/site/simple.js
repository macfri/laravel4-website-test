require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',

    'modal': '../modules/modal',
    'auth': '../modules/auth',
    'register': '../modules/register',
  },
  shim: {
    'modal': ['foundation'],
    'jquery.scrolltop': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'auth',
  'register'
], function ($, modal, auth, register) {
  modal();
  auth($('#login'));
  register($('#register'));
});
