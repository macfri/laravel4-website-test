<?php

use Illuminate\Database\Migrations\Migration;

class UpdateOrderSlider extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        Schema::table('sliders', function($t) {
                $t->integer('order');
        });



		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
    {

       Schema::table('sliders', function($t) {
                $t->dropColumn('order');
       });

		//
	}

}
