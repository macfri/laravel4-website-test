@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif


<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'List Role')</legend>
</fieldset>

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>
  <!--  <li><a href="{{ URL::route('admin_user_add') }}">Create</a></li> -->

    <li><a href="{{ URL::route('admin_role_add') }}">Create</a></li>

</ul>



<table class="table table-striped table-bordered model-list">
    <thead>
        <!-- <th class="span1"><input type="checkbox" name="rowtoggle" class="action-rowtoggle" /></th> -->
        <th class="span1">&nbsp;</th>
        <th class="span1">name</th>
        <th class="span1">created_at</th>
      <!--  <th class="span1">status</th> -->
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <!--<td><input type="checkbox" name="rowid" class="action-checkbox" value="{{ $p->id }}" /></td>-->
        <td>
<!--            <a class="icon" href="{{ URL::route('admin_system_user_edit', $p->id) }}"><i class="icon-pencil"></i></a> -->

            <form class="icon" method="POST" action="{{ URL::route('admin_role_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form>


         <!--   <form class="icon" method="POST" action="{{ URL::route('admin_user_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form> -->
        </td>
        <td><a href="{{ URL::route('admin_role_edit', $p->id) }}">{{ $p->name }}</a></td>
        <td>{{ $p->created_at }}</td>

<!--
        <td>
            <i class="@if($p->status == 'inactive') text-error @else text-success @endif">
                {{ $p->status }}
            </i>
        </td>
-->

    </tr>
    @endforeach
    @else
    <tr><td colspan="6">No results</td></tr> 
    @endif
</table>



    

<?php echo $items->links(); ?>
@stop
