<?php
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFiles extends Migration {

	public function up()
    {
		Schema::create('files', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->integer('size')->nullable();
            $table->text('description');
            $table->string('extension', 5)->nullable();
            $table->string('original_name', 100)->nullable();
            $table->string('mime', 100)->nullable();
            $table->string('path', 300)->nullable();
			$table->timestamps();
        });
	}

	public function down()
    {
        Schema::dropIfExists('files');
	}
}
