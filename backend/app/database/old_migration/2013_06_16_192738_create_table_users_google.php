<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersGoogle extends Migration {

	public function up()
    {
		Schema::create('users_google', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('gg_id', 100)->nullable()->unique();
            $table->string('gg_email', 100)->nullable();
            $table->string('gg_gender', 100)->nullable();
            $table->string('gg_first_name', 100)->nullable();
            $table->string('gg_last_name', 100)->nullable();
            $table->string('gg_oauth_token', 100)->nullable();
            $table->string('gg_expires', 100)->nullable();
            $table->timestamps();

        });
	}

	public function down()
    {
        Schema::dropIfExists('google');
	}
}
