    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games_detail', $item->slug) }}">{{ $item->title }}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="game-content">
          <div id="game-vars">
            <form>
              <input type="hidden" value="{{ $item->id }}" name="game_id">
              <input type="hidden" value="@if($token_rank){{$token_rank}} @else null @endif" name="token_rank" id="token_rank">
              <input type="hidden" value="" name="face_id" id="face_id">
            </form>
            <script>
              window.game = window.game || {};
              game.gamerank_url = "{{ URL::route('games_rank') }}";

            </script>
          </div>
          <div class="row">
            <div class="columns large-12 pt-27">
    
              <div class="@if($item->slug === 'tu-gran-dia') cont-game @else cont-game-full @endif">@if($item->slug === 'tu-gran-dia')@endif
                <div class="cont-messages js-game-modal hide">
                  <div class="int-msj-modal chulls-modal">
                    <div class="chulls-stick"></div>
                    <div class="chulls-date"></div>
                    <a class="btn-register-foot btn-color-green js-trigger-register">registrate para participar</a>

                    <p class="line-middle">o</p>

                    <a class="btn-register-foot js-trigger-login">inicia sesión para participar</a>

                    <a href="{{ URL::route('games') }}" class="btn-register-foot">ver otros juegos</a>
                  </div>
                </div>{{ $game_content }}
              </div>@if($item->id==6)
              <div class="rd">R.D.Nº 4801-2013-ONAGI-DGAE</div>@endif
              @if($item->image_ctrl_id)
              <div class="bg-controls">
                <h3>controles</h3>
                <div class="int-controls"><img src="{{ asset($item->image_ctrl->path) }}"></div>
              </div>@endif
              <div class="rate-game custom-game">
                <div class="left">
                  <h3>rankea el juego</h3>
                  <ul class="star">
                    <li class="face-rank face1"></li>
                    <li class="face-rank face2"></li>
                    <li class="face-rank face3"></li>
                    <li class="face-rank face4"></li>
                    <li class="face-rank face5"></li>
                  </ul>
                </div>
                <div class="right social-icons">
                  <h3>Comparte <small>a TUS PATAS</small></h3>
                  <ul>
                    <li><a onclick="onShareCucharitas(); _gaq.push(['_trackEvent','Web-Informativo', 'Compartir-Puntaje', ''])" class="shape-facebook"></a></li>
                    <li><a target="_blank" href="https://twitter.com/intent/tweet?url={{ URL::route('games_detail', $item->slug) }}&text=Rec%C3%A1rgate%20con%20MILO%20y%20demuestra%20%20tu%20energ%C3%ADa%20en%20el%20v%C3%B3ley%20o%20en%20el%20f%C3%BAtbol.%20%C2%A1T%C3%BA%20eliges!" onclick="_gaq.push(['_trackEvent','Web-Informativo', 'Publicar-Puntaje', ''])" class="shape-twitter"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row ratings comenta-12">
            <div class="columns large-12 cont-face">
              <h3>comenta</h3>
              <div data-colorscheme="light" data-width="750" data-href="{{ URL::route('games_detail', $item->slug) }}" class="fb-comments"></div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>

      </div>

      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos-detalle'))
    @include('site.register-model')
<script data-main="{{ asset('static/site/scripts/site/games') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>


  </body>
</html>
