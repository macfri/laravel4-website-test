  <div class="nav-side">
    <div class="inner row">
      <div class="columns small-10">
        <ul>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-inicio', '{{ $section }}'])">Inicio</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('history') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-historia', '{{ $section }}'])">Historia</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('products') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-productos', '{{ $section }}'])">Productos</a></li>
          <li></li>
          <li><span class="sep"></span></li>
          <li><a href="{{ URL::route('spots') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Ver-spots-de-tv', '{{ $section }}'])">Spots de TV</a></li>
          <li></li>
        </ul>
      </div>
      <div class="columns small-2">
        <div class="social-icons right"><a href="https://www.facebook.com/MiloPeru" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-FP',''])" class="icon-facebook"></a>
          <div class="div"></div><a href="http://www.youtube.com/channel/UC054ysyX5RH2FF7UHKIcXKw/feed" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-YT',''])" class="icon-youtube"></a>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-main">
    <div class="inner row">
      <div class="columns large-8 patt-left"> <a href="{{ URL::route('home') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Volver-Home','{{ $section }}'])" class="logo">
          <h1>Milo</h1></a>
        <ul>
          <li><a href="{{ URL::route('games') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-juegos', '{{ $section }}'])">Juegos</a></li>
          <li><a href="{{ URL::route('events') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-eventos', '{{ $section }}'])">Eventos</a></li>
           <li><a href="{{ URL::route('freetime') }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-meme', '{{ $section }}'])">Pasa el rato</a></li>
        </ul>
      </div>
      <div class="columns large-4 patt-right">@if(Auth::check())
        <div class="login-user">
          <div class="left">
            <div class="avatar-small"><a href="{{ URL::route('profile') }}">@if(Auth::user()->picture)<img src="{{ Auth::user()->picture}}">@endif</a></div>
          </div>
          <div class="level left"><small>Nivel</small><span>{{ Auth::user()->level()['level'] }}</span></div>
          <div class="status-bar left">
            <h3>{{ Auth::user()->first_name }}</h3>
            <div><span style="width:{{ Auth::user()->level()['pc'] }}%"></span></div>
          </div>
          <div class="left"><a href="{{ URL::route('user_logout') }}" class="btn-logout"><i class="icon-close-sesion"></i></a></div>
        </div>@else<a href="#" data-reveal-id="register-modal" onclick="_gaq.push(['_trackEvent','Web-Conversion', 'Paso1-Iniciar', '{{ $section }}'])" class="btn-register">Regístrate</a><a href="#" data-reveal-id="login-modal" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar', ''])" class="btn-login js-login">Inicia Sesión</a>@endif 
      </div>
    </div>
  </div>
