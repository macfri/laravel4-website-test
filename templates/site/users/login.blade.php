    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.users.menu', array('section'=>''))
        <!-- MENU -->
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div id="login-page">
          <div class="row modal-session myform">
            <div class="columns large-12 large-centered">
                <form id="login" action="{{ URL::route('user_dologin') }}" method="post" autocomplete="off">

               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

                <a class="close-modal close-reveal-modal">x</a>
                <h3>Inicia Sesión</h3>
                <h4>Conéctate con una red social</h4>
                <div class="row">
                  <div class="columns large-6"><a href="{{ URL::route('user_login_facebook') }}" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-Fcbk', ''])" class="btn-facebook">Conéctate con Facebook</a></div>
                  <div class="columns large-6"><a href="{{ URL::route('user_login_google') }}" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-Google', ''])" class="btn-google">Conéctate con Google</a></div>
                </div>
                <div class="row pad-left-15"><small>Inicia sesión con tu usuario y tu contraseña</small>

                   @if($errors->has())
                        @foreach($errors->all() as $m)
                        <p class="alert">{{ $m }}</p>
                        @endforeach
                    @endif

                </div>
                <div class="row">
                  <div class="columns">
                    <label for="email">Correo</label>
                    <input type="text" name="email" id="email">
                    <p class="alert hide">* Correo electrónico y contraseña incorrectos.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="columns">
                    <label for="password">Contraseña</label>
                    <input type="password" name="password" id="password">
                    <p class="alert hide">* Correo electrónico y contraseña incorrectos.</p>
                  </div>
                </div>

                @if($show_captcha)
                <div class="row">
                  <div class="columns">
                    <label for="password">&nbsp;</label>
                   {{ Form::recaptcha(array('use_ssl'=>true)) }}
                  </div>
                </div>
                @endif

                <br /><br /><br /><br /><br /><br />

                <div class="row">
                  <div class="columns pad-top-25">
                    <button type="submit" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-cuenta', ''])" class="btn-session">Inicia Sesión</button><a href="{{ URL::route('user_forgot_password') }}" class="link-bottom">Olvide mi contraseña</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>''))
        <!-- FOOTER -->
      </div>
    </div>
    <script src="{{ asset('static/site/scripts/libs/require.js') }}" data-main="{{ asset('static/site/scripts/site/login') }}"></script>
  </body>
</html>
