<?php
class GamesBadgeTableSeeder extends Seeder {

    public function run()
    {
        /* Payaso */
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/1001.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 1001,
            'title'     => 'Un gran baile',
            'description' => 'Termina el juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 1,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/1002.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 1002,
            'title'     => 'Bailarín innato',
            'description' => 'Haz un combo',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 1,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/1003.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 1003,
            'title'     => '¡Buen ritmo!',
            'description' => 'Haz 7000 puntos',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 1,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/1004.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 1004,
            'title'     => 'Este baile es tuyo',
            'description' => 'Haz 4 combos en un solo juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 1,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/1005.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 1005,
            'title'     => 'Rey de la pista',
            'description' => 'Termina sin perder ni una vida',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 1,
            'badge'     => 'oro',
            'points'     => 500
        ));

       /* Fantasmas */
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/2001.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 2001,
            'title'     => 'No tengo miedo…',
            'description' => 'Termina el juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 2,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/2002.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 2002,
            'title'     => 'El cerrajero',
            'description' => 'Encuentra todas las llaves',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 2,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/2003.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 2003,
            'title'     => 'Caza-Fantasmas',
            'description' => 'Encuentra a todos los fantasmas',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 2,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/2004.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 2004,
            'title'     => '¡Mira qué brillante!',
            'description' => 'Encuentra todos los cofres de tesoro',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 2,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/2005.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 2005,
            'title'     => '¡ A mí con fantasmas!',
            'description' => 'Terminar sin perder ni una vida',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 2,
            'badge'     => 'oro',
            'points'     => 500
        ));


        /* Mecanico */
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/3001.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 3001,
            'title'     => '¡Problema resuelto!',
            'description' => 'Terminar el juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 3,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/3002.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 3002,
            'title'     => '“Pedalear” es mi segundo nombre',
            'description' => 'Supera el primer nivel sin caer”',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 3,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/3003.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 3003,
            'title'     => '¡Más velocidad!',
            'description' => 'Alcanzar 6500 puntos',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 3,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/3004.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 3004,
            'title'     => 'Dígame ingeniero',
            'description' => 'Terminar el juego con 3 vidas o más',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 3,
            'badge'     => 'oro',
            'points'     => 500
        ));


        /* Acuario */
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/4001.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 4001,
            'title'     => '¡Liberen a las focas!',
            'description' => 'Terminar el juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 4,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/4002.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 4002,
            'title'     => 'Glug, glug…',
            'description' => 'Descender 500 metros',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 4,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/4003.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 4003,
            'title'     => 'Como un tiburón',
            'description' => 'Terminar con 400,000 puntos o más',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 4,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/4004.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 4004,
            'title'     => 'Dulces profundidades',
            'description' => 'Atrapa todas las latas del primer nivel',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 4,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/4005.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 4005,
            'title'     => '¿Obstáculos? ¿Dónde?',
            'description' => 'Terminar el juego sin sufrir daños',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 4,
            'badge'     => 'oro',
            'points'     => 500
        ));


        /* Disparos  */
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/5001.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 5001,
            'title'     => '¡Sabía que ganaría!',
            'description' => 'Termina el juego',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 5,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/5002.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 5002,
            'title'     => 'Buena concentración',
            'description' => 'Dispara a un target MILO',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 5,
            'badge'     => 'bronce',
            'points'     => 50
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/5003.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 5003,
            'title'     => 'No me pintes!',
            'description' => 'Cúbrete 3 veces con el escudo',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 5,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/5004.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 5004,
            'title'     => '¡A pintar!',
            'description' => 'Consigue 100,000 puntos',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 5,
            'badge'     => 'plata',
            'points'     => 150
        ));
        $file = Photo::create(array('title' => 'badge 1', 'path'  => 'upload/badges/5005.jpg'));
        DB::table('game_badges')->insert(array(
            'id'        => 5005,
            'title'     => 'Todo lo veo MILO',
            'description' => 'Dispara a todos los targets MILO',
            'status'    => 'active',
            'author_id' => 1,
            'image_id'  => $file->id,
            'game_id'   => 5,
            'badge'     => 'oro',
            'points'     => 500
        ));

    }
}
