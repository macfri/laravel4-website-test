define([
  'jquery',
  'foundation'
], function($){
  function modal(){
    $(document).foundation('reveal',{
      open: function(){
        $('body').css('overflow', 'hidden');
      },
      close:function(){
        $('body').css('overflow', 'auto');
        var fr = $(this).children().find('form').attr("id");
        if (fr !== undefined) {
          var id = $(this).children().find('form').attr("id");
          $('#'+id).validate().resetForm();
        }
      },
      closed:function(){
        $('body').css('overflow', 'auto');
      }
    });
  }
  return modal;
});
