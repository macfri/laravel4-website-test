<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSpots extends Migration {

	public function up()
    {
		Schema::create('spots', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('slug', 100)->index();
            $table->text('description');
            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');

            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
            $table->string('code');
			$table->timestamps();
        });

	}

	public function down()
    {
        Schema::dropIfExists('sposts');
	}
}
