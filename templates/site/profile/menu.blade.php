    <img src="{{ asset('static/site/images/background-profile/') }}/cover-{{ Auth::user()->cover_id }}.jpg" class="bg-profile">
        
    <div class="description-banner">

        <a class="btn-edit-picture"><div class="icon-picture"></div></a>
        <div class="photo-profile">
            <img src="{{ Auth::user()->profile_picture() }}">
            <a href="{{ URL::route('profile_edit') }}" class="btn-edit-profile">EDITAR PERFIL</a>
            <a href="#" class="btn-edit-photo"><i class="icon-pencil"></i></a>
        </div>
        
        <div class="data-level-profile">
          <div class="name-profile left">{{ Auth::user()->first_name }}</div>
          <div class="point-profile right">{{ Auth::user()->total_points() }} PUNTOS</div>
          <div class="level-profile left"><span>NIVEL</span><b>{{ Auth::user()->level()['level'] }}</b></div>
          <div class="progressbar-profile right">
            <div><span style="width:{{ Auth::user()->level()['pc'] }}%;"></span></div>
            <b class="left">{{ Auth::user()->level()['left'] }}</b>
            <b class="right">{{ Auth::user()->level()['right'] }}</b>
          </div>
        </div>

        <div id="buttons" class="select-avatar hide">
          <form id="upload-image" action="{{ URL::route('user_upload_photo') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
            <input id="input-file" type="file" name="photo" style="display:block; width:0; height:0">
          </form>
            @if(Auth::user()->facebook_id)
            <a href ="{{ URL::route('user_photos') }}" class="btn-edit-fb">Desde Facebook</a><br>
            @endif
            <a href="#" class="btn-edit-pc">Desde tu PC</a>
        </div>

        <div id="bgheader" class="medium hide">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <h2>SELECCIONA UNA IMAGEN</h2>
          <div class="clear">
            <a href="#" data-cover-id="1" class="left cover-link"><img src="{{ asset('static/site/images/background-profile/small/cover-1.jpg') }}"></a>
            <a href="#" data-cover-id="2" class="left cover-link"><img src="{{ asset('static/site/images/background-profile/small/cover-2.jpg') }}"></a>
            <a href="#" data-cover-id="3" class="left cover-link"><img src="{{ asset('static/site/images/background-profile/small/cover-3.jpg') }}"></a>
            <a href="#" data-cover-id="4" class="left cover-link"><img src="{{ asset('static/site/images/background-profile/small/cover-4.jpg') }}"></a>
            <a data-cover-id="0" class="btn-register-foot right btn-send-cover">FINALIZAR</a>
          </div>
        </div>

    </div>
    
        @if( count( $profile_images ) > 0 )
      <div class="modal-profile-images hide">
        <form id="form-user-image-src" style="width:0; height: 0; display: block" method="post" action="{{ URL::route('user_update_photo') }}">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <input type="hidden" name="src" value="" class="user-image-src">
        </form>
        <div class="row modal-session myform">
          <div class="columns large-12 large-centered"><a class="close-modal">x</a>
            <h4>Selecciona una imagen pra tu perfil</h4>
            <div class="row">@foreach ( $profile_images as $profile_image )
              <div class="columns large-3"><a href="#" data-user-image-src="{{ $profile_image['src'] }}" class="fb-avatar"><img src="{{ $profile_image['src'] }}"></a></div>@endforeach
            </div>
          </div>
        </div>
      </div>
      @endif

