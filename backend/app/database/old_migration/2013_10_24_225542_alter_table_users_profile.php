<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableUsersProfile extends Migration {

	public function up()
	{
        Schema::table('users', function($t) {
            $t->integer('age');
            $t->integer('authorize_father');
        });

	}

	public function down()
	{
       Schema::table('users', function($t) {
            $t->dropColumn('age');
            $t->dropColumn('authorize_father');
       });
	}

}
