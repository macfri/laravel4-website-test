<?php

use Illuminate\Database\Migrations\Migration;

class UpdateLikeUpDownMemes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {

        Schema::table('section_memes', function($t) {
            $t->integer('like');
            $t->integer('dislike');
        });



		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
    {

       Schema::table('section_memes', function($t) {
                $t->dropColumn('like');
                $t->dropColumn('dislike');
       });

		//
	}

}
