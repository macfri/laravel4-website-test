<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersFacebook extends Migration {

	public function up()
    {
		Schema::create('users_facebook', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fb_id', 100)->nullable()->unique();
            $table->string('fb_email', 100)->nullable();
            $table->string('fb_username', 100)->nullable();
            $table->string('fb_gender', 100)->nullable();
            $table->string('fb_first_name', 100)->nullable();
            $table->string('fb_last_name', 100)->nullable();
            $table->string('fb_oauth_token', 100)->nullable();
            $table->string('fb_expires', 100)->nullable();
            $table->timestamps();

        });
    }

	public function down()
    {
        Schema::dropIfExists('facebook');
	}
}
