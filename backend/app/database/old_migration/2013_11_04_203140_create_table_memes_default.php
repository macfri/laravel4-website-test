<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMemesDefault extends Migration {

	public function up()
    {
        /*
		Schema::create('section_memes_default', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');

            $table->timestamps();
        });
         */
	}

	public function down()
	{
        Schema::dropIfExists('section_memes_default');
	}

}
