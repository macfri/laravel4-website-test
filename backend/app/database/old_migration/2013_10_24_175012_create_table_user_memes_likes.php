<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserMemesLikes extends Migration {

	public function up()
	{
		Schema::create('users_memes_likes', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->on_update('cascade');

            $table->integer('meme_id')
                ->unsigned()
                ->index();
            $table->foreign('meme_id')
                ->references('id')
                ->on('section_memes')
                ->on_update('cascade');

            $table->boolean('vote_like')
                ->default(0);

            $table->boolean('vote_dislike')
                ->default(0);

            //$table->integer('vote_total_likes')
                //->default(0);

            //$table->integer('vote_total_dislikes')
                //->default(0);

            //$table->integer('vote_total')
                //->default(0);

            $table->timestamps();
        });
	}

	public function down()
    {
		Schema::drop('users_memes_likes');
	}
}
