<?php

class AdminProductController extends BaseController {

    private $viewpath = 'admin/product/';

    public function index() {

        if (!SystemUser::has_perm('products-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = Product::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('products-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        return View::make(
                        $this->viewpath . __function__
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('products-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Product::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('products-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Product::validate(Input::all());
        if ($v->passes()) {
            $item = new Product();
            $item->title = Input::get('title');
            $item->slug = Product::get_slug(Input::get('title'));
            $item->description = Input::get('description');
            $item->status = (bool) Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->block_size = Input::get('block_size');
            $item->show_home = (bool) Input::get('show_home');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_product_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {

                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'products', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'products', $item->slug, 'photo', $ext
                );

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo', $ext)
                    );

                    $item->image_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('photo_nutritional_info')) {
                $file = Input::file('photo_nutritional_info');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'products', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'products', $item->slug, 'photo_nutri', $ext
                );

                try {

                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo_nutri', $ext)
                    );
                    $item->image_nutritional_info_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'products');

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'products', $item->slug, $ext
                );

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', $item->slug, $ext)
                    );
                    $item->image_home_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            return Redirect::route('admin_product')
                            ->with('success', sprintf(
                                            'Product %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()
                            ->withErrors($v);
        }
    }

    public function update($id) {

        if (!SystemUser::has_perm('products-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Product::validate(Input::all());
        if ($v->passes()) {

            $item = Product::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->block_size = Input::get('block_size');
            $item->status = (bool) Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->show_home = (bool) Input::get('show_home');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_product_edit', $id)
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'products', $item->slug);

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'products', $item->slug, 'photo', $ext
                );

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo', $ext)
                    );

                    if (isset($is_new)) {
                        $item->image_id = $image->id;
                        $item->save();
                    }
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                if (isset($is_new)) {
                    unset($is_new);
                }
            }

            if (Input::hasFile('photo_nutritional_info')) {
                $file = Input::file('photo_nutritional_info');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'products', $item->slug);

                $image = Photo::find($item->image_nutritional_info_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'products', $item->slug, 'photo_nutri', $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo_nutri', $ext)
                    );

                    if (isset($is_new)) {
                        $item->image_nutritional_info_id = $image->id;
                        $item->save();
                    }
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                if (isset($is_new)) {
                    unset($is_new);
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'products');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'products', $item->slug, $ext
                );

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', $item->slug, $ext)
                    );

                    if (isset($is_new)) {
                        $item->image_home_id = $image->id;
                        $item->save();
                    }
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                if (isset($is_new)) {
                    unset($is_new);
                }
            }

            return Redirect::route('admin_product')
                            ->with('success', sprintf(
                                            'Product %s Updated Successfully.', $item->title));
        } else {
            return Redirect::back()
                            ->withErrors($v);
        }
    }

    public function destroy($id) {
        if (!SystemUser::has_perm('products-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        Product::find($id)->delete();
        return Redirect::back();
    }

}
