
<table border="1" width="100%">
    <thead>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">created_at</th>
        <th class="span1">facebook</th>
        <th class="span1">google</th>
        <th class="span1">web</th>
        <th class="span1">was_anonymous</th>
        <th class="span1">created_at</th>
        <th class="span1">new user</th>
    </tr>
    </thead>
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->user->first_name }}</td>
        <td>{{ $p->user->last_name }}</td>
        <td>{{ $p->user->email }}</td>
        <td>{{ $p->user->created_at }}</td>
        <td>@if ($p->user->facebook_id!='')si @else no @endif</td>
        <td>@if ($p->user->google_id!='')si @else no @endif</td>
        <td>@if ($p->user->password!='')si @else no @endif</td>
        <td>{{ $p->was_anonymous }}</td>
        <td>{{ $p->created_at }}</td>
        <td>{{ date($p->user->created_at) >= '2013-12-14'?'1':'0' }}</td>
    </tr>
    @endforeach
</table>
