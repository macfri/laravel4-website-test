<?php

class SiteController extends BaseController {

    public function home() {

        Session::set('current_route', Route::current()->getPath());
        $items = Grid::all();
        $slides = Slider::where('status', '=', 'active')
                ->orderBy('order', 'asc')
                ->get();
        return View::make(
                        'site/home', array(
                    'slides' => $slides,
                    'items' => $items
                        )
        );
    }

    public function history() {

        Session::set('current_route', Route::current()->getPath());
        return View::make('site/history');
    }

    public function _404() {

        $category = 'not-found';
        Seo::addPlaceholder('meta_title', $category);
        Seo::addPlaceholder('meta_description', $category);
        Seo::addPlaceholder('meta_keywords', $category);
        return Response::view('site.not-found', array(), 404);
    }

    public function spots() {

        Session::set('current_route', Route::current()->getPath());
        $items = Spot::orderby('order', 'asc')->get();
        $item = Spot::orderby('order', 'asc')->first();

        return View::make(
                        'site/spots', array(
                    'items' => $items,
                    'item' => $item
                        )
        );
    }

    public function contact() {

        Session::set('current_route', Route::current()->getPath());
        return View::make('site/contact');
    }

    public function terms() {

        Session::set('current_route', Route::current()->getPath());
        return View::make('site/terms');
    }

    public function privacy() {

        Session::set('current_route', Route::current()->getPath());
        return View::make('site/privacy');
    }

}
