<?php

class Event extends Eloquent {

    protected $table = 'section_events';

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public function imageDetail() {
        return $this->belongsTo('Photo');
    }

    public function imageHome() {
        return $this->belongsTo('Photo');
    }

    public function imageSlider() {
        return $this->belongsTo('Photo');
    }

    public function gallery() {
        return $this->hasMany('EventPhoto');
    }

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public static function validate($input) {
        $rules = array(
            'title' => 'Required|Min:1|Max:500',
            'description' => 'Required|Min:1|Max:10250',
        );
        return Validator::make($input, $rules);
    }

    public function month() {
        $months = array(
            'ene', 'feb', 'mar',
            'abr', 'may', 'jun',
            'jul', 'ago', 'set',
            'oct', 'nov', 'dic');
        $month = (int) substr($this->event_date, 5, 2);
        if (array_key_exists($month - 1, $months)) {
            return $months[$month - 1];
        }
    }

    public function day() {
        return substr($this->event_date, 8, 2);
    }

}
