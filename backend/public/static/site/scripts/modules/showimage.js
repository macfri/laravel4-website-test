define([
  'jquery',
  'foundation'
], function($){
  function showImage($item){
    $item.click(function(event) {
      event.preventDefault();
      var modal = $('<div>').addClass('reveal-modal table-nutritional').appendTo('body'),
          img = '<img src="' + $(this).attr('data-table') + '">';
      modal.empty().append('<a class="btn-close-modal close-reveal-modal" onclick="_gaq.push([\'_trackEvent\',\'Web-Contenido\', \'Cerrar-InfoNutri\', \'\'])">CERRAR</a>'+img).foundation('reveal', 'open');
    });
  }
  return showImage;
});
