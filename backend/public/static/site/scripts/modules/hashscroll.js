define([
  'jquery'
], function($){

  function hashScroll(){

    $.localScroll.defaults.axis = 'xy';

    $.localScroll.hash({
      target: 'body',
      queue:true,
      duration:1500
    });

    $.localScroll({
      target: 'body',
      queue:true,
      duration:400,
      hash:true,
      onBefore:function( e, anchor, $target ){
        // The 'this' is the settings object, can be modified
      },
      onAfter:function( anchor, settings ){
        //$('.btn-bottom-gallery').attr('href','#gallery3');
      }
    });
    
    
  }
  return hashScroll;
});
