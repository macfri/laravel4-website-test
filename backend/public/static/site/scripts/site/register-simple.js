require.config({
  paths: {
    'jquery':'../libs/jquery',
    'register': '../modules/register',
    // 'profile-fb': '../modules/edit-profile-fb',

  },
  shim: {
    'register': ['jquery']
  }
});

require([
  'jquery',
  'register'
], function ($, register) {
  register($('#register'));
});
