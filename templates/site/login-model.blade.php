    <div id="login-modal" class="reveal-modal" style="top:30px">
      <div class="row modal-session myform">
        <div class="columns large-12 large-centered">
            <form id="login" action="{{ URL::route('user_dologin') }}" method="post" autocomplete="off">



<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


<!-- {{ Form::token() }} -->

<a class="close-modal close-reveal-modal">x</a>
            <h3>Inicia Sesión</h3>
            <h4>Conéctate con una red social</h4>
            <div class="row">
              <div class="columns large-6"><a href="{{ URL::route('user_login_facebook') }}" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-Fcbk', '{{ $page }}'])" class="btn-facebook">Conéctate con Facebook</a></div>
              <div class="columns large-6"><a href="{{ URL::route('user_login_google') }}" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-Google', '{{ $page }}'])" class="btn-google">Conéctate con Google</a></div>
            </div>
            <div class="row pad-left-15"><small>Inicia sesión con tu usuario y tu contraseña</small>@if($errors->has())<br><br>
              <p class="alert">{{ $errors->first('message') }}
              </p>@endif
            </div>
            <div class="row">
              <div class="columns">
                <label for="email">Correo</label>
                <input type="text" name="email" id="email>
                <p class="alert hide">* Correo electrónico y contraseña incorrectos.</p>
              </div>
            </div>
            <div class="row">
              <div class="columns">
                <label for="password">Contraseña</label>
                <input type="password" name="password" id="password">
                <p class="alert hide">* Correo electrónico y contraseña incorrectos.</p>
              </div>
            </div>
            <div class="row">
              <div class="columns pad-top-25">
                <button type="submit" onclick="_gaq.push(['_trackEvent','Web-Login', 'Ingresar-cuenta', '{{ $page }}'])" class="btn-session">Inicia Sesión</button><a href="{{ URL::route('user_forgot_password') }}" class="link-bottom">Olvide mi contraseña</a>
              </div>
            </div>

            <div class="row new-register">
              <div class="columns">
                <h3>Si no estas registrado</h3>
                <small>¡No te preocupes, puedes hacerlo en pocos pasos!</small>
                <a href="{{ URL::route('user_view_simple') }}" class="btn-session">Registrate</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
