<?php

class UbigeoController extends BaseController {

    public function departments() {
        $items = Ubigeo::where('cod_prov', '=', '00')
                        ->where('cod_dist', '=', '00')->get();
        return Response::json($items);
    }

    public function provinces($cod_dpto) {
        $items = Ubigeo::where('cod_dpto', '=', $cod_dpto)
                ->where('cod_prov', '<>', '00')
                ->where('cod_dist', '=', '00')
                ->get();
        return Response::json($items);
    }

    public function districts($cod_dpto, $cod_prov) {
        $items = Ubigeo::where('cod_dpto', '=', $cod_dpto)
                ->where('cod_prov', '=', $cod_prov)
                ->where('cod_dist', '<>', '00')
                ->get();
        return Response::json($items);
    }

}
