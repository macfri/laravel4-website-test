<?php

function get_data_fb2($url, $params) {
    $url .= '?' . http_build_query($params);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($status == 200) {
        return json_decode($data);
    } else {
        var_dump($data);
        var_dump($status);
    }
}

class AdminMemeController extends BaseController {

    private $viewpath = 'admin/meme/';

    public function index() {

        if (!SystemUser::has_perm('memes-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }


        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');
        $search_category = Input::get('category');

        $memes = Meme::where('id', '>', 0);

        if (strlen($search_title) > 0) {
            $memes->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_category) > 0) {
            $memes->where('category', $search_category);
        }

        if (strlen($search_status) > 0) {
            $memes->where('status', '=', $search_status);
        }

        $memes = $memes->orderby('created_at', 'desc');
        $total = $memes->count();
        $memes = $memes->paginate(10);

        $memes_category = MemeCategory::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $memes,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'search_category' => $search_category,
                    'memes_category' => $memes_category
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('memes-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $memes_category = MemeCategory::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => array(),
                    'memes_category' => $memes_category
                        )
        );
    }

    public function edit($id) {
        if (!SystemUser::has_perm('memes-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $meme = Meme::find($id);
        $memes_category = MemeCategory::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $meme,
                    'memes_category' => $memes_category
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('memes-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Meme::validate(Input::all());
        if (!$v->passes()) {
            return Redirect::back()
                            ->withInput()
                            ->withErrors($v);
        }

        $meme = new Meme();
        $meme->title = Input::get('title');
        $meme->slug = Meme::get_slug(Input::get('title'));
        $meme->show_home = (bool) Input::get('show_home');
        $meme->block_size = Input::get('block_size');
        $meme->status = Input::get('status');
        $meme->author_id = Session::get('session_user');
        $meme->category = Input::get('category');
        $meme->seo_meta_title = Input::get('seo_meta_title');
        $meme->seo_meta_description = Input::get('seo_meta_description');
        $meme->seo_meta_keywords = Input::get('seo_meta_keywords');
        $meme->link = Input::get('link');

        try {
            $meme->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_meme_add')
                            ->withInput()
                            ->with('failure', $exception->getMessage());
        }

        if (Input::hasFile('photo')) {
            $file = Input::file('photo');
            $name = $file->getClientOriginalName();
            $size = $file->getSize();
            $mime = $file->getMimeType();
            $ext = pathinfo($name, PATHINFO_EXTENSION);

            $upload_path = sprintf(
                    '%s/%s/%s', public_path(), 'upload', 'memes');

            $image = new Photo();
            $image->title = $name;
            $image->extension = $ext;
            $image->size = $size;
            $image->mime = $mime;
            $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes', $meme->slug, $ext
            );
            $image->save();

            $file->move(
                    $upload_path, sprintf('%s.%s', $meme->slug, $ext)
            );

            $meme->image_id = $image->id;

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_meme_add')
                                ->with('failure', $exception->getMessage());
            }
        }

        if (Input::hasFile('photo_home')) {
            $file = Input::file('photo_home');
            $name = $file->getClientOriginalName();
            $size = $file->getSize();
            $mime = $file->getMimeType();
            $ext = pathinfo($name, PATHINFO_EXTENSION);

            $upload_path = sprintf(
                    '%s/%s/%s/%s', public_path(), 'upload', 'home', 'memes');

            $image = new Photo();
            $image->title = $name;
            $image->extension = $ext;
            $image->size = $size;
            $image->mime = $mime;
            $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'memes', $meme->slug, $ext
            );

            $image->save();

            $file->move(
                    $upload_path, sprintf('%s.%s', $meme->slug, $ext)
            );

            $meme->image_home_id = $image->id;

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_meme_add')
                                ->with('failure', $exception->getMessage());
            }
        }

        return Redirect::route('admin_meme')
                        ->with('success', sprintf(
                                        'Meme %s Created Successfully.', $meme->title));
    }

    public function update($id) {

        if (!SystemUser::has_perm('memes-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Meme::validate(Input::all());
        if ($v->passes()) {

            $meme = Meme::find($id);
            $meme->title = Input::get('title');
            $meme->show_home = Input::get('show_home');
            $meme->block_size = Input::get('block_size');
            $meme->status = Input::get('status');
            $meme->author_id = Session::get('session_user');
            $meme->category = Input::get('category');
            $meme->seo_meta_title = Input::get('seo_meta_title');
            $meme->seo_meta_description = Input::get('seo_meta_description');
            $meme->seo_meta_keywords = Input::get('seo_meta_keywords');
            $meme->link = Input::get('link');

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_meme_edit', $id)
                                ->with('failure', $exception->getMessage());
            }


            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'memes');

                $image = Photo::find($meme->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes', $meme->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_meme_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $meme->slug, $ext)
                );

                if (isset($is_new)) {
                    $meme->image_id = $image->id;

                    try {
                        $meme->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_meme_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'memes');

                $image = Photo::find($meme->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'memes', $meme->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_meme_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $meme->slug, $ext)
                );

                if (isset($is_new)) {
                    $meme->image_home_id = $image->id;

                    try {
                        $meme->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_meme_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            return Redirect::route('admin_meme')
                            ->with('success', sprintf(
                                            'Meme %s Updated Successfully.', $meme->title));
        } else {

            return Redirect::back()->withErrors($v);
        }
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('memes-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        Meme::find($id)->delete();
        return Redirect::back();
    }

    public function change_status() {
        $status_code = 0;
        $meme_id = Input::get('video_id');
        $enabled = Input::get('enabled');

        if (strlen($meme_id) < 1 or strlen($enabled) < 1) {
            $message = 'Not Found1';
            $status_code = 1;
        } else {
            $meme_id = (int) $meme_id;
            $meme = Meme::where('id', $meme_id)->first();
            if (!$meme) {
                $message = 'Not Found2';
                $status_code = 1;
            } else {
                $meme->status = ($enabled == 'active') ? 'inactive' : 'active';

                try {
                    $meme->save();
                    $do = true;
                } catch (Exception $exception) {
                    $status_code = 1;
                    $message = 'ErroDb';
                    Log::error($exception->getMessage());
                }

                if (isset($do)) {
                    $message = 'save change status';
                }
            }
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    public function import_facebook() {
        $app_facebook = Config::get('app.facebook');
        $source_id = $app_facebook['page_id'];
        $access_token = $app_facebook['page_access_token'];
        $url = $app_facebook['url_fql'];
        $params = array(
            'q' => sprintf(
                    "select attachment, post_id, message, type from stream where source_id='%s' and type=247 limit 10", $source_id),
            'access_token' => $access_token
        );

        $data_last_post = get_data_fb2($url, $params);
        $items = array();

        foreach ($data_last_post->data as $row) {
            $pid = $row->attachment->media[0]->photo->pid;
            $exists = Meme::where('pid', $pid)->count();

            if ($exists < 1) {
                $params = array(
                    'q' => sprintf("SELECT caption, images FROM photo WHERE pid='%s'", $pid),
                    'access_token' => $access_token
                );

                $detail_data_last_post = get_data_fb2($url, $params);
                $detail_data_last_post = $detail_data_last_post->data[0];
                $message = $detail_data_last_post->caption;
                $picture = $detail_data_last_post->images[2]->source;

                $meme = new Meme();
                $meme->title = $message;
                $meme->slug = Meme::get_slug($meme->title);
                $meme->author_id = Session::get('session_user');
                $meme->category = 'facebook';
                $meme->status = 'inactive';
                $meme->pid = $pid;

                try {
                    $meme->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'memes/facebook');

                $image = new Photo();
                $image->title = $meme->title;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes/facebook', $meme->slug, 'jpg'
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $meme->image_id = $image->id;

                try {
                    $meme->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $pic_facebook = sprintf(
                        '%s/%s/%s/%s.jpg', public_path(), 'upload', 'memes/facebook', $meme->slug);

                $file_handler = fopen($pic_facebook, 'w');
                $curl = curl_init($picture);
                curl_setopt($curl, CURLOPT_FILE, $file_handler);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($curl);
                curl_close($curl);
                fclose($file_handler);
            }
        }

        return Redirect::back()->with('success', 'Facebook import');
    }

}
