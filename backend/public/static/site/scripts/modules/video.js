define([
  'jquery',
], function($){

  var yt = $('#youtube');

  // If you cant store the html directly
  var ythtml = yt.html();
  yt.hide().children().remove();

  // load iframe once needed
  $('#imageID').click(function(){
    $(this).hide();
    yt.html(ythtml).show();
  });

  //return closeAlert;
});
