@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>

   @if(SystemUser::has_perm('memes-add'))
    <li><a href="{{ URL::route('admin_meme_add') }}">Create</a></li>
    @endif

</ul>


<form id="filter_form" method="POST" action="{{ URL::route('admin_meme_import_facebook') }}">
    <div class="pull-right"><button type="submit" class="btn btn-primary" style="">Import Facebook</button></div>
</form>

<form id="filter_form" method="GET" action="{{ URL::route('admin_meme') }}">
    <div class="pull-right"><button type="submit" class="btn btn-primary" style="">Apply</button></div>
    <table class="filters">
        <tbody>
            <tr>
                <td>Title</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{ Form::select('search_status',
                    array(''=>'Seleccionar', 'active' => 'active', 'inactive' => 'inactive'), $search_status) }}</td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                <select name="category">
                    <option value="">[Seleccionar]</option>
                    <option value="meme-milo" @if($search_category == 'meme-milo') selected @endif >Meme milo</option>
                    <option value="bravazo"   @if($search_category == 'bravazo') selected @endif >Bravazo</option>
                    <option value="lol"  @if($search_category == 'lol') selected @endif >Lol</option>
                    <option value="videos-graciosos"  @if($search_category == 'videos-graciosos') selected @endif >Videos graciosos</option>
                    <option value="facebook"  @if($search_category == 'facebook') selected @endif >facebook</option>
                </select >
                </td>
            </tr>
        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
      <!--  <th class="span1"><input type="checkbox" name="rowtoggle" class="action-rowtoggle" /></th> -->
     <th class="span1">&nbsp;</th>
        <th class="span1">category</th>
        <th class="span1">title</th>
        <th class="span1">created_at</th>
        <th class="span1">author</th>
    
        <th class="span1">image / yb_code</th>

        <th class="span1"></th>
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <td>
            <a class="icon" href="{{ URL::route('admin_meme_edit', $p->id) }}"><i class="icon-pencil"></i></a>
            <form class="icon" method="POST" action="{{ URL::route('admin_meme_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form>
        </td>
        <td>{{ $p->category }}</td>
        <td><a href="{{ URL::route('admin_meme_edit', $p->id) }}">{{ $p->title }}</a></td>
        <td>{{ $p->created_at }}</td>
        <td>{{ $p->author->username }}</td>


        @if($p->category != 'videos-graciosos')
            @if(!is_null($p->image))
            <td> <img style="width:80px;" src="{{ asset($p->image->path) }}" /></td>
            @else
            <td></td>
            @endif 
        @else
            <td>{{ $p->link }}</td>
        @endif

        <td><a  class="btn_change_status btn @if($p->status == 'active')btn-success @else btn-danger @endif" data-enabled='{{ $p->status }}' data-id='{{ $p->id }}'>
             @if($p->status == 'active') On @else Off @endif
          </a></td>

    </tr>
    @endforeach
    @else
    <tr><td colspan="8">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>



<script>
    $( document ).ready(function() 
    {
        $('.btn_change_status').live('click', function(){

            var href = $(this);
            var video_id = href.attr('data-id');
            var enabled = href.attr('data-enabled')

            var posting = $.post(
                "{{ URL::route('admin_meme_change_status') }}",
                {video_id: video_id, enabled: enabled},
                'json'
            );

            posting.done(function(data) {
                if (data.status_code == 0)
                {
                    if (enabled == 'active'){
                        href.html('Off');
                        href.removeClass( "btn-success" );
                        href.addClass( "btn-danger" );
                        href.attr('data-enabled', 'inactive');
                    }else{
                        href.html('On');
                        href.removeClass( "btn-danger" );
                        href.addClass( "btn-success" );
                        href.attr('data-enabled', 'active');
                    }
                }
                else
                {
                    alert(data.message);
                }
            });

        });

  });
</script>


@stop
