<?php

use Illuminate\Console\Command;

class LoadBadWordsCommand extends Command {

    protected $name = 'load_badwords';
    protected $description = 'load_badwords';

    public function __construct() {
        parent::__construct();
    }

    public function fire() {
        $file = fopen("badwords.txt", "r") or exit("Unable to open file!");
        while (!feof($file)) {
            $row = fgets($file);
            $badword = new GameMundialBadword();
            $badword->name = trim($row);
            try {
                $badword->save();
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
                print $exception->getMessage();
            }
        }
        fclose($file);
    }

}
