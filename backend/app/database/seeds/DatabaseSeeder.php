<?php
require_once 'SystemUserSeeder.php';
require_once 'ProductSeeder.php';
require_once 'EventSeeder.php';
require_once 'GameSeeder.php';
require_once 'MemeSeeder.php';
require_once 'VideoSeeder.php';
require_once 'UbigeoSeeder.php';
require_once 'SocialRedSeeder.php';
require_once 'SliderSeeder.php';
require_once 'SpotSeeder.php';
require_once 'GameBadgeSeeder.php';
require_once 'GameFaceSeeder.php';
require_once 'BannerSeeder.php';

class DatabaseSeeder extends Seeder {

	public function run()
	{
        Eloquent::unguard();

        DB::table('sliders')->delete();
        DB::table('section_events_files')->delete();
        DB::table('section_events')->delete();
        DB::table('game_badges')->delete();
        DB::table('game_faces')->delete();
        DB::table('game_games')->delete();
        DB::table('section_products')->delete();
        DB::table('section_videos')->delete();
        DB::table('section_memes')->delete();
        DB::table('spots')->delete();
        DB::table('system_users')->delete();
        DB::table('files')->delete();

        $this->call('SystemUserTableSeeder');
        $this->call('SliderTableSeeder');
        $this->call('ProductsTableSeeder');
        $this->call('EventsTableSeeder');
        $this->call('GamesTableSeeder');
        $this->call('MemesTableSeeder');
        $this->call('VideosTableSeeder');
        $this->call('SpotTableSeeder');
        $this->call('GamesBadgeTableSeeder');
        $this->call('GamesFacesTableSeeder');
	}
}
