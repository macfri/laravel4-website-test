<?php

use Illuminate\Database\Migrations\Migration;

class AlterGames extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {

        Schema::table('game_games', function($t) {
                $t->integer('order');
        });


		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
    {


       Schema::table('game_games', function($t) {
                $t->dropColumn('order');
       });


		//
	}

}
