@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    .png@endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'files' => true)) }}
<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit User')</legend>

    <div class="control-group">
        <div class="control-label">username<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="username" id="username" value="@if(isset($item)){{$item->username}}@else{{ Form::old('username')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">first_name<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="first_name" id="first_name" value="@if(isset($item)){{$item->first_name}}@else{{ Form::old('first_name')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">last_name<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="last_name" id="last_name" value="@if(isset($item)){{$item->last_name}}@else{{ Form::old('last_name')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">email<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="email" id="email" value="@if(isset($item)){{$item->email}}@else{{ Form::old('email')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">dni<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="dni" id="email" value="@if(isset($item)){{$item->dni}}@else{{ Form::old('dni')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>


</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="control-label">fb_id</div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_id)){{$item->facebook->fb_id}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">fb_first_name</div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_first_name)){{$item->facebook->fb_first_name}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label"></div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_last_name)){{$item->facebook->fb_last_name}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">fb_email<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_email)){{$item->facebook->fb_email}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">fb_username<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_username)){{$item->facebook->fb_username}}@endif</div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">fb_gender<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->facebook->fb_gender)){{$item->facebook->fb_gender}}@endif</div>
        </div>
    </div>

</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="control-label">gg_id</div>
        <div class="controls">
           <div>@if(isset($item->google->gg_id)){{$item->google->gg_id}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">gg_first_name</div>
        <div class="controls">
           <div>@if(isset($item->google->gg_first_name)){{$item->google->gg_first_name}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label"></div>
        <div class="controls">
           <div>@if(isset($item->google->gg_last_name)){{$item->google->gg_last_name}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">gg_email<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->google->gg_email)){{$item->google->gg_email}}@endif</div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">gg_username<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->google->gg_username)){{$item->google->gg_username}}@endif</div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">gg_gender<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>@if(isset($item->google->gg_gender)){{$item->google->gg_gender}}@endif</div>
        </div>
    </div>

</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="controls"> 
        <input id="btn_submit" type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div> 

</fieldset>

{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>
<script>
$("#attach").on("change", " input[type=file]:last", function(){
    var item = $(this).clone(true);
    var fileName = $(this).val();
    if(fileName){
        $(this).parent().append("<br/>").append(item);
    }  
});

$('.btn_event_photo_delete').on('click', function(e){
    e.preventDefault();
    var $this = $(this);
    var div = $this.parent();
    var id = div.find('#event_photo_id').val();
    var posting = $.post(
        "{{ URL::route('admin_event_gallery_destroy') }}",
        { id: id },
        "json"
    );
    posting.done(function( data ) {
        status_code = data.status_code;
        //if (status_code == 0)
        //{
            div.remove();
        //}
    });
});
</script>
@stop
