@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<h3><a href="{{ URL::route('admin_game') }}">Games</a> -> {{ $slug }}</h3>
<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>
</ul>

<form id="filter_form" method="GET">
    <div class="pull-right">
        <button type="submit" class="btn btn-primary" style="">Apply</button>
    </div>
    <table class="filters">
        <tbody>
            <tr>
                <td>Name/Email/Dni</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                    <td>
                    Start Date:
                  <div class="input-append date" id="dp1" data-date="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd">
                    <input size="16" type="text" name="dp1" value="{{ $dp1 }}" readonly>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                  </div>

                    End Date:
                  <div class="input-append date" id="dp2" data-date="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd">
                    <input size="16" type="text" name="dp2" value="{{ $dp2 }}" readonly>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                  </div>
                </td>
            </tr>
        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">dni</th>
        <th class="span1">phone</th>
        <th class="span1">trophies</th>
        <th class="span1">max_score</th>
        <th class="span1">total_score</th>
        <th class="span1">attempts</th>
        <th class="span1">created_at</th>
        <th class="span1">is_new</th>
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->first_name }}</td>
        <td>{{ $p->last_name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->dni }}</td>
        <td>{{ $p->phone }}</td>
        <td>{{ $p->trophies }}</td>
        <td>{{ $p->max_score }}</td>
        <td>{{ $p->total_score }}</td>
        <td>{{ $p->attempts }}</td>
        <td>{{ $p->created_at }}</td>
        <td>{{ $p->is_new }}</td>
    </tr>
    @endforeach
    @else
    <tr><td colspan="12">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>

<br />
<table class="table table-striped table-bordered model-list">
    <tr>
        <td>Fecha</td>
        <td>Total</td>
    </tr>
    @foreach($players as $row)
    <tr>
        <td>{{ $row->f }}</td>
        <td>{{ $row->tot }}</td>
    </tr>
    @endforeach
</table>
@stop

@section('scripts')
<script>
    $('#dp1').datepicker();
    $('#dp2').datepicker();
</script>
@stop
