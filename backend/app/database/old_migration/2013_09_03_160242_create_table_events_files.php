<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventsFiles extends Migration {

	public function up()
    {
		Schema::create('section_events_files', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->integer('size')->nullable();
            $table->string('extension', 5)->nullable();
            $table->string('original_name', 100)->nullable();
            $table->string('mime', 100)->nullable();
            $table->string('path', 300)->nullable();
            $table->text('description');

            $table->integer('event_id')
                ->unsigned()
                ->nullable()
                ->index();

            $table->foreign('event_id')
                ->references('id')
                ->on('section_events')
                ->on_update('cascade');

			$table->timestamps();
        });
	}

	public function down()
    {
        Schema::dropIfExists('section_events_files');
	}
}
