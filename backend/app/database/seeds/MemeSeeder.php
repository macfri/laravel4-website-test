<?php
class MemesTableSeeder extends Seeder {

    public function run()
    {
        $file2 = Photo::create(array('title' => 'image 2', 'path'  => 'upload/home/memes/320x320.jpg'));
        $file3 = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/memes/640x320.jpg'));


        DB::table('section_memes')->insert(array(
            'title' => 'meme 2',
            'slug' => 'meme-2',
            'description' => 'description 2',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file2->id,
            'block_size' => '2x2'
        ));

        DB::table('section_memes')->insert(array(
            'title' => 'meme 3',
            'slug' => 'meme-3',
            'description' => 'description 3',
            'status' => 'active',
            'author_id' => 1,
            'image_home_id' => $file3->id,
            'block_size' => '4x2'
        ));
    }
}
