    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU --> 
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('freetime') }}">Pasa el rato</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <!-- HEADER -->
        @include('site.fun.menu')
        <!-- HEADER -->
      </div>
      <div class="content">
        <div class="row fun video-fun">
          <div class="fun-stream">
            <div class="columns large-8">

                @foreach ($items as $item)
              <div data-meme-id="{{ $item->id }}" class="row fun-post-detail">
                <div class="columns large-2">
                  <div class="fun-buttons-vote">

                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    <a onclick="_gaq.push(['_trackEvent','Web-Contenido','Like-Video','{{ $item->title }}'])" href="#" class="vote-icon like-large @if($item->vote_like == 1) active-vote @endif"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-Contenido','UnLike-Video','{{ $item->title }}'])" href="#" class="vote-icon dislike-large @if($item->vote_dislike == 1) active-vote @endif"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-RedesSociales','Compartir-Meme','{{ $item->title }}'])" href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" data-post-image="@if( isset($item->image->path) ){{ asset($item->image->path) }}@endif" class="vote-icon facebook-share"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-RedesSociales','Compartir-Meme','{{ $item->title }}'])" href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" class="vote-icon twitter-share"></a>
                </div>
                </div>

                <div class="columns large-10">
                  <div class="fun-post">

                    <h2>{{ $item->title }}</h2>
                    <div class="video-block clearfix">
                        <!--
                      <a href="javascript:;" id="imageID">
                        <i></i>
                        <img src="http://img.youtube.com/vi/eBG7P-K-r1Y/0.jpg" width="520" height="290">
                      </a> -->
                      <div id="">
                        <iframe width="506" height="290" src="//www.youtube.com/embed/{{ $item->link }}" frameborder="0" allowfullscreen wmode="transparent"></iframe>
                      </div>
                    </div>
                    @if($item->author_web_id)
                    <p>Video subido por: <b>{{ $item->authorWeb->first_name.' '. $item->authorWeb->last_name }}</b> </p>
                    @endif
                    <div class="votes desc-meme">
                      <div class="count">
                        <div class="like icon-fun-like-large"></div><span class="var-like">{{ $item->vote_total_likes }}</span>
                      </div>
                      <div class="count">
                        <div class="dislike icon-fun-dislike-large"></div><span class="var-dislike">{{ $item->vote_total_dislikes }}</span>
                      </div>
                    </div>

                  </div>
                </div>

              </div>
            @endforeach

            </div>

            <div class="columns large-4">
              <div class="video-meme">

                <form id="upload-video" action="{{ URL::route('freetime_do_upload_video') }}" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <div class="begin-meme">
                    <h2>Sube tu video gracioso</h2>
                    <input type="text" name="video_title" class="text-input" placeholder="Titulo del video">
                    <input type="text" name="video_link" class="text-input" placeholder="Url del video">
                    <button class="btn-load">Cargar</button>
                  </div>
                </form>
                @if(Session::has('message'))
                  <div class="finish-meme">
                    <h3>{{ Session::get('message') }}</h3>
                  </div>
                @endif

                @if($errors->has())
                  <div class="finish-meme"> 
                    @foreach($errors->all() as $m)
                    <h3>{{ $m }}</h3>
                    @endforeach 
                  </div>
                @endif
              </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'memes'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'meme'))
    @include('site.register-model')
    <script>
      window.app = window.app || {};
      app.freetime_rank = "{{ URL::route('freetime_rank') }}";
      app.auth = '@if(Auth::check()) auth_true @endif';
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/fun') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
