    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'eventos-todos'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events') }}">Eventos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('events_all') }}">Todos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="cont-filter">
        <div class="cont-buttons"><a href="{{ URL::route('events') }}" class="btn-past bubble">Destacados</a><a href="{{ URL::route('events_all') }}" class="btn-next bubble on">Todos</a></div>
      </div>
      <div class="content">
        <div class="row events">@foreach ($items as $item)
          <div class="columns large-4 left"><a href="{{ URL::route('events_detail', $item->slug) }}" onclick="_gaq.push(['_trackEvent','Web-Contenido', 'Menu-Eventos','{{$item->slug}}')">
              <div class="figure">@if(isset($item->image_id))<img src="{{ asset($item->image->path) }}">@else<img src="http://placehold.it/300x300">@endif
                <div class="figcaption icon icon-category"><img src="{{ asset('static/site/images/icon-events.png') }}"></div>
                <div class="fade"></div>
              </div>
              <div class="event">
                <div class="row"> 
                  <div class="columns large-3 date"><small>{{ $item->month() }} </small><span>{{ $item->day() }}</span></div>
                  <div class="columns large-9">   
                    <h3>{{ $item->title }}</h3>
                    <p>{{ substr($item->description, 0, 118)." ..." }}</p>
                  </div>
                </div>
              </div></a></div>@endforeach
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'eventos-todos'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'eventos-todos'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/events') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
