require.config({
  paths: {
    'jquery':'../libs/jquery',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'jquery.localscroll': '../libs/plugins/jquery.localscroll',
    'foundation': '../libs/plugins/foundation',
    'navfixed': '../modules/navfixed',
    'hashscroll': '../modules/hashscroll',

    'magellan': '../modules/magellan',
    'modal': '../modules/modal',
    'showimage': '../modules/showimage',
    'resize': '../modules/resize',
    'scrolltop': '../modules/scrolltop',

    'register': '../modules/register',
    'auth': '../modules/auth'
  },
  shim: {
    'jquery.scrolltop': ['jquery'],
    'jquery.localscroll': ['jquery.scrolltop'],
    'foundation' : ['jquery'],
    'hashscroll': ['jquery.localscroll'],
    'navfixed': ['jquery'],
    'magellan' : ['foundation'],
    'modal' : ['foundation'],
    'showimage' : ['foundation'],
    'resize' : ['jquery'],
    'scrolltop': ['jquery.scrolltop'],
    'register': ['jquery'],
    'auth': ['jquery']
  }
});

require([
  'jquery',
  'navfixed',
  'hashscroll',
  'magellan',
  'modal',
  'showimage',
  'resize',
  'scrolltop',
  'register',
  'auth'
], function ($, navFixed, hashscroll, magellan, modal, showImage, resize, scrollTop, register, auth) {
  navFixed($('.cont-filter'), 'fixed');
  hashscroll();
  magellan();
  modal();
  showImage($('.products .btn-products'));
  resize($('.products .product'));
  scrollTop($('.cont-filter'));
  register($('#register'));
  auth($('#login'));
});
