define([
  'jquery',
], function($){

  function closeAlert(){
    $('.close-alert').click(function(e) {
    e.preventDefault();
    $('.alert-bar').animate({
    height:"toggle"
    });
    });
  }

  return closeAlert;
});
