<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddIsLima extends Migration {


	public function up()
    {
        Schema::table('users', function($t) {
            $t->boolean('is_lima');
        });
	}

	public function down()
    {
        Schema::table('users', function($t) {
            $t->dropColumn('is_lima');
        });
	}
}
