<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideos extends Migration {

	public function up()
	{
		Schema::create('section_videos', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('slug', 100)->index();
            $table->string('brief', 300);
            $table->string('code', 300);
            $table->text('description');
            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');

            $table->text('block_size', 20);
            $table->boolean('show_home')
                ->default(0);
            $table->integer('image_home_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_home_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

			$table->timestamps();
        });
	}


	public function down()
	{
		Schema::drop('section_videos');
	}
}
