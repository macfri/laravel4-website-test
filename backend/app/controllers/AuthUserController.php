<?php

class AuthUserController extends BaseController {

    public function login() {

        if (Auth::check()) {
            return Redirect::route('home');
        }

        $show_captcha = false;

        if (Session::has('login_attempts')) {
            if ((int) Session::get('login_attempts') > 3) {
                $show_captcha = true;
            }
        }

        return View::make(
                        'site/users/login', array(
                    'show_captcha' => $show_captcha));
    }

    public function dologin() {

        $rules = array(
            'email' => 'Required|Min:3|Max:64|Email',
            'password' => 'Required|Min:4|Max:120',
        );

        if (Session::has('login_attempts')) {
            if ((int) Session::get('login_attempts') > 3) {
                $rules['recaptcha_response_field'] = 'required|recaptcha';
            }
        }

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'email' => 'Necesitas un correo válido',
            'recaptcha_response_field.recaptcha' => 'Recaptcha es incorrecto',
            'recaptcha_response_field.required' => 'Recaptcha es incorrecto',
        );

        $v = Validator::make(Input::all(), $rules, $messages);

        if (!$v->passes()) {
            return Redirect::route('user_login')
                            ->withInput()
                            ->withErrors($v);
        }

        $email = Input::get('email');
        $password = Input::get('password');

        $credentials = array(
            'email' => $email,
            'password' => $password
        );

        if (Auth::validate($credentials)) {
            $user = User::where('email', $email)->first();
            Auth::login($user);
            Session::put('user_type', 1);

            if (is_null($user->dni) or $user->dni == '') {
                return Redirect::route('profile_edit');
            } else {

                if (Session::has('guest_game_id')) {
                    $game = Game::where(
                                    'id', Session::get('guest_game_id')
                            )->first();
                    if ($game->slug == 'knockea-a-la-almohada') {
                        _Event::fire('guest.save_round_anonymous');
                    }
                }

                _Event::fire('guest.save_fun_video');
                _Event::fire('guest.save_points');
                Session::regenerate();
                return Redirect::to(Session::get('current_route'));
            }
        }

        if (Session::has('login_attempts')) {
            $attempts = Session::get('login_attempts');
            Session::put('login_attempts', (int) $attempts + 1);
        } else {
            Session::put('login_attempts', 1);
        }

        return Redirect::route('user_login')
                        ->withInput()
                        ->withErrors(array(
                            'message' =>
                            'Correo electrónico o contraseña incorrectos'));
    }

    public function login_facebook() {

        $app_facebook = Config::get('app.facebook');
        $client_id = $app_facebook['client_id'];
        $client_secret = $app_facebook['client_secret'];
        $redirect_uri = URL::route('user_login_facebook');

        $code = Input::get('code');
        $_state = md5('__m4cf1');

        if (empty($code)) {
            $params = array(
                'client_id' => $client_id,
                'redirect_uri' => $redirect_uri,
                'state' => $_state,
                'scope' => implode(',', $app_facebook['permissions'])
            );
            $url = sprintf(
                    '%s?%s', $app_facebook['url_dialog'], http_build_query(
                            $params));
            return Redirect::to($url);
        }

        $url = sprintf(
                '%s/oauth/access_token', $app_facebook['url_graph']);

        $params = array(
            'client_id' => $client_id,
            'redirect_uri' => $redirect_uri,
            'client_secret' => $client_secret,
            'code' => $code
        );

        $data = get_data_fb($url, $params);

        if (strpos($data, 'error') === true) {
            return Redirect::route('home');
        }

        $data = explode('&', $data);
        $access_token = str_replace('access_token=', '', $data[0]);

        if (isset($data[1])) {
            $expires = str_replace('expires=', '', $data[1]);
        }

        _Event::fire('user.choose_photo', array($access_token, $expires));

        $url = sprintf('%s/me', $app_facebook['url_graph']);
        $params = array('access_token' => $access_token);

        $profile = json_decode(get_data_fb($url, $params));
        $social_red = FacebookUser::where(
                        'fb_id', $profile->id)->first();

        if (!$social_red) {
            $social_red = new FacebookUser();
            $social_red->fb_id = $profile->id;

            if (property_exists($profile, 'username')) {
                $social_red->fb_username = $profile->username;
            }

            $social_red->fb_email = $profile->email;
            $social_red->fb_gender = $profile->gender;
            $social_red->fb_first_name = $profile->first_name;
            $social_red->fb_last_name = $profile->last_name;
        }
        $social_red->fb_oauth_token = $access_token;
        if (isset($expires)) {
            $social_red->fb_expires = $expires;
        }
        try {
            $social_red->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        $user = User::where('facebook_id', $social_red->id)->first();
        if (!$user) {
            $user = new User();
            $user->first_name = $profile->first_name;
            $user->last_name = $profile->last_name;
            $user->email = $profile->email;
            $user->token_game = $social_red->id . '_' . md5(
                            microtime(TRUE) . rand(0, 100000));
            $user->facebook_id = $social_red->id;
        }

        $picture_file = sprintf(
                '%s/%s/picture?type=small', $app_facebook['url_graph'], $profile->id);

        $file = @file_get_contents($picture_file);

        if (strlen($file) > 0) {
            $user->picture = $picture_file;
        }

        try {
            $user->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        Auth::login($user);
        Session::put('user_type', 2);

        if (is_null($user->dni) or $user->dni == '') {
            return Redirect::route('profile_edit');
        }

        if (Session::has('guest_game_id')) {
            $game = Game::where('id', Session::get('guest_game_id'))->first();
            if ($game->slug == 'knockea-a-la-almohada') {
                _Event::fire('guest.save_round_anonymous');
            }
        }

        _Event::fire('guest.save_fun_video');
        _Event::fire('guest.save_points');
        Session::regenerate();

        return Redirect::to(Session::get('current_route'));
    }

    public function login_google() {

        $app_google = Config::get('app.google');
        $client_id = $app_google['client_id'];
        $client_secret = $app_google['client_secret'];
        $redirect_uri = URL::route('user_login_google');
        $code = Input::get('code');

        if (empty($code)) {
            $url = sprintf('%s/auth', $app_google['url_auth']);

            $params = array(
                "response_type" => "code",
                "client_id" => $client_id,
                "redirect_uri" => $redirect_uri,
                "scope" => implode(' ', $app_google['permissions'])
            );

            $url = sprintf('%s?%s', $url, http_build_query($params));
            return Redirect::to($url);
        }

        $url = sprintf('%s/token', $app_google['url_auth']);

        $params = array(
            "code" => $code,
            "client_id" => $client_id,
            "client_secret" => $client_secret,
            "redirect_uri" => $redirect_uri,
            "grant_type" => "authorization_code"
        );

        $data = json_decode(get_data_google($url, $params));
        $profile = json_decode(get_data_google2(
                        $app_google['user_info'], $data->access_token));

        $social_red = GoogleUser::where('gg_id', $profile->id)->first();

        if (!$social_red) {
            $social_red = new GoogleUser();
            $social_red->gg_id = $profile->id;
            $social_red->gg_email = $profile->email;
            $social_red->gg_first_name = $profile->given_name;
            $social_red->gg_last_name = $profile->family_name;
            $social_red->gg_oauth_token = $data->access_token;
            $social_red->save();
        }

        $user = User::where('email', $profile->email)->first();

        if (!$user) {
            $user = new User();
            $user->first_name = $profile->given_name;
            $user->last_name = $profile->family_name;
            $user->email = $profile->email;
            $user->google_id = $social_red->id;
            $user->token_game = $social_red->id . '_' . md5(
                            microtime(TRUE) . rand(0, 100000));
            //   $redirect_to .= '?c=2';
        } else {
            $user->google_id = $social_red->id;
        }

        $picture_file = sprintf(
                $app_google['get_picture'], $profile->id);
        $file = @file_get_contents($picture_file);

        if (strlen($file) > 0) {
            $user->picture = $picture_file;
        }

        try {
            $user->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
        Auth::login($user);
        Session::put('user_type', 3);

        if (is_null($user->dni) or $user->dni == '') {
            return Redirect::route('profile_edit');
        }

        if (Session::has('guest_game_id')) {
            $game = Game::where('id', Session::get('guest_game_id'))->first();
            if ($game->slug == 'knockea-a-la-almohada') {
                _Event::fire('guest.save_round_anonymous');
            }
        }

        _Event::fire('guest.save_fun_video');
        _Event::fire('guest.save_points');
        Session::regenerate();
        return Redirect::to(Session::get('current_route'));
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect::to(Session::get('current_route'));
    }

}
