<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSlider extends Migration {

	public function up()
    {
		Schema::create('sliders', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->text('description');
            $table->string('backgroung_color', 100);
            $table->string('button_title', 100);
            $table->string('button_backgroung_color', 100);
            $table->string('link', 300);
            $table->enum('status', array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');
            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
			$table->timestamps();
		});
	}

	public function down()
    {
        Schema::dropIfExists('sliders');
	}
}
