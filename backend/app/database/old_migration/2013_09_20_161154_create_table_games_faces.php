<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGamesFaces extends Migration {

	public function up()
	{

		Schema::create('game_faces', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('face1');
            $table->integer('face2');
            $table->integer('face3');
            $table->integer('face4');
            $table->integer('face5');

            $table->integer('game_id')->unsigned()->index();
            $table->foreign('game_id')
                ->references('id')->on('game_games')
                ->on_update('cascade');

			$table->timestamps();
        });

	}

	public function down()
    {
        Schema::dropIfExists('game_faces');
	}

}
