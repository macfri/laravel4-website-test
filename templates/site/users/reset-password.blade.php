    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.users.menu', array('section'=>''))
        <!-- MENU -->
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div id="login-page">
          <div class="row modal-session myform">
            <div class="columns large-8 large-centered">

                @if(Session::has('message'))
                  <h3>{{ Session::get('message') }}</h3>
                @else

                  <h3>Resetea tu contraseña</h3><small>Ingresa tu nueva contraseña.</small>
                  <form action="" method="post">
                    
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

                    <label>Nueva contraseña</label>
                    <input type="password" name="password" id="password">
                    <label>Repetir nueva contraseña.</label>
                    <input type="password" name="repassword" id="repassword">

                    @if($errors->has())
                        @foreach($errors->all() as $m)
                        <p class="alert">{{ $m }}</p>
                        @endforeach
                    @endif


                    <button type="submit" class="btn-login">Actualizar Contraseña</button>
                  </form>

                @endif

              <div class="mb-50"></div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>''))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>''))
    @include('site.register-model')
    <script src="{{ asset('static/site/scripts/libs/require.js') }}" data-main="{{ asset('static/site/scripts/site/login') }}"></script>
  </body>
</html>
