<?php

Route::group(array('before' => 'seorules.before', 'prefix' => 'pre-registro'), function() {

    Route::get('/', array(
        'as' => 'user_view_simple',
        'uses' => 'UserController@add'));

    Route::post('/', array(
        'before' => 'csrf',
        'as' => 'user_add_simple',
        'uses' => 'UserController@save'));
});



Route::group(array('before' => 'seorules.before', 'prefix' => 'perfil'), function() {

    Route::get('/', array(
        'before' => 'auth',
        'as' => 'profile',
        'uses' => 'UserController@profile'));

    Route::group(array('prefix' => 'editar'), function() {
        Route::get('/', array(
            'before' => 'auth',
            'as' => 'profile_edit',
            'uses' => 'UserController@profile_edit'));

        Route::post('/', array(
            'before' => 'csrf|auth',
            'before' => 'auth',
            'as' => 'profile_save',
            'uses' => 'UserController@profile_update'));

        Route::get('password', array(
            'before' => 'auth',
            'as' => 'profile_change_password',
            'uses' => 'UserController@profile_change_password'));

        Route::post('password', array(
            'before' => 'csrf|auth',
            #'before' => 'auth',
            'as' => 'do_profile_change_password',
            'uses' => 'UserController@profile_change_password_do'));
    });

    Route::get('seleccionar-foto-facebook', array(
        'before' => 'auth',
        'as' => 'user_photos',
        'uses' => 'UserController@profile_choose_photo_facebook'));

    Route::post('actualizar-foto-facebook', array(
        'before' => 'csrf|auth',
        'as' => 'user_update_photo',
        'uses' => 'UserController@profile_update_photo_facebook'));

    Route::post('actualizar-foto', array(
        'before' => 'csrf|auth',
        'as' => 'user_upload_photo',
        'uses' => 'UserController@profile_upload_photo'));

    Route::post('actualizar-foto-portada', array(
        #'before' => 'csrf|auth',
        #'before' => 'csrf',
        'as' => 'user_update_cover',
        'uses' => 'UserController@profile_update_cover'));
});



Route::group(array('before' => 'seorules.before', 'prefix' => 'olvide-mi-contrasena'), function() {

    Route::get('/', array(
        'as' => 'user_forgot_password',
        'uses' => 'UserController@forgot_password'));

    Route::post('/', array(
        #'before' => 'csrf',
        'as' => 'user_do_forgot_password',
        'uses' => 'UserController@forgot_password_do'));
});



Route::group(array('before' => 'seorules.before', 'prefix' => 'resetear-contrasena/'), function() {

    Route::get('{token}', array(
        'as' => 'user_change_password',
        'uses' => 'UserController@reset_password'));

    Route::post('/{token}', array(
        'before' => 'csrf',
        'as' => 'user_do_change_password',
        'uses' => 'UserController@reset_password_do'));
});



Route::group(array('before' => 'seorules.before', 'prefix' => 'validar-datos-perfil'), function() {

    Route::get('email', array(
        //'before' => 'auth',
        'as' => 'validate_form_email',
        'uses' => 'UserController@profile_validate_email'));

    Route::get('dni', array(
        //'before' => 'auth',
        'as' => 'validate_form_dni',
        'uses' => 'UserController@profile_validate_dni'));
});



Route::get('iniciar-sesion', array(
    'before' => 'seorules.before',
    'as' => 'user_login',
    'uses' => 'AuthUserController@login'));

Route::post('/iniciar-sesion', array(
    'before' => 'csrf',
    'as' => 'user_dologin',
    'uses' => 'AuthUserController@dologin'));

Route::get('/iniciar-sesion-facebook', array(
    #'before'=>'seorules.before',  
    'as' => 'user_login_facebook',
    'uses' => 'AuthUserController@login_facebook'));

Route::get('/iniciar-sesion-google', array(
    #'before'=>'seorules.before',  
    'as' => 'user_login_google',
    'uses' => 'AuthUserController@login_google'));

Route::get('/iniciar-sesion-game', array(
    #'before'=>'seorules.before',  
    'as' => 'user_login_game',
    'uses' => 'AuthUserController@login_guest_game'));

Route::get('/cerrar-sesion', array(
    #'before'=>'seorules.before',  
    'as' => 'user_logout',
    'uses' => 'AuthUserController@logout'));
?>
