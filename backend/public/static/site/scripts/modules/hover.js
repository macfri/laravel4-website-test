define([
  'jquery'
], function($){
  function hover($target){
    $target.on('mouseenter', startHover);
    $target.on('mouseleave', stopHover);

    function startHover(){
      $(this).find('.over').fadeIn(200);

      $(this).find('a').animate({bottom: '20px'}, 200);
      $(this).find('b').animate({bottom: '60px'}, 200);
    }

    function stopHover(){
      $(this).find('.over').fadeOut(100);
      $(this).find('a, b').animate({
        bottom: '0px'
      }, 200);
    }

  }
  return hover;
});
