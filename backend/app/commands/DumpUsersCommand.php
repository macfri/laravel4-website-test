<?php

use Illuminate\Console\Command;

class DumpUsersCommand extends Command {

    protected $name = 'dump_users';
    protected $description = 'dump_users';

    public function __construct() {
        parent::__construct();
    }

    public function fire() {

        $file = fopen("results.txt", "r") or exit("Unable to open file!");
        while (!feof($file)) {
            $row = explode(',', fgets($file));
            $first_name = $row[0];
            $last_name = $row[1];
            $email = $row[2];
            $dni = $row[3];
            $phone = $row[4];
            $password = $row[8];

            $user = new User();
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->dni = $dni;
            $user->phone = $phone;
            $user->password = Hash::make($password);
            $user->status = 'active';

            try {
                $user->save();
                $is_save = 1;
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
                print $exception->getMessage();
            }

            if (isset($is_save)) {
                echo "ok";
            }
        }

        fclose($file);
    }

}
