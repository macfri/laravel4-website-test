    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games_detail', $item->slug) }}">{{ $item->title }}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="game-content">
          <div id="game-vars">
            <form>
              <input type="hidden" value="{{ $item->id }}" name="game_id">
              <input type="hidden" value="@if($token_rank){{$token_rank}}@elsenull@endif" name="token_rank" id="token_rank">
              <input type="hidden" value="" name="face_id" id="face_id">
            </form>
            <script>
              window.game = window.game || {};
              game.gamerank_url = "{{ URL::route('games_rank') }}";

            </script>
          </div>
          <div class="row">
            <div class="columns large-12 pt-27">
              @if(!Auth::check())
              <!-- <div class="alert-bar"><i class="icon-warning left"></i>
                <p>
                  ¡Espera! Estás jugando sin entrar a tu cuenta. Recuerda que si quieres grabar tus puntos,
                  recolectar “logros” y entrar a los sorteos que realicemos, debes estar conectado. Si aún no tienes cuenta,  <a href="#" data-reveal-id="register-modal" onclick="_gaq.push(['_trackEvent','Web-Conversion', 'Paso1-Iniciar', 'juegos-detalle'])">regístrate aquí</a>
                </p><a class="close-alert right"><i class="icon-close-game"></i></a>
              </div> -->
              @endif

              <?php
                /*
              <div class="badget hide">
                <div class="left top-arrow-game"> <span>

                    SUPERA LOS RETOS<br>Y SUBE TU NIVEL</span></div>
                <h3 class="title-game">{{ $item->title }}</h3>
                <div class="cont-badget">@foreach($badges as $item2)<a class="left">
                    <div class="info-badge"><img src="{{ asset($item2->path) }}" class="left @if(!$item2->active)inactive@endif trophies-{{ $item2->id }}"><span class="left"><b>{{ $item2->title }}</b>
                        <p>{{ $item2->description }}</p></span></div></a>@endforeach
                  <div class="right top-detail-game">
                    <div>TU MAYOR PUNTAJE<b>{{ $max_score }}</b></div>
                  </div>
                </div>
              </div>

                */
                ?>
              <div class="@if($item->slug === 'tu-gran-dia') cont-game @else cont-game-full @endif">@if($item->slug === 'tu-gran-dia')@endif
                <div class="cont-messages js-game-modal hide">
                  <div class="int-msj-modal">
                    <!--<div class="chulls-stick"></div>-->
                    <!--<div class="chulls-date"></div>-->
                    <a class="btn-register-foot btn-color-green js-trigger-register">registrate para participar</a>

                    <p class="line-middle">o</p>

                    <a class="btn-register-foot js-trigger-login">inicia sesión para participar</a>

                    <a href="{{ URL::route('games') }}" class="btn-register-foot">ver otros juegos</a>
                  </div>
                </div>{{ $game_content }}
              </div>@if($item->id==6)
              <div class="rd">R.D.Nº 4801-2013-ONAGI-DGAE</div>@endif
              @if($item->image_ctrl_id)
              <div class="bg-controls">
                <h3>controles</h3>
                <div class="int-controls"><img src="{{ asset($item->image_ctrl->path) }}"></div>
              </div>@endif
              <div class="rate-game custom-game">
                <div class="left">
                  <h3>rankea el juego</h3>
                  <ul class="star">
                    <li class="face-rank face1"></li>
                    <li class="face-rank face2"></li>
                    <li class="face-rank face3"></li>
                    <li class="face-rank face4"></li>
                    <li class="face-rank face5"></li>
                  </ul>
                </div>
                <div class="right social-icons">
                  <h3>Comparte <small>a TUS PATAS</small></h3>
                  <ul>
                    <li><a onclick="feedDialog2(); _gaq.push(['_trackEvent','Web-Informativo', 'Compartir-Puntaje', ''])" class="shape-facebook"></a></li>
                    <li><a onclick="popTwitter('{{$item->title}}'); _gaq.push(['_trackEvent','Web-Informativo', 'Publicar-Puntaje', ''])" class="shape-twitter"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row ratings comenta-12">
            <div class="columns large-12 cont-face">
              <h3>comenta</h3>
              <div data-colorscheme="light" data-width="750" data-href="{{ URL::route('games_detail', $item->slug) }}" class="fb-comments"></div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>

      <div id="terms" class="reveal-modal">
        <div class="terms">

          <a class="btn-close-modal" onClick="closeTerms();"><i>&nbsp;</i></a>


            <div class="p20">

              <h1>Mecanismo y condiciones de la Promoción</h1>
              <div class="cont-scroll">
                <div class="scrollbar">
                  <div class="track">
                    <div class="thumb">
                      <div class="end"></div>
                    </div>
                  </div>
                </div>
                <div class="viewport">
                  <div class="overview">
                   <p> Todos los usuarios podrán registrarse vía Facebook Connect y cuando terminen de probar el juego por primera vez, se validará si cuentan con los datos correctos o si hay algún dato por completar.</p>

                   <p>a. Forma De Jugar</p>

            <p>Para ganar, el usuario debe golpear a la almohada hasta dejarla sin energía. Pierde al ser vencido por la almohada, registrándose la partida perdida con el round y el puntaje obtenido.</p>

            <p>Mientras más temprano el usuario juegue, mayor será el puntaje que recibirá. Así todo el puntaje obtenido se multiplicará de acuerdo a la hora de entrada:</p>

            
              <p>8:00 a.m. - 8:30 a.m.   X5 El puntaje obtenido se multiplica por 5.</p>
              <p>8:30 a.m. - 9:00 a.m.   X3 El puntaje obtenido se multiplica por 3.</p>
              <p>9:00 a.m. - 9:30 a.m.   X2 El puntaje obtenido se multiplica por 2.</p>
              <p>9:30 a.m. - 10:00 a.m. X1 Ya no recibe bono multiplicador.</p>
            

            <p>b. Controles</p>

<p>Clic izquierdo: Golpear</p>
<p>Barra espaciadora: Bloquear</p>
<p>Botón “X”: Especial</p>

<p>c. Rounds</p>

<p>El juego consta de tres rounds de 40 segundos y el usuario recibirá diferentes puntajes, dependiendo del round en el que gane:</p>

<p>Primer round: 1000 pts.</p>
<p>Segundo round: 700 pts.</p>
<p>Tercer round: 350 pts.</p>


<p>d. MOVIMIENTOS Y PUNTOS</p>

<p>El usuario puede golpear a la almohada solo en la cara y el cuerpo:</p>
<ul>
<p>Si golpea en la cara y el contrincante no se cubre recibirá 1000 pts.</p>
<p>Si golpea en el cuerpo y el contrincante no se cubre recibirá 250 pts.</p>
<p>Cada vez que el jugador mande a la almohada a la lona recibirá 500 pts. esto se logrará golpeando al contrincante 4 veces seguidas.</p>

<p>Si el usuario le gana a la almohada sin que ella le quite energía, recibirá una bonificación de Perfect de 2000 pts.</p>
<p>Para llevar al Knockout a la almohada, el usuario deberá golpearla 5 veces seguidas sin que llegue a bloquear. Cada vez que el jugador lo logré recibirá 500 pts.</p>
<p>El 5to golpe hará que la almohada se mareé, tambaleándose con estrellas en la cabeza, en ese momento el jugador puede darle un golpe que lo mandará a la lona, ganando 500 pts. El tiempo de duración de la almohada mareada será de 3 segundos. Si el usuario no le dio ningún golpe durante el mareo, la almohada volverá a la normalidad.</li>

<p>Si el usuario venció a la almohada y obtuvo 500 pts. por los golpes, este se multiplicará por el bono que obtuvo al momento de ingresar al juego 2500 (x5), 1500 (x3), 1000 (x2), 500 (x1).</li>  
</ul>

<p>1. Premios:</p>

<p>Los premios que se pondrán a disposición en esta campaña, serán los siguientes:</p>
<ul>
<p>Un (1) iPad Mini de Apple</p>
<p>Una (1) Bicicleta Best</p>
<p>Un (1) celular Samsung Galaxy S3</p>
<p>Una (1) Tablet Samsung Galaxy note </p>
<p>Cuenta (40) Packs Milo conteniendo una lata de 400 mg. y obsequios</p>
</ul>

<p>2.  Sorteo:</p>

<p>Los sorteos se realizaran semanalmente, a las 17.00 horas, en nuestras oficinas de la Av. Federico Villarreal Nº 128, Miraflores. La modalidad del sorteo consiste en la elección al azar de un ganador dentro de los 100 primeros puestos del ranking. Esto se realizará utilizando un listado de todos los participantes en una hoja de cálculo Excel. Todos los usuarios listados en la hoja tendrán un número correlativo que los acompaña. Para seleccionar a los ganadores en forma aleatoria se utilizará la función de Excel "random" o "aleatorio entre" que permite seleccionar un número al azar. Este número, corresponde al correlativo que acompaña a cada entrada de la hoja.</p>

<p>Los sorteos se realizaran las siguientes fechas:</p>

<p>Primer sorteo: UN  (1) IPAD MINI y DIEZ (10) PACKS MILO: 06 de febrero del 2014</p>
<p>Segundo sorteo: UNA (1)  BICICLETA BEST Y DIEZ (10) PACKS MILO: 13 de febrero del 2014</p>
<p>Tercer sorteo: UN (1) CELULAR GALAXY S3 Y DIEZ (10) PACKS MILO: 20 de febrero del 2014</p>
<p>Cuarto sorteo: UNA (1) TABLET SAMSUNG GALAXY NOTE Y DIEZ (10) PACKS MILO: 27 de febrero del 2014</p>

<p>El mismo día a la fecha del sorteo, el ganador de la semana será notificado por correo electrónico y al número telefónico con la información de contacto que éste hubiere proporcionado al inscribirse en el Concurso. Además, cada ganador será publicado en la página de Facebook (MILO PERÚ) y en la web www.milo.com.pe; se comunicará un ganador cada semana.</p>



                  
                </div>
              </div>
              </div>


            </div>
          </div>
        </div>
      </div>

      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos-detalle'))
    @include('site.register-model')
<script data-main="{{ asset('static/site/scripts/site/games') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>

  <script type="text/javascript">

    function modalTerms() {
      $("#terms").foundation('reveal', 'open');
      $(".cont-scroll").tinyscrollbar();

    }

    function closeTerms() {
      $(".btn-close-modal").foundation('reveal', 'close')
    }

    function feedDialog2(){
      FB.ui({
        method: 'feed',
        link: 'https://www.milo.com.pe/juegos/knockea-a-la-almohada',
        picture: 'https://www.milo.com.pe/static/site/images/icon-share-almohada.jpg',
        name: '¡KNOCKEA A LA ALMOHADA Y LLÉVATE ESTOS PREMIAZOS!',
        description: 'Véncela y participa por un Ipad Mini, un Galaxy S3, un Galaxy Note, una Bicicleta Best y 10 Packs de MILO cada semana. ¡Juega Aquí!'
      }, function ( response ) {});
    }

  </script>

  </body>
</html>
