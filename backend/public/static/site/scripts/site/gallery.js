require.config({
  paths: {
    'jquery':'../libs/jquery',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'jquery.localscroll': '../libs/plugins/jquery.localscroll',

    'hashscroll': '../modules/hashscroll',
    'scrollarrow': '../modules/scrollarrow',
    'posfixed': '../modules/posfixed'

  },
  shim: {
    'jquery.scrolltop': ['jquery'],
    'jquery.localscroll': ['jquery.scrolltop'],
    'hashscroll': ['jquery.localscroll'],
    'scrollarrow': ['jquery.localscroll'],
    'posfixed': ['jquery']
  }
});

require([
  'hashscroll',
  'posfixed',
  'scrollarrow'
], function (hashScroll, posFixed, scrollArrow) {
  hashScroll();
  posFixed($('.arrows'), 'fixed-arrows');
  scrollArrow();
});
