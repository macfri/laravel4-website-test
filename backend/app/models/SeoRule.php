<?php

class SeoRule extends Eloquent {

    protected $table = 'seorules';
    public $timestamps = false;

    public static function validate($input) {
        $rules = array(
            #'alias' => array('required'),
            'route' => array('required'),
            'priority' => array('required'),
            'title' => array('required'),
            'description' => array('required'),
            'keywords' => array('required'),
        );

        $messages = array(
            'alias.required' => 'El campo alias es obligatorio.',
            'route.required' => 'El campo route es obligatorio.',
            'priority.required' => 'El campo priority es obligatorio.',
            'title.required' => 'El campo title es obligatorio.',
            'description.required' => 'El campo description es obligatorio.',
            'keywords.required' => 'El campo keywords es obligatorio.',
        );

        return Validator::make($input, $rules, $messages);
    }

}
