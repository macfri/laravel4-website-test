@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<h3><a href="{{ URL::route('admin_game') }}">Games</a> -> {{ $slug }}</h3>

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>
</ul>
<br />


<form id="filter_form" method="GET">
    <div class="pull-right">

        <button type="submit" class="btn btn-primary" style="">Apply</button>

        <a href="{{ URL::route('admin_game_export', 'knockea-a-la-almohada') }}" class="btn btn-primary" style="">Exportar</a>

    </div>


    <table class="filters">
        <tbody>
            <tr>
                <td>Name/Email/Dni</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>
            <tr>
                <td>Week</td>
                <td>{{ Form::select('search_week',
                    array(
                        ''  => '[Select]' ,
                        '1' => 'week1',
                        '2' => 'week2', 
                        '3' => 'week3',
                        '4' => 'week4'
                    ), $search_week) }}</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                    <td>
                    Start Date:
                  <div class="input-append date" id="dp1" data-date="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd">
                    <input size="16" type="text" name="dp1" value="{{ $dp1 }}" readonly>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                  </div>

                    End Date:
                  <div class="input-append date" id="dp2" data-date="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd">
                    <input size="16" type="text" name="dp2" value="{{ $dp2 }}" readonly>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                  </div>
                </td>
            </tr>

        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">dni</th>
        <th class="span1">phone</th>
        <th class="span1">score</th>
        <th class="span1">play date</th>
        <th class="span1">fb_id</th>
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->first_name }}</td>
        <td>{{ $p->last_name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->dni }}</td>
        <td>{{ $p->phone }}</td>
        <td>{{ $p->score }}</td>
        <td>{{ $p->created_at }}</td>
        <td>{{ $p->fb_id }}</td>
    </tr>
    @endforeach
    @else
    <tr><td colspan="12">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>

total de jugadores: {{ $players_count }}
<br />
<table class="table table-striped table-bordered model-list">
    <tr>
        <td>Fecha</td>
        <td>Total</td>
    </tr>
    @foreach($players as $row)
    <tr>
        <td>{{ $row->f }}</td>
        <td>{{ $row->tot }}</td>
    </tr>
    @endforeach
</table>


total de juadores nuevos: {{ $players_new_count }}
<br />
<table class="table table-striped table-bordered model-list">
    <tr>
        <td>Fecha</td>
        <td>Total</td>
    </tr>
    @foreach($players_new as $row)
    <tr>
        <td>{{ $row->f }}</td>
        <td>{{ $row->tot }}</td>
    </tr>
    @endforeach
</table>


total de jugadores antiguos: {{ $players_old_count }}
<br />
<table class="table table-striped table-bordered model-list">
    <tr>
        <td>Fecha</td>
        <td>Total</td>
    </tr>
    @foreach($players_old as $row)
    <tr>
        <td>{{ $row->f }}</td>
        <td>{{ $row->tot }}</td>
    </tr>
    @endforeach
</table>
@stop

@section('scripts')
<script>
    $('#dp1').datepicker();
    $('#dp2').datepicker();
</script>
@stop
