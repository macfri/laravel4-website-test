<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEvents extends Migration {

	public function up()
	{
		Schema::create('section_events', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('slug', 100)->index();
            $table->text('description');
            $table->boolean('is_xtreme')
                ->default(0);
            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade');

            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->integer('image_detail_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_detail_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->text('block_size', 20);
            $table->boolean('show_home')
                ->default(0);

            $table->boolean('important')
                ->index()
                ->default(0);

            $table->string('when_where', 200);

            $table->integer('image_home_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_home_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->boolean('important_slider')
                ->default(0);

            $table->integer('image_slider_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_slider_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');


            $table->date('event_date');
			$table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('section_events');
	}
}
