<div data-magellan-expedition="fixed" class="cont-filter">
  <div class="cont-center">
    <div class="left"><a href="{{ URL::route('home') }}" class="btn-logo hide"><span class="logo-fixed"></span></a></div>
    <div class="cont-buttons center fun-menu">
        <a onclick="_gaq.push(['_trackEvent','Web-Contenido','Eleccion-Meme',''])" href="{{ URL::route('freetime_category', 'meme-milo') }}" class="btn-next bubble @if(isset($category))@if($category == 'meme-milo') on @endif @endif">MEME MILO</a>
        <a onclick="_gaq.push(['_trackEvent','Web-Conversion', Eleccion-Bravazo',''])" href="{{ URL::route('freetime_category', 'bravazo') }}" class="btn-next bubble @if(isset($category))@if($category == 'bravazo') on @endif @endif">BRAVAZO</a>
        <a onclick="_gaq.push(['_trackEvent','Web-Conversion','Eleccion-LOL',''])" href="{{ URL::route('freetime_category', 'lol') }}" class="btn-next bubble @if(isset($category))@if($category == 'lol') on @endif @endif">LOL</a>
        <a onclick="_gaq.push(['_trackEvent','Web-Contenido','Ir-Video',''])" href="{{ URL::route('freetime_category', 'videos-graciosos') }}" class="btn-next bubble @if(isset($category))@if($category == 'videos-graciosos') on @endif @endif">VIDEOS GRACIOSOS</a>
        <a onclick="_gaq.push(['_trackEvent','Web-Conversion','Subir-Foto',''])" href="{{ URL::route('freetime_category', 'sube-tu-meme') }}" class="btn-next bubble @if(isset($category))@if($category == 'sube-tu-meme') on @endif @endif">SUBE TU MEME</a>
        <a  href="{{ URL::route('freetime_category', 'facebook') }}" class="btn-next bubble @if(isset($category))@if($category == 'facebook') on @endif @endif">FACEBOOK</a>
    </div><a href="#" class="btn-top"><span>Arriba</span></a>
  </div>
</div>
