<?php

return array(
    'placeholder' => '[#_placeholder]',
    'rule' => array(
        'title' => 'your application seo title',
        'description' => 'your application long seo description',
        'keywords' => 'your application seo keywords',
        'noindex' => 0
    )
);