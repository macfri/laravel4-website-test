define([
  'jquery'
], function($){

  function viewVideo(){
    $('.list-video a').click(function(e) {
      e.preventDefault();
      var $target = $(this);
      var text = $target.parent().find('p').text();
      var idvideo = $target.attr('data-video');
      var iframe = '<iframe width="620" height="380" src="https://www.youtube.com/embed/' + idvideo + '?wmode=transparent&amp;rel=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>';
      var $video = $("#video");
      $video.html(iframe);
      $video.parent().find('p').text(text);
      $('body').scrollTo( 0, 800, { queue:true } );
    });
  }

  return viewVideo;
});
