<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRoundDetail extends Migration {

	public function up()
    {
		Schema::create('game_games_round_detail', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->on_update('cascade')
                ->onDelete('cascade');
            $table->integer('round')
                ->unsigned()
                ->nullable();
            $table->integer('lUser')
                ->unsigned()
                ->nullable();
            $table->integer('lEnemy')
                ->unsigned()
                ->nullable();
            $table->integer('totalScore')
                ->unsigned()
                ->nullable();
            $table->integer('roundScore')
                ->unsigned()
                ->nullable();
            $table->integer('kos')
                ->unsigned()
                ->nullable();
            $table->integer('koP')
                ->unsigned()
                ->nullable();
            $table->integer('spe')
                ->unsigned()
                ->nullable();
            $table->integer('multi')
                ->unsigned()
                ->nullable();
            $table->integer('hEnemy')
                ->unsigned()
                ->nullable();
            $table->integer('hEnemyBlock')
                ->unsigned()
                ->nullable();
            $table->integer('bodyBlock')
                ->unsigned()
                ->nullable();
            $table->integer('bodyAccert')
                ->unsigned()
                ->nullable();
            $table->integer('bodyGiven')
                ->unsigned()
                ->nullable();
            $table->integer('headAccert')
                ->unsigned()
                ->nullable();
            $table->integer('headBlock')
                ->unsigned()
                ->nullable();
            $table->integer('headGiven')
                ->unsigned()
                ->nullable();
            $table->integer('user_type')
                ->unsigned()
                ->nullable();
            $table->integer('lifetime')
                ->unsigned()
                ->nullable();
            $table->timestamps();
        });
	}


	public function down()
    {
        Schema::dropIfExists('game_games_round_detail');
	}

}
