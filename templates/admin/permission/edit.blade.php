@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal')) }}
<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit Role')</legend>

    <div class="control-group">
        <div class="control-label">name<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="name" id="name" value="@if(isset($item)) {{$item->name}} @else {{ Form::old('name')}} @endif">
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="controls"> 
        <input id="btn_submit" type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div> 
</fieldset>

{{ Form::close() }}
@stop
