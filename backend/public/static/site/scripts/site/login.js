require.config({
  paths: {
    'jquery':'../libs/jquery',
    'auth': '../modules/auth',
    'foundation': '../libs/plugins/foundation',
    'modal': '../modules/modal'

  },
  shim: {
    'auth': ['jquery'],
    'foundation' : ['jquery'],
    'modal': ['foundation']
  }
});

require([
  'jquery',
  'auth',
  'modal'
], function ($, auth, modal) {
  auth($('#login'));
  modal();
});
