@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif


@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal')) }}



<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit Seo')</legend>

    <!--
    <div class="control-group">
        <div class="control-label">alias<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="alias" id="alias" value="e">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">url<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="url" id="url" value="@if(isset($item->url)){{$item->url}}@else{{ Form::old('url')}}@endif">
            </div>
        </div>
    </div>
    -->

    <div class="control-group">
        <div class="control-label">route<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="route" id="route" value="@if(isset($item->route)){{$item->route}}@else{{ Form::old('route')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">priority<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="priority" id="priority" value="@if(isset($item->priority)){{$item->priority}}@else{{ Form::old('priority')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" style="width:600px;"  name="title" id="title" value="@if(isset($item->title)){{$item->title}}@else{{ Form::old('title')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">description<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <textarea id="description" style="width:600px;"  name="description" cols="80" rows="4">@if(isset($item->description)){{$item->description}}@else{{Form::old('description')}}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">keywords<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <textarea id="keywords" style="width:600px;"  name="keywords" cols="80" rows="4">@if(isset($item->keywords)){{$item->keywords}}@else{{Form::old('keywords')}}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">noindex</div>
        <div class="controls">
           <div>
                <select name="noindex" id="noindex">
                    <option value="1"  @if(isset($item->noindex))  @if($item->noindex == '1') selected @endif @endif  >active</option>
                    <option value="0"  @if(isset($item->noindex))  @if($item->noindex == '0') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="controls">
        <input type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div>

</fieldset>

{{ Form::close() }}
@stop
