require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'modal': '../modules/modal',
    'profile-fb': '../modules/edit-profile-fb',
    'profile-fb-user': '../modules/edit-profile-fb-user',

    'profile-register': '../modules/profile-register',
    'profile-register-user': '../modules/profile-register-user',
    'easymodal': '../libs/plugins/jquery.easymodal'

  },
  shim: {
    'modal': ['jquery'],
    'profile-fb': ['jquery'],
    'profile-fb-user': ['jquery'],
    'easymodal': ['jquery'],
    'profile-register': ['jquery'],
    'profile-register-user': ['jquery'],
    'foundation': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'easymodal',
  'profile-fb',
  'foundation'
],
function ( $, modal, easymodal, editProfileFb ) {
  modal();
  var $profile_images = $('.modal-profile-images');

  if ( $profile_images.length ) {
    $profile_images.easyModal({
      autoOpen: true,
      closeButtonClass: '.close-modal'

    });
  }

  $(document).foundation();

  editProfileFb($('#profile_edit'));

  $('a.fb-avatar').click(function(event){

    event.preventDefault();
    var $target = $(this);
    var $form = $('#form-user-image-src');
    var $input = $form.find('.user-image-src');

    var src = $target.attr('data-user-image-src');
    $input.val(src);
    $form.submit();
  });

  $('.cover-link').on('click', updateCover);
  $('.btn-send-cover').on('click', sendCover);

  $('.btn-edit-pc').on('click', function(event) {
    event.preventDefault();
    $('#input-file').trigger('click');
  });

  $('#input-file').change(function() {
      $('#upload-image').submit();
  });

  $('#bgheader').click(function(event){
    event.stopPropagation();
  });

  $('.btn-edit-picture').on('click', function(event){
    event.preventDefault();
    $('#bgheader').show();
  });

  $('.btn-edit-photo').on('click', function(event){
    event.preventDefault();
    $('#buttons.select-avatar').show();
  });

  $('html').on('click', function(event){
    var $target = $(event.target);

    if ( !$target.is('.btn-edit-picture, .icon-picture') ) {
      $('#bgheader').hide();
    }
    if ( !$target.is('.btn-edit-photo, .icon-pencil') ) {
      $('#buttons.select-avatar').hide();
    }
  });

  function updateCover(event){
    event.preventDefault();
    var $el = $(this);
    var id = $el.attr('data-cover-id');
    var $send_cover = $('.btn-send-cover');
    var $bg = $('.bg-profile');
    var src = $bg.attr('src').replace(/cover-(\d+).jpg/g, 'cover-' + id + '.jpg');
    $bg.attr('src', src);

    $send_cover.attr('data-cover-id', id);
  }

  function sendCover(event){
    event.preventDefault();
    var $el = $(this);
    $('#bgheader').hide();

    $.ajax({
      type: 'post',
      data:{
        cover_id: $el.attr('data-cover-id'),
        _token: $('#bgheader #_token').val()
      },
      url: app.user_update_cover
    }).done(function(response){

      if( response.status_code === 0 ) {

      }

    });

  }

});
