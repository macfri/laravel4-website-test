    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos-detalle'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games_detail', $item->slug) }}">{{ $item->title }}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="game-content">
          <div id="game-vars">
            <form>
              <input type="hidden" value="{{ $item->id }}" name="game_id">
              <input type="hidden" value="@if($token_rank) {{$token_rank}} @else null @endif" name="token_rank" id="token_rank">
              <input type="hidden" value="" name="face_id" id="face_id">
            </form>
            <script>
              window.game = window.game || {};
              game.gamerank_url = "{{ URL::route('games_rank') }}";

            </script>
          </div>
          <div class="row">
            <div class="columns large-12 pt-27">
              @if(!Auth::check())
              <!-- <div class="alert-bar"><i class="icon-warning left"></i>
                <p>
                  ¡Espera! Estás jugando sin entrar a tu cuenta. Recuerda que si quieres grabar tus puntos,
                  recolectar “logros” y entrar a los sorteos que realicemos, debes estar conectado. Si aún no tienes cuenta,  <a href="#" data-reveal-id="register-modal" onclick="_gaq.push(['_trackEvent','Web-Conversion', 'Paso1-Iniciar', 'juegos-detalle'])">regístrate aquí</a>
                </p><a class="close-alert right"><i class="icon-close-game"></i></a>
              </div> -->
              @endif

              <?php
                /*
              <div class="badget hide">
                <div class="left top-arrow-game"> <span>

                    SUPERA LOS RETOS<br>Y SUBE TU NIVEL</span></div>
                <h3 class="title-game">{{ $item->title }}</h3>
                <div class="cont-badget">@foreach($badges as $item2)<a class="left">
                    <div class="info-badge"><img src="{{ asset($item2->path) }}" class="left @if(!$item2->active)inactive@endif trophies-{{ $item2->id }}"><span class="left"><b>{{ $item2->title }}</b>
                        <p>{{ $item2->description }}</p></span></div></a>@endforeach
                  <div class="right top-detail-game">
                    <div>TU MAYOR PUNTAJE<b>{{ $max_score }}</b></div>
                  </div>
                </div>
              </div>

                */
                ?>
              <div class="@if($item->slug === 'tu-gran-dia') cont-game @else cont-game-full @endif">@if($item->slug === 'tu-gran-dia')@endif
                <div class="cont-messages js-game-modal hide">
                  <div class="int-msj-modal chulls-modal">
                    <div class="chulls-stick"></div>
                    <div class="chulls-date"></div>
                    <a class="btn-register-foot btn-color-green js-trigger-register">registrate para participar</a>

                    <p class="line-middle">o</p>

                    <a class="btn-register-foot js-trigger-login">inicia sesión para participar</a>

                    <a href="{{ URL::route('games') }}" class="btn-register-foot">ver otros juegos</a>
                  </div>
                </div>{{ $game_content }}
              </div>@if($item->id==6)
              <div class="rd">R.D.Nº 4801-2013-ONAGI-DGAE</div>@endif
              @if($item->image_ctrl_id)
              <div class="bg-controls">
                <h3>controles</h3>
                <div class="int-controls"><img src="{{ asset($item->image_ctrl->path) }}"></div>
              </div>@endif
              <div class="rate-game custom-game">
                <div class="left">
                  <h3>rankea el juego</h3>
                  <ul class="star">
                    <li class="face-rank face1"></li>
                    <li class="face-rank face2"></li>
                    <li class="face-rank face3"></li>
                    <li class="face-rank face4"></li>
                    <li class="face-rank face5"></li>
                  </ul>
                </div>
                <div class="right social-icons">
                  <h3>Comparte <small>a TUS PATAS</small></h3>
                  <ul>
                    <li><a onclick="feedDialog2(); _gaq.push(['_trackEvent','Web-Informativo', 'Compartir-Puntaje', ''])" class="shape-facebook"></a></li>
                    <li><a onclick="popTwitter('{{$item->title}}'); _gaq.push(['_trackEvent','Web-Informativo', 'Publicar-Puntaje', ''])" class="shape-twitter"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row ratings comenta-12">
            <div class="columns large-12 cont-face">
              <h3>comenta</h3>
              <div data-colorscheme="light" data-width="750" data-href="{{ URL::route('games_detail', $item->slug) }}" class="fb-comments"></div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>

      <div id="terms" class="reveal-modal">
        <div class="terms">

          <a class="btn-close-modal" onClick="closeTerms();"><i>&nbsp;</i></a>


            <div class="p20">

              <h1>DATOS DE LA PROMOCIÓN COMERCIAL</h1>
              <div class="cont-scroll">
                <div class="scrollbar">
                  <div class="track">
                    <div class="thumb">
                      <div class="end"></div>
                    </div>
                  </div>
                </div>
                <div class="viewport">
                  <div class="overview">

                    <p>1.  Denominación de la promoción: “El Chulls Navideño”</p>
                    <p>2.  Ámbito que abarca la promoción:
                      <br> Nacional    Dptal.
                      <br> Provincial Otro: LIMA METROPLITANA</p>
                    <p>3. Duración de la Promoción: <br>
                      Fecha de inicio: 13 de Diciembre de 2013<br>
                      Fecha de finalización: 27 de Diciembre de 2013</p>
                    <p>4. Mecanismo y condiciones de la Promoción:</p>
                    <p>El juego consiste en un sólo nivel y es necesario que el usuario complete todos los datos y permisos de manera exitosa para que cuente como una jugada/oportunidad.<br>
                      Primero, el usuario deberá encontrar la lata de MILO  solicitada en 3 oportunidades. Él tendrá que hacer clic en las cajas de regalos visualizados para que pueda ver el contenido en cada uno.  Por programación, el usuario SIEMPRE encontrará las latas al tercer clic.<br>
                      Luego de esto, el usuario tendrá que seleccionar sólo 3 regalos. Un regalo de cada grupo propuesto. <br>
                      Los grupos de regalos propuestos para el usuario son los siguientes: </p>
                    <p>GRUPO  Techie <br>
                      Toshiba - Notebook Satellite B40-ASP4202KL<br>
                      Samsung  Tablet Galaxy Tab 3 <br>
                      Consolas PS Vita <br>
                      Fujifilm Cámara fotográfica <br>
                      Cámara Go-Pro </p>
                    <p>GRUPO  Deporte<br>
                      Hydro – Bodyboard <br>
                      Patines Urbanos L.A <br>
                      Skate san Cruz <br>
                      Oxford Bicicleta </p>
                    <p>GRUPO   Accesorios <br>
                      Audífonos Skullcandy <br>
                      Lentes DGK <br>
                      Gifcard de S/. 160 Dunkelvolk<br>
                      Gifcard de S/. 160 Do It</p>
                    <p>Al finalizar exitosamente el juego, y habiendo superado el proceso de registro, el usuario entra automáticamente al sorteo de los 03 regalos que haya elegido. </p>
                    <p>De otro lado, en este concurso deberá observarse las siguientes reglas:</p>
                    <p>El sorteo se hará en base a todas las personas debidamente inscritas en la web de Milo Perú, www.milo.com.pe. Se asumirá que todos los datos son datos reales.<br>
                      Se desestimará como participantes del concurso a usuarios ficticios, usuarios con sobrenombres, o cualquiera que intente transgredir las reglas dispuestas para este concurso.<br>
                      La titularidad del premio no podrá cederse a otro título o persona. <br>
                    </p>
                    <p>Premios:</p>
                    <p>Los premios dentro de los cuales podrán escoger sus premios los 3 ganadores, han   sido divididos en tres categorías: Grupo Techie, Grupo Deporte y Grupo Accesorios, siendo los mismos los siguientes:<br>
                    </p>
                    <p>GRUPO  Techie <br>
                      Toshiba - Notebook Satellite B40-ASP4202KL<br>
                      Samsung  Tablet Galaxy Tab 3 <br>
                      Consolas PS Vita <br>
                      Fujifilm Cámara fotográfica <br>
                      Cámara Go-Pro </p>
                    <p>GRUPO  Deporte<br>
                      Hydro – Bodyboard <br>
                      Patines Urbanos L.A <br>
                      Skate san Cruz <br>
                      Oxford Bicicleta </p>
                    <p>GRUPO   Accesorios <br>
                      Audífonos Skullcandy <br>
                      Lentes DGK <br>
                      Gifcard de S/. 160 Dunkelvolk<br>
                      Gifcard de S/. 160 Do It<br>
                      Nota: Cada uno de los 3 ganadores podrá escoger un premio de cada uno de los grupos   mencionados.</p>
                    <p>6. Sorteo:</p>
                    <p>El sorteo se realizará el 27 de diciembre de 2013, a las 17.00 horas, en nuestras oficinas de la Av. Federico Villarreal Nº 128, Miraflores. La modalidad del sorteo consiste en la elección al azar de un ganador. Esto se realizará utilizando un listado de todos los participantes en una hoja de cálculo Excel. Todos los usuarios listados en la hoja tendrán un número correlativo que los acompaña. Para seleccionar a los ganadores en forma aleatoria se utilizará la función de Excel &quot;random&quot; o &quot;aleatorio entre&quot; que permite seleccionar un número al azar. Este número, corresponde al correlativo que acompaña a cada entrada de la hoja.</p>
                    <p>El mismo día a la fecha del sorteo, los 03 ganadores serán notificados por correo electrónico y al número telefónico con la información de contacto que éste hubiere proporcionado al inscribirse en el Concurso. Además, cada ganador será publicado en la página de Facebook (MILO PERÚ) y en la web www.milo.com.pe; se comunicará un ganador por día.<br>
                    </p>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos-detalle'))
    @include('site.register-model')
<script data-main="{{ asset('static/site/scripts/site/games') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>

  <script type="text/javascript">

    function modalTerms() {
      $("#terms").foundation('reveal', 'open');
      $(".cont-scroll").tinyscrollbar();

    }

    function closeTerms() {
      $(".btn-close-modal").foundation('reveal', 'close')
    }

    function feedDialog2(){
      FB.ui({
        method: 'feed',
        link: 'https://www.milo.com.pe/juegos/chulls',
        picture: 'https://www.milo.com.pe/static/site/images/icon-250x250.jpg',
        name: '¡El Chulls te hace la Navidad!',
        description: 'La Navidad Llegó y el Chulls te trae unos regalazos con MILO. ¡Entra aquí y elige los tuyos!'
      }, function ( response ) {});
    }

  </script>

  </body>
</html>
