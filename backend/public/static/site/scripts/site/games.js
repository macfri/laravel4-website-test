require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'tinyscroll':['../libs/plugins/jquery.tinyscrollbar'],

    'modal': '../modules/modal',
    'closeAlert': '../modules/closeAlert',
    'acordeon': '../modules/acordeon',
    'register': '../modules/register',
    'basicregister': '../modules/proile-basic-register',
    'auth': '../modules/auth',
    'slider': '../modules/slider',
    'gamerank': '../modules/gamerank'
  },
  shim: {
    'modal': ['jquery'],
    'closeAlert': ['jquery'],
    'acordeon': ['jquery'],
    'register': ['jquery'],
    'basicregister': ['jquery'],
    'auth': ['jquery'],
    'foundation' : ['jquery'],
    'slider' : ['foundation'],
    'gamerank': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'closeAlert',
  'acordeon',
  'register',
  'basicregister',
  'auth',
  'slider',
  'gamerank'
],
function ($, modal, closeAlert, acordeon, register, basicregister, auth, slider, gameRank) {
  modal();
  closeAlert();
  acordeon($('.cont-badget a'));
  register($('#register'));
  basicregister.setupRegisterForm({
                                    form: '#basic-register'
                                  });
  auth($('#login'));
  gameRank($('.rate-game'), $('#game-vars form'));
  slider();

  $('.btn-close-x').on('click', function(event){
    event.preventDefault();
    var $target = $(this);
    $target.parent().hide(400).remove();

  });

});
