    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'historia'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('history') }}">Historia</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"><img src="{{ asset('static/site/images/banners/history.png') }}" style="background:#C6A575">
          <h3>Historia</h3>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <div data-magellan-expedition="fixed" class="cont-filter">
          <div class="cont-center">
            <div class="left"><a href="{{ URL::route('home') }}" class="btn-logo hide"><span class="logo-fixed"></span></a></div>
            <div class="cont-buttons center"><a data-magellan-arrival='1' href="#1" class="btn-next bubble">1919</a><a data-magellan-arrival='2' href="#2" class="btn-next bubble">1934</a><a data-magellan-arrival='3' href="#3" class="btn-next bubble">1940</a><a data-magellan-arrival='4' href="#4" class="btn-next bubble">1942</a><a data-magellan-arrival='5' href="#5" class="btn-next bubble">1946</a><a data-magellan-arrival='6' href="#6" class="btn-next bubble">1949</a><a data-magellan-arrival='7' href="#7" class="btn-next bubble">2005</a><a data-magellan-arrival='8' href="#8" class="btn-next bubble">2010</a><a data-magellan-arrival='9' href="#9" class="btn-next bubble">AHORA</a>
            </div><a href="#" class="btn-top"><span>Arriba</span></a>
          </div>
        </div>
      </div>
      <div class="content">
        <div class="history">
          <div id="1" data-magellan-destination="1" class="history-01 item"></div>
          <div id="2" data-magellan-destination="2" class="history-02 item"></div>
          <div id="3" data-magellan-destination="3" class="history-03 item"></div>
          <div id="4" data-magellan-destination="4" class="history-04 item"></div>
          <div id="5" data-magellan-destination="5" class="history-05 item"></div>
          <div id="6" data-magellan-destination="6" class="history-06 item"></div>
          <div id="7" data-magellan-destination="7" class="history-07 item"></div>
          <div id="8" data-magellan-destination="8" class="history-08 item"></div>
          <div id="9" data-magellan-destination="9" class="history-09 item"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'historia'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'historia'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/history') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
