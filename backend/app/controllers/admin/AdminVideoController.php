<?php

class AdminVideoController extends BaseController {

    private $viewpath = 'admin/video/';

    public function index() {
        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = Video::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'items2' => Video::all()
                        )
        );
    }

    public function add() {
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => array(),
                        )
        );
    }

    public function edit($id) {
        $item = Video::find($id);
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {
        $v = Video::validate(Input::all());
        if ($v->passes()) {
            try {
                $item = Video::create(array(
                            'title' => Input::get('title'),
                            'slug' => Video::get_slug(Input::get('title')),
                            'description' => Input::get('description'),
                            'status' => 'active',
                            'author_id' => 1,
                            'block_size' => Input::get('block_size'),
                            'show_home' => (bool) Input::get('show_home')
                ));
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_video_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'videos');

                $photo = Photo::create(array(
                            'title' => $name,
                            'extension' => $ext,
                            'size' => $size,
                            'mime' => $mime
                ));

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'videos', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                $item->image_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_video_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'videos');

                $image = Photo::create(array(
                            'title' => $name,
                            'extension' => $ext,
                            'size' => $size,
                            'mime' => $mime,
                ));

                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'videos', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $image->id, $ext)
                );

                $item->image_home_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_video_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            return Redirect::route('admin_video')
                            ->with('success', sprintf(
                                            'Video %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {
        $v = Video::validate(Input::all());
        if ($v->passes()) {

            $item = Video::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->show_home = Input::get('show_home');
            $item->block_size = Input::get('block_size');
            $item->status = Input::get('status');
            $item->author_id = 1;

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_event_edit', $id)
                                ->with('failure', $exception->getMessage());
            }


            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'videos');

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_video_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'videos', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );


                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_video_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'videos');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_video_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'videos', $image->id, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%d.%s', $image->id, $ext)
                );

                if (isset($is_new)) {
                    $item->image_home_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_video_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            return Redirect::route('admin_video')
                            ->with('success', 'Video Updated Successfully.');
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function destroy($id) {
        Video::find($id)->delete();

        return Redirect::route('admin_video')
                        ->with('success', 'Video Deleted Successfully.');
    }

}
