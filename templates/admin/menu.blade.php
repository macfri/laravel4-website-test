{{-- */ $i=class_basename(Route::getCurrentRoute()->getPath()) /* --}}

{{-- */ $t=Route::getCurrentRoute()->getPath() /* --}}

<!--
{{ $i }}
<br />
{{ $t }}
<br />
{{ var_dump(strpos($t, '/admin/games/')) }}
-->

<ul class="nav nav-list">  

        @if(SystemUser::has_perm('sliders-list'))
        <li class="nav-header">Home</li>  
            <li class="@if(strpos($t, '/admin/sliders') !==false ) active @endif"><a href="{{ url::route('admin_slider') }}"><i class="icon-home"></i>Sliders</a></li>  
        @endif

        @if(SystemUser::has_perm('events-list') or SystemUser::has_perm('spots-list')  or SystemUser::has_perm('products-list')  )
        <li class="nav-header">Sections</li>  
            @if(SystemUser::has_perm('events-list'))
            <li class="@if(strpos($t, '/admin/events') !==false ) active @endif"><a href="{{ url::route('admin_event') }}"><i class="icon-pencil"></i>Events</a></li>
            @endif
            @if(SystemUser::has_perm('spots-list'))
            <li class="@if(strpos($t, '/admin/spots') !==false ) active @endif"><a href="{{ url::route('admin_spot') }}"><i class="icon-star"></i>Spots tv</a></li>
            @endif
            @if(SystemUser::has_perm('products-list'))
            <li class="@if(strpos($t, '/admin/products') !==false ) active @endif"><a href="{{ url::route('admin_product') }}"><i class="icon-book"></i>Products</a></li>  
            @endif
        @endif

        @if(SystemUser::has_perm('memes-list') or SystemUser::has_perm('memes-default-list'))
        <li class="nav-header">Memes</li>  
            @if(SystemUser::has_perm('memes-list'))
            <li class="@if($i == 'memes') active @endif"><a href="{{ url::route('admin_meme') }}"><i class="icon-book"></i>Memes</a></li>  
            @endif
            @if(SystemUser::has_perm('memes-default-list'))
            <li class="@if($i == 'memes_default') active @endif"><a href="{{ url::route('admin_meme_default') }}"><i class="icon-cog"></i>Memes default</a></li>  
            @endif
        @endif

        @if(SystemUser::has_perm('games-list') or SystemUser::has_perm('badges-list'))
        <li class="nav-header">Games</li>  
            @if(SystemUser::has_perm('games-list'))
            <li class="@if( strpos($t, '/admin/games') !==false ) active @endif"><a href="{{ url::route('admin_game') }}"><i class="icon-book"></i>Games</a></li>  
            @endif
            @if(SystemUser::has_perm('badges-list'))
            <li class="@if(strpos($t, '/admin/badges') !==false ) active @endif"><a href="{{ url::route('admin_badge') }}"><i class="icon-star"></i>badges</a></li>  
            @endif
        @endif

        @if(SystemUser::has_perm('users-list'))
        <li class="nav-header">users</li>  
            <li class="@if(strpos($t, '/admin/users') !==false ) active @endif"><a href="{{ url::route('admin_user') }}"><i class="icon-user"></i>users</a></li>  
        @endif

        @if(SystemUser::has_perm('system-users-list') or SystemUser::has_perm('roles-list')  or SystemUser::has_perm('permissions-list')  )
        <li class="nav-header">System Users</li>  
            @if(SystemUser::has_perm('system-users-list'))
            <li class="@if(strpos($t, '/admin/system_users') !==false ) active @endif"><a href="{{ url::route('admin_system_user') }}"><i class="icon-pencil"></i>System Users</a></li>
            @endif
            @if(SystemUser::has_perm('roles-list'))
            <li class="@if(strpos($t, '/admin/roles') !==false ) active @endif"><a href="{{ url::route('admin_role') }}"><i class="icon-pencil"></i>Roles</a></li>
            @endif
            @if(SystemUser::has_perm('permissions-list'))
            <li class="@if(strpos($t, '/admin/permissions') !==false ) active @endif"><a href="{{ url::route('admin_permission') }}"><i class="icon-pencil"></i>Permissions</a></li>
            @endif    
        @endif

        @if(SystemUser::has_perm('seo-list'))
        <li class="nav-header">seo</li>  
            <li class="@if($i=='seo') active @endif"><a href="{{ url::route('admin_seo') }}"><i class="icon-user"></i>seo</a></li>  
        @endif

</ul>  



<ul class="nav pull-right"></ul>
