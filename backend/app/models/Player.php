<?php

class Player extends Eloquent {

    protected $table = 'game_players';
    protected $guarded = array('id');
    protected $fillable = array(
        'game_id',
        'user_id',
        'total_score',
        'max_score',
        'was_guest',
        'attempts',
        'trophies'
    );

    public function user() {
        return $this->belongsTo('User');
    }

}
