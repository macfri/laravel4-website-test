    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'editar-password'))
        <!-- MENU -->
        </div>

        <div class="banner header-profile">
        <!-- HEADER PROFILE -->
        @include('site.profile.menu')
        <!-- HEADER PROFILE -->
        </div>
       </div>
    
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="row edit-profile-fb">
          <h2>EDITAR PERFIL</h2>
          <div data-section="tabs" class="section-container tabs">
            <div class="section">
              <p class="title number-1"><a href="{{ URL::route('profile_edit') }}" class="profile-link-tab">Cuenta</a></p>
            </div>
            <div class="section active">
              <p class="title number-2"><a href="{{ URL::route('profile_change_password') }}" class="profile-link-tab">Contraseña</a></p>
              <div class="content">

                @if($errors->has())
                    @foreach($errors->all() as $m)
                    <p class="alert">{{ $m }}</p>
                    @endforeach
                @endif

                @if(Session::has('message'))
                <p style="background: red; color: #fff; height: 25px">{{ Session::get('message') }}</p>
                @endif

                <h3>datos de cuentas</h3>
                <p>Con estos datos podras iniciar sesion en nuestra web</p>
                <form id="profile_user_fb" method="post" action="" class="custom">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

                  @if(Auth::user()->password)
                  <div class="row">
                    <div class="columns large-6">
                      <label>Contraseña anterior:</label>
                      <input type="password" name="old_password" id="old_password">
                    </div>
                    <div class="columns large-6 end"><span class="text-help"></span></div>
                  </div>
                @endif

                  <div class="row">
                    <div class="columns large-6">
                      <label>Nueva Contraseña:</label>
                      <input type="password" name="password" id="password">
                    </div>
                    <div class="columns large-6 end"><span class="text-help">
                        Debe de tener entre 8-20 caracteres.
                        No le digas a nadie tu contraseña!</span></div>
                  </div>
                  <div class="row">
                    <div class="columns large-6">
                      <label>Vuelva a escribir la nueva contraseña</label>
                      <input type="password" name="repassword" id="repassword">
                    </div>
                    <div class="columns large-6 end"><span class="text-help">Vuelve a escribir tu contraseña</span></div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="columns large-10">
                      <p>Para poder crear tu cuenta en Milo, necesitamos que aceptes los siguientes Términos</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="columns large-12">
                      <label class="check">
                        <input type="checkbox" name="tyc" id="tyc" value="1" checked>
                        <p>He leido y acepto los <a href="{{ URL::route('terms') }}" onclick="_gaq.push(['_trackEvent','Web-Informativo', 'Ver-TyC', ''])" class="lnk-tyc">Términos y Condiciones</a></p>
                      </label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="columns large-12">
                      <button class="right btn-continue">GUARDAR</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'editar-password'))
        <!-- FOOTER -->
      </div>
    </div>

    <script>
      window.app = window.app || {};
      app.user_update_cover = "{{ URL::route('user_update_cover') }}";
      app.validate_profile_email = "{{ URL::route('validate_form_email') }}";
      app.validate_profile_dni = "{{ URL::route('validate_form_dni') }}";
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/profile') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
