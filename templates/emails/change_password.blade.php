<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <table width="600" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td height="41">
      <font face="Arial, Helvetica, sans-serif" color="#005710">
      <p style="line-height:12px; text-align:center;font-size:10px;display:block;margin:auto;color:#005710">Si este mensaje no se muestra correctamente, revísalo <a href="" style="color:#0E2C95">aquí</a></p>
    </font></td>
  </tr>
  <tr>
    <td><img src="{{ asset('static/site/images/head-mailing.jpg') }}" width="600" height="138" alt="" style="display:block;"></td>
  </tr>
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="76"><img src="{{ asset('static/site/images/left-mail.jpg') }}" width="76" height="264" alt="" style="display:block;"></td>
        <td width="447" align="center" valign="middle" style="line-height:1">
          <h2><font color="#005710" face="Arial, Helvetica, sans-serif" style="text-transform:uppercase"><strong>Hola {{ $first_name }}</strong></font></h2>
          <p style="width:358px;text-align:left;font-size:14px;padding-top:0;line-height:18px"><font face="Arial, Helvetica, sans-serif" color="#006633">Puedes entrar <a href="{{ $url }}" style="color:#123784">aqu&iacute;</a> para cambiar tu contraseña. Busca una contraseña que sea fácil de recordar. Si olvidas la nueva, no te preocupes, nosotros no te juzgaremos. </font></p>
        </td>
        <td width="76"><img src="{{ asset('static/site/images/right-mail.jpg') }}" width="77" height="264" alt="" style="display:block;"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="{{ asset('static/site/images/mail-bottom.jpg') }}" width="600" height="179" alt="" style="display:block;"></td>
  </tr>
  <tr>
    <td height="65"><font face="Arial, Helvetica, sans-serif" color="#005710">
      <p style="line-height:12px; text-align:center;font-size:10px;width:464px;display:block;margin:auto">Este es un correo electrónico enviado por un servicio automatizado, por favor NO RESPONDA, NI ENVÍE CORRESPONDENCIA a este correo, ya que no será respondido. El presente mensaje le será enviado según lo solicitado en el momento  de su registro como usuario en la web de Milo, www.milo.com.pe. Para cancelar este servicio <a href="" style="color:#0E2C95">clic aquí</a></p></font></td>
  </tr>
</table>

</body>
</html>

