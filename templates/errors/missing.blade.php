<!DOCTYPE html>
<!--[if lt IE 7]>
<html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"></html>
<![endif]-->
<!--[if IE 7]>
<html lang="es" class="no-js lt-ie9 lt-ie8"></html>
<![endif]-->
<!--[if IE 8]>
<html lang="es" class="no-js lt-ie9"></html>
<![endif]-->
<!--[if gt IE 8]><!-->
<html lang="es">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Milo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('static/site/styles/main.css') }}">
  </head>
  <body>
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
          <div class="nav-side">
            <div class="inner row">
              <div class="columns small-10">
                <ul>
                  <li><a href="http://www.nestle.com/" target="_blank">nestlé.com</a></li>
                  <li><span class="sep"></span></li>
                  <li><a href="{{ URL::route('history') }}">Historia</a></li>
                  <li></li>
                  <li><span class="sep"></span></li>
                  <li><a href="{{ URL::route('spots') }}">Spots de TV</a></li>
                  <li></li>
                  <li><span class="sep"></span></li>
                  <li><a href="{{ URL::route('contact') }}">Contáctanos</a></li>
                  <li></li>
                </ul>
              </div>
              <div class="columns small-2">
                <div class="social-icons right"><a href="https://www.facebook.com/MiloPeru" target="_blank" class="icon-facebook"></a>
                  <div class="div"></div><a href="http://www.youtube.com/channel/UC054ysyX5RH2FF7UHKIcXKw/feed" target="_blank" class="icon-youtube"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="nav-main">
            <div class="inner row">
              <div class="columns large-8 patt-left"> <a href="{{ URL::route('home') }}" class="logo">
                  <h1>Milo</h1></a>
                <ul>
                  <li><a href="{{ URL::route('games') }}">Juegos</a></li>
                  <li><a href="{{ URL::route('events') }}">Eventos</a></li>
                  <li><a href="{{ URL::route('products') }}">Productos</a></li>
                </ul>
              </div>
              <div class="columns large-4 patt-right"><a href="#href" class="btn-register">Regístrate</a><a href="{{ asset('static/site/user_login') }}" data-modal="login" class="btn-login">Inicia Sesión </a></div>
            </div>
          </div>
        </div>
        <div class="orbit">
          <ul data-orbit>
            <li><img src="{{ asset('static/site/images/banner.jpg') }}">
              <div class="caption">
                <h2>ERROR 404 NOT FOUND</h2>
                <p>El día está lleno de retos… ¡Toma Milo todas las mañanas y la victoria será tuya!</p><a class="btn-slider">Ingresar</a>
              </div>
            </li>

          </ul>
        </div>
        <div class="nav-fixed">
          <div class="inner row">
            <ul>
              <li data-filter="productos" class="selected"><a href="{{ asset('static/site/products') }}"><i class="check-on"></i><span>Productos</span></a></li>
              <li data-filter="xtreme" class="selected"><a href="{{ asset('static/site/xtreme') }}"><i class="check-on"></i><span>Xtreme</span></a></li>
              <li data-filter="memes" class="selected"><a href="{{ asset('static/site/memes') }}"><i class="check-on"></i><span>Memes</span></a></li>
              <li data-filter="videos" class="selected"><a href="{{ asset('static/site/videos') }}"><i class="check-on"></i><span>Videos</span></a></li>
              <li data-filter="juegos" class="selected"><a href="{{ asset('static/site/games') }}"><i class="check-on"></i><span>Juegos</span></a></li>
              <li data-filter="eventos" class="selected"><a href="{{ asset('static/site/events') }}"><i class="check-on"></i><span>Eventos</span></a></li>
            </ul><a href="#" class="btn-top"><span>Arriba</span></a>
          </div>
        </div>
      </div>
    </div>
       <div class="footer">
        <div class="footer-degree"></div>
        <div class="footer-main">
          <div class="inner row">
            <div class="columns small-4 none">
              <h2>MILO</h2>
              <ul>
                <li><a href="#">Términos y Condiciones</a></li>
                <li><a href="#">Politicas de privacidad</a></li>
                <li><a href="#">Información para Padres</a></li>
                <li><a href="contact">Contáctanos</a></li>
              </ul>
            </div>
            <div class="columns small-4 footer-social">
              <h2>Conéctate</h2>
              <ul>
                <li><a href="https://www.facebook.com/MiloPeru" target="_blank" class="facebook">Facebook</a></li>
                <li><a href="http://www.youtube.com/channel/UC054ysyX5RH2FF7UHKIcXKw/feed" target="_blank" class="youtube">Youtube</a></li>
              </ul>
            </div>
            <div class="columns small-4">
              <h2>Regístrate</h2>
              <p>Estamos llenos de cosas chéveres para ti. Inscríbete aquí y no te pierdas de nada. ¡Es al toque!</p><a href="#href" class="btn-register-foot">Ingresa</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-wrap hide">
      <div class="modal-shadow"></div>
      <div class="modal-login hide">
        <div class="row modal-session myform">
          <div class="columns large-8 large-centered">
            <form id="login" action="{{ URL::route('user_dologin') }}" method="post">
              <h3>Inicia Sesión</h3><small>Conéctate con una red social</small>
              <div class="row">
                <div class="columns large-6"><a href="{{ URL::route('user_login_facebook') }}" class="btn-facebook">Conéctate con Facebook</a></div>
                <div class="columns large-6"><a href="{{ URL::route('user_login_google') }}" class="btn-google">Conéctate con Google</a></div>
              </div>
              <hr>
              <div class="row">
                <div class="columns large-12"><small>Inicia sesión con tu usuario y tu contraseña</small>
                  <label for="email">Correo</label>
                  <input type="text" name="email" id="email">
                  <label for="password">Contraseña</label>
                  <input type="password" name="password" id="password">
                  <p class="red">
                    @if($errors->has())
                    {{ $errors->first('message') }}
                    @endif
                  </p>
                  <button type="submit" class="btn-login">Inicia Sesión</button><a class="link-bottom">No puedo ingresar</a>
                </div>
              </div>
            </form>
            <div class="mb-50"></div>
          </div>
        </div>
      </div>
      <div class="modal-register hide">
        <div class="row modal-session myform">
          <div class="columns large-8 large-centered">
            <form id="register" action="{{ URL::route('user_add_simple') }}" method="post">
              <h3>Sé parte de Milo</h3>
              <div class="row">
                <div class="columns large-6">
                  <label for="first_name">Tu Nombre</label>
                  <input type="text" name="first_name" id="first_name">
                </div>
                <div class="columns large-6">
                  <label for="last-name">Tu Nombre</label>
                  <input type="text" name="last_name" id="last_name">
                </div>
              </div>
              <div class="row">
                <div class="columns">
                  <label>Correo electrónico</label>
                  <input type="text" name="email" id="email">
                  <label>Contraseña</label>
                  <input type="password" name="password" id="password">
                  <button type="submit" class="btn-login">Registrate</button>
                  <p class="red">@foreach($errors->all() as $message)
                    <li>{{ $message }}</li>@endforeach
                  </p><a class="grey">Regresar</a>
                </div>
              </div>
            </form>
          </div>
          <div class="mb-50"></div>
        </div>
        <div class="mb-50"></div>
      </div>
    </div>
    <script data-main="{{ asset('static/site/scripts/site/home') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>

