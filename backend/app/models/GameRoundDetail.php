<?php

class GameRoundDetail extends Eloquent {

    protected $table = 'game_games_round_detail';

    public static function validate($input) {
        $rules = array(
            'score' => 'Required',
        );
        return Validator::make($input, $rules);
    }

    public function user() {
        return $this->belongsTo('User');
    }

}
