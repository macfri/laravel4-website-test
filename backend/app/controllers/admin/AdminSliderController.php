<?php

class AdminSliderController extends BaseController {

    private $viewpath = 'admin/slider/';

    public function index() {

        if (!SystemUser::has_perm('sliders-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = Slider::where('id', '>', 0);

        if (strlen($search_title) > 0) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status) > 0) {
            $items->where('status', '=', $search_status);
        }

        $items = $items->orderby('order', 'asc');

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'items2' => Slider::all()
                        )
        );
    }

    public function add() {
        if (!SystemUser::has_perm('sliders-new')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        return View::make(
                        $this->viewpath . __function__);
    }

    public function edit($id) {
        if (!SystemUser::has_perm('sliders-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Slider::find($id);
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {
        if (!SystemUser::has_perm('sliders-new')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Slider::validate(Input::all());
        if ($v->passes()) {
            $order = (int) Input::get('order');
            $item = new Slider();
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->backgroung_color = Input::get('backgroung_color');
            $item->button_backgroung_color = Input::get('button_backgroung_color');
            $item->button_title = Input::get('button_title');
            $item->link = Input::get('link');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->order = $order;
            $item->category = Input::get('category');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_slider_add')
                                ->with('failure', $exception->getMessage());
            }

            order_table_new('website_sliders', $order, $item->id);

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'sliders');

                $image = new Photo;
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'sliders', $item->id, $ext
                );
                $image->save();

                Input::file('photo')->move(
                        $upload_path, sprintf('%s.%s', $item->id, $ext)
                );

                $item->image_id = $image->id;
                $item->save();
            }

            return Redirect::route('admin_slider')
                            ->with('success', 'Slider Created Successfully.');
        } else {
            return Redirect::back()
                            ->withErrors($v)
                            ->withInput();
        }
    }

    public function update($id) {
        if (!SystemUser::has_perm('sliders-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Slider::validate(Input::all());
        if ($v->passes()) {

            $order = (int) Input::get('order');
            $item = Slider::find($id);
            $order_old = $item->order;

            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->backgroung_color = Input::get('backgroung_color');
            $item->button_backgroung_color = Input::get('button_backgroung_color');
            $item->button_title = Input::get('button_title');
            $item->link = Input::get('link');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->order = $order;
            $item->category = Input::get('category');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_slider_edit', $id)
                                ->with('failure', $exception->getMessage());
            }

            order_table_update('website_sliders', $order_old, $order, $item->id);

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = Input::file('photo')->getClientOriginalName();
                $size = Input::file('photo')->getSize();
                $mime = Input::file('photo')->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);
                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'sliders');

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'sliders', $item->id, $ext
                );
                $image->save();
                Input::file('photo')->move(
                        $upload_path, sprintf('%s.%s', $item->id, $ext)
                );

                if (isset($is_new)) {
                    $item->image_id = $image->id;
                    $item->save();
                }
            }

            return Redirect::route('admin_slider')
                            ->with('success', 'Slider Updated Successfully.');
        } else {
            return Redirect::back()
                            ->withErrors($v)
                            ->withInput();
        }
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('sliders-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Slider::find($id);
        $order = $item->order;
        $item->delete();
        order_table_delete('website_sliders', $order);
        return Redirect::route('admin_slider')
                        ->with('success', 'Slider Deleted Successfully.');
    }

}
