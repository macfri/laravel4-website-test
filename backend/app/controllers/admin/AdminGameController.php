<?php

function get_by_total($is_new = null) {
    $sql = "SELECT count(*) as tot from(
    select t1.f, t1.user_id, DATE(u.created_at) , DATE(u.created_at) >= '2014-01-31' as is_new from 
    (select  DATE(created_at) as f, user_id from website_game_games_round order by user_id desc, f asc) as t1 inner join website_users as u on (u.id = t1.user_id)
    group by t1.user_id order by t1.user_id desc
    ) as tt";

    if (!is_null($is_new)) {
        $sql .= sprintf(" where is_new = %d", $is_new);
    }

    $c = DB::select(DB::raw($sql));
    return $c[0]->tot;
}

function get_by_day($is_new = null, $week = null) {
    $sql = "select f, count(user_id) as tot from (
    SELECT f, user_id, is_new from(
        select t1.f, t1.user_id, DATE(u.created_at) , DATE(u.created_at) >= '2014-01-31' as is_new from 
        (select  DATE(created_at) as f, user_id from website_game_games_round ";

    $sql .= " order by user_id desc, f asc) as t1 inner join website_users as u on (u.id = t1.user_id)
        group by t1.user_id order by t1.user_id desc
    ) as tt";

    if (!is_null($is_new)) {
        $sql .= sprintf(" where is_new = %d", $is_new);
    }

    $sql .= "  ) as t2 group by f order by f asc;";
    $items = DB::select(DB::raw($sql));
    return $items;
}

function awards_chulls() {
    $data_awards[1] = "Cámara Lumix 10x Full HD";
    $data_awards[2] = "Cámara Go-Pro";
    $data_awards[3] = "Lap top Samsung";
    $data_awards[4] = "PSP VITA";
    $data_awards[5] = "Galaxy Tab";
    $data_awards[6] = "Tabla Surf";
    $data_awards[7] = "Skate Cruiser Z-Flex Jimmy  Plummer";
    $data_awards[8] = "Bicicleta de hombre color zul con tapabarro modelo Jet 202013";
    $data_awards[9] = "Patines Marca Gotcha color naranja";
    $data_awards[10] = "Audífonos modelo Unisex Skullcandy";
    $data_awards[11] = "Giftcard S/. 160 Do It!";
    $data_awards[12] = "Giftcard S/. 160 Dunkelvolk";
    $data_awards[13] = "Lentes de Sol";
    return $data_awards;
}

class AdminGameController extends BaseController {

    private $viewpath = 'admin/game/';

    public function index() {

        if (!SystemUser::has_perm('games-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = Game::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'items2' => Game::all()
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('games-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }


        return View::make(
                        $this->viewpath . __function__, array()
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('games-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Game::find($id);
        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('games-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Game::validate(Input::all());
        if ($v->passes()) {

            $item = new Game();
            $item->title = Input::get('title');
            $item->slug = Game::get_slug(Input::get('title'));
            $item->description = Input::get('description');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->block_size = Input::get('block_size');
            $item->show_home = (bool) Input::get('show_home');
            $item->order = (int) Input::get('order');
            $item->path_swf = Input::get('path_swf');
            $item->seo_meta_title = Input::get('seo_meta_title');
            $item->seo_meta_description = Input::get('seo_meta_description');
            $item->seo_meta_keywords = Input::get('seo_meta_keywords');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_game_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games'
                );

                $image = New Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                $item->image_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_short')) {
                $file = Input::file('photo_short');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games'
                );

                $image = New Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s_short.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s_short.%s', $item->slug, $ext)
                );

                $item->image_short_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_cover')) {
                $file = Input::file('photo_cover');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games'
                );

                $image = New Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s_cover.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s_cover.%s', $item->slug, $ext)
                );

                $item->image_cover_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'games');

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                $item->image_home_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $item->id)
                                    ->with('failure', $exception->getMessage());
                }
            }

            return Redirect::route('admin_game')
                            ->with('success', sprintf(
                                            'Game %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {

        if (!SystemUser::has_perm('games-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Game::validate(Input::all());
        if ($v->passes()) {

            $item = Game::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->show_home = (bool) Input::get('show_home');
            $item->block_size = Input::get('block_size');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->order = (int) Input::get('order');
            $item->path_swf = Input::get('path_swf');
            $item->seo_meta_title = Input::get('seo_meta_title');
            $item->seo_meta_description = Input::get('seo_meta_description');
            $item->seo_meta_keywords = Input::get('seo_meta_keywords');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_game_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games');

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );


                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_game_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_short')) {
                $file = Input::file('photo_short');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games');

                $image = Photo::find($item->image_short_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s_short.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s_short.%s', $item->slug, $ext)
                );


                if (isset($is_new)) {
                    $item->image_short_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_game_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'games');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                if (isset($is_new)) {
                    $item->image_home_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_game_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_cover')) {
                $file = Input::file('photo_cover');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s', public_path(), 'upload', 'games');

                $image = Photo::find($item->image_cover_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s_cover.%s', 'upload', 'games', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_game_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s_cover.%s', $item->slug, $ext)
                );


                if (isset($is_new)) {
                    $item->image_cover_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_game_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            return Redirect::route('admin_game')
                            ->with('success', 'Game Updated Successfully.');
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('games-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        Game::find($id)->delete();

        return Redirect::route('admin_game')
                        ->with('success', 'Game Deleted Successfully.');
    }

    public function report($slug) {
        $data = array();
        $search_title = Input::get('search_title');
        $search_week = Input::get('search_week');
        $dp1 = Input::get('dp1');
        $dp2 = Input::get('dp2');

        if ($slug == 'chulls') {
            $items = GameChulls::join('users', 'users.id', '=', 'game_games_chulls.user_id')
                    ->where('users.id', '>', 0);


            $data['data_awards'] = awards_chulls();
            $tpl = $this->viewpath . 'report_chulls';

            $sql = "select DATE(created_at) as f, count(user_id) as tot from 
                    website_game_games_chulls group by f asc";
            $data['players'] = DB::select(DB::raw($sql));

            $fields = array(
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.email',
                'users.phone',
                'game_games_chulls.tries',
                'game_games_chulls.created_at',
            );

            if (strlen($dp1) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_games_chulls.created_at) >= "%s"', $dp1)
                );
            }

            if (strlen($dp2) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_games_chulls.created_at) <= "%s"', $dp2)
                );
            }

            $items = $items->orderby('game_games_chulls.created_at', 'asc');
        } else if ($slug == 'knockea-a-la-almohada') {
            $items = GameRound::join('users', 'users.id', '=', 'game_games_round.user_id')
                    ->leftjoin('users_facebook', 'users_facebook.id', '=', 'users.facebook_id')
                    ->where('users.id', '>', 0);
            if (strlen($search_week) > 0) {
                $items = $items->where('game_games_round.week', $search_week);
            }

            if (strlen($dp1) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_games_round.created_at) >= "%s"', $dp1)
                );
            }

            if (strlen($dp2) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_games_round.created_at) <= "%s"', $dp2)
                );
            }

            #$items = $items->orderby('game_games_round.created_at', 'asc');
            $items = $items->orderby('game_games_round.score', 'desc');

            $fields = array(
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.email',
                'users.phone',
                'users.dni',
                'game_games_round.tries',
                'game_games_round.score',
                'game_games_round.created_at',
                'game_games_round.week',
                'users_facebook.fb_id'
            );

            $data['players'] = get_by_day(1, $search_week);
            $data['players_new'] = get_by_day(1, $search_week);
            $data['players_old'] = get_by_day(0, $search_week);
            $data['players_count'] = get_by_total();
            $data['players_new_count'] = get_by_total(1);
            $data['players_old_count'] = get_by_total(0);
            $tpl = $this->viewpath . 'report_round';
        } else {
            $game = Game::where('slug', $slug)->first();
            $items = Player::join('users', 'users.id', '=', 'game_players.user_id')
                    ->where('game_players.game_id', $game->id);

            if (strlen($dp1) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_players.created_at) >= "%s"', $dp1)
                );
            }

            if (strlen($dp2) > 0) {
                $items = $items->whereRaw(
                        sprintf('DATE(website_game_players.created_at) <= "%s"', $dp2)
                );
            }

            $items = $items->orderby('game_players.created_at', 'asc');

            $tpl = $this->viewpath . 'report';

            $sql = "select DATE(created_at) as f, count(user_id) as tot from 
                    website_game_players group by f asc";
            $data['players'] = DB::select(DB::raw($sql));

            $fields = array(
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.email',
                'users.phone',
                'game_players.trophies',
                'game_players.max_score',
                'game_players.total_score',
                'game_players.attempts',
                'game_players.created_at',
                'game_players.is_new'
            );
        }

        if (strlen($search_title) > 0) {
            $items = $items->where('first_name', 'LIKE', '%' . $search_title . '%')
                    ->orWhere('last_name', 'like', '%' . $search_title . '%')
                    ->orWhere('email', 'like', '%' . $search_title . '%')
                    ->orWhere('dni', 'like', '%' . $search_title . '%');
        }

        $total = $items->count();
        $items = $items->paginate(100, $fields);

        /*
          $queries = DB::getQueryLog();
          $last_query = end($queries);
          print '<pre>';
          print_r( $queries);
          print '</pre>';
          die();
         */

        $data['items'] = $items;
        $data['total'] = $total;
        $data['slug'] = $slug;
        $data['search_title'] = $search_title;
        $data['search_week'] = $search_week;
        $data['dp1'] = $dp1;
        $data['dp2'] = $dp2;
        return View::make($tpl, $data);
    }

    public function export() {
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=say.csv');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $file = fopen("php://output", "w");
        $keys = array(
            'first_name',
            'last_name',
            'email',
            'created_at',
            'dni',
            'phone',
            'date_birth',
            'facebook_id',
            'score'
        );

        $table = GameRound::join('users', 'game_games_round.user_id', '=', 'users.id')
                ->join('users_facebook', 'users_facebook.id', '=', 'users.facebook_id')
                ->select(
                        'users.first_name', 'users.last_name', 'users.email', 'users.created_at', 'users.dni', 'users.phone', 'users.date_birth', 'users_facebook.fb_id', 'game_games_round.score'
                )->orderby('game_games_round.score', 'desc')
                ->take(100)
                ->get()
                ->toArray();

        fputcsv($file, $keys);
        foreach ($table as $row) {
            fputcsv($file, $row);
        }
        fclose($file);
    }

}
