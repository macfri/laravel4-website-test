<!DOCTYPE html>
<html>
  <head>
    <title>ADMIN-MILO-WEBSITE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap-lightbox.min.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/bootstrap/css/bootstrap-responsive.css') }}"rel="stylesheet">
        <link href="{{ asset('static/admin/css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/select2/select2.css') }}" rel="stylesheet">
        <link href="{{ asset('static/admin/css/datepicker.css') }}" rel="stylesheet">
        <link href="http://www.eyecon.ro/bootstrap-colorpicker/css/colorpicker.css"  rel="stylesheet">
  </head>
    <div class="container">
      <div class="navbar">
        <div class="navbar-inner">
          <span class="brand">ADMIN-MILO-WEBSITE</span>
             </div>
      </div>

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif


{{ Form::open(array('route' => 'admin_login')) }}


    <table class="table table-striped table-bordered model-list">
        <thead>
            <tr>
                <th>Username</th>
                <td> <input type="text" name="username" id="username" value="{{ Form::old('username') }}" /> </td>
            </tr>
        </thead>
        <tr>
            <th>Password</th>
            <th><input type="password" name="password" /></th>
        </tr>

        <tr>
            <th>&nbsp;</th>
            <td><input type="submit" class="btn btn-primary btn-large" value="Submit" /></td>
        </tr>
</table>

{{ Form::close() }}


    </div>
    </body>
