<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableMemesAuthorWebId extends Migration {

	public function up()
	{
        Schema::table('section_memes', function($t) {

            $t->integer('author_web_id')
                ->unsigned()
                ->nullable()
                ->index();
            $t->foreign('author_web_id')
                ->references('id')
                ->on('users')
                ->on_update('cascade');

        });
	}

	public function down()
	{
       Schema::table('section_memes', function($t) {
            $t->dropColumn('author_web_id');
       });
	}

}
