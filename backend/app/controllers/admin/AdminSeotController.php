<?php

class AdminSeoController extends BaseController {

    private $viewpath = 'admin/seo/';

    public function index() {

        if (!SystemUser::has_perm('seo-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = SeoRule::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $items->orderby('route');

        $total = $items->count();
        $items = $items->paginate(15);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status
                        )
        );
    }

    public function add() {

        if (!SystemUser::has_perm('seo-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        return View::make(
                        $this->viewpath . __function__
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('seo-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = SeoRule::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {

        if (!SystemUser::has_perm('seo-add')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = SeoRule::validate(Input::all());
        if ($v->passes()) {
            $item = new SeoRule();
            $item->alias = Input::get('alias', 'e');
            $item->route = Input::get('route');
            $item->url = Input::get('route');
            $item->priority = (int) Input::get('priority');
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->keywords = Input::get('keywords');
            $item->noindex = (bool) Input::get('noindex');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_seo_add')
                                ->with('failure', $exception->getMessage());
            }

            return Redirect::route('admin_seo')
                            ->with('success', sprintf(
                                            'SeoRule %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()
                            ->withErrors($v);
        }
    }

    public function update($id) {

        if (!SystemUser::has_perm('seo-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = SeoRule::validate(Input::all());

        if (!$v->passes()) {
            return Redirect::back()->withErrors($v);
        }

        $item = SeoRule::find($id);
        $item->alias = Input::get('alias');
        $item->route = Input::get('route');
        $item->url = Input::get('url');
        $item->priority = (int) Input::get('priority');
        $item->title = Input::get('title');
        $item->description = Input::get('description');
        $item->keywords = Input::get('keywords');
        $item->noindex = (bool) Input::get('noindex');

        try {
            $item->save();
        } catch (Exception $exception) {
            Log::error($exception);
            return Redirect::route('admin_seo_edit', $id)
                            ->with('failure', $exception->getMessage());
        }

        return Redirect::route('admin_seo')
                        ->with('success', sprintf(
                                        'SeoRule %s Updated Successfully.', $item->title));
    }

    public function destroy($id) {

        if (!SystemUser::has_perm('seo-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        SeoRule::find($id)->delete();
        return Redirect::back();
    }

}
