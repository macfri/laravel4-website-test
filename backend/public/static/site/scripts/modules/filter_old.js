define([
  'jquery',
  '../libs/hellboy'
], function($, Packer){
  $nav_fixed = $('.nav-fixed');
  function filter($container, $blocks){
    var $links = $nav_fixed.find('li a');
    var blocks = serialize($blocks);
    var _blocks = JSON.parse(JSON.stringify(blocks));
    packing(_blocks, $blocks, sortByRandom());

    $links.on('click', sortBlocks);

    function sortBlocks(event){
      event.preventDefault();
      var $target = $(this);
      _blocks = JSON.parse(JSON.stringify(blocks));
      var criteria = [];

      $('.item.select').removeClass('select');

      $target.find('i').toggleClass('check-on').toggleClass('check-off');

      $target.closest('li').toggleClass('selected');


      var $lis = $nav_fixed.find('.selected');
      $lis.each(function(){
        $el = $(this);
        criteria.push(parseInt($el.attr('data-id'), 10));
      });

      _blocks.sort(sortByRandom);

      var len = criteria.length;

      if( len === 0 || len === $lis.length){
        packing(_blocks, $blocks, sortByRandom());
      }
      else{
        packing(_blocks, $blocks, sortByCriteria(criteria));

      }
    }
  }

  function serialize($blocks){
    var blocks = [];

    $blocks.each(function(i){
      var $el = $(this);
      blocks[i] = {
        w: parseInt($el.attr('data-w'), 10),
        h: parseInt($el.attr('data-h'), 10),
        eq: i,
        id: parseInt($el.attr('data-id'), 10)
      };
    });
    return blocks;
  }

  function packing(blocks, $blocks, fnSort){
    var packer = new Packer(6, 100);
    var col = 160;
    var max_height = 0;
    blocks.sort(fnSort);
    packer.fit(blocks);

    for(var n = 0 ; n < blocks.length ; n++) {
      var block = blocks[n];
      if (block.fit) {
        $blocks.eq(block.eq).css({
          width: block.w*col,
          height: block.h*col,
        }).animate({
          top: block.fit.y*col,
          left: block.fit.x*col
        }, {duration: 1000, queue: false});
        var tmp_max_height = block.fit.y*col + block.h * col;
        if(max_height < tmp_max_height){
          max_height = tmp_max_height;
        }
      }
    }
    $('#blocks-grid').animate({
      height: max_height
    }, 1000);
  }
//filters
  function sortByCriteria(criteria){
    return function (a, b){
      if(criteria.indexOf(a.id)=== -1){
          return 1;
      }else {
        return -1;
      }
    };
  }

  function sortByRandom(){
    return function (a, b) { return Math.random() - 0.5; };
  }

  return filter;

});
