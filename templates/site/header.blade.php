<!DOCTYPE html>
<!--[if lt IE 7]>
<html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"></html>
<![endif]-->
<!--[if IE 7]>
<html lang="es" class="no-js lt-ie9 lt-ie8"></html>
<![endif]-->
<!--[if IE 8]>
<html lang="es" class="no-js lt-ie9"></html>
<![endif]-->
<!--[if IE 9]>
<html lang="es" class="no-js lt-ie10"></html>
<![endif]-->
<!--[if gt IE 8]><!-->
<html lang="es">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:image" content="http://www.milo.com.pe/static/site/images/milo_share128x128.jpg">


	<meta property="og:url" content="https://www.milo.com.pe"/>
	<meta property="og:title" content="MILO PERU"/>
    <meta property="og:type" content="website"/>


    <link href="{{ asset('static/site/images/favicon.ico') }}" rel="Shortcut Icon" type="image/x-icon">

    <title>{{ Seo::get('title') }} | MILO</title>
    <meta name="description" content="{{ Seo::get('description') }}" />
    <meta name="keywords" content="{{ Seo::get('keywords') }}" />
    @if (Seo::get('noindex'))
    <meta name="robots" content="noindex" />
    @endif

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('static/site/styles/main.css') }}?v={{ time() }}">

    <script type="text/javascript" src="/content/pages/milo.com.pe.google-analytics.js"></script> 

  </head>
  <body>
    
    <!-- SOCIAL -->
    @include('site.social')
    <!-- SOCIAL -->
