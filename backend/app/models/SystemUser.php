<?php

class SystemUser extends Eloquent {

    protected $table = 'system_users';
    protected $hidden = array('password');

    public static function has_perm($permission) {
        $id = Session::get('session_user');
        
        $user = SystemUser::where('id', $id)->first();
                
        if ($user->username == 'admin'){
        	return true;
        }
        
        $sql = sprintf('
        select 
            permission.slug 
        from 
            website_system_users su inner join 
            website_system_users_role role 
                on (su.role_id = role.id) inner join 
            website_system_users_role_permissions role_permission 
                on (role_permission.role_id = role.id) inner join 
            website_system_users_permissions permission 
                on (permission.id = role_permission.permission_id) 
        where su.id = %d and permission.slug="%s"', $id, $permission);

        return (bool) count(DB::select(DB::raw($sql)));
    }

}
