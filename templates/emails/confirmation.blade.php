<p style="margin: 0 0 25px">{{ first_name }} {{ last_name }}, Welcome.</p>
<p style="margin: 0 0 25px">We ask follow this link to activate your account.</p>

<a href="{{ url }}" style="display: block; line-height: 39px; text-decoration: none; width: 240px; height: 39px; margin: 60px auto 0; color: #FFF; background: #F48937;">ACTIVATE ACCOUNT</a>
