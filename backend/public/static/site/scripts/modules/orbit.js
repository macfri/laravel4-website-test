define([
  'jquery',
  'foundation.min'
], function($){

  function orbit(){
    var self = this;
    $(document)
      .foundation('forms')
      .foundation('orbit',{
      timer:false,
      bullets:true,
      slide_number:false,
      bullets_container_class: 'slider-controls'
    });
  }
  return orbit;
});
