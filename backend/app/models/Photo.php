<?php

class Photo extends Eloquent {

    protected $table = 'files';
    protected $fillable = array(
        'title',
        'size',
        'mime',
        'extension',
    );

}
