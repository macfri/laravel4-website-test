define([
  'jquery'
], function($){

  function scrollTop($nav_fixed){
    var $window = $(window);
    var nav_pos = $nav_fixed.position().top;
    $window.on('scroll', setFixed);
    $nav_fixed.on('click','.btn-top',scrollT);

    function scrollT(event)
    {
      event.preventDefault();
      $('body').scrollTo( 0, 800, { queue:true } );
    }

    function setFixed(){
      if ($window.scrollTop() > nav_pos) {
        $('.btn-top').fadeIn();
        $('.left a.btn-logo').fadeIn();
      }
      else {
        $('.btn-top').fadeOut();
        $('.left a.btn-logo').fadeOut();
      }
    }
  }

  return scrollTop;
});
