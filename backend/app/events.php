<?php

class Paginator {

    public $total_items;
    public $per_page;
    public $adjacents;
    public $page;

    public function __construct($page, $total_items, $adjacents = 2, $per_page = 10) {
        $this->page = $page;
        $this->total_items = $total_items;
        $this->adjacents = $adjacents;
        $this->per_page = $per_page;
    }

    public function total_pages() {
        return (int) ceil($this->total_items / ($this->per_page * 1.0));
    }

    public function pages() {
        if ($this->page > $this->total_pages()) {
            $pages = array();
        } else if ($this->total_pages() <= 2 * $this->adjacents) {
            $pages = range(1, $this->total_pages());
        } else {
            if ($this->page <= $this->adjacents) {
                $frm = 1;
            } else {
                if ($this->total_pages() - $this->page < $this->adjacents) {
                    $frm = $this->total_pages() - 2 * $this->adjacents;
                } else {
                    $frm = $this->page - $this->adjacents;
                }
            }
            $inc = 2 * $this->adjacents + 1;
            $pages = range($frm, $frm + $inc);
        }
        return $pages;
    }

}

function get_size($block_size, $key) {
    $index = ($key == 'w') ? 0 : 1;
    $a = explode('x', $block_size);
    if (!is_array($a)) {
        $a = split('x', $block_size);
    }

    if (count($a) > 1) {
        return $a[$index];
    } else {
        return '1';
    }
}

function get_images_profile($access_token) {
    $app_facebook = Config::get('app.facebook');
    $url = sprintf(
            '%s/fql', $app_facebook['url_graph']);
    $q = sprintf(
            "SELECT pid, src FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner = %s AND type = '%s') limit 8", 'me()', 'profile'
    );

    $params = array(
        'access_token' => $access_token,
        'q' => $q
    );

    $data = get_data_fb($url, $params);

    $profile_images = array();

    if (strpos($data, 'error') === false) {
        $photos = json_decode($data);
        foreach ($photos->data as $row) {
            $profile_images[] = array('id' => $row->pid, 'src' => $row->src);
        }
    }

    return $profile_images;
}

function order_table_new($tbl, $order, $id) {
    $sql = sprintf('update %s
        set `order` = `order` + 1 
        where `order` >= %d and id<>%d', $tbl, $order, $id);
    DB::statement($sql);
}

function order_table_delete($tbl, $order) {
    $sql = sprintf('update %s 
        set `order` = `order` - 1 
        where `order` > %d', $tbl, $order);
    DB::statement($sql);
}

function order_table_update($tbl, $order_old, $order, $id) {
    $sql = sprintf('update %s
        set `order` = %d 
        where `order` = %d and 
            `id` <> %d', $tbl, $order_old, $order, $id);
    DB::statement($sql);
}

function round_formula($ts, $rs, $koP, $spe, $multi, $round, $lUser) {
    $_round[1] = 10000;
    $_round[2] = 5000;
    $_round[3] = 2000;
    $verify_ts = ($rs + $koP + $spe + $_round[$round]);
    if ($lUser == 200) {
        $verify_ts += 2000;
    }
    $verify_ts *= $multi;
    return $verify_ts == $ts;
}

function in_range() {
    $current = mktime(date("H"), date("i"), date("s"), 0, 0, 0);

    $start_time_hours = '16,17,18';
    $start_time_hours = '8,9,10';
    $start_time_hours = explode(',', $start_time_hours);

    $time_block_1 = mktime($start_time_hours[0], 0, 0, 0, 0, 0);
    $time_block_2 = mktime($start_time_hours[0], 29, 0, 0, 0, 0);

    $time_block_3 = mktime($start_time_hours[0], 30, 0, 0, 0, 0);
    $time_block_4 = mktime($start_time_hours[0], 59, 0, 0, 0, 0);

    $time_block_5 = mktime($start_time_hours[1], 0, 0, 0, 0, 0);
    $time_block_6 = mktime($start_time_hours[1], 29, 0, 0, 0, 0);

    $time_block_7 = mktime($start_time_hours[1], 30, 0, 0, 0, 0);
    $time_block_8 = mktime($start_time_hours[2], 0, 0, 0, 0, 0);

    if ($current >= $time_block_1 and $current <= $time_block_2) {
        $s = '5';
    } else if ($current >= $time_block_3 and $current <= $time_block_4) {
        $s = '3';
    } else if ($current >= $time_block_5 and $current <= $time_block_6) {
        $s = '2';
    } else if ($current >= $time_block_7 and $current <= $time_block_8) {
        $s = '1';
    } else {
        $s = '0';
    }
    return $s;
}

function base64_encode_image($filename = string, $filetype = string) {
    if ($filename) {
        $imgbinary = fread(fopen($filename, "r"), filesize($filename));
        return 'data:' . $filetype . ';base64,' . base64_encode($imgbinary);
    }
}

function crop_image($src, $mime, $thumb_width, $thumb_height) {
    if ($mime == 'image/jpeg') {
        $image = imagecreatefromjpeg($src);
    } else if ($mime == 'image/png') {
        $image = imagecreatefrompng($src);
    } else {
        return false;
    }

    $width = imagesx($image);
    $height = imagesy($image);

    $original_aspect = $width / $height;
    $thumb_aspect = $thumb_width / $thumb_height;

    if ($original_aspect >= $thumb_aspect) {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $thumb_height;
        $new_width = $width / ($height / $thumb_height);
    } else {
        // If the thumbnail is wider than the image
        $new_width = $thumb_width;
        $new_height = $height / ($width / $thumb_width);
    }

    $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

    // Resize and crop
    imagecopyresampled($thumb, $image, 0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
            0, 0, $new_width, $new_height, $width, $height);
    ob_start();
    imagejpeg($thumb, NULL, 80);
    $buffer = ob_get_clean();
    ob_end_clean();
    $src = 'data:' . $mime . ';base64,' . base64_encode($buffer);
    return $src;
}

function youtube_id_from_url($url) {
    $pattern = '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        %x'
    ;
    $result = preg_match($pattern, $url, $matches);

    if (false !== $result) {
        if (count($matches) > 0) {
            return $matches[1];
        } else {
            return false;
        }
    }
    return false;
}

function get_img_base64($img) {
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    return $data;
}

function resize($img, $w, $h, $newfilename) {

    //Check if GD extension is loaded
    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
        //trigger_error("GD is not loaded", E_USER_WARNING);
        return false;
    }

    //Get Image size info
    $imgInfo = getimagesize($img);
    switch ($imgInfo[2]) {
        case 1: $im = imagecreatefromgif($img);
            break;
        case 2: $im = imagecreatefromjpeg($img);
            break;
        case 3: $im = imagecreatefrompng($img);
            break;
        default:
            trigger_error('Unsupported filetype!', E_USER_WARNING);
            break;
    }

    //If image dimension is smaller, do not resize
    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
        $nHeight = $imgInfo[1];
        $nWidth = $imgInfo[0];
    } else {
        //yeah, resize it, but keep it proportional
        if ($w / $imgInfo[0] > $h / $imgInfo[1]) {
            $nWidth = $w;
            $nHeight = $imgInfo[1] * ($w / $imgInfo[0]);
        } else {
            $nWidth = $imgInfo[0] * ($h / $imgInfo[1]);
            $nHeight = $h;
        }
    }
    $nWidth = round($nWidth);
    $nHeight = round($nHeight);

    $newImg = imagecreatetruecolor($nWidth, $nHeight);

    /* Check if this image is PNG or GIF, then set if Transparent */
    if (($imgInfo[2] == 1) OR ( $imgInfo[2] == 3)) {
        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
    }
    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);

    //Generate the file, and rename it to $newfilename
    switch ($imgInfo[2]) {
        case 1: imagegif($newImg, $newfilename);
            break;
        case 2: imagejpeg($newImg, $newfilename);
            break;
        case 3: imagepng($newImg, $newfilename);
            break;
        default: trigger_error('Failed resize image!', E_USER_WARNING);
            break;
    }

    return $newfilename;
}

function get_data_google2($url, $access_token) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        ('Authorization: Bearer ' . $access_token)));
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function get_data_google($url, $params) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $data;
}

function get_data_fb($url, $params) {
    $url .= '?' . http_build_query($params);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $data;
}

_Event::listen('guest.save_round_anonymous', function() {

    $status_code = 0;
    $message = NULL;

    $user = Auth::user();
    $token = Session::get('guest_token');
    $token_post = Session::get('guest_token_post');
    $round = (int) Session::get('guest_game_round_round');
    $lUser = (int) Session::get('guest_game_round_lUser');
    $lEnemy = (int) Session::get('guest_game_round_lEnemy');
    $totalScore = (int) Session::get('guest_game_round_totalScore');
    $roundScore = (int) Session::get('guest_game_round_roundScore');
    $kos = (int) Session::get('guest_game_round_kos');
    $koP = (int) Session::get('guest_game_round_koP');
    $spe = (int) Session::get('guest_game_round_spe');
    $multi = (int) Session::get('guest_game_round_multi');
    $hEnemy = (int) Session::get('guest_game_round_hEnemy');
    $hEnemyBlock = (int) Session::get('guest_game_round_hEnemyBlock');
    $bodyBlock = (int) Session::get('guest_game_round_bodyBlock');
    $bodyAccert = (int) Session::get('guest_game_round_bodyAccert');
    $bodyGiven = (int) Session::get('guest_g');
    $headAccert = (int) Session::get('guest_game_round_headAccert');
    $headBlock = (int) Session::get('guest_game_round_headBlock');
    $headGiven = (int) Session::get('guest_game_round_headGiven');
    $lifetime = (int) Session::get('guest_game_round_lifetime');

    if (in_range() == 0) {
        $message = 'block_time';
        if (Config::get('app.round_block_time')) {
            $status_code = 1;
        } else {
            $status_code = 0;
        }
    } else if ($totalScore < 1 or $totalScore < 1 or $totalScore > 210000) {
        $message = 'score-negative';
        $status_code = 1;
    } else if (!in_array($multi, array(5, 3, 2, 1))) {
        $message = 'not-multi';
        $status_code = 1;
    } else if (!in_array($round, array(1, 2, 3))) {
        $message = 'not-round';
        $status_code = 1;
    } else if (!round_formula($totalScore, $roundScore, $koP, $spe, $multi, $round, $lUser)) {
        $message = 'bad-formula';
        $status_code = 1;
    } else if ($token != $token_post) {
        $message = 'invalid token';
        $status_code = 1;
    }

    if ($status_code == 0) {
        $user = Auth::user();
        $point = GameRound::where('user_id', $user->id)
                ->where('week', 4)
                ->first();
        if (!$point) {
            $point = new GameRound();
            $point->user_id = $user->id;
            $point->full_name = $user->first_name . ' ' . $user->last_name;
            $point->picture = $user->picture;
            $point->score = $totalScore;
            $point->tries = 1;
            $point->week = 4;
        } else {
            if ($totalScore > $point->score) {
                $point->score = $totalScore;
            }
            $point->tries += 1;
        }
        $point->lifetime = $lifetime;
        $point->multi = Session::get('guest_game_round_multi');
        try {
            $point->save();
            $message = 'save round';
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            Log::error($message);
        }

        $point2 = new GameRoundDetail();
        $point2->user_id = $user->id;
        $point2->totalScore = $totalScore;
        $point2->lifetime = $lifetime;
        $point2->round = $round;
        $point2->lUser = $lUser;
        $point2->lEnemy = $lEnemy;
        $point2->roundScore = $roundScore;
        $point2->kos = $kos;
        $point2->koP = $koP;
        $point2->spe = $spe;
        $point2->multi = $multi;
        $point2->hEnemy = $hEnemy;
        $point2->hEnemyBlock = $hEnemyBlock;
        $point2->bodyBlock = $bodyBlock;
        $point2->bodyAccert = $bodyAccert;
        $point2->bodyGiven = $bodyGiven;
        $point2->headAccert = $headAccert;
        $point2->headBlock = $headBlock;
        $point2->headGiven = $headGiven;
        $point2->user_type = Session::get('user_type');
        try {
            $point2->save();
            $message = 'save round';
            Session::set('round_save_ok', true);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            Log::error($message);
        }
    }

    Session::forget('guest_game_round');
    Session::forget('guest_game_round_totalScore');
    Session::forget('guest_game_round_lifetime');
    Session::forget('guest_token');
    Session::forget('guest_token_post');
    Session::forget('guest_game_round_round');
    Session::forget('guest_game_round_lUser');
    Session::forget('guest_game_round_lEnemy');
    Session::forget('guest_game_round_roundScore');
    Session::forget('guest_game_round_kos');
    Session::forget('guest_game_round_koP');
    Session::forget('guest_game_round_spe');
    Session::forget('guest_game_round_multi');
    Session::forget('guest_game_round_hEnemy');
    Session::forget('guest_game_round_hEnemyBlock');
    Session::forget('guest_game_round_bodyBlock');
    Session::forget('guest_game_round_bodyAccert');
    Session::forget('guest_game_round_bodyGiven');
    Session::forget('guest_game_round_headAccert');
    Session::forget('guest_game_round_headBlock');
    Session::forget('guest_game_round_headGiven');

    Log::info('Log message', array(
        'guest_message' => $message,
        'guest_status_code' => $status_code
            )
    );
});

_Event::listen('user.send_mail', function($user) {

    $data = array('first_name' => $user->first_name);
    $message_data['to'] = $user->email;
    $message_data['name'] = $user->first_name;
    Mail::send('emails.welcome', $data, function(
            $message) Use ($message_data) {
        $message->to(
                $message_data['to'], $message_data['name']
        )->subject('Welcome!');
    });
});

_Event::listen('user.choose_photo', function($access_token, $expires) {

    if (Session::has('choose_photo') and Auth::check()) {
        $social_red = Auth::user()->facebook;
        $social_red->fb_oauth_token = $access_token;

        if (isset($expires)) {
            $social_red->fb_expires = $expires;
        }
        try {
            $social_red->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
        return Redirect::to(Session::get('current_route'));
    }
});

_Event::listen('guest.save_chulls', function() {

    $message = null;
    $status_code = 0;

    if (Session::has('guest_game_chulls')) {
        $user = Auth::user();
        if (!Session::has('guest_token') or ! Session::has('guest_token_post') or ! Session::has('guest_game_item1') or ! Session::has('guest_game_item2') or ! Session::has('guest_game_item3') or ! Session::has('guest_game_guess') or ! Session::has('guest_game_lifetime') or ! Session::has('guest_game_tries') or ! Session::has('guest_game_id')) {
            $status_code = 1;
            $message = 'not session guest';
        }
        if ($status_code == 0) {
            $guest_token = Session::get('guest_token');
            $guest_token_post = Session::get('guest_token_post');
            if ($guest_token != $guest_token_post) {
                $message = 'invalid token';
                $status_code = 1;
            }
        }
        if ($status_code == 0) {
            $user = Auth::user();
            $guest_game_item1 = Session::get('guest_game_item1');
            $guest_game_item2 = Session::get('guest_game_item2');
            $guest_game_item3 = Session::get('guest_game_item3');
            $guest_game_guess = Session::get('guest_game_guess');
            $guest_game_tries = Session::get('guest_game_tries');
            $guest_game_lifetime = Session::get('guest_game_lifetime');
            $point = GameChulls::where('user_id', $user->id)->first();
            if (!$point) {
                $point = new GameChulls();
                $point->user_id = $user->id;
                $point->item1 = $guest_game_item1;
                $point->item2 = $guest_game_item2;
                $point->item3 = $guest_game_item3;
                $point->tries = $guest_game_tries;
                $point->lifetime = $guest_game_lifetime;
                $point->guess = 1;
                $point->enabled = true;
                $point->was_anonymous = 1;
                $is_save_point = true;
            } else {
                Log::info('exists chulls');
                if (!$point->enabled) {
                    $point->item1 = $guest_game_item1;
                    $point->item2 = $guest_game_item2;
                    $point->item3 = $guest_game_item3;
                    $point->tries = $guest_game_tries;
                    $point->lifetime = $guest_game_lifetime;
                    $point->guess = 1;
                    $point->enabled = true;
                    $point->was_anonymous = 2;
                    $is_save_point = true;
                } else {
                    $message = 'enabled point';
                    Log::info('emabled point');
                }
            }
            if (isset($is_save_point)) {
                try {
                    $point->save();
                    $message = 'save chulls';
                } catch (Exception $exception) {
                    $message = $exception->getMessage();
                    Log::error($message);
                }
            }
        }
        Session::forget('guest_game_id');
        Session::forget('guest_game_item1');
        Session::forget('guest_game_item2');
        Session::forget('guest_game_item3');
        Session::forget('guest_game_guess');
        Session::forget('guest_game_tries');
        Session::forget('guest_game_lifetime');
        Session::forget('guest_token');
        Session::forget('guest_token_post');
        Session::forget('guest_game_chulls');
    }
});

_Event::listen('guest.save_fun_video', function() {

    $fun_video_data = Session::get('fun_video');

    if (!is_null($fun_video_data)) {

        $title = isset($fun_video_data['title'][0]) ? $fun_video_data['title'][0] : null;
        $code = isset($fun_video_data['code'][0]) ? $fun_video_data['code'][0] : null;

        if (!is_null($title) && !is_null($code)) {

            $meme = new Meme();
            $meme->title = $title;
            $meme->link = $code;
            $meme->slug = Meme::get_slug($meme->title);
            $meme->author_id = 1;
            $meme->author_web_id = Auth::user()->id;
            $meme->category = 'videos-graciosos';
            $meme->status = 'active';
            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
            }
            return Redirect::route('freetime_category', 'videos-graciosos')
                            ->with('message', '¡Se subio tu Video exitosamente!');
        }
    }
});

_Event::listen('guest.save_points', function() {

    $message = null;
    $status_code = null;

    if (Session::has('guest_score')) {
        if ($status_code == 0) {
            Log::info('guest_points: ' . Session::has('guest_points'));
            Log::info('guest_token: ' . Session::has('guest_token'));
            Log::info('guest_token_post: ' . Session::has('guest_token_post'));
            Log::info('guest_score: ' . Session::has('guest_score'));
            Log::info('guest_game_id: ' . Session::has('guest_game_id'));
            Log::info('guest_trophies: ' . Session::has('guest_trophies'));

            if (!Session::has('guest_points') or ! Session::has('guest_token') or ! Session::has('guest_trophies') or ! Session::has('guest_token_post') or ! Session::has('guest_score') or ! Session::has('guest_game_id')) {
                $status_code = 1;
                $message = 'not session guest';
            }
        }

        if ($status_code == 0) {
            $guest_token = Session::get('guest_token');
            $guest_token_post = Session::get('guest_token_post');
            if ($guest_token != $guest_token_post) {
                $message = 'invalid token';
                $status_code = 1;
            }
        }

        if ($status_code == 0) {
            $user = Auth::user();
            $guest_score = Session::get('guest_score');
            $guest_points = Session::get('guest_points');
            $guest_game_id = Session::get('guest_game_id');

            foreach ($guest_points as $row) {
                $point = new Point();
                $point->user_id = $user->id;
                $point->score = $row['score'];
                $point->game_id = $guest_game_id;
                $point->winner = $row['winner'];

                try {
                    $point->save();
                } catch (Exception $exception) {
                    $error = True;
                    Log::error($exception->getMessage());
                }
            }

            if (!isset($error)) {
                $player = Player::where('user_id', '=', $user->id)
                                ->where('game_id', '=', $guest_game_id)->first();

                $trophies = Session::get('guest_trophies');

                if (!$player) {
                    $player = new Player();
                    $player->game_id = $guest_game_id;
                    $player->user_id = $user->id;
                    $player->max_score = $guest_score;
                    $player->total_score = $guest_score;
                    $player->was_guest = true;
                    $player->attempts = 1;
                    $player->is_new = 0;
                    $message = 'new player';

                    if (is_array($trophies)) {
                        $player->trophies = implode(',', $trophies);
                    }

                    /*                     * ***************************************************** */
                    if (is_array($trophies)) {
                        $player_badge = PlayerBadge::where('game_id', $guest_game_id)
                                ->where('user_id', $user->id);
                        if ($player_badge) {
                            $player_badge->delete();
                        }
                        $inser_data = array();
                        foreach ($trophies as $row) {
                            $player_badge = new PlayerBadge();
                            $player_badge->game_id = $guest_game_id;
                            $player_badge->user_id = $user->id;
                            $player_badge->badge_id = $row;
                            $player_badge->save();
                        }
                    }

                    /*                     * **************************************************** */
                } else {

                    if ($guest_score > $player->max_score) {
                        $player->max_score = $guest_score;

                        if (is_array($trophies)) {
                            $player->trophies = implode(',', $trophies);
                        }
                    } else {
                        Log::info('menor');
                    }

                    /*                     * ***************************************************** */
                    if (is_array($trophies)) {
                        $player_badge = PlayerBadge::where('game_id', $guest_game_id)
                                ->where('user_id', $user->id);
                        if ($player_badge) {
                            $player_badge->delete();
                        }
                        $inser_data = array();
                        foreach ($trophies as $row) {
                            $player_badge = new PlayerBadge();
                            $player_badge->game_id = $guest_game_id;
                            $player_badge->user_id = $user->id;
                            $player_badge->badge_id = $row;
                            $player_badge->save();
                        }
                    }

                    /*                     * **************************************************** */
                    $player->total_score += $guest_score;
                    $player->attempts += 1;
                    $message = 'update player';
                }

                try {
                    $player->save();
                    $is_player_save = true;
                } catch (Exception $exception) {
                    $status_code = 1;
                    Log::error($exception->getMessage());
                    $message = $exception->getMessage();
                }

                if (isset($is_player_save)) {
                    $user->level_score = $player->max_score;
                    try {
                        $user->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        $message = $exception->getMessage();
                    }
                }
            }
        }

        Session::forget('guest_score');
        Session::forget('guest_points');
        Session::forget('guest_game_id');
        Session::forget('guest_token');
        Session::forget('guest_token_post');
    }

    Log::info('Log message', array(
        'guest_message' => $message,
        'guest_status_code' => $status_code
            )
    );
});
