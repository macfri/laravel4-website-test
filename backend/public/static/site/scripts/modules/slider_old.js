define([
  'jquery'
], function($){
  function slider($slider){
    //initialize functionality
    var $slides = $slider.find('.slide');
    var len = $slides.length;

    if(len === 1){
      return null;
    }else{
      $slider.hide().first().show();
      var tmp = [
        '<div class="slider-btn-prev"></div>',
        '<div class="slider-btn-next"></div>',
        '<ul class="slider-controls">'
      ];
      for (var i = 0; i < len; i++){
        tmp.push('<li></li>');
      }
      tmp.push('</ul>');

      $slider.append(tmp.join(''));
    }

    var $controls = $slider.find('.slider-controls li');
    var $prev = $slider.find('.slider-btn-prev');
    var $next = $slider.find('.slider-btn-next');
    var target;
    var last = len -1;
    var timer = setInterval(function() { sliderTimer(); },5000);

    $controls.first().addClass('active');

    //define events
    $controls.on('click', function(event){
      event.preventDefault();
      var $control = $(this);
      if(!$control.hasClass('active')){
        target = $control.index();
        sliderResponse(target);
        resetTimer();
      }
    });

    $prev.on('click', function(event){
      event.preventDefault();
      target = $controls.filter('.active').index();
      last = $controls.length-1;
      if(target === 0){
        target = last;
      }
      else{
        target -=1;
      }
      sliderResponse(target);
      resetTimer();
    });

    $next.on('click',  function(event){
      event.preventDefault();
      target = $controls.filter('.active').index();
      if(target === last){
        target = 0;
      }else{
        target += 1;
      }
      sliderResponse(target);
      resetTimer();

    });

    function sliderResponse(target){
      $slides.fadeOut(300).eq(target).fadeIn(300);
      $controls.removeClass('active').eq(target).addClass('active');
    }

    function sliderTimer() {
      target = $controls.filter('.active').index();

      if(target === last){
        target = 0;
      }else{
        target += 1;
      }
      sliderResponse(target);
    }
    function resetTimer() {
      clearInterval(timer);
      timer = setInterval(function() { sliderTimer(); },5000);
    }
  }
  return slider;
});
