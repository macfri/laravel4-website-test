define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){
  function register($form){
    
    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    var index = 1;

    $form.validate({
      submitHandler: function(form){
        if(index <= 2){
          $(".step"+(index+1)).find('input').addClass('required');
          $(".step"+index).fadeOut(400, function(){
            $(".step"+(index+1)).fadeIn();
            index += 1;
          });
        }
        if(index===2){
          $('.btn-session').html("FINALIZAR");
          $(".btn-session").click(function(event){
            form.submit();
          });
        }
      }
    });
  }
  return register;
});
