<?php

class GameController extends BaseController {

    private $view_path = 'site/games/';

    public function games() {
        Session::set('current_route', URL::current());

        $items = Game::where('status', '=', 'active')
                ->orderBy('order', 'asc')
                ->get();

        return View::make(
                        $this->view_path . __function__, array(
                    'items' => $items
                        )
        );
    }

    public function detail($slug) {


        
        Session::set('current_route', URL::current());
        Session::forget('guest_token_post');
        Session::forget('guest_score');
        Session::forget('guest_game_id');
        Session::forget('guest_points');

        Session::forget('guest_game_chulls');
        Session::forget('guest_game_item1');
        Session::forget('guest_game_item2');
        Session::forget('guest_game_item3');

        $item = Game::where('slug', '=', $slug)
                ->where('status', '=', 'active')
                ->firstOrFail();

        Seo::addPlaceholder('meta_title', $item->title);
        Seo::addPlaceholder('meta_description', $item->description);
        Seo::addPlaceholder('meta_keywords', $item->title);

        $_badges = Badge::where('game_id', '=', $item->id)->get();
        $trophies = '';
        $user_complete = 0;

        $user = null;
        if (Auth::check()) {
            $user = Auth::user();
            if (!is_null($user->dni) and $user->dni != '') {
                $user_complete = 1;
            }
            $user_id = $user->id;
            $token = Auth::user()->token_game;
            $player = Player::where('user_id', '=', $user->id)
                    ->where('game_id', '=', $item->id)
                    ->first();
            if ($player) {
                $max_score = $player->max_score;
                $trophies = $player->trophies;
            } else {
                $max_score = 0;
            }
            $token_rank = md5(microtime(TRUE) . rand(0, 100000));
            Session::put('token_rank', $token_rank);
            $badges = $user->has_trophies($item->id);
            if (!is_null($user->facebook_id) and $user->facebook_id != '') {
                $user_facebook = 1;
            } else {
                $user_facebook = 0;
            }
            Session::put('guest_token', $token);

             
            if ($item->slug == 'chulls') {
                $user_played_chulls = GameChulls::where('user_id', '=', $user->id)
                                ->where('enabled', true)->first();
                if ($user_played_chulls) {
                    $user_ids_awards = $user_played_chulls->item1 . ',' . $user_played_chulls->item2 . ',' . $user_played_chulls->item3;
                    $user_played_chulls = 1;
                } else {
                    $user_played_chulls = 0;
                    $user_ids_awards = '';
                }
            }

           /*
            if ($item->slug == 'crea-tu-camiseta') {
                if (Session::get('user_type') == 2) {
                    $flashvars['mundial_user_auth'] = 1;
                } else {
                    $flashvars['mundial_user_auth'] = 0;
                }
            }
             */
        } else {
            $flashvars['mundial_user_auth'] = 0;
            $round_max_score = 0;
            $user_facebook = 0;
            $user_ids_awards = '';
            $user_played_chulls = 0;
            $profile_photo = '';

            $badges = Badge::where('game_id', $item->id)->get();
            foreach ($badges as $k => $v) {
                $v->path = $v->image->path;
            }
            $user_id = 0;
            $token = md5(microtime(TRUE) . rand(0, 100000));
            Session::put('guest_token', $token);
            $max_score = 0;
            $token_rank = null;
        }

        $flashvars['user_facebook'] = $user_facebook;
        $flashvars['token'] = $token;
        $flashvars['user_id'] = $user_id;
        $flashvars['basePath'] = asset($item->path_swf) . '/';
        $flashvars['game_id'] = $item->id;
        $flashvars['trophies'] = $trophies;
        $flashvars['user_complete'] = $user_complete;

        if ($item->slug == 'chulls') {
            $flashvars['user_ids_awards'] = $user_ids_awards;
            $flashvars['user_played_chulls'] = $user_played_chulls;
        }

        if ($item->slug == 'crea-tu-camiseta') {  
            //App::abort(404);
        }
        
        /*
        if ($item->slug == 'crea-tu-camiseta') {
          
                      return Response::make(
                            'You are not authorized', '403');
         {
            Session::put('mundial_game', 1);
            $flashvars['mundial_user_login_facebook'] = URL::route('user_login_facebook');
            $flashvars['mundial_gallery'] = URL::route('game_mundial_gallery');

            if (Auth::check()) {
                $mundial_user_fb_id = $user->facebook->fb_id;
                $mundial_player = GameMundialPlayer::where('fb_id', '=', $mundial_user_fb_id)->first();
                $player_team = GameMundialPlayerTeam::where('fb_id_to', $mundial_user_fb_id)->count();
                if ($mundial_player) {
                    $mundial_user_not_complete_friends = GameMundialPlayerTeam::where('player_id', $mundial_player->id)->count();
                    $flashvars['mundial_user_has_photo'] = asset(
                            $mundial_player->tshirt_path_front . $mundial_player->tshirt_path_front_ext);
                    $flashvars['mundial_user_picture_share'] = asset(
                            $mundial_player->tshirt_path_front . '_share.jpg');
                } else {
                    $flashvars['mundial_user_has_photo'] = '';
                    $flashvars['mundial_user_picture_share'] = '';
                    $mundial_user_not_complete_friends = 0;
                }
                $flashvars['mundial_friend_has_photo'] = $player_team == 0 ? 0 : 1;
                $flashvars['mundial_api_save_undershirt'] = URL::route('mundial_api_save_undershirt');
                $flashvars['mundial_api_get_team_from_friend'] = URL::route('mundial_api_get_team_from_friend');
                $flashvars['mundial_api_bad_words'] = URL::route('mundial_api_bad_words');
                $flashvars['mundial_user_not_complete_friends'] = $mundial_user_not_complete_friends == 10 ? 0 : 1;

                $token = md5(microtime(TRUE) . rand(0, 100000));
                $flashvars['mundial_token'] = $token;
                Session::put('mundial_token', $token);
                unset($flashvars['trophies']);
                unset($flashvars['token']);
                $flashvars['mundial_user_fb_id'] = $mundial_user_fb_id;
            } else {
                $flashvars['mundial_user_has_photo'] = '';
            }
        }
         * 
         */

        $_flashvars = '';
        foreach ($flashvars as $k => $v) {
            $_flashvars .= $k . '=' . $v . '&';
        }
        $_flashvars = substr($_flashvars, 0, -1);

        $game_content = View::make(
                        'site/games/swf', array(
                    'flashvars' => $_flashvars,
                    'item' => $item,
                    'token' => $token,
                    'user_id' => $user_id,
                    'trophies' => $trophies,
                    'user_complete' => $user_complete,
                        )
        );

        $best_points = Player::where('game_id', '=', $item->id)
                ->take(10)
                ->orderBy('max_score', 'desc')
                ->get(array(
            'max_score',
            'user_id'
        ));

        if ($item->slug == 'chulls') {
            $_view_path_tlp = $this->view_path . 'detail_chulls';
        } else if ($item->slug == '3cucharaditas') {
            $_view_path_tlp = $this->view_path . 'detail_cucharadas';
        } else if ($item->slug == 'crea-tu-camiseta') {
            $_view_path_tlp = $this->view_path . $item->slug;
        } else {
            $_view_path_tlp = $this->view_path . __function__;
        }

        $token_facebook_user = md5(microtime(TRUE) . rand(0, 100000));
        Session::set('token_facebook_user', $token_facebook_user);

        return View::make(
                        $_view_path_tlp, array(
                    'user_data' => $user,
                    'item' => $item,
                    'badges' => $badges,
                    'max_score' => $max_score,
                    'token_rank' => $token_rank,
                    'best_points' => $best_points,
                    'game_content' => $game_content,
                    'user_complete' => $user_complete,
                    'token_facebook_user' => $token_facebook_user
                        )
        );
    }

    public function rank() {
        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $token_rank = Input::get('token_rank');

        if (!Session::has('token_rank')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        if (Session::get('token_rank') != $token_rank) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $face_id = Input::get('face_id');
        $game_id = (int) Input::get('game_id');

        if (!in_array($face_id, array('face1', 'face2', 'face3', 'face4', 'face5'))) {
            return Response::make('Not Found', '404');
        }

        $item = Game::where('id', '=', $game_id)
                ->where('status', '=', 'active')
                ->firstOrFail();

        $status_code = 0;
        $message = 0;

        $game_face = GameFace::where('game_id', '=', $item->id)
                ->first();

        $game_face->$face_id += 1;

        try {
            $game_face->save();
        } catch (Exception $exception) {
            $status_code = 1;
            Log::error($exception->getMessage());
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    public function game_mundial_gallery() {
        $curren_path = Route::current()->getPath();
        Session::set('current_route', URL::current());
        $per_page = 8;
        $page = (int) Input::get('page', 0);
        if ($page < 1) {
            $page = 1;
        }

        $skip = ($page - 1) * $per_page;
        $count = GameMundialPlayer::count();
        $items = GameMundialPlayer::skip($skip)->take($per_page)->orderBy('created_at', 'DESC')->get();
        $paginator = new Paginator($page, $count, 2, $per_page);
        $data_add['offset'] = count($items) * $page;
        $data_add['items'] = $items;
        $data_add['pages'] = $paginator->pages();
        $data_add['total_items'] = $count;
        $data_add['current_page'] = $page;
        $data_add['total_pages'] = $paginator->total_pages();
        $data_add['path'] = URL::current() . '?page=' . $page;
        return View::make($this->view_path . 'crea-tu-camiseta-gallery', $data_add);
    }

}
