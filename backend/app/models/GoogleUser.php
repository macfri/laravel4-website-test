<?php

class GoogleUser extends Eloquent {

    protected $table = 'users_google';
    protected $guarded = array('id');
    protected $fillable = array(
        'gg_id',
        'gg_first_name',
        'gg_last_name',
        'gg_email',
        'gg_gender',
        'gg_oauth_token',
        'gg_expires',
        'gg_username'
    );

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public static function validate($input) {
        $rules = array(
            'username' => 'Required|Min:3|Max:80|Alpha|Unique:users',
            'first_name' => 'Required|Min:3|Max:80|Alpha',
            'last_name' => 'Required|Min:3|Max:80|Alpha',
            'email' => 'Required|Between:3,64|Email|Unique:users',
            'dni' => 'Required|Size:8|Unique:users',
            'phone' => 'Max:18',
            'password' => 'Required|AlphaNum|Between:4,8|Confirmed',
            'password_confirmation' => 'Required|AlphaNum|Between:4,8'
        );
        return Validator::make($input, $rules);
    }

    public static function validate_simple($input) {
        $rules = array(
            'first_name' => 'Required|Min:3|Max:80|Alpha',
            'last_name' => 'Required|Min:3|Max:80|Alpha',
            'email' => 'Required|Between:3,64|Email|Unique:users',
            'password' => 'Required|AlphaNum|Between:4,8',
        );
        return Validator::make($input, $rules);
    }

}
