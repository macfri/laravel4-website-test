<?php

class GameChulls extends Eloquent {

    protected $table = 'game_games_chulls';

    public static function validate($input) {
        $rules = array(
            'item1' => 'Required',
            'item2' => 'Required',
            'item3' => 'Required'
        );
        return Validator::make($input, $rules);
    }

    public function user() {
        return $this->belongsTo('User');
    }

}
