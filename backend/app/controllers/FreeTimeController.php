<?php

define('FONT', public_path().'/static/site/fonts/opensans-condbold-webfont.ttf');

function imagecreatefromfile($filename) {
    switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($filename);
            break;

        case 'png':
            return imagecreatefrompng($filename);
            break;

        case 'gif':
            return imagecreatefromgif($filename);
            break;

        default:
            throw new InvalidArgumentException('File "' . $filename . '" is not valid jpg, png or gif image.');
            break;
    }
}

class FreeTimeController extends BaseController {

    private $s = 50;
    private $index = 0;
    private $index2 = 0;
    private $tmp_array = array();
    private $tmp_array2 = array();

    private function deaw_text_on_image($text_words, $text_words_button) {

        $all = 480;
        $count = 0;
        $count2 = 0;

        foreach ($text_words as $k => $w) {
            $_y = imagettfbbox(24, 0, FONT, $w);
            $e = $_y[2] - $_y[0];
            $count += $e;
            if ($count >= $all) {
                $this->index ++;
                $this->deaw_text_on_image($text_words, $text_words_button);
            } else {
                if (isset($this->tmp_array[$this->index])) {
                    $this->tmp_array[$this->index] .=' ' . $w;
                } else {
                    $this->tmp_array[$this->index] = $w;
                }
                unset($text_words[$k]);
            }
        }

        foreach ($text_words_button as $k => $w) {
            $_y = imagettfbbox(24, 0, FONT, $w);
            $e = $_y[2] - $_y[0];
            $count2 += $e;
            if ($count2 >= $all) {
                $this->index2 ++;
                $this->deaw_text_on_image($text_words, $text_words_button);
            } else {
                if (isset($this->tmp_array2[$this->index2])) {
                    $this->tmp_array2[$this->index2] .=' ' . $w;
                } else {
                    $this->tmp_array2[$this->index2] = $w;
                }
                unset($text_words_button[$k]);
            }
        }
    }

    public function home() {
        

                
        $items = Meme::where('status', 'active')
                ->where('category', '<>', 'videos-graciosos')
                ->orderby('created_at', 'desc')
                ->get();

        $slider = Slider::where('status', '=', 'active')
                ->where('category', 'pasa-el-rato')
                ->orderBy('created_at', 'desc')
                ->first();

        Session::set('current_route', URL::current());
        return View::make('site/fun/fun', array('items' => $items, 'slider' => $slider));
    }

    public function category($category) {
        Session::put('_token', md5(microtime()));
        Session::set('current_route', URL::current());

        Seo::addPlaceholder('meta_title', $category);
        Seo::addPlaceholder('meta_description', $category);
        Seo::addPlaceholder('meta_keywords', $category);

        $items = Meme::where('status', 'active')
                ->where('category', $category)
                ->orderby('created_at', 'desc')
                ->get();

        foreach ($items as $item) {
            if (Auth::check()) {
                $user = Auth::user();
                $voted = $item->voted($user->id);

                if (!is_null($voted)) {
                    $item->vote_like = $voted->vote_like;
                    $item->vote_dislike = $voted->vote_dislike;
                } else {
                    $item->vote_like = false;
                    $item->vote_dislike = false;
                }
            } else {
                $item->vote_like = false;
                $item->vote_dislike = false;
            }
        }

        $template = 'site/fun/category';
        if ($category == 'videos-graciosos') {
            $template = 'site/fun/funny-videos';
        }

        return View::make($template, array(
                    'items' => $items, 'category' => $category));
    }

    public function detail($category, $slug) {
        $item = Meme::where('status', 'active')
                ->where('category', $category)
                ->where('slug', $slug)
                ->firstOrFail();

        Seo::addPlaceholder('meta_title', $item->seo_meta_title);
        Seo::addPlaceholder('meta_description', $item->seo_meta_description);
        Seo::addPlaceholder('meta_keywords', $item->seo_meta_keywords);

        if (Auth::check()) {
            $user = Auth::user();
            $voted = $item->voted($user->id);

            if (!is_null($voted)) {
                $item->vote_like = $voted->vote_like;
                $item->vote_dislike = $voted->vote_dislike;
            } else {
                $item->vote_like = false;
                $item->vote_dislike = false;
            }
        } else {
            $item->vote_like = false;
            $item->vote_dislike = false;
        }

        $items = Meme::where('status', 'active')
                ->where('category', '<>', 'videos-graciosos')
                ->take(4)
                ->orderby('vote_total_likes', 'desc')
                ->get();

        Session::set('current_route', URL::current());

        return View::make('site/fun/detail', array(
                    'item' => $item,
                    'items' => $items,
        ));
    }

    public function rank() {
        if (!Request::ajax()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        if (!Auth::check()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $user = Auth::user();
        $status_code = 0;
        $message = 0;

        $vote_id = (int) Input::get('vote_id');
        $meme_id = (int) Input::get('meme_id');

        if (strlen($vote_id) < 1 or strlen($meme_id) < 1) {
            return Response::make('Not Found', '404');
        }

        $vote_id = (int) $vote_id;
        $meme_id = (int) $meme_id;

        if (!in_array($vote_id, array(0, 1))) {
            return Response::make('Not Found', '404');
        }

        $meme = Meme::where('id', $meme_id)->first();

        if (!$meme) {
            return Response::make('Not Found', '404');
        }

        $meme_like = UserMemeLike::where('meme_id', $meme_id)
                ->where('user_id', $user->id)
                ->first();

        if (!$meme_like) {
            $meme_like = new UserMemeLike();
            $meme_like->user_id = $user->id;
            $meme_like->meme_id = $meme->id;

            try {
                $meme_like->save();
                $do2 = true;
            } catch (Exception $exception) {
                $status_code = 1;
                $message = 'ErroDb';
                Log::error($exception->getMessage());
            }
        } else {
            $do2 = true;
        }

        if (isset($do2)) {

            if ($vote_id == 1) {
                if (!$meme_like->vote_like) {
                    $meme_like->vote_like = true;
                    $meme->vote_total_likes += 1;

                    if (!$meme_like->vote_dislike) {
                        $meme->vote_total += 1;
                    } else {
                        $meme->vote_total += 2;
                        $meme_like->vote_dislike = false;
                        $meme->vote_total_dislikes -= 1;
                    }
                } else {
                    $meme_like->vote_like = false;
                    $meme->vote_total -= 1;
                    $meme->vote_total_likes -= 1;
                }
            } else {
                if (!$meme_like->vote_dislike) {
                    $meme_like->vote_dislike = true;
                    $meme->vote_total_dislikes += 1;

                    if (!$meme_like->vote_like) {
                        $meme->vote_total += 1;
                    } else {
                        $meme->vote_total += 2;
                        $meme_like->vote_like = false;
                        $meme->vote_total_likes -= 1;
                    }
                } else {
                    $meme_like->vote_dislike = false;
                    $meme->vote_total -= 1;
                    $meme->vote_total_dislikes -= 1;
                }
            }

            try {
                $meme->save();
                $meme_like->save();
                $do = true;
            } catch (Exception $exception) {
                $status_code = 1;
                $message = 'ErroDb';
                Log::error($exception->getMessage());
            }

            if (isset($do)) {
                $message = 'save vote';
            }
        }

        return Response::json(array(
                    'status_code' => $status_code,
                    'message' => $message), 200
        );
    }

    public function upload() {

        $static = URL::asset('static');
        Session::put('_token', md5(microtime()));
        Session::set('current_route', URL::current());

        $defaul_memes = MemeDefault::where('status', 'active')
                ->take(12)
                ->get();

        if (Session::has('default_meme')) {
            $status_code = Session::get('status_code');
            $default_meme = Session::get('default_meme');
        } else {
            $status_code = '';
            $default_meme = MemeDefault::where('status', 'active')
                    ->orderby((DB::raw('RAND()')))
                    ->take(1)
                    ->first();

            if (!is_null($default_meme)) {
                $default_meme = secure_asset($default_meme->image->path);
            } else {
                $default_meme = '';
            }
        }

        return View::make('site/fun/upload-meme', array(
                    'default_memes' => $defaul_memes,
                    'default_meme' => '',
                    'status_code' => $status_code,
                    'category' => 'sube-tu-meme',
            'url_static' => $static
                        )
        );
    }

    public function do_upload_meme() {

        $user = Auth::user();

        $text_button = strtoupper(Input::get('text2'));
        $text = strtoupper(Input::get('text1'));
        $category = Input::get('category');
        $img = Input::get('image');

        $v = Validator::make(
                        array('image' => $img, 'category' => $category), array(
                    'image' => array('required'),
                    'category' => array('required')
                        ), array(
                    'image.required' => 'La imagen obligatorio',
                    'category.required' => 'La categoria es obligatoria.',
                        )
        );

        if ($v->fails()) {
            Log::error($v->messages());
            return Redirect::back()
                            ->withErrors($v);
        }

        //$ext = strtolower(pathinfo($url_img, PATHINFO_EXTENSION));
        $ext = 'jpg';
            
        /*
        $url_img = public_path().parse_url($image)['path'];
        $img = imagecreatefromfile($url_img);
        $img_size = getimagesize($url_img);
        $text_words = explode(" ", $text);
        $text_words_button = explode(" ", $text_button);
        $textcolor = imagecolorallocate($img, 255, 255, 255);
        $ext = strtolower(pathinfo($url_img, PATHINFO_EXTENSION));
      //  $im_dest = '/home/ronald/Desktop/1.' . $ext;
        $this->deaw_text_on_image($text_words, $text_words_button);

        foreach ($this->tmp_array as $text) {
            $_y = imagettfbbox(30, 0, FONT, $text);
            $width = ($img_size[0] - $_y[2]) / 2;
            imagettftext($img, 30, 0, $width, $this->s, $textcolor, FONT, $text);
            $this->s += 30;
        }
        
        $this->s -= 30;
        
        $text_all_button_height =0;
        foreach ($this->tmp_array2 as $text) {
            $_yy = imagettfbbox(30, 0, FONT, $text);
            $text_all_button_height += abs($_yy[5]);
            
        }

        $this->s += $img_size[1]-50-$text_all_button_height;

        foreach ($this->tmp_array2 as $text) {
            $_y = imagettfbbox(30, 0, FONT, $text);
            $width = ($img_size[0] - $_y[2]) / 2;
            imagettftext($img, 30, 0, $width, $this->s, $textcolor, FONT, $text);
            $this->s += 30;
        }
   
        $this->s -= 30;
   

        //imagedestroy($img);
        */
        
        $meme = new Meme();
        $meme->title = $user->first_name . ' ' . $user->last_name;
        $meme->slug = Meme::get_slug($meme->title);
        $meme->author_id = 1;
        $meme->author_web_id = $user->id;
        $meme->category = $category;
        $meme->status = 'active';
        $meme->description = $meme->title;
        $meme->block_size = 12;
        $meme->like = 0;
        $meme->dislike = 0;

        try {
            $meme->save();
        } catch (Exception $exception) {
            Log::error($exception);
        }

        $upload_path = sprintf(
                '%s/%s/%s', public_path(), 'upload', 'memes');

        $image = new Photo();
        $image->title = $meme->title;
        $image->path = sprintf('%s/%s/%s.%s', 'upload', 'memes', $meme->slug, $ext);
        $image->description = $meme->title;

        try {
            $image->save();
        } catch (Exception $exception) {
            Log::error($exception);
        }

        $meme->image_id = $image->id;

        try {
            $meme->save();
        } catch (Exception $exception) {
            Log::error($exception);
        }

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0777, true);
        }

        @file_put_contents(
                        sprintf('%s/%s.%s', $upload_path, $meme->slug, $ext), get_img_base64($img));

        /*
        if ($ext == 'png') {
            imagepng($img, sprintf('%s/%s.%s', $upload_path, $meme->slug, $ext));
        } elseif ($ext == 'jpg') {
            imagejpeg($img, sprintf('%s/%s.%s', $upload_path, $meme->slug, $ext));
        }
        */
   
        return Redirect::route('freetime_upload')
                        ->with('meme', $meme);
    }

    public function do_upload_me() {
        Validator::extend('pixeles', function($attribute, $value, $parameters) {
            $photo = $value;
            $path = $photo->getRealPath();
            $mime = $photo->getMimeType();
            $src = base64_encode_image($path, $mime);
            list($width, $height) = getimagesize($path);
            if ($width >= 520 and $height >= 520) {
                return true;
            } else {
                return false;
            }
        });

        $v = Validator::make(
                        Input::all(), array(
                    'photo' => array(
                        'image', 'max:700',
                        'mimes:jpeg,bmp,png',
                        'pixeles'),
                        ), array(
                    'photo.image' => 'LA FOTO DEBE SER UNA IMAGEN.',
                    'photo.required' => 'LA IMAGEN ES OBLIGATORIA.',
                    'photo.max' => 'LA IMAGEN DEBE TENER UN PESO MENOR A 700kb.',
                    'photo.mimes' => 'LA IMAGEN DEBE SER DE TIPO: JPG, PNG.',
                    'photo.pixeles' => 'LA IMAGEN DEBE SER MAYOR O IGUAL A 520x520 PIXELES.'
                        )
        );

        if ($v->fails()) {
          
            return Redirect::back()
                            ->withErrors($v)
                            ->with('status_code', 1);
        }

        $photo = Input::file('photo');
        $path = $photo->getRealPath();
        $mime = $photo->getMimeType();
        
        //echo $path;
        //die();
        
        $default_meme = base64_encode_image($path, $mime);

        //echo $default_meme;
        //die();
        
        $defaul_memes = MemeDefault::where('status', 'active')
                ->take(12)
                ->get();

        return View::make('site/fun/upload-meme', array(
                    'default_memes' => $defaul_memes,
                    'default_meme' => $default_meme,
                    'category' => 'sube-tu-meme'
                        )
        );
    }

    public function do_upload_video() {
        $video_title = Input::get('video_title');
        $video_link = Input::get('video_link');

        $v = Validator::make(
                        array('title' => $video_title, 'link' => $video_link), array(
                    'title' => array('required', 'min:3', 'max:64'),
                    'link' => array('required', 'url')
                        ), array(
                    'title.required' => 'El titulo es obligatorio',
                    'link.required' => 'La url del video es obligatoria.',
                    'link.url' => 'La url es incorrecta'
                        )
        );

        if ($v->fails()) {
            return Redirect::back()
                            ->withErrors($v);
        }

        $code = youtube_id_from_url($video_link);

        if (!$code) {
            return Redirect::back()
                            ->with('message', '¡La url es incorrecta!');
        }

        if (Auth::check()) {
            $meme = new Meme();
            $meme->title = $video_title;
            $meme->link = $code;
            $meme->slug = Meme::get_slug($meme->title);
            $meme->author_id = 1;
            $meme->author_web_id = Auth::user()->id;
            $meme->category = 'videos-graciosos';
            $meme->status = 'active';

            try {
                $meme->save();
            } catch (Exception $exception) {
                Log::error($exception);
            }

            return Redirect::back()
                            ->with('message', '¡Se subio tu Video exitosamente!');
        } else {
            Session::push('fun_video.title', $video_title);
            Session::push('fun_video.code', $code);
            return Redirect::route('user_login');
        }
    }

    public function download($id) {
        $meme = Meme::where('id', $id)->first();

        if ($meme) {
            if (!is_null($meme->image)) {
                $url = $meme->image->path;
                $upload_path = sprintf(
                        '%s/%s', public_path(), $url);
                return Response::download(
                                $upload_path, 200
                );
            }
        }
    }

}
