define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){
  function editProfileRegisterUser($form){

    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {
        pass: {
          required: true
        },
        repass: {
          required: true,
          equalTo: '#pass'
        }
      },
      messages: {
        pass: {
          required: 'Olvidaste  ingresar tu password.'
        },
        repass: {
          required: 'Olvidaste  confirmar tu password.',
          equalTo: 'Tu contraseña no coinciden.'
        }
      },
      submitHandler: function(form){
        form.submit();
      }
    });
  }
  return editProfileRegisterUser;
});
