    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'juegos'))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('games') }}">Juegos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="orbit slider-game slideshow-wrapper">
          <div class="preloader"></div>
          <ul data-orbit>
            @foreach($items as $row)
            @if($row->image_cover)
             <li class="banner-game"><img src="{{ asset($row->image_cover->path) }}">
              <div class="caption">
                <h2>{{ $row->description }}</h2><a href="{{ URL::route('games_detail', $row->slug) }}" class="btn-slider">Jugar</a>
              </div>
            </li>
            @endif
            @endforeach
          </ul>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="games">
          <div class="row">
            <div class="columns large-12 top-banner">
            </div>
          </div>
          <div class="row more-games">
            @foreach($items as $item)
            <div class="columns large-4 left"><a href="{{ URL::route('games_detail', $item->slug) }}" class="more-games">
                <div class="figure"><img src="@if (isset($item->image_id)) {{ asset($item->image->path) }} @else http://placehold.it/300x140 @endif">
                  <div class="figcaption icon icon-category"><img src="{{ asset('static/site/images/icon-games.png') }}"></div>
                  <div class="fade"></div>
                  <h3>{{ $item->title }}</h3>
                  <div class="left">
                    <p>Puntuación</p>
                  </div>
                  <div class="star">
                    <ul class="star">
                      <li class="small-{{ $item->get_face() }}"></li>
                    </ul>
                  </div>
                </div></a><a href="{{ URL::route('games_detail', $item->slug) }}" class="btn-play">Jugar</a></div>
            @endforeach
          </div>
          <div class="mb-50"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'juegos'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'juegos'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/games') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
