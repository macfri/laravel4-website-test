@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>
  <!--  <li><a href="{{ URL::route('admin_user_add') }}">Create</a></li> -->
</ul>

<form id="filter_form" method="GET">
    <div class="pull-right">
        <button type="submit" class="btn btn-primary" style="">Apply</button>
        <a href="{{ URL::route('admin_user_export') }}" class="btn btn-primary" style="">Exportar</a>
    </div>
    <table class="filters">
        <tbody>
            <tr>
                <td>Nombres, DNI or Email: &nbsp;</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>

            <tr>
                <td>Complete data: &nbsp;</td>
                <td><input type="checkbox" name="chk_user_complete" @if($chk_user_complete) checked @endif value="1"></td>
            </tr>

<!--
            <tr>
                <td>User Redes sociales: &nbsp;</td>
                <td>

                    <input type="checkbox" name="chk_facebook" value="1">Facebook
                    <input type="checkbox" name="chk_facebook" value="1">Google
                    <input type="checkbox" name="chk_facebook" value="1">Todos
</td>
            </tr>
-->


<!--
            <tr>
                <td>Status</td>
                <td>{{ Form::select('search_status',
                    array('active' => 'active', 'inactive' => 'inactive'), $search_status) }}</td>
            </tr> -->

        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
        <!-- <th class="span1"><input type="checkbox" name="rowtoggle" class="action-rowtoggle" /></th> -->
        <th class="span1">&nbsp;</th>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">dni</th>
        <th class="span1">created_at</th>
      <!--  <th class="span1">status</th> -->
    </tr>
    </thead>
    @if($total > 0)
	    @foreach ($items as $p)
	    <tr>
	        <!--<td><input type="checkbox" name="rowid" class="action-checkbox" value="{{ $p->id }}" /></td>-->
	        <td><a class="icon" href="{{ URL::route('admin_user_edit', $p->id) }}"><i class="icon-pencil"></i></a></td>
	        <td><a href="{{ URL::route('admin_user_edit', $p->id) }}">{{ $p->first_name }}</a></td>
	        <td>{{ $p->last_name }}</td>
	        <td>{{ $p->email }}</td>
	        <td>{{ $p->dni }}</td>
	        <td>{{ $p->created_at }}</td>
	    </tr>
	    @endforeach
    @else
    <tr><td colspan="6">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>
@stop
