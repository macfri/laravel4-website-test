define([
  'jquery'
], function($){

  function navFixed($nav_fixed, class_fixed){
    var $window = $(window);
    $window.on('scroll', setFixed);
    var nav_pos = $nav_fixed.position().top;

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    $(window).resize(function() {
        delay(function(){
          nav_pos = $nav_fixed.position().top;
        }, 500);
    });

    function setFixed(){
      if ($window.scrollTop() > nav_pos) {
        $nav_fixed.addClass(class_fixed);
        $('.content').css({'padding-top': '30px'});
      }
      else {
        $nav_fixed.removeClass(class_fixed);
        $('.content').css({'padding-top': '0'});

      }
    }
  }

  return navFixed;
});
