<script type="text/javascript" src="{{ asset($item->path_swf) }}/js/swfobject2.2/swfobject.js"></script>
<script type="text/javascript">
window.game = window.game || {};
game.auth = '@if(Auth::check()) auth_true @endif'
game.user_complete = '{{ $user_complete }}';

function insertSwf()
{
    var flashvars = {};
    flashvars.as_swf_name = "flashObject";
    var params = {};
    params.allowscriptaccess = "always";
    params.wmode = "transparent"; // direct
    params.flashvars = '{{ $flashvars }}';

    swfobject.embedSWF("{{ asset($item->path_swf) }}/main_standalone.swf?v=57",
        "flashContent",
        "970", "582", "10.0.0",
        "{{ asset($item->path_swf) }}/js/swfobject2.2/expressInstall.swf",
        flashvars, params ,{id:"main" , name:"main"});
}

insertSwf();

function onEndLoadGame() {
    $('.js-trigger-login').on('click', function(event){
        event.preventDefault();
        $('.js-login').trigger('click');
        $('.js-game-modal').toggleClass("hide");
    });

    $('.js-trigger-register').on('click', function(event){
        event.preventDefault();
        $('.btn-register').trigger('click');
        $('.js-game-modal').toggleClass("hide");
    });
}

function onShareCamiseta(picture,type) {
    _gaq.push(['_trackEvent', 'Web-RedesSociales', 'Compartirfacebook']);
    if(type==1){
        msg="¡Tú también vive la fiebre del fútbol! Crea tu camiseta y haz tu diseño realidad. Ingresa aquí y gana camisetas para todo tu equipo.";
        pic='https://www.milo.com.pe/static/site/images/share.jpg';
        name='Crea tu Camiseta Milo';
    }if(type>1){
        name='¡Acabo de crear mi camiseta!';
        msg="Tú también diseña la tuya y Milo la hace realidad. Llévate camisetas para todo tu equipo, aquí";
        pic=picture;
    }
    FB.ui({
      method: 'feed',
      picture: pic,
      link: 'https://www.milo.com.pe/juegos/crea-tu-camiseta',
      name: name,
      description: msg
    }, function(response){

    });
}

function trackEventFlash (category,action,label,value) {
     _gaq.push([category,action, label, value]);
}

function onRegisterChulls(){
    $('.js-game-modal').toggleClass("hide");
}

function reloadGame(){
    //window.location.reload();
}

function onChooseItemChulls(data)
{
    var obj = JSON.parse(data);
    var posting = $.post("{{ URL::route('api_game_chulls_choose') }}", {
        token: obj.token,
        tries: obj.tries,
        guess: obj.guess,
        lifetime: obj.lifetime
    }, "json");

    posting.done(function(data) {
    });
}

function onStartChulls(data)
{
    var obj = JSON.parse(data);
    var posting = $.post("{{ URL::route('api_game_chulls_start') }}", {
        token: obj.token
    }, "json");

    posting.done(function(data) {
    });
}

function onShareFbChulls(data)
{
    var obj = JSON.parse(data);
    FB.ui({
      method: 'feed',
      link: 'https://www.milo.com.pe/juegos/chulls',
      picture: 'https://www.milo.com.pe/static/site/images/icon-250x250.jpg',
      name: 'Yo ya armé mi lista de 3 regalazos',
      description: 'Participa tú también. ¡Arma tu lista y el Chulls te la concederá'
    }, function(response){

        if(response && response.post_id) {
            var posting = $.post("{{ URL::route('api_game_chulls_share') }}", {
                token: obj.token
            }, "json");

            posting.done(function(data) {
                window.location.reload();
            });
        }else{
            window.location.reload();
        }
    });
}

function onShareCucharitas () {
   FB.ui({
        method: 'feed',
        link: 'https://www.milo.com.pe/juegos/3cucharaditas',
        picture: 'https://www.milo.com.pe/static/site/images/icon-cucharadas.jpg',
        name: 'Descubre todo el poder de 3 cdtas.',
        description: 'Recárgate al máximo con MILO y demuestra todo tu energía en el vóley o en el fútbol. ¡Tú eliges!'
      }, function ( response ) {});
}



function checkStatus(){
 if(game.auth === ''){
       $('.js-game-modal').toggleClass("hide");
    }
    else if(game.user_complete == 0){
        window.location.href = "{{ URL::route('profile_edit') }}";
    }
}

function onSaveChulls(data)
{
    var obj = JSON.parse(data);
    var posting = $.post("{{ URL::route('api_game_chulls') }}", {
        guess: obj.guess,
        token: obj.token,
        item1: obj.item1,
        item2: obj.item2,
        item3: obj.item3,
        tries: obj.tries,
        lifetime: obj.lifetime
    }, "json");

    posting.done(function(data) {
        if(game.auth === '' && obj.event != 0 ){
            $('.js-game-modal').toggleClass("hide");
        }
        else if(game.user_complete == 0 && obj.event != 0){
            window.location.href = "{{ URL::route('profile_edit') }}";
        }
    });
}

function onSaveData(data) {
    
    var obj = JSON.parse(data);
    
    var trophies_len = obj.trophies.length;
    for( i = 0; i < trophies_len; i++ ) {
        $('.trophies-'+ obj.trophies[i]).removeClass('inactive');
    }

    var posting = $.post("{{ URL::route('api_game') }}", {
        token: obj.token,
        user_id: obj.user_id,
        game_id: obj.game_id,
        level: obj.level,
        score_level: obj.score_level,
        score_total: obj.score_total,
        trophies: obj.trophies,
        time_game: obj.time_game,
        event_id: obj.event
    }, "json");

    posting.done(function(data) {
    });

    // obj.event == 1 -> gano
    // obj.event == 2 -> perdio

    if(game.auth === '' && obj.event != 0 ){
        //if(game.auth === '' && obj.event != 0 ){
        $('.js-game-modal').toggleClass("hide");
    }
    else if(game.user_complete == 0 && obj.event != 0){
        window.location.href = "{{ URL::route('profile_edit') }}";
    }
}


function onSaveDataFacebookTshirt(data) {
    
    $('.js-game-modal').toggleClass("hide");

}


function callRetryGame() {
    var flashObject = getFlashMovieObject("main");
    flashObject.onFlashRetryGame();
}

function inviteFriends(max,exclude_friends,index){
    if(index>-1){
        max_recipients=1;
    }
    FB.ui({method: 'apprequests',title:'Crea tu Camiseta', max_recipients:max,exclude_ids:exclude_friends,message: 'Te invito a formar parte de mi equipo.', data: 'tracking information for the user' },
    function(response) {
        if(response!=null){
            if(response.to!=null){
            var flash=getFlashMovieObject('main');
               if(index==-1){
                  flash.getIDS(response.to);
               }if(index>=0){
                  flash.changeID(response.to,index);
               }
            }
        }
    });
      
}

function gameTracking(game_name, game_action){
    _gaq.push(['_trackEvent','Web-Contenido', game_name, game_action]);
}

function getFlashMovieObject(movieName){
    if (window.document[movieName]){
        return window.document[movieName];
    }

    if (navigator.appName.indexOf("Microsoft Internet")==-1){
        if (document.embeds && document.embeds[movieName])
            return document.embeds[movieName];
    }
    else{
        return document.getElementById(movieName);
    }
}


function modalRegister() {
    $('#register-facebook-modal').foundation('reveal', 'open');
}

</script>

<div id="mainwrap">
    <div id="flashwrap">
        <style type="text/css" media="screen">
            #flashwrap, #mainwrap, #flashContent { width:100%; height:100%; }
        </style>
        <div id="flashContent">
            <a href="http://www.adobe.com/go/getflashplayer" target="_blank">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
            </a>
        </div>
    </div>
</div>
