<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSpotsOrder extends Migration {

	public function up()
    {
        Schema::table('spots', function($t) {
            $t->integer('order')
                ->index();
        });
	}

	public function down()
    {
        Schema::table('spots', function($t) {
            $t->dropColumn('order');
        });
	}

}
