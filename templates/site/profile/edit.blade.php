    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'profile'))
        <!-- MENU -->
        </div>

        <div class="banner header-profile">
        <!-- HEADER PROFILE -->
        @include('site.profile.menu')
        <!-- HEADER PROFILE -->
        </div>
      </div>

    </div>
    <div class="bg-home">
      <div class="content">
        <div class="row edit-profile-fb">
          <h2>EDITAR PERFIL</h2>
          <div data-section="tabs" class="section-container tabs">
            <div class="section active">
              <p class="title number-1"><a href="{{ URL::route('profile_edit') }}" class="profile-link-tab">Cuenta</a></p>
              <div data-slug="panel1" class="content">
                @if(Session::has('message'))<p style="background: #009A44; color: #fff; height: 25px">{{ Session::get('message') }}</p>@endif
                <h3>datos personales</h3>
                <p>Para poder participar de nuestros concursos y promociones, necesitamos algunos datos.</p>
                <form id="profile_edit" name="profile_fb" action="" method="post" class="custom">{{ Form::token() }}
                  <input id="user_id" type="hidden" value="{{ $user->id }}">
                  <div class="row">
                    <div class="columns large-6">
                      <label>Tu Nombre</label>
                      <input type="text" name="first_name" id="first_name" maxlength="50" value="@if( isset($user->first_name) ){{ $user->first_name }}@endif" class="text">
                        @if($errors->has('first_name'))<label for="email" class="error">{{ $errors->first('first_name') }}</label>@endif
                    </div>
                    <div class="columns large-6">
                      <label>Tu Apellido</label>
                      <input type="text" name="last_name" id="last_name" maxlength="50" value="@if( isset($user->last_name) ){{ $user->last_name }}@endif" class="text">
                        @if($errors->has('last_name'))<label for="email" class="error">{{ $errors->first('last_name') }}</label>@endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="columns large-6">
                      <label>Tu Correo electrónico</label>
                      <input type="text" name="email" id="email" value="@if( isset( $user->email ) ){{ $user->email }}@endif">
                        @if($errors->has('email'))<label for="email" class="error">{{ $errors->first('email') }}</label>@endif
                    </div>
                    <div class="columns large-6">
                      <label>¿Qué edad tienes?</label>
                      <div class="row">
                        <div class="columns large-4">
                          <select name="day" id="day">
                            <option value="">Día</option>@foreach ($days as $day=>$row)
                            @if( $day == $user->day )
                            <option value="{{ $day }}" selected>{{ $row }}</option>@else
                            <option value="{{ $day }}">{{ $row }}</option>@endif
                            @endforeach                      
                          </select>
                        </div>
                        <div class="columns large-4">
                          <select name="month" id="month">
                            <option value="">Mes</option>@foreach ($months as $month=>$row)
                            @if($month == $user->month )
                            <option value="{{ $month }}" selected>{{ $row }}</option>@else
                            <option value="{{ $month }}">{{ $row }}</option>@endif
                            @endforeach
                          </select>
                        </div>
                        <div class="columns large-4">
                          <select name="year" id="year">
                            <option value="">Año</option>@foreach ($years as $year=>$row)
                            @if($year == $user->year)
                            <option value="{{ $year }}" selected>{{ $row }}</option>@else
                            <option value="{{ $year }}">{{ $row }}</option>@endif
                            @endforeach
                          </select>
                        </div>
                        @if($errors->has('date_birth'))<label for="email" class="error">{{ $errors->first('date_birth') }}</label>@endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="columns large-6">
                      <label>DNI</label>
                      <input type="text" name="dni" id="dni" maxlength="8" value="@if( isset( $user->dni ) ){{ $user->dni }}@endif" class="numeric">
                        @if($errors->has('dni'))<label for="email" class="error">{{ $errors->first('dni') }}</label>@endif
                    </div>
                    <div class="columns large-6"><span class="text-help">Si eres menos de edad, necesitamos el<br>DNI de tu papá, mamá o apoderado</span></div>
                  </div>

                  <div class="row">
                    <div class="columns large-6">
                      <label>Teléfono</label>
                      <input type="text" name="phone" id="phone" maxlength="9" value="@if( isset( $user->phone ) ){{ $user->phone }}@endif" class="numeric">
                        @if($errors->has('phone'))<label for="phone" class="error">{{ $errors->first('phone') }}</label>@endif
                    </div>
                  </div>



                  <div class="row">
                    <div class="columns large-12">
                      <label class="check">@if( $user->authorize_father == 1 )
                        <input type="checkbox" name="authorize_father" id="authorize_father" value="1" checked>@else
                        <input type="checkbox" name="authorize_father" id="authorize_father" value="1">@endif
                        <p>Mis padres autorizan que participe de las promociones y concursos.</p>
                      </label>
                      <div class="authorize-error"></div><br>
                    </div>
                  </div>
                  <div class="row">
                    <div class="columns large-12">
                      <label class="check">@if( $user->reply_email == 1 )
                        <input type="checkbox" name="reply_email" id="reply_email" value="1" checked>@else
                        <input type="checkbox" name="reply_email" id="reply_email" value="1">@endif
                        <p>Deseo recibir notificaciones y promociones sobre las novedades de Milo.</p>
                      </label>
                    </div>
                  </div>

                <div class="row">
                  <div class="columns large-12 h-45">
                    <label class="check">
                        @if( $user->is_lima == 1 )
                        <input type="checkbox" name="is_lima" id="is_lima" checked="checked" value="1">
                      @else
                          <input type="checkbox" name="is_lima" id="is_lima" value="1">
                      @endif
                      <p>Eres de lima.</p>
                    </label>
                    <div class="authorize-error"></div><br>
                  </div>
                </div>

                  <div class="row">
                    <div class="columns large-12">
                      @if( Session::has('mundial_game'))
                      <button class="right btn-continue" onclick="_gaq.push(['_trackEvent','Web-Conversion','Paso2-Guardardatos',''])">Guardar</button>@else
                      <button class="right btn-continue">Guardar</button>
                      @endif
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="section">
              <p class="title number-2"><a href="{{ URL::route('profile_change_password') }}" class="profile-link-tab">Contraseña</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'profile'))
        <!-- FOOTER -->
      </div>
    </div>
    <script>
      window.app = window.app || {};
      app.user_update_cover = "{{ URL::route('user_update_cover') }}";
      app.validate_profile_email = "{{ URL::route('validate_form_email') }}";
      app.validate_profile_dni = "{{ URL::route('validate_form_dni') }}";
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/profile') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
