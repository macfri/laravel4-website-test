define([
  'jquery'
], function($){

  function acordeon($item){

    $item.mouseenter(function(){
      $(this).animate({width:"162px"},{queue:false});
    }).mouseleave(function() {
      $(this).animate({width:"40px"},{queue:false});
    });

  }
  return acordeon;
});
