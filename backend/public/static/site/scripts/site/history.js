require.config({
  paths: {
    'jquery':'../libs/jquery',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'jquery.localscroll': '../libs/plugins/jquery.localscroll',
    'foundation': '../libs/plugins/foundation',

    'hashscroll': '../modules/hashscroll',

    'scrolltop': '../modules/scrolltop',
    'magellan': '../modules/magellan',
    'resize': '../modules/resize',
    'modal': '../modules/modal',
    'auth': '../modules/auth',
    'register': '../modules/register'
  },
  shim: {
    'jquery.scrolltop': ['jquery'],
    'jquery.localscroll': ['jquery.scrolltop'],
    'foundation' : ['jquery'],
    'hashscroll': ['jquery.localscroll'],
  
    'scrolltop': ['jquery.scrolltop'],
    'magellan' : ['foundation'],
    'resize': ['jquery'],
    'auth': ['jquery'],
    'register': ['jquery']
  }
});

require([
  'jquery',
  'magellan',
  'scrolltop',
  'hashscroll',
  'resize',
  'modal',
  'auth',
  'register'
], function ($, magellan, scrollTop, hashscroll, resize, modal, auth, register) {
  magellan();
  scrollTop($('.bg-home .inner'));
  hashscroll();
  resize($('.history .item'));
  modal();
  auth($('#login'));
  register($('#register'));
});
