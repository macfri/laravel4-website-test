<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration {

	public function up()
    {
		Schema::create('users', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username', 100)->unique()->nullable();
            $table->string('password', 100);
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('email', 100)->unique('email');
            $table->string('dni', 20)->unique('dni')->nullable();
            $table->string('phone', 20);
            $table->boolean('reply_email')->default(false)->nullable();
            $table->string('cod_dpto', 2)->nullable();
            $table->string('cod_prov', 2)->nullable();
            $table->string('cod_dist', 2)->nullable();
            $table->enum('status', array('active', 'inactive'))
                ->default('inactive')->index();
            $table->string('activation', 200)->unique()
                ->nullable();
            $table->string('activation_password', 200)->unique()
                ->nullable();
            //$table->string('activation_password_fb', 200)->unique()
                //->nullable();
            $table->string('token_game', 200)->unique()
                ->nullable();
            $table->integer('facebook_id')->unsigned()->index()->nullable();
            $table->foreign('facebook_id')
                ->references('id')->on('users_facebook')
                ->on_update('cascade')
                ->onDelete('cascade');
            $table->integer('google_id')->unsigned()->index()->nullable();
            $table->foreign('google_id')
                ->references('id')->on('users_google')
                ->on_update('cascade')
                ->onDelete('cascade');
            $table->datetime('last_login_at');
            $table->timestamps();
            $table->boolean('is_new')->default(true)->nullable();
            $table->string('picture', 500);
            $table->boolean('email_send')->default(false)->nullable();
            $table->date('date_birth')->nullable();
            $table->integer('level_score');
        });
	}

	public function down()
    {
        Schema::dropIfExists('users');
	}
}
