<?php

class UserController extends BaseController {

    public function add() {
        Session::put('_token', md5(microtime()));
        if (Auth::check()) {
            return Redirect::route('home');
        }

        $days = array();
        for ($i = 1; $i <= 31; $i++) {
            $days[] = $i;
        }

        $months = array(
            '1' => 'Ene', '2' => 'Feb', '3' => 'Mar',
            '4' => 'Abr', '5' => 'May', '6' => 'Jun',
            '7' => 'Jul', '8' => 'Ago', '9' => 'Set',
            '10' => 'Oct', '11' => 'Nov', '12' => 'Dic'
        );

        $years = array();
        for ($i = date('Y') - 6; $i >= date('Y') - 50; $i--) {
            $years[$i] = $i;
        }

        return View::make('site/users/register-simple', array(
                    'days' => $days,
                    'months' => $months,
                    'years' => $years,
        ));
    }

    public function save() {

        $day = Input::get('day');
        $month = Input::get('month');
        $year = Input::get('year');
        $reply_email = Input::get('reply_email') == null ? 0 : 1;
        $authorize_father = Input::get('authorize_father') == null ? 0 : 1;

        Input::merge(array(
            'date_birth' => sprintf('%s-%s-%s', $year, $month, $day),
            'reply_email' => $reply_email,
            'authorize_father' => $authorize_father
        ));

        $v = User::validate_simple(Input::all());

        if (!$v->passes()) {
            return Redirect::route('user_view_simple')
                            ->with('status_code', 1)
                            ->withInput()
                            ->withErrors($v);
        }

        $user = new User();
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->dni = Input::get('dni');
        $user->phone = Input::get('phone');
        $user->authorize_father = Input::get('authorize_father');
        $user->reply_email = Input::get('reply_email');
        $user->date_birth = Input::get('date_birth');
        $user->password = Hash::make(Input::get('password'));
        $user->token_game = md5(microtime(TRUE) . rand(0, 100000));
        $user->is_lima = (bool) Input::get('is_lima');
        $user->last_login_at = date('Y-m-d H:i:s', time());
        $user->picture = '';
        $user->level_score = 0;
        $user->age = 0;

        $user->save();
        Auth::login($user);

        #print_r($_SESSION);
        #die();
        ///_Event::fire('guest.save_points');
        /*
          $data = array('first_name' => $user->first_name);
          $message_data['to'] = $user->email;
          $message_data['name'] = $user->first_name;

          Mail::send('emails.welcome', $data,
          function($message) Use ($message_data){
          $message->to(
          $message_data['to'],
          $message_data['name']
          )->subject('Welcome!');
          }); */
        //   return Redirect::route('user_login_game');




        if (Session::has('guest_game_id')) {
            $game = Game::where('id', Session::get('guest_game_id'))->first();
            if ($game->slug == 'knockea-a-la-almohada') {
                _Event::fire('guest.save_round_anonymous');
            }
        }
        _Event::fire('guest.save_fun_video');
        _Event::fire('guest.save_points');
        _Event::fire('user.send_mail', array($user));
        Session::regenerate();
        return Redirect::to(Session::get('current_route'));
    }

    public function profile() {

        Session::set('current_route', URL::current());
        $user = Auth::user();
        $profile_images = array();

        if (Session::has('choose_photo')) {
            $profile_images = get_images_profile(
                    $user->facebook->fb_oauth_token);
        }

        $last_badges = PlayerBadge::where('user_id', $user->id)
                ->take(5)
                ->get();

        return View::make(
                        'site/profile/profile', array(
                    'last_badges' => $last_badges,
                    'most_played' => $user->most_played(),
                    'profile_images' => $profile_images
                        )
        );
    }

    public function profile_edit() {



        Session::put('_token', md5(microtime()));
        Session::set('current_route', URL::current());


        //print_r(Session::all());


        $user = Auth::user();

        $profile_images = array();
        if (Session::has('choose_photo')) {
            $profile_images = get_images_profile(
                    $user->facebook->fb_oauth_token);
        }

        $days = array();
        for ($i = 1; $i <= 31; $i++) {
            $days[] = $i;
        }

        $months = array(
            '1' => 'Ene', '2' => 'Feb', '3' => 'Mar',
            '4' => 'Abr', '5' => 'May', '6' => 'Jun',
            '7' => 'Jul', '8' => 'Ago', '9' => 'Set',
            '10' => 'Oct', '11' => 'Nov', '12' => 'Dic'
        );

        $years = array();
        for ($i = date('Y') - 6; $i >= date('Y') - 50; $i--) {
            $years[$i] = $i;
        }

        $user->year = date('Y', strtotime($user->date_birth));
        $user->month = date('m', strtotime($user->date_birth));
        $user->day = date('d', strtotime($user->date_birth));

        return View::make(
                        'site/profile/edit', array(
                    'user' => $user,
                    'days' => $days,
                    'months' => $months,
                    'years' => $years,
                    'profile_images' => $profile_images
                        )
        );
    }

    public function profile_update() {

        $day = Input::get('day');
        $month = Input::get('month');
        $year = Input::get('year');
        $reply_email = Input::get('reply_email') == null ? 0 : 1;
        $authorize_father = Input::get('authorize_father') == null ? 0 : 1;

        Input::merge(array(
            'date_birth' => sprintf('%s-%s-%s', $year, $month, $day),
            'reply_email' => $reply_email,
            'authorize_father' => $authorize_father
        ));

        $v = User::validate(Input::all());

        if (!$v->passes()) {
            Log::error($v->messages());
            return Redirect::back()
                            ->with('status_code', 1)
                            ->withInput()
                            ->withErrors($v);
        }

        $user = Auth::user();
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->phone = Input::get('phone');
        $user->dni = Input::get('dni');
        $user->authorize_father = Input::get('authorize_father');
        $user->reply_email = Input::get('reply_email');
        $user->date_birth = Input::get('date_birth');
        $user->is_lima = (bool) Input::get('is_lima');
        $user->save();

        if (Session::has('guest_game_id')) {
            $game = Game::where('id', Session::get('guest_game_id'))->first();
            if ($game->slug == 'knockea-a-la-almohada') {
                _Event::fire('guest.save_round_anonymous');
                return Redirect::route('games_detail', $game->slug);
            }
        }

        _Event::fire('guest.save_fun_video');
        _Event::fire('guest.save_points');

        return Redirect::back()
                        ->with('message', 'Tus datos se guardaron correctamente')
                        ->withInput();
    }

    public function profile_validate_email() {
        if (!Auth::check()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $value = Input::get('value');

        $v = Validator::make(
                        array('value' => $value), array('value' => array('required', 'min:3', 'max:64', 'email'))
        );

        if ($v->fails()) {
            return Response::make('Page not found', '404');
        }

        $item = User::where('email', $value)
                ->where('id', '<>', Auth::user()->id)
                ->count();

        return $item > 0 ? 'false' : 'true';
    }

    public function profile_validate_dni() {
        if (!Auth::check()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $value = Input::get('value');

        Validator::extend('dni', function($attribute, $value, $parameters) {
            if (strlen($value) == 8 and is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        });

        $input = array('value' => $value);
        $rules = array('value' => array('required', 'dni'));
        $messages = array('dni' => 'El dni es inválido');

        $v = Validator::make($input, $rules, $messages);

        if ($v->fails()) {
            return Response::make('Page not found', '404');
        }

        $item = User::where('dni', $value)
                ->where('id', '<>', Auth::user()->id)
                ->count();
        return (bool) $item > 0 ? 'false' : 'true';
    }

    public function profile_upload_photo() {
        $rules = array(
            'photo' => 'required|image|mimes:jpeg,png,gif|max:100'
        );

        $v = Validator::make(Input::all(), $rules);

        if (!$v->passes()) {
            return Redirect::route('profile_edit')
                            ->withErrors($v);
        }

        $file = Input::file('photo');
        $name = $file->getClientOriginalName();
        $size = $file->getSize();
        $ext = pathinfo($name, PATHINFO_EXTENSION);

        $upload_path = sprintf(
                '%s/%s/%s', public_path(), 'upload', 'profile');

        $user = Auth::user();
        $file->move(
                $upload_path, sprintf('%s.%s', $user->id, $ext)
        );

        $user->picture = secure_asset(sprintf(
                        'upload/profile/%s.%s', $user->id, $ext));

        try {
            $user->save();
        } catch (Exception $exception) {
            Log::error($exception);
        }

        return Redirect::route('profile_edit');
    }

    public function profile_update_cover() {
        if (!Auth::check()) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $status_code = 0;
        $user = Auth::user();
        $cover_id = (int) Input::get('cover_id', 0);

        if ($cover_id < 1 or $cover_id > 4) {
            App::abort(404, 'Page not found');
        }

        $user->cover_id = $cover_id;

        try {
            $user->save();
        } catch (Exception $exception) {
            Log::error($exception);
            $status_code = 1;
        }

        return Response::json(array(
                    'status_code' => $status_code), 200
        );
    }

    public function profile_choose_photo_facebook() {
        Session::put('choose_photo', true);
        return Redirect::route('user_login_facebook');
    }

    public function profile_update_photo_facebook() {
        $user = Auth::user();
        $src = Input::get('src');

        if (!filter_var($src, FILTER_VALIDATE_URL)) {
            return Response::view('site.not-found', array(), 404);
        }

        if (strlen($src) > 0) {
            $user->picture = $src;

            try {
                $user->save();
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
            }
        }

        return Redirect::back();
    }

    public function profile_change_password() {
        Session::put('_token', md5(microtime()));
        Session::set('current_route', URL::current());
        $user = Auth::user();

        $profile_images = array();
        if (Session::has('choose_photo')) {
            $profile_images = get_images_profile(
                    $user->facebook->fb_oauth_token);
        }

        return View::make(
                        'site/profile/password', array('profile_images' => $profile_images
                        )
        );
    }

    public function profile_change_password_do() {
        $user = Auth::user();
        $old_password = Input::get('old_password');
        $password = Input::get('password');
        $repassword = Input::get('repassword');

        Validator::extend('strong_password', function($attribute, $value, $parameters) {
            if (preg_match(
                            "#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $value)) {
                return true;
            } else {
                return false;
            }
        });

        Validator::extend('check_old_password', function($attribute, $value, $parameters) {
            if (!is_null($value)) {
                return Hash::check($value, Auth::user()->password);
            } else {
                return false;
            }
        });

        $input = array(
            'old_password' => $old_password,
            'password' => $password,
            'password_confirmation' => $repassword);

        $rules = array();
        if (strlen($user->password) > 0) {
            $rules['old_password'] = 'required|check_old_password';
        }
        $rules['password'] = 'Required|Min:8|Max:20|strong_password|Confirmed';

        $messages = array(
            'check_old_password' => 'Tu constraseña anterior es incorrecta.',
            'old_password.required' => 'La contraseña anterior es obligatorio.',
            'password.required' => 'La nueva contraseña es obligatorio.',
            'strong_password' => 'La nueva contraseña no es segura, (Crea a partir de una combinación de números, letras mayúsculas y minúsculas)',
            'password.min' => 'La longitud minima de la nueva contraseña es de :min.',
            'password.max' => 'La longitud máxima de la nueva contraseña es de :max.',
            'confirmed' => 'La nueva contraseña con la contraseña de confirmación no coinciden.'
        );

        $v = Validator::make($input, $rules, $messages);

        if ($v->fails()) {
            return Redirect::back()
                            ->withErrors($v);
        }

        $user->password = Hash::make($password);
        $user->save();

        return Redirect::back()
                        ->with('message', 'Tus datos se guardaron correctamente.')
                        ->with('status_code', 0);
    }

    public function forgot_password() {
        Session::put('_token', md5(microtime()));
        return View::make('site/users/forgot-password');
    }

    public function forgot_password_do() {
        $ip = Request::getClientIp();
        $login_attempt = LoginAttempt::where('ip', $ip)
                ->where('type', 'fp')
                ->first();

        if (!$login_attempt) {
            $login_attempt = new LoginAttempt();
            $login_attempt->ip = $ip;
            $login_attempt->attempts = 0;
            $login_attempt->type = 'fp';
            $login_attempt->last_login = time();
            $login_attempt->save();
        }

        $status_code = 0;
        $email = Input::get('email');

        $messages = array(
            'min' => 'La longitud minima es de :min.',
            'max' => 'La longitud máxima es de :max.',
            'required' => 'Este campo es obligatorio.',
            'email' => 'Necesitas un correo válido',
        );

        $v = Validator::make(
                        array('email' => $email), array('email' => array('required', 'min:3', 'max:64', 'email')), $messages
        );

        if ($v->fails()) {
            return Redirect::route('user_forgot_password')
                            ->with('message', $v->messages()->first('email'));
        }

        $user = User::where('email', '=', $email)->first();

        $message_send = 'Se te envio un correo para restablecer tu correo.';
        $message_block = 'Muchos intentos, Regrese en 1 hora.';
        $message_not_email = 'Tu Correo electrónico no existe.';
        $attack_attempts = 10;
        #$attack_block_time = 3600;
        $attack_block_time = 3600;

        function send($user) {
            $user->activation_password = md5(
                    microtime(TRUE) . rand(0, 100000));

            try {
                $user->save();
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
            }

            $data = array(
                'first_name' => $user->first_name,
                'url' => URL::route(
                        'user_do_change_password', $user->activation_password),
            );

            $message_data['to'] = $user->email;
            $message_data['name'] = $user->first_name;
            Mail::send('emails.change_password', $data, function($message) Use ($message_data) {
                $message->to(
                        $message_data['to'], $message_data['name']
                )->subject('¿Olvidaste tu contraseña?');
            });
        }

        if ($user) {
            if ($login_attempt->attempts > $attack_attempts) {
                $life_time = (time() - $login_attempt->last_login);
                if ($life_time > $attack_block_time) {
                    $affectedRows = LoginAttempt::where('ip', $ip)
                            ->where('type', 'fp')
                            ->update(
                            array(
                                'attempts' => 0,
                                'last_login' => time()
                            )
                    );
                    send($user);
                    return Redirect::route('user_forgot_password')
                                    ->with('message', $message_send);
                } else {
                    return Redirect::route('user_forgot_password')
                                    ->with('message', $message_block);
                }
            } else {
                $affectedRows = LoginAttempt::where('ip', $ip)
                                ->where('type', 'fp')->update(
                        array(
                            'attempts' => 0,
                            'last_login' => time()
                        )
                );
                send($user);
                return Redirect::route('user_forgot_password')
                                ->with('message', $message_send);
            }
        } else {
            if ($login_attempt->attempts > $attack_attempts) {
                $life_time = (time() - $login_attempt->last_login);
                if ($life_time > $attack_block_time) {
                    return Redirect::route('user_forgot_password')
                                    ->with('message', $message_not_email);
                } else {
                    return Redirect::route('user_forgot_password')
                                    ->with('message', $message_block);
                }
            } else {
                $attempts = $login_attempt->attempts + 1;
                $affectedRows = LoginAttempt::where('ip', $ip)
                        ->where('type', 'fp')
                        ->update(
                        array(
                            'attempts' => $attempts,
                            'last_login' => time()
                ));
                return Redirect::route('user_forgot_password')
                                ->with('message', $message_not_email);
            }
        }
    }

    public function reset_password($token) {
        Session::put('_token', md5(microtime()));

        if (!Session::has('status_code')) {
            $item = User::where('activation_password', $token)
                    ->firstOrFail();
        }

        return View::make('site/users/reset-password', array('token' => $token)
        );
    }

    public function reset_password_do($token) {
        $user = User::where('activation_password', $token)
                ->firstOrFail();

        $status_code = 0;
        $password = Input::get('password');
        $repassword = Input::get('repassword');

        Validator::extend('strong_password', function($attribute, $value, $parameters) {
            if (preg_match(
                            "#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $value)) {
                return true;
            } else {
                return false;
            }
        });

        $input = array(
            'password' => $password,
            'password_confirmation' => $repassword);

        $rules = array(
            'password' => 'Required|Min:8|Max:20|strong_password|Confirmed',
        );

        $messages = array(
            'password.required' => 'La nueva contraseña es obligatorio.',
            'strong_password' => 'La nueva contraseña no es segura, (Crea a partir de una combinación de números, letras mayúsculas y minúsculas)',
            'password.min' => 'La longitud minima de la nueva contraseña es de :min.',
            'password.max' => 'La longitud máxima de la nueva contraseña es de :max.',
            'confirmed' => 'La nueva contraseña con la contraseña de confirmación no coinciden.'
        );

        $v = Validator::make($input, $rules, $messages);

        if ($v->fails()) {
            return Redirect::back()
                            ->withErrors($v);
        }

        $user->password = Hash::make($password);
        $user->activation_password = md5(
                microtime(TRUE) . rand(0, 100000));

        try {
            $user->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return Redirect::back()
                        ->with('message', 'La clave fue cambiada correctamente.')
                        ->with('status_code', 0);
    }

    private function __save_chulls() {
        $status_code = 0;
        Log::info('guest_token: ' . Session::has('guest_token'));
        Log::info('guest_token_post: ' . Session::has('guest_token_post'));
        Log::info('guest_game_id: ' . Session::has('guest_game_id'));
        Log::info('guest_game_item1: ' . Session::has('guest_game_item1'));
        Log::info('guest_game_item2: ' . Session::has('guest_game_item2'));
        Log::info('guest_game_item3: ' . Session::has('guest_game_item3'));
        Log::info('guest_game_guess: ' . Session::has('guest_game_guess'));
        Log::info('guest_game_lifetime: ' . Session::has('guest_game_lifetime'));

        if (!Session::has('guest_token') or ! Session::has('guest_token_post') or ! Session::has('guest_game_item1') or ! Session::has('guest_game_item2') or ! Session::has('guest_game_item3') or ! Session::has('guest_game_guess') or ! Session::has('guest_game_lifetime') or ! Session::has('guest_game_tries') or ! Session::has('guest_game_id')) {
            $status_code = 1;
            $message = 'not session guest';
        }

        if ($status_code == 0) {
            $guest_token = Session::get('guest_token');
            $guest_token_post = Session::get('guest_token_post');
            if ($guest_token != $guest_token_post) {
                $message = 'invalid token';
                $status_code = 1;
            }
        }

        if ($status_code == 0) {

            $user = Auth::user();
            $guest_game_item1 = Session::get('guest_game_item1');
            $guest_game_item2 = Session::get('guest_game_item2');
            $guest_game_item3 = Session::get('guest_game_item3');
            $guest_game_guess = Session::get('guest_game_guess');
            $guest_game_tries = Session::get('guest_game_tries');
            $guest_game_lifetime = Session::get('guest_game_lifetime');

            $point = GameChulls::where('user_id', $user->id)->first();

            if (!$point) {
                $point = new GameChulls();
                $point->user_id = $user->id;
                $point->item1 = $guest_game_item1;
                $point->item2 = $guest_game_item2;
                $point->item3 = $guest_game_item3;
                $point->tries = $guest_game_tries;
                $point->lifetime = $guest_game_lifetime;
                $point->guess = 1;
                $point->enabled = true;
                $point->was_anonymous = 1;
                $is_save_point = true;
            } else {
                Log::info('exists chulls');
                if (!$point->enabled) {
                    $point->item1 = $guest_game_item1;
                    $point->item2 = $guest_game_item2;
                    $point->item3 = $guest_game_item3;
                    $point->tries = $guest_game_tries;
                    $point->lifetime = $guest_game_lifetime;
                    $point->guess = 1;
                    $point->enabled = true;
                    $point->was_anonymous = 2;
                    $is_save_point = true;
                } else {
                    $message = 'enabled point';
                    Log::info('emabled point');
                }
            }

            if (isset($is_save_point)) {
                try {
                    $point->save();
                    $message = 'save chulls';
                } catch (Exception $exception) {
                    $message = $exception->getMessage();
                    Log::error($message);
                }
            }
        }


        Session::forget('guest_game_id');
        Session::forget('guest_game_item1');
        Session::forget('guest_game_item2');
        Session::forget('guest_game_item3');
        Session::forget('guest_game_guess');
        Session::forget('guest_game_tries');
        Session::forget('guest_game_lifetime');
        Session::forget('guest_token');
        Session::forget('guest_token_post');
        Session::forget('guest_game_chulls');
        Session::forget('123');
        return $message;
    }

}
