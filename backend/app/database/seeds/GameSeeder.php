<?php
class GamesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('game_games')->delete();

        $photo_home = Photo::create(array('title' => 'image 1', 'path'  => 'upload/home/games/2.jpg'));
        $photo_list = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/payaso.jpg'));
        $photo_ctrl = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/payaso-ctrl.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 1,
            'title' => 'Baila con el payaso',
            'slug' => 'payaso',
            'description' => 'payaso',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'image_ctrl_id' => $photo_ctrl->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/gclown/',
            'order' => 6
        ));


        $photo_home = Photo::create(array('title' => 'image 2', 'path'  => 'upload/home/games/6.jpg'));
        $photo_list = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/fantasma.jpg'));
        $photo_ctrl = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/fantasma-ctrl.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 2,
            'title' => 'Fantasmas',
            'slug' => 'fantasma',
            'description' => 'fantasma',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'image_ctrl_id' => $photo_ctrl->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/gghosts/',
            'order' => 6
        ));


        $photo_home = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/games/4.jpg'));
        $photo_list = Photo::create(array('title' => 'image 1', 'path'  => 'upload/games/mecanico.jpg'));
        $photo_ctrl = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/mecanico-ctrl.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 3,
            'title' => 'Mecánicos',
            'slug' => 'mecanico',
            'description' => 'mecanico',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'image_ctrl_id' => $photo_ctrl->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/gmecanicos/',
            'order' => 6
        ));


        $photo_home = Photo::create(array('title' => 'image 1', 'path'  => 'upload/home/games/1.jpg'));
        $photo_list = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/busca-la-llave.jpg'));
        $photo_ctrl = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/busca-la-llave-ctrl.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 4,
            'title' => 'Busca la llave',
            'slug' => 'busca-la-llave',
            'description' => 'busca-la-llave',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'image_ctrl_id' => $photo_ctrl->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/gacuario/',
            'order' => 6
        ));


        $photo_home = Photo::create(array('title' => 'image 2', 'path'  => 'upload/home/games/5.jpg'));
        $photo_list = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/tiro-al-blanco.jpg'));
        $photo_ctrl = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/tiro-al-blanco-ctrl.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 5,
            'title' => 'Tiro al blanco',
            'slug' => 'tiro-al-blanco',
            'description' => 'tiro-al-blanco',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'image_ctrl_id' => $photo_ctrl->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/gshoot/',
            'order' => 6
        ));

        $photo_home = Photo::create(array('title' => 'image 3', 'path'  => 'upload/home/games/3.jpg'));
        $photo_list = Photo::create(array('title' => 'image 2', 'path'  => 'upload/games/tu-gran-dia.jpg'));
        DB::table('game_games')->insert(array(
            'id'    => 6,
            'title' => 'Tu gran día',
            'slug' => 'tu-gran-dia',
            'description' => 'Tu gran día',
            'status' => 'active',
            'author_id' => 1,
            'image_id' => $photo_list->id,
            'image_home_id' => $photo_home->id,
            'block_size' => '2x1',
            'path_swf' => 'games/deploy/ggrandia/',
            'order' => 1
        ));

    }
}
