<?php

class Video extends Eloquent {

    protected $table = 'section_videos';
    protected $fillable = array(
        'title',
        'description',
        'slug',
        'brief',
        'show_home',
        'enabled',
        'author_id',
        'photo_id',
        'photo_home_id',
        'block_size'
    );

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public function imageHome() {
        return $this->belongsTo('Photo');
    }

    public static function validate($input) {
        $rules = array(
            'title' => 'Required|Min:1|Max:100',
            'description' => 'Required|Min:1|Max:250',
            'photo' => 'mimes:jpeg,bmp,png'
        );
        return Validator::make($input, $rules);
    }

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw(
                        "slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

}
