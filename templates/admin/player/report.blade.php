
<table border="1" width="100%">
    <thead>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">created_at</th>
        <th class="span1">facebook</th>
        <th class="span1">google</th>
    </tr>
    </thead>
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->first_name }}</td>
        <td>{{ $p->last_name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->created_at }}</td>
        <td>@if ($p->facebook_id!='')si @else no @endif</td>
        <td>@if ($p->google_id!='')si @else no @endif</td>
    </tr>
    @endforeach
</table>
