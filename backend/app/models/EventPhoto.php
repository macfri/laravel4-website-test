<?php

class EventPhoto extends Eloquent {

    protected $table = 'section_events_files';
    protected $fillable = array(
        'title',
        'size',
        'mime',
        'extension',
        'event_id'
    );

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw(
                        "slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

}
