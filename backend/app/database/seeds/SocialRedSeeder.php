<?php
class SocialRedTableSeeder extends Seeder {

    public function run()
    {
        DB::table('social_red')->delete();
        DB::table('social_red')->insert(array(
            'caption' => 'admin',
            'picture' => 'http://sphotos-d.ak.fbcdn.net/hphotos-ak-frc1/s600x600/1175418_10153225174460438_59809366_n.jpg',
            'detail_link' => 'https://www.facebook.com/MiloPeru?fref=ts',
            'type' => 'facebook'
        ));
    }
}


