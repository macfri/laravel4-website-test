define([
  'jquery'
], function($){

  function resize($item){
    detectHeight($item);

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    $(window).resize(function() {
        delay(function(){
          detectHeight($item);
        }, 500);
    });
  }

  function detectHeight($item){
    var heightScreen = $(window).height();
    $item.css("height", heightScreen);
  }

  return resize;
});
