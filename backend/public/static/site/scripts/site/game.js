require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'modal': '../modules/modal',
    'closeAlert': '../modules/closeAlert',
    'acordeon': '../modules/acordeon',
    'register': '../modules/register',
    'auth': '../modules/auth',
    'slider': '../modules/slider',
    'gamerank': '../modules/gamerank'
  },
  shim: {
    'modal': ['jquery'],
    'closeAlert': ['jquery'],
    'acordeon': ['jquery'],
    'register': ['jquery'],
    'auth': ['jquery'],
    'foundation' : ['jquery'],
    'slider' : ['foundation'],
    'gamerank': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'closeAlert',
  'acordeon',
  'register',
  'auth',
  'slider',
  'gamerank'
],
function ($, modal, closeAlert, acordeon, register, auth, slider, gameRank) {
  modal();
  closeAlert();
  acordeon($('.cont-badget a'));
  register($('#register'));
  auth($('#login'));
  gameRank($('.rate-game'), $('#game-vars form'));
  slider();
  $('.js-back-game').on('click', function(event){
    event.preventDefault();
    $('.js-game-modal').toggleClass('hide');
  });
  $('.btn-close-x').on('click', function(event){
    event.preventDefault();
    var $target = $(this);
    $target.parent().hide(400).remove();

  });
});
