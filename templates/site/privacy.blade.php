    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'politicas-privacidad'))
        <!-- MENU -->
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="row privacy-tems">
        <div class="columns large-12">
          <h3>Política de Privacidad</h3>
          <p>Cualquier material o dato personal enviado a las páginas web del Grupo Nestlé queda amparado por la política de privacidad de dicho Grupo y por la protección de datos personales expuesta en el Comunicado de Privacidad que se encuentra en esta misma página web.</p>
          <p>Exactitud, carácter completo y puntualidad de la información: Aunque usamos todos los medios a nuestro alcance para asegurar la exactitud y el carácter completo de la información contenida en esta página web, no asumimos responsabilidad alguna en el caso de que la información que se encuentra disponible en la misma no sea exacta o completa. Cualquier modificación que hagas sobre el material disponible en esta página web se realizará por tu cuenta y riesgo. Asimismo, aceptas que será responsabilidad tuya comprobar los eventuales cambios que se puedan producir.</p>
          <h4>Transmisión:</h4>
          <p>Cualquier material o comunicación no personal que transmitas a esta página web por e-mail o cualquier otro medio, incluyendo datos, preguntas, comentarios, sugerencias o similares, es y será tratada como no confidencial y sin derechos de propiedad. Cualquier contenido que transmitas o que coloques en la página pasa a ser propiedad del Grupo Nestlé y se puede utilizar para cualquier finalidad, incluyendo, sin carácter limitativo, la reproducción, acceso, transmisión, publicación, emisión y colocación de contenidos en la web. Aún más, el Grupo Nestlé es totalmente libre de usar, para cualquier finalidad que estime oportuna (incluyendo, sin carácter limitativo, el desarrollo, fabricación, publicidad y marketing de productos), las ideas, material gráfico, invenciones, desarrollos, sugerencias o conceptos en general contenidos en cualquier comunicación que envíes a esta página web. Dicho uso no tendrá compensación para la parte que ha facilitado la información. Facilitando información, también estás garantizando que eres el propietario del material/contenido proporcionado, que no es de carácter difamatorio y que su uso por parte del Grupo Nestlé no violará derechos de terceras personas ni comportará un incumplimiento por nuestra parte de la legislación aplicable. El Grupo Nestlé no tiene ninguna obligación de utilizar la información facilitada.</p>
        </div>
      </div>
      <div class="content">
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'politicas-privacidad'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model')
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/home') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
