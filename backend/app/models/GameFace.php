<?php

class GameFace extends Eloquent {

    protected $table = 'game_faces';
    protected $fillable = array(
        'face_1',
        'face_2',
        'face_3',
        'face_4',
        'face_5',
        'game_id'
    );

}
