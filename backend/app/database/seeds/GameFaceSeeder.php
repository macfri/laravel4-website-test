<?php
class GamesFacesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('game_faces')->insert(array(
            'game_id' => 1,
        ));
        DB::table('game_faces')->insert(array(
            'game_id' => 2,
        ));
        DB::table('game_faces')->insert(array(
            'game_id' => 3,
        ));
        DB::table('game_faces')->insert(array(
            'game_id' => 4,
        ));
        DB::table('game_faces')->insert(array(
            'game_id' => 5,
        ));
        DB::table('game_faces')->insert(array(
            'game_id' => 6,
        ));
    }
}
