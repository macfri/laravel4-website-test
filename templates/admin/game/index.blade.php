@extends('admin.layout')
@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

<h3><a href="{{ URL::route('admin_game') }}">Games</a></h3>


<ul class="nav nav-tabs">
    <li class="active"><a href="javascript:void(0)">List ({{ $total }})</a></li>

    @if(SystemUser::has_perm('games-add'))
    <li><a href="{{ URL::route('admin_game_add') }}">Create</a></li>
    @endif
</ul>

<form id="filter_form" method="GET">
    <div class="pull-right"><button type="submit" class="btn btn-primary" style="">Apply</button></div>
    <table class="filters">
        <tbody>
            <tr>
                <td>Title</td>
                <td>{{ Form::text('search_title', $search_title) }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{ Form::select('search_status',
                    array('active' => 'active', 'inactive' => 'inactive'), $search_status) }}</td>
            </tr>
        </tbody>
    </table>
</form>

<table class="table table-striped table-bordered model-list">
    <thead>
        <!-- <th class="span1"><input type="checkbox" name="rowtoggle" class="action-rowtoggle" /></th> -->
        <th class="span1">&nbsp;</th>
        <th class="span1">title</th>
        <th class="span1">created_at</th>
        <th class="span1">author</th>
        <th class="span1">status</th>
        <th class="span1">report</th>
        <th class="span1">&nbsp;</th>
    </tr>
    </thead>
    @if($total > 0)
    @foreach ($items as $p)
    <tr>
        <!--<td><input type="checkbox" name="rowid" class="action-checkbox" value="{{ $p->id }}" /></td>-->
        <td>
            <a class="icon" href="{{ URL::route('admin_game_edit', $p->id) }}"><i class="icon-pencil"></i></a>
            <form class="icon" method="POST" action="{{ URL::route('admin_game_destroy', $p->id) }}">
                <button onclick="return confirm('You sure you want to delete this item?');"><i class="icon-trash"></i></button>
            </form>
        </td>
        <td><a href="{{ URL::route('admin_game_edit', $p->id) }}">{{ $p->title }}</a></td>
        <td>{{ $p->created_at }}</td>
        <td>{{ $p->author->username }}</td>

        <td>
            <i class="@if($p->status == 'inactive') text-error @else text-success @endif">
                {{ $p->status }}
            </i>
        </td>

        <td><a href="{{ URL::route('admin_game_report', $p->slug) }}">Ver Reporte</a></td>
        <td> <a href="{{ URL::route('games_detail', $p->slug) }}" target="_blank" >Ver web</a></td>

    </tr>
    @endforeach
    @else
    <tr><td colspan="6">No results</td></tr> 
    @endif
</table>

<?php echo $items->links(); ?>
@stop
