define([
  'jquery',
  '../libs/plugins/jquery.validate'
], function($){
  function login($form){

    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
        }
        },
        messages: {
          email: {
            required: '* Correo electrónico y contraseña incorrectos.',
            email: 'Necesitas un correo válido'
          },
          password: {
            required: 'contraseña incorrectos.'
          }
        },
        submitHandler: function(form){
          form.submit();
        }

    });

  }

  return login;

});
