    <div class="footer-degree"></div>
    <div class="footer-main">
      <div class="inner row">
        <div class="columns small-4 none">
          <h2>MILO</h2>
          <ul>
            <li><a href="{{ URL::route('terms') }}" onclick="_gaq.push(['_trackEvent','Web-Informativo', 'Ver-TyC', ''])">Términos y Condiciones</a></li>
            <li><a href="{{ URL::route('privacy') }}" onclick="_gaq.push(['_trackEvent','Web-Informativo', 'Ver-Pp', ''])">Politicas de privacidad</a></li>
          </ul>
        </div>
        <div class="columns small-4 footer-social">
          <h2>Visítanos</h2>
          <ul>
            <li><a href="https://www.facebook.com/MiloPeru" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-FanPageMilo','{{ $section }}'])" class="facebook">Facebook</a></li>
            <li><a href="http://www.youtube.com/channel/UC054ysyX5RH2FF7UHKIcXKw/feed" target="_blank" onclick="_gaq.push(['_trackEvent','Web-RedesSociales', 'Ver-CuentaYoutubeMilo','{{ $section }}'])" class="youtube">Youtube</a></li>
          </ul>
        </div>
        <div class="columns small-4">@if(!Auth::check())
          <h2>Regístrate</h2>
          <p>Estamos llenos de cosas chéveres para ti. Inscríbete aquí y no te pierdas de nada. ¡Es al toque!</p><a href="#" data-reveal-id="register-modal" onclick="_gaq.push(['_trackEvent','Web-Conversion', 'Paso1-Registrate2', ''])" class="btn-register-foot modal">Ingresa</a>@endif
        </div>
      </div>
    </div>
