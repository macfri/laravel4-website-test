require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'jquery.localscroll': '../libs/plugins/jquery.localscroll',

    'modal': '../modules/modal',
    'register': '../modules/register',
    'auth': '../modules/auth',

    'hashscroll': '../modules/hashscroll',
    'scrollarrow': '../modules/scrollarrow',
    'posfixed': '../modules/posfixed'
  },
  shim: {
    'jquery.scrolltop': ['jquery'],
    'jquery.localscroll': ['jquery.scrolltop'],
    'hashscroll': ['jquery.localscroll'],
    'scrollarrow': ['jquery.localscroll'],
    'posfixed': ['jquery'],

    'modal': ['jquery'],
    'register': ['jquery'],
    'auth': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'register',
  'auth',
  'hashscroll',
  'posfixed',
  'scrollarrow'
],
function ($, modal, register, auth, hashScroll, posFixed, scrollArrow) {
  modal();
  register($('#register'));
  auth($('#login'));
  hashScroll();
  
  if ($('.arrows').length) {
    posFixed($('.arrows'), 'fixed-arrows');
  }

  scrollArrow();
});



