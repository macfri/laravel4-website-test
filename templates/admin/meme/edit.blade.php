@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}

<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit Meme')</legend>
    <div class="control-group">
        <div class="control-label">category<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <select name="category" id="category" onchange="init();">
                    <option value="meme-milo"  @if(isset($item->category))  @if($item->category == 'meme-milo') selected @endif @endif  >Meme milo</option>
                    <option value="bravazo"  @if(isset($item->category))  @if($item->category == 'bravazo') selected @endif @endif  >Bravazo</option>
                    <option value="lol"  @if(isset($item->category))  @if($item->category == 'lol') selected @endif @endif  >Lol</option>
                    <option value="videos-graciosos"  @if(isset($item->category))  @if($item->category == 'videos-graciosos') selected @endif @endif  >Videos graciosos</option>
                    <option value="facebook"  @if(isset($item->category))  @if($item->category == 'facebook') selected @endif @endif  >facebook</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="title" id="title" value="@if($item) {{$item->title}} @else {{ Form::old('title')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Status<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group" id="show_link">
        <div class="control-label">code</div>
        <div class="controls">
           <div>
                <input type="text" name="link" id="link" value="@if($item) {{$item->link}} @else {{ Form::old('link')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group" id="show_photo">
        <div class="control-label">Photo&nbsp;</div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>
            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#demoLightbox">{{ $item->image->title  }}</a>
            <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>
</fieldset>


<fieldset class="scheduler-border">
    <legend class="scheduler-border">SEO</legend>

    <div class="control-group">
        <div class="control-label">meta_title</div>
        <div class="controls">
           <div>

                <input type="text" maxlength="70" style="width:600px;" name="seo_meta_title" id="seo_meta_title" value="@if(($item)) {{$item->seo_meta_title}} @else {{Form::old('seo_meta_title')}} @endif" >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">meta_description</div>
        <div class="controls">
           <div>
                <textarea style="width:600px;" name="seo_meta_description" id="seo_meta_description" >@if(($item)) {{$item->seo_meta_description}} @else {{Form::old('seo_meta_description')}} @endif</textarea>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">meta_keywords</div>
        <div class="controls">
           <div>
                <input type="text" maxlength="70" style="width:600px;" name="seo_meta_keywords" id="seo_meta_keywords" value="@if(($item)) {{$item->seo_meta_keywords}} @else {{Form::old('seo_meta_keywords')}} @endif" >
            </div>
        </div>
    </div>
</fieldset>


<fieldset class="scheduler-border">
    <legend class="scheduler-border">Display Home</legend>
    <div class="control-group">
        <div class="control-label">show_home</div>
        <div class="controls">
           <div>
                <input type="checkbox" name="show_home" id="show_home" @if($item) @if($item->show_home) checked="checked" @endif @else @if(Form::old('show_home')) checked="checked" @endif @endif >
            </div>
        </div>
    </div>
    <div class="control-group">
        <div class="control-label">Photo&nbsp; Home</div>
        <div class="controls">
            <div>{{Form::file('photo_home', $attributes = array())}}</div>
            @if(isset($item->image_home_id))
            <a data-toggle="lightbox" href="#demoLightbox_2">{{ $item->image_home->title  }}</a>
            <div id="demoLightbox_2" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image_home->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">block-size</div>
        <div class="controls">
           <div>
 
                <select name="block_size">
                    <option value="1x1"  @if(isset($item->block_size))  @if($item->block_size == '1x1') selected @endif @endif  >1x1</option>
                    <option value="1x2"  @if(isset($item->block_size))  @if($item->block_size == '1x2') selected @endif @endif >1x2</option>
                    <option value="1x3"  @if(isset($item->block_size))  @if($item->block_size == '1x3') selected @endif @endif >1x3</option>
                    <option value="1x4"  @if(isset($item->block_size))  @if($item->block_size == '1x4') selected @endif @endif >1x4</option>
                    <option value="2x1"  @if(isset($item->block_size))  @if($item->block_size == '2x1') selected @endif @endif >2x1</option>
                    <option value="2x2"  @if(isset($item->block_size))  @if($item->block_size == '2x2') selected @endif @endif >2x2</option>
                    <option value="2x3"  @if(isset($item->block_size))  @if($item->block_size == '2x3') selected @endif @endif >2x3</option>
                    <option value="2x4"  @if(isset($item->block_size))  @if($item->block_size == '2x4') selected @endif @endif >2x4</option>
                    <option value="3x1"  @if(isset($item->block_size))  @if($item->block_size == '3x1') selected @endif @endif >3x1</option>
                    <option value="3x2"  @if(isset($item->block_size))  @if($item->block_size == '3x2') selected @endif @endif >3x2</option>
                    <option value="3x3"  @if(isset($item->block_size))  @if($item->block_size == '3x3') selected @endif @endif >3x3</option>
                    <option value="3x4"  @if(isset($item->block_size))  @if($item->block_size == '3x4') selected @endif @endif >3x4</option>
                    <option value="4x1"  @if(isset($item->block_size))  @if($item->block_size == '4x1') selected @endif @endif >4x1</option>
                    <option value="4x2"  @if(isset($item->block_size))  @if($item->block_size == '4x2') selected @endif @endif >4x2</option>
                    <option value="4x3"  @if(isset($item->block_size))  @if($item->block_size == '4x3') selected @endif @endif >4x3</option>
                    <option value="4x4"  @if(isset($item->block_size))  @if($item->block_size == '4x4') selected @endif @endif >4x4</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
        <input type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div>
</fieldset>
{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>
<script>
function init(){
    var category = $('#category').val();
    var show_link = $('#show_link');
    var show_photo = $('#show_photo');

    if (category == 'videos-graciosos'){
        show_link.css('display', '');
        show_photo.css('display', 'none');
    }else{
        show_link.css('display', 'none');
        show_photo.css('display', '');
    }
}
init();
</script>
@stop
