<!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>

        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>'spots-tv'))
        <!-- MENU -->
        </div>

        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('spots') }}">SPOTS DE TV</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div class="main-spot">
          <div class="row">
            <div class="columns large-centered bg-video">
              <h2>Spots de TV</h2>
              <div id="video">
                <iframe width="620" height="380" src="https://www.youtube.com/embed/{{ $item->code }}?wmode=transparent&rel=0" frameborder="0" allowfullscreen></iframe>
              </div>
              <p>{{ $item->title }}</p>
            </div>
          </div>
          <div class="row list-video">
            <div class="columns large-centered large-8">

                @foreach($items as $i => $v)

                    @if(($i+1)%2==0)
                    <div class="row">          
                    @endif

                    <div class="columns large-6"><a href="#" data-video="{{ $v->code }}"><i></i><img src="{{ asset($v->image->path) }}"></a>
                    <p>{{ $v->title }}</p>
                    </div>

                    @if(($i+1)%2==0)
                    </div>
                    @endif

                    @if(($i+1)%4==0)
                    <div class="separator"></div>
                    @endif

                @endforeach

      
<!--

                <div class="row">          
                <div class="columns large-6"><a href="#" data-video="iPL0mAYaQ7k"><i></i><img src="{{ asset('static/site/images/content/verano.jpg') }}"></a>
                  <p>¡Hagamos que nuestro verano sea más largo!</p>
                </div>
                 <div class="columns large-6"><a href="#" data-video="rrm8UQLsGu4"><i></i><img src="{{ asset('static/site/images/content/milo-energia-para-ganar.jpg') }}"></a>
                  <p>Milo, energía para ganar cada día.</p>
                </div>
              </div>

               <div class="separator"></div>
              <div class="row"> 
               <div class="columns large-6"><a href="#" data-video="KMccgKw1zkg"><i></i><img src="{{ asset('static/site/images/content/cree-en-ti-mismo.jpg') }}"></a>
                  <p>Cree en ti mismo y busca nuevos retos… ¡Nosotros ponemos la energía!</p>
                </div>         
                <div class="columns large-6"><a href="#" data-video="Sllhq0qpKdE"><i></i><img src="{{ asset('static/site/images/content/si-creias-que-un-skatepark.jpg') }}"></a>
                  <p>Si creías que un skatepark de hielo era una buena idea, mejor mira este video…</p>
                </div>
              
              </div>
              <div class="separator"></div>
              <div class="row">      
                <div class="columns large-6"><a href="#" data-video="EA3lEdy4n9Y"><i></i><img src="{{ asset('static/site/images/content/no-le-tengas-miedo.jpg') }}"></a>
                  <p>No le tengas miedo a las cosas gigantes… ¡Supéralas con Milo!</p>
                </div>    
                <div class="columns large-6"><a href="#" data-video="7mPoirCY3NQ"><i></i><img src="{{ asset('static/site/images/content/chapa-tu-skate.jpg') }}"></a>
                  <p>Chapa tu skate y supera todas las barandas del camino. ¡Juntos lo lograremos!</p>
                </div>
               
              </div>
              <div class="separator"></div>
              <div class="row">     
               <div class="columns large-6"><a href="#" data-video="jzg3lawrzvg"><i></i><img src="{{ asset('static/site/images/content/eres-creativo.jpg') }}"></a>
                  <p>¿Eres creativo en todo lo que haces? ¡Este video es para ti!</p>
                </div>     
                <div class="columns large-6"><a href="#" data-video="QlPxHo6vOsc"><i></i><img src="{{ asset('static/site/images/content/con-un-vaso-de-milo.jpg') }}"></a>
                  <p>Con un vaso de Milo, ¡serás más grande que cualquiera!</p>
                </div>
               
              </div>
              <div class="separator"></div>
              <div class="row">  
               <div class="columns large-6"><a href="#" data-video="IHBpofhBdYQ"><i></i><img src="{{ asset('static/site/images/content/una-de-las-mejores.jpg') }}"></a>
                  <p>Una de las mejores combinaciones del universo: ¡Skateboard + Basketball!</p>
                </div>        
                <div class="columns large-6"><a href="#" data-video="kWVuSOlkAqc"><i></i><img src="{{ asset('static/site/images/content/recarga-tu-energia.jpg') }}"></a>
                  <p>Recarga tu energía todos los días y ningún reto te vencerá</p>
                </div>
              </div>
-->


            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'spots-tv'))
        <!-- FOOTER -->
      </div>
    </div>

    @include('site.login-model', array('page'=>'spots-tv'))
    @include('site.register-model')
    <script data-main="{{ asset('static/site/scripts/site/spots') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
