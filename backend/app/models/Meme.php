<?php

class Meme extends Eloquent {

    protected $table = 'section_memes';
    protected $fillable = array(
        'title',
        'description',
        'slug',
        'show_home',
        'enabled',
        'author_id',
        'photo_id',
        'photo_home_id',
        'block_size'
    );

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function authorWeb() {
        return $this->belongsTo('User');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public function imageHome() {
        return $this->belongsTo('Photo');
    }

    public static function validate($input) {
        $messages = array(
            'title.required' => 'El titulo es obligatorio',
            'title.min' => 'La longitud minima de caracteres del campo titulo es de 1.',
            'title.max' => 'La longitud máxima de caracteres del campo titulo es de 50.',
            'photo.mimes' => 'La imagen debe ser de tipo: JPG, PNG.',
        );

        $rules = array(
            'title' => 'Required|Min:1|Max:150',
            'photo' => 'mimes:jpeg,png'
        );
        return Validator::make($input, $rules, $messages);
    }

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw(
                "slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function voted($user_id) {
        return UserMemeLike::where('user_id', $user_id)
                        ->where('meme_id', $this->id)
                        ->first();
    }

}
