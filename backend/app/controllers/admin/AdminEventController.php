<?php

class AdminEventController extends BaseController {

    private $viewpath = 'admin/event/';

    public function index() {
        if (!SystemUser::has_perm('events-list')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');
        $search_important = Input::get('search_important');

        $items = Event::where('id', '>', 0);

        if (strlen($search_title) > 0) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status) > 0) {
            $items->where('status', '=', $search_status);
        }

        if (strlen($search_important) > 0) {
            $items->where('important', '=', $search_important);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'search_important' => $search_important,
                        )
        );
    }

    public function add() {
        if (!SystemUser::has_perm('events-new')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        return View::make(
                        $this->viewpath . __function__, array(
                        )
        );
    }

    public function edit($id) {

        if (!SystemUser::has_perm('events-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $item = Event::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {
        if (!SystemUser::has_perm('events-new')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Event::validate(Input::all());
        if ($v->passes()) {
            $item = new Event();
            $item->title = Input::get('title');
            $item->slug = Event::get_slug(Input::get('title'));
            $item->description = Input::get('description');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->is_xtreme = (bool) Input::get('is_xtreme');
            $item->block_size = Input::get('block_size');
            $item->show_home = (bool) Input::get('show_home');
            $item->event_date = Input::get('event_date');
            $item->when_where = Input::get('when_where');
            $item->important = (bool) Input::get('important');
            $item->important_slider = (bool) Input::get('important_slider');
            $item->seo_meta_title = Input::get('seo_meta_title');
            $item->seo_meta_description = Input::get('seo_meta_description');
            $item->seo_meta_keywords = Input::get('seo_meta_keywords');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_event_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo', $ext
                );

                try {
                    $image->save();

                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo', $ext)
                    );

                    $item->image_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('photo_detail')) {
                $file = Input::file('photo_detail');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo_detail', $ext
                );

                try {
                    $image->save();

                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo_detail', $ext)
                    );

                    $item->image_detail_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'events');

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'events', $item->slug, $ext
                );

                try {
                    $image->save();
                    $file->move(
                            $upload_path, sprintf('%s.%s', $item->slug, $ext)
                    );

                    $item->image_home_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('photo_slider')) {
                $file = Input::file('photo_slider');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo_slider', $ext
                );

                try {
                    $image->save();

                    $file->move(
                            $upload_path, sprintf('%s.%s', 'photo_slider', $ext)
                    );

                    $item->image_slider_id = $image->id;
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }

            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug, 'gallery');

                        $image = new EventPhoto();
                        $image->title = $name;
                        $image->extension = $ext;
                        $image->description = $name;
                        $image->size = $size;
                        $image->mime = $mime;
                        $image->event_id = $item->id;
                        $image->save();

                        $image->path = sprintf('%s/%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'gallery', $image->id, $ext
                        );

                        try {
                            $image->save();
                            $file->move(
                                    $upload_path, sprintf('%s.%s', $image->id, $ext)
                            );
                        } catch (Exception $exception) {
                            Log::error($exception);
                        }
                    }
                }
            }

            return Redirect::route('admin_event')
                            ->with('success', sprintf(
                                            'Event %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {

        if (!SystemUser::has_perm('events-edit')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $v = Event::validate(Input::all());
        if ($v->passes()) {
            $item = Event::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->show_home = (bool) Input::get('show_home');
            $item->block_size = Input::get('block_size');
            $item->is_xtreme = (bool) Input::get('is_xtreme');
            $item->status = Input::get('status');
            $item->author_id = Session::get('session_user');
            $item->event_date = Input::get('event_date');
            $item->seo_meta_title = Input::get('seo_meta_title');
            $item->seo_meta_description = Input::get('seo_meta_description');
            $item->seo_meta_keywords = Input::get('seo_meta_keywords');

            $gallery_description = Input::get('gallery_description');
            $gallery_description_new = Input::get('gallery_description_new');

            if ($gallery_description) {
                foreach ($gallery_description as $k => $v) {
                    $image = EventPhoto::find($k);

                    if ($image) {
                        $image->description = $v;

                        try {
                            $image->save();
                        } catch (Exception $exception) {
                            Log::error($exception);
                        }
                    }
                }
            }

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_event_edit', $id)
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo', $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_event_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo', $ext)
                );

                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                    }
                }
            }

            if (Input::hasFile('photo_detail')) {
                $file = Input::file('photo_detail');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = Photo::find($item->image_detail_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo_detail', $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_event_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo_detail', $ext)
                );

                if (isset($is_new)) {
                    $item->image_detail_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'events');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'events', $item->slug, $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                if (isset($is_new)) {
                    $item->image_home_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                    }
                }
            }

            if (Input::hasFile('photo_slider')) {
                $file = Input::file('photo_slider');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug);

                $image = Photo::find($item->image_slider_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'photo_slider', $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo_slider', $ext)
                );

                if (isset($is_new)) {
                    $item->image_slider_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                    }
                }
            }

            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                $key = 0;
                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s/%s', public_path(), 'upload', 'events', $item->slug, 'gallery');

                        $image = new EventPhoto();
                        $image->title = $name;
                        $image->extension = $ext;

                        if (isset($gallery_description_new[$key])) {
                            $image->description = $gallery_description_new[$key];
                        } else {
                            $image->description = $name;
                        }

                        $key++;

                        $image->size = $size;
                        $image->mime = $mime;
                        $image->event_id = $item->id;
                        $image->save();

                        $image->path = sprintf('%s/%s/%s/%s/%s.%s', 'upload', 'events', $item->slug, 'gallery', $image->id, $ext
                        );

                        $image->save();

                        $file->move(
                                $upload_path, sprintf('%s.%s', $image->id, $ext)
                        );
                    }
                }
            }

            return Redirect::route('admin_event')
                            ->with('success', 'Event Created Successfully.');
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function destroy($id) {
        if (!SystemUser::has_perm('events-delete')) {
            return Response::make(
                            'You are not authorized', '403');
        }

        $event = Event::find($id);

        if ($event) {
            $items = EventPhoto::where('event_id', '=', $id);
            $items->delete();
            $event->delete();
        }

        return Redirect::route('admin_event')
                        ->with('success', 'Event Deleted Successfully.');
    }

    public function gallery_destroy() {
        $id = Input::get('id');
        EventPhoto::find($id)->delete();
        return Response::json(array(
                    'status_code' => 0,
                    'message' => 'delete photo'), 200
        );
    }

}
