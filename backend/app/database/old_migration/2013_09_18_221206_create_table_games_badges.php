<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGamesBadges extends Migration {


	public function up()
    {
		Schema::create('game_badges', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('description');

            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();

            $table->string('badge', 30);
            $table->integer('points');
            $table->integer('game_id')->unsigned()->index();
            $table->foreign('game_id')
                ->references('id')->on('game_games')
                ->on_update('cascade');


            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade')
                ->onDelete('cascade');
			$table->timestamps();
        });
	}

	public function down()
    {
        Schema::dropIfExists('game_badges');
	}
}
