require.config({
  paths: {
    'jquery':'../libs/jquery',
    'register': '../modules/register-complete',

  },
  shim: {
    'register': ['jquery']
  }
});

require([
  'jquery',
  'register'
], function ($, register) {
  register($('#register-complete'));
});
