<?php

class Slider extends Eloquent {

    protected $table = 'sliders';
    protected $fillable = array(
        'title',
        'description',
        'backgroung_color',
        'button_title',
        'button_backgroung_color',
        'link',
        'author_id',
        'file_id'
    );

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public static function validate($input) {
        $rules = array(
            'title' => 'Required|Min:1|Max:50',
            'description' => 'Required|Min:1|Max:250',
            'backgroung_color' => 'Required|Min:1|Max:250',
            'photo' => 'mimes:jpeg,bmp,png',
            'button_title' => 'Min:1|Max:250',
            'button_backgroung_color' => 'Min:1|Max:250',
            'link' => 'url',
        );
        return Validator::make($input, $rules);
    }

}
