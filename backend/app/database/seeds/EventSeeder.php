<?php
class EventsTableSeeder extends Seeder {

    public function run()
    {
        /*********************************************************************************************************************************************/
        $photo_home = Photo::create(array('title' => 'image', 'path'  => 'upload/home/events/1.jpg'));
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 1,
            'title' => 'Carrera de campeones',
            'slug' => 'carrera-de-campeones',
            'description' => 'En los coles, parques, plazas, periódicos… No importaba a dónde fueras,
                ¡todos sabían que se acercaba la carrera más chévere! Más de 44.000 chicos y chicas de los distintos 
                colegios de Arequipa, Chiclayo, Huancayo e Iquitos se inscribieron para demostrar que solo un 
                verdadero campeón puede llevarse el premio mayor.
                Hubieron shows espectaculares y unos premiazos irresistibles. 1',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'active',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id,
            'image_home_id' => $photo_home->id,
            'block_size' => '1x1',
            'important' => true
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto1.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto2.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto3.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto4.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto5.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto6.jpg', 'event_id' => 1));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/carrera-de-campeones/gallery/foto7.jpg', 'event_id' => 1));


        /*********************************************************************************************************************************************/
        $photo_home = Photo::create(array('title' => 'image', 'path'  => 'upload/home/events/2.jpg'));
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 2,
            'title' => 'Advertencia: ¡Desafío apto solo para maestrazos!',
            'slug' => 'advertencia-desafio-apto-solo-para-maestrazos',
            'description' => 'Nada más y nada menos que sesenta y cuatro coles de la gran Lima fueron escogidos 
                para afrontar el reto más exigente que jamás se ha visto… Los mejores deportistas fueron seleccionados
                para desenvolverse en su mejor cancha, ya sea atletismo, fulbito, o vóley. El enfrentamiento comenzó el 21 de abril,
                pero esta historia aún tiene para rato… Tomemos nota de la entrega de estos maestros, pronto veremos quién es el mejor.',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'active',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id,
            'image_home_id' => $photo_home->id,
            'block_size' => '1x1',
            'important' => true
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto1.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto2.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto3.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto4.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto5.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto6.jpg', 'event_id' => 2));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/advertencia-desafio-apto-solo-para-maestrazos/gallery/foto7.jpg', 'event_id' => 2));


        /*********************************************************************************************************************************************/
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/un-verdadero-reto-de-altura/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/un-verdadero-reto-de-altura/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 3,
            'title' => '¡Un verdadero reto de altura!',
            'description' => 'Un skater de verdad puede hacer los trucos más chéveres y volar con su tabla cuando se lo propongan.
                Por eso, del 26 de enero al 29 de febrero, llevamos a distintos skateparks de Lima, Piura y Trujillo un reto apto solo
                para los maestrazos: superar una meta de dos metros con un salto alucinante. Muchos aceptaron el desafío y 
                se inscribieron para demostrarle al público de qué está hecho un verdadero skater… ¡Y para que todo el mundo lo sepa,
                les tomamos unas fotazos!',
            'slug' => 'un-verdadero-reto-de-altura',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'active',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto1.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto2.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto3.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto4.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto5.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto6.jpg', 'event_id' => 3));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/un-verdadero-reto-de-altura/gallery/foto7.jpg', 'event_id' => 3));


        /*********************************************************************************************************************************************/
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 4,
            'title' => '¿Skateboard y basketball? ¡Me apunto!',
            'description' => 'Para este reto, necesitábamos que los mejores skaters y basketbolistas unieran sus conocimientos y se
                aventuraran en uno de los deportes más pajas que se han jugado en nuestro país. Lima y Huancayo abrieron sus canchas y
                empezaron a correr los skates… ¡Pum!, ¡pum!, rebotaban los balones y fuimos testigos de las canastas más alucinantes que
                jamás habíamos visto. ¡Nosotros ❤ el skateball!',
            'slug' => 'skateboard-y-basketabal-me-apunto',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'active',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto1.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto2.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto3.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto4.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto5.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto6.jpg', 'event_id' => 4));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/skateboard-y-basketabal-me-apunto/gallery/foto7.jpg', 'event_id' => 4));


        /*********************************************************************************************************************************************/
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 6,
            'title' => '¡Escuela de campeones!',
            'description' => 'La final del Interescolar “Milo 2012” se acerca cada día más y los retos se hacen cada vez más difíciles. 
                Pero un maestrazo del fútbol se encargará de preparar a los mejores equipos en su escuela de fútbol. ¿Ya sabes quién es? 
                ¡Obvio! ¡Oscar Ibáñez! Uno de los mejores arqueros que ha visto nuestra Selección Peruana de Fútbol. Con la guía de este ídolo, 
                la competencia va a convertirse en un verdadero reto de campeones…',
            'slug' => 'escuela-de-campeones',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'inactive',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto1.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto2.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto3.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto4.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto5.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto6.jpg', 'event_id' => 6));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/escuela-de-campeones/gallery/foto7.jpg', 'event_id' => 6));


        /*********************************************************************************************************************************************/
        $photo_list = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo.jpg'));
        $photo_deta = Photo::create(array('title' => 'image', 'path'  => 'upload/events/carrera-de-campeones/photo_detail.jpg'));
        DB::table('section_events')->insert(array(
            'id'    => 7,
            'title' => '¡Los guerreros de Egipto!',
            'description' => 'El noveno Campeonato Mundial Juvenil de Taekwondo se realizó en Egipto, que está lejaaazos. ¡Pero no importa! 
                Porque, a pesar de todo, acompañamos a Alejandro Rodríguez Escalante y a Kevin John Díaz Zamudio en una mechaza. Se agarraron 
                con los más fuertes y dejaron en alto el nombre de nuestro querido Perú. ¡Recontra guerreros!',
            'slug' => 'los-guerreros-de-egipto',
            'event_date' => '2012-09-12',
            'when_where' => '10 - 15 de Julio de 2013, Ayacucho',
            'status' => 'inactive',
            'author_id' => 1,
            'image_id'  => $photo_list->id,
            'image_detail_id'  => $photo_deta->id
        ));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto1.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto2.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto3.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto4.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto5.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto6.jpg', 'event_id' => 7));
         EventPhoto::create(array('title' => 'image 1', 'path'  => 'upload/events/los-guerreros-de-egipto/gallery/foto7.jpg', 'event_id' => 7));
    }
}
