define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){
  function registerFB($form){
    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {
        
        user: {
          required: true,
        },
        pass: {
          required: true
        },
        repass: {
          required: true,
          equalTo: "#pass"
        },
        tyc: {
          required: true
        }
      },
      messages: {
        user: {
          required: 'Olvidaste ingresar tu nombre'
        },
        pass: {
          required: 'Olvidaste ingresar tus apellidos'
        },
        repass: {
          required: 'Olvidaste  ingresar tu correo electrónico.',
          equalTo: 'Necesitas un correo válido'
        },
        tyc: {
          required: 'true'
        }
        
      },
      submitHandler: function(form){
        form.submit();
      }
    });
  }
  return registerFB;
});
