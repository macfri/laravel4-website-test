<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableUserProfileCover extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	        Schema::table('users', function($t) {
                $t->integer('cover_id')
                    ->default(0);
            });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	       Schema::table('users', function($t) {
            $t->dropColumn('cover_id');
           });
	}

}
