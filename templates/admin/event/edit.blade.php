@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'files' => true)) }}
<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit Event')</legend>
    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="title" id="title" value="@if(isset($item)) {{$item->title}} @else {{ Form::old('title')}} @endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">description<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <textarea style="width:600px;height:200px;"  id="description" name="description" cols="80" rows="4">@if(isset($item)) {{$item->description}} @else {{Form::old('description')}} @endif</textarea>
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">important</div>
        <div class="controls">
           <div>
                <input type="checkbox" name="important" id="important" @if(isset($item)) @if($item->important) checked="checked" @endif @else @if(Form::old('important')) checked="checked" @endif @endif>
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">when_where<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" style="width:300px;"  name="when_where" id="when_where" value="@if(isset($item->when_where)) {{$item->when_where}} @else {{ Form::old('when_where')}} @endif ">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">event_date<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="event_date" id="event_date" value="@if(isset($item->event_date)){{$item->event_date}} @else {{ Form::old('event_date')}} @endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Foto Listado&nbsp;</div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>
            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#image_id">{{ $item->image->title  }}</a>
            <div id="image_id" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Foto Detalle</div>
        <div class="controls">
            <div>{{Form::file('photo_detail', $attributes = array())}}</div>
            @if(isset($item->image_detail_id))
            <a data-toggle="lightbox" href="#image_detail_id">{{ $item->image_detail->title  }}</a>
            <div id="image_detail_id" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image_detail->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

</fieldset>


<fieldset class="scheduler-border">
    <legend class="scheduler-border">SEO</legend>


    <div class="control-group">
        <div class="control-label">meta_title</div>
        <div class="controls">
           <div>
                <input type="text" maxlength="70" style="width:600px;" name="seo_meta_title" id="seo_meta_title" value="@if(isset($item)) {{$item->seo_meta_title}} @else {{Form::old('seo_meta_title')}} @endif" >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">meta_description</div>
        <div class="controls">
           <div>
                <textarea style="width:600px;" name="seo_meta_description" id="seo_meta_description" >@if(isset($item)) {{$item->seo_meta_description}} @else {{Form::old('seo_meta_description')}} @endif </textarea>
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">meta_keywords</div>
        <div class="controls">
           <div>
                <input type="text" maxlength="70" style="width:600px;" name="seo_meta_keywords" id="seo_meta_keywords" value="@if(isset($item)) {{$item->seo_meta_keywords}} @else {{Form::old('seo_meta_keywords')}} @endif" >
            </div>
        </div>
    </div>


</fieldset>


<fieldset class="scheduler-border">
    <legend class="scheduler-border">Photo Home</legend>


    <div class="control-group">
        <div class="control-label">show_home</div>
        <div class="controls">
           <div>
                <input type="checkbox" name="show_home" id="show_home" @if(isset($item)) @if($item->show_home) checked="checked" @endif @else @if(Form::old('show_home')) checked="checked" @endif @endif >

            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Foto Home</div>
        <div class="controls">
            <div>{{Form::file('photo_home', $attributes = array())}}</div>
            @if(isset($item->image_home_id))
            <a data-toggle="lightbox" href="#image_home_id">{{ $item->image_home->title  }}</a>
            <div id="image_home_id" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image_home->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">block-size</div>
        <div class="controls">
           <div>

                <select name="block_size">
                    <option value="1x1"  @if(isset($item->block_size))  @if($item->block_size == '1x1') selected @endif @endif  >1x1</option>
                    <option value="1x2"  @if(isset($item->block_size))  @if($item->block_size == '1x2') selected @endif @endif >1x2</option>
                    <option value="1x3"  @if(isset($item->block_size))  @if($item->block_size == '1x3') selected @endif @endif >1x3</option>
                    <option value="1x4"  @if(isset($item->block_size))  @if($item->block_size == '1x4') selected @endif @endif >1x4</option>
                    <option value="2x1"  @if(isset($item->block_size))  @if($item->block_size == '2x1') selected @endif @endif >2x1</option>
                    <option value="2x2"  @if(isset($item->block_size))  @if($item->block_size == '2x2') selected @endif @endif >2x2</option>
                    <option value="2x3"  @if(isset($item->block_size))  @if($item->block_size == '2x3') selected @endif @endif >2x3</option>
                    <option value="2x4"  @if(isset($item->block_size))  @if($item->block_size == '2x4') selected @endif @endif >2x4</option>
                    <option value="3x1"  @if(isset($item->block_size))  @if($item->block_size == '3x1') selected @endif @endif >3x1</option>
                    <option value="3x2"  @if(isset($item->block_size))  @if($item->block_size == '3x2') selected @endif @endif >3x2</option>
                    <option value="3x3"  @if(isset($item->block_size))  @if($item->block_size == '3x3') selected @endif @endif >3x3</option>
                    <option value="3x4"  @if(isset($item->block_size))  @if($item->block_size == '3x4') selected @endif @endif >3x4</option>
                    <option value="4x1"  @if(isset($item->block_size))  @if($item->block_size == '4x1') selected @endif @endif >4x1</option>
                    <option value="4x2"  @if(isset($item->block_size))  @if($item->block_size == '4x2') selected @endif @endif >4x2</option>
                    <option value="4x3"  @if(isset($item->block_size))  @if($item->block_size == '4x3') selected @endif @endif >4x3</option>
                    <option value="4x4"  @if(isset($item->block_size))  @if($item->block_size == '4x4') selected @endif @endif >4x4</option>
                </select >
            </div>
        </div>
    </div>

</fieldset>
<fieldset class="scheduler-border">
    <legend class="scheduler-border">Photo SLider</legend>

    <div class="control-group">
        <div class="control-label">important_slider</div>
        <div class="controls">
           <div>
                <input type="checkbox" name="important_slider" id="important_slider" @if(isset($item)) @if($item->important_slider) checked="checked" @endif @else @if(Form::old('important_slider')) checked="checked" @endif @endif >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Photo&nbsp; Slider</div>
        <div class="controls">
            <div>{{Form::file('photo_slider', $attributes = array())}}</div>
            @if(isset($item->image_slider_id))
            <a data-toggle="lightbox" href="#photo_slider">{{ $item->image_slider->title  }}</a>
            <div id="photo_slider" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image_slider->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

</fieldset>
<fieldset class="scheduler-border">
    <legend class="scheduler-border">Photo Gallery</legend>

    <div class="control-group">
        <div class="control-label">gallery</div>
        <div class="controls">
           <div id="attach">
            @if(isset($item))
            @foreach($item->gallery as $file)
            <div id="div_event_photo">
                <a data-toggle="lightbox" href="#demoLightbox_{{ $file->id }}">{{ $file->title  }}</a>
                <div id="demoLightbox_{{ $file->id }}" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                    <div class='lightbox-content'>
                        <img src="{{ asset($file->path) }}">
                    </div>
                </div>
                <input type="hidden" id="event_photo_id" value="{{ $file->id }}">
                <button class="btn_event_photo_delete">Delete</button>
                <input type="text" name="gallery_description[{{ $file->id }}]" value="{{ $file->description }}" style="width:500px;">

            </div>
            @endforeach
            @endif
            <div id="xxx">
                <input type="file" name="attachment[]"/>
                <input type="text" value="sds" name="gallery_description_new[]" style="width:500px;">
                <button class="btn_event_photo_delete">Delete</button>
                RONALD
            </div>
            </div>
        </div>
    </div>

 <button onclick="return add_category()">Add a new category</button>

    <div class="control-group">
        <div class="controls"> 
        <input id="btn_submit" type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div> 

</fieldset>
{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>
<script>
$("#attach").on("change", " input[type=file]:last", function(){
    //var item = $(this).clone(true);
    var item = $('#xxx').last().clone(true);
    var fileName = $(this).val();
    if(fileName){

        $('#attach').append("<br/>").append(item);
    }  
});

$('.btn_event_photo_delete').on('click', function(e){
    e.preventDefault();
    var $this = $(this);
    var div = $this.parent();
    var id = div.find('#event_photo_id').val();
    var posting = $.post(
        "{{ URL::route('admin_event_gallery_destroy') }}",
        { id: id },
        "json"
    );
    posting.done(function( data ) {
        status_code = data.status_code;
        //if (status_code == 0)
        //{
            div.remove();
        //}
    });
});
</script>

   <!-- <script src="{{ asset('static/admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('static/admin/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document ).ready( function() {
            $( 'textarea#description' ).ckeditor();
         });
    </script>

    -->

@stop
