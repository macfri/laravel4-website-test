@extends('admin.layout')
@section('content')

@if(Session::has('failure'))
    <div class="alert alert-error">
        <strong>{{ Session::get('failure') }}</strong>
    </div>
@endif

@if($errors->has())
<ul>
    @foreach($errors->all() as $message)
    <li class="text-error">{{ $message }}</li>
    @endforeach
</ul>
@endif

{{ Form::open(array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'files' => true)) }}
<fieldset>
    <legend class="scheduler-border">@yield('title_module', 'Edit Spot')</legend>
    <div class="control-group">
        <div class="control-label">title<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input style="width:800px;" type="text" name="title" id="title" value="@if(isset($item)){{$item->title}}@else{{ Form::old('title')}}@endif">
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">Status</div>
        <div class="controls">
           <div>
                <select name="status">
                    <option value="active"  @if(isset($item->status))  @if($item->status == 'active') selected @endif @endif  >active</option>
                    <option value="inactive"  @if(isset($item->status))  @if($item->status == 'inactive') selected @endif @endif >inactive</option>
                </select >
            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="control-label">yt video<strong style="color: red">&#42;</strong></div>
        <div class="controls">
           <div>
                <input type="text" name="code" id="code" value="@if(isset($item)){{$item->code}}@else{{ Form::old('code')}}@endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">order</div>
        <div class="controls">
           <div>
                <input type="text" name="order" id="order" value="@if(isset($item)){{$item->order}}@else{{ Form::old('order')}}@endif">
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="control-label">Photo&nbsp;<strong style="color: red">&#42;</strong></ div>
        <div class="controls">
            <div>{{Form::file('photo', $attributes = array())}}</div>
            @if(isset($item->image_id))
            <a data-toggle="lightbox" href="#demoLightbox">{{ $item->image->title  }}</a>
            <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image->path) }}">
                </div>
            </div>
            @endif
        </div>
        </div>
    </div>

</fieldset>


<fieldset class="scheduler-border">
    <legend class="scheduler-border"></legend>

    <div class="control-group">
        <div class="control-label">important</div>
        <div class="controls">
           <div>
                <input type="checkbox" name="important_slider" id="important_slider" @if(isset($item)){{$item->important_slider}}@else{{Form::old('important_slider')}}@endif >
            </div>
        </div>
    </div>
    <div class="control-group">
        <div class="control-label">Photo&nbsp; Slider</div>
        <div class="controls">
            <div>{{Form::file('photo_slider', $attributes = array())}}</div>
            @if(isset($item->image_slider_id))
            <a data-toggle="lightbox" href="#demoLightbox_2">{{ $item->image_slider->title  }}</a>
            <div id="demoLightbox_2" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
                <div class='lightbox-content'>
                    <img src="{{ asset($item->image_slider->path) }}">
                </div>
            </div>
            @endif
        </div>
    </div>

</fieldset>
<fieldset class="scheduler-border">
    <div class="control-group">
        <div class="controls"> 
        <input id="btn_submit" type="submit" class="btn btn-primary btn-large" value="Submit" />     
        </div>
    </div> 

</fieldset>
{{ Form::close() }}
@stop

@section('scripts')
<script src="{{ asset('static/admin/bootstrap/js/bootstrap-lightbox.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('static/admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('static/admin/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document ).ready( function() {
            $( 'textarea#description' ).ckeditor();
        });
    </script>

@stop
