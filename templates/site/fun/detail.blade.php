    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('freetime') }}">Pasa el rato</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <!-- HEADER -->
        @include('site.fun.menu')
        <!-- HEADER -->
      </div>
      <div class="content">
        <div class="row fun">
          <div class="fun-detail">
            <div class="columns large-8">
              <div data-meme-id="{{ $item->id }}" class="row fun-post-detail">
                <div class="columns large-2">
                  <div class="fun-buttons-vote">
                    {{ Form::token() }}
                    <a onclick="_gaq.push(['_trackEvent','Web-Contenido','Like-Meme','{{ $item->title }}'])" href="#" class="vote-icon like-large @if($item->vote_like == 1) active-vote @endif"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-Contenido','UnLike-Meme','{{ $item->title }}'])" href="#" class="vote-icon dislike-large @if($item->vote_dislike == 1) active-vote @endif"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-RedesSociales','Compartir-Meme','{{ $item->title }}'])" href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" data-post-image="@if( isset($item->image->path) ){{ asset($item->image->path) }}@endif" class="vote-icon facebook-share"></a>
                    <a onclick="_gaq.push(['_trackEvent','Web-RedesSociales','Compartir-Meme','{{ $item->title }}'])" href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" class="vote-icon twitter-share"></a>
                  </div>
                </div>
                <div class="columns large-10">
                  <div class="fun-post">
                    <h2>{{ $item->title }}</h2>@if( $item->category == 'videos-graciosos' )
                    <div class="video-block">
                      <iframe width="505" height="315" src="//www.youtube.com/embed/{{ $item->link }}" frameborder="0" allowfullscreen></iframe>
                    </div>@else<a href="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}"><img src="@if( isset($item->image->path) ) {{ asset($item->image->path) }} @endif"></a>@endif
                    <div class="votes">
                      <div class="count">
                        <div class="like icon-fun-like-large"></div><span class="var-like">{{ $item->vote_total_likes }}</span>
                      </div>
                      <div class="count">
                        <div class="dislike icon-fun-dislike-large"></div><span class="var-dislike">{{ $item->vote_total_dislikes }}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="columns large-2"></div>
              <div class="columns large-10 fun-detail-comments">
                <h3>COMENTA</h3>
                <div class="fun-detail-comments">
                  <div data-colorscheme="light" data-width="400" data-href="" class="fb-comments"></div>
                </div>
              </div>
            </div>
            <div class="columns large-4"><a href="{{ URL::route('freetime_category', 'sube-tu-meme') }}" class="btn-upload-meme">sube un meme</a>
              <div class="fun-featured">@foreach ($items as $mini_post)
                <div class="featured-block"><a href="{{ URL::route('freetime_detail', array($mini_post->category, $mini_post->slug)) }}"><img src="@if( isset($mini_post->image->path) ) {{ asset($mini_post->image->path) }} @endif"></a>
                  <div class="featured-description">
                    <h3>{{ $mini_post->title }}</h3>
                    <ul>
                      <li><a href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" data-post-image="@if( isset($mini_post->image->path) ) {{ asset($mini_post->image->path) }} @endif" class="btn-share-fb facebook-share-mini"></a></li>
                      <li><a href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" class="btn-share-tw twitter-share"></a></li>
                    </ul> 
                    <div class="featured-votes">
                      <div class="count">
                        <div class="like"></div><span>{{ $mini_post->vote_total_likes}}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="featured-separator"></div>@endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'memes-detalle'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'memes-detalle'))
    @include('site.register-model')
    <script>
      window.app = window.app || {};
      app.freetime_rank = "{{ URL::route('freetime_rank') }}";
      app.auth = '@if(Auth::check()) auth_true @endif';
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/fun') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
