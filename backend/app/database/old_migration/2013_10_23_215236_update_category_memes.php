<?php

use Illuminate\Database\Migrations\Migration;

class UpdateCategoryMemes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        //
        //
                Schema::table('section_memes', function($t) {
                $t->string('category');
        });
        
        //
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	       Schema::table('section_memes', function($t) {
                $t->dropColumn('category');
           });

	}

}
