require.config({
  paths: {
    'jquery':'../libs/jquery',
    'jquery.scrolltop': '../libs/plugins/jquery.scrolltop',
    'foundation': '../libs/plugins/foundation',

    'viewvideo': '../modules/viewvideo',
    'modal': '../modules/modal',
    'auth': '../modules/auth',
    'register': '../modules/register'
  },
  shim: {
    'jquery.scrolltop': ['jquery'],
    'viewvideo': ['jquery.scrolltop'],
    'auth': ['jquery'],
    'register': ['jquery']
  }
});

require([
  'jquery',
  'viewvideo',
  'modal',
  'auth',
  'register'
], function ($, viewVideo, modal, auth, register) {
  viewVideo();
  modal();
  auth($('#login'));
  register($('#register'));
});
