<?php
class SystemUserTableSeeder extends Seeder {

    public function run()
    {

        DB::table('system_users')->insert(array(
            'id'        => 1,
            'username' => 'admin',
            'password' => '.p4r4qu3.',
            'email' => 'admin@milo.no2.pe',
            'status' => 'active',
            'created_at' => date('Y-m-d H:i:s'),
        ));
    }
}
