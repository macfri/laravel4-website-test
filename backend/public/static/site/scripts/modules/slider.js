define([
  'jquery',
  'foundation'
], function($){

  function slider(){
    $(document).foundation('orbit',{
        timer_speed: 4000,
        pause_on_hover: false,
        animation: 'slide',
        timer:true,
        bullets:true,
        slide_number:false,
        bullets_container_class: 'slider-controls',
        before_slide_change: function(){
          $(".caption h2, .caption p").fadeOut({queue: false});
        },
        after_slide_change: function(){
          $(".caption h2, .caption p").fadeIn({queue: false});
        }
    });
  }
  return slider;
});
