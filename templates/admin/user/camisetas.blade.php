
<table border="1" width="100%">
    <thead>
        <th class="span1">first_name</th>
        <th class="span1">last_name</th>
        <th class="span1">email</th>
        <th class="span1">dni</th>
        <th class="span1">phone</th>
        <th class="span1">is_lima</th>
        <th class="span1">created_at</th>
        <th class="span1">is_new</th>
    </tr>
    </thead>
    @foreach ($items as $p)
    <tr>
        <td>{{ $p->first_name }}</td>
        <td>{{ $p->last_name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->dni }}</td>
        <td>{{ $p->phone }}</td>
        <td>{{ $p->is_lima }}</td>
        <td>{{ $p->created_at }}</td>
        <td>{{ $p->is_new }}</td>
    </tr>
    @endforeach
</table>
