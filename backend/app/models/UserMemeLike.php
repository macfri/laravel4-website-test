<?php

use Illuminate\Auth\UserInterface;

class UserMemeLike extends Eloquent {

    protected $table = 'users_memes_likes';

    public function user() {
        return $this->belongsTo('User');
    }

    public function meme() {
        return $this->belongsTo('Meme');
    }

}
