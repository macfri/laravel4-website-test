<?php

class EventController extends BaseController {

    public function index() {

        Session::set('current_route', URL::current());
        $items = Event::where('important', '=', true)
                ->where('status', '=', 'active')
                ->orderby('event_date', 'desc')
                ->take(5)
                ->get();
        return View::make(
                        'site/events/events', array(
                    'items' => $items
                        )
        );
    }

    public function show() {

        Session::set('current_route', URL::current());
        $items = Event::where(
                        'status', '=', 'active')
                ->orderby('event_date', 'desc')
                ->get();
        return View::make(
                        'site/events/all', array('items' => $items));
    }

    public function detail($slug) {

        Session::set('current_route', URL::current());
        $item = Event::where('slug', '=', $slug)
                ->where('status', '=', 'active')
                ->firstOrFail();
        Seo::addPlaceholder('meta_title', $item->seo_meta_title);
        Seo::addPlaceholder('meta_description', $item->seo_meta_description);
        Seo::addPlaceholder('meta_keywords', $item->seo_meta_keywords);
        return View::make(
                        'site/events/detail', array(
                    'item' => $item
                        )
        );
    }

    public function gallery($slug) {

        Session::set('current_route', URL::current());
        $item = Event::where('slug', '=', $slug)
                ->where('status', '=', 'active')
                ->firstOrFail();
        Seo::addPlaceholder('meta_title', $item->title);
        Seo::addPlaceholder('meta_description', $item->description);
        Seo::addPlaceholder('meta_keywords', $item->title);
        return View::make(
                        'site/events/gallery', array('item' => $item));
    }

}
