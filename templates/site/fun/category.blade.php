    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.menu', array('section'=>''))
        <!-- MENU -->
        </div>
        <div class="nav-bread">
          <div class="row">
            <div class="columns large-12">
              <ul class="breadscrum">
                <li> <span class="shape-bread-here">Estás aquí</span></li>
                <li><a href="{{ URL::route('home') }}">Home</a></li>
                <li><span class="shape-separator"></span></li>
                <li><a href ="{{ URL::route('freetime') }}">Pasa el rato</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="banner"></div>
      </div>
    </div>
    <div class="bg-home">
      <div class="inner">
        <!-- HEADER -->
        @include('site.fun.menu')
        <!-- HEADER -->
      </div>
      <div class="content">
        <div class="row fun">
          <div class="fun-stream">
            <div class="columns large-8">@foreach ($items as $item)
              <div data-meme-id="{{ $item->id }}" class="row fun-post-detail">
                <div class="columns large-2">
                  <div class="fun-buttons-vote">{{ Form::token() }}<a href="#" class="vote-icon like-large @if($item->vote_like == 1) active-vote @endif"></a><a href="#" class="vote-icon dislike-large @if($item->vote_dislike == 1) active-vote @endif"></a><a href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" data-post-image="@if( isset($item->image->path) ){{ asset($item->image->path) }}@endif" class="vote-icon facebook-share"></a><a href="#" data-post-link="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}" class="vote-icon twitter-share"></a></div>
                </div>
                <div class="columns large-10">
                  <div class="fun-post">
                    <h2>{{ $item->title }}</h2>@if( $item->category == 'videos-graciosos' )
                    <div columnsass="video-block">
                      <iframe width="505" height="315" src="//www.youtube.com/embed/{{ $item->link }}" frameborder="0" allowfullscreen></iframe>
                    </div>@else<a href="{{ URL::route('freetime_detail', array($item->category, $item->slug)) }}"><img src="@if( isset($item->image->path) ){{ asset($item->image->path) }}@endif"></a>@endif
                    <div class="votes">
                      <div class="count">
                        <div class="like icon-fun-like-large"></div><span class="var-like">{{ $item->vote_total_likes }}</span>
                      </div>
                      <div class="count">
                        <div class="dislike icon-fun-dislike-large"></div><span class="var-dislike">{{ $item->vote_total_dislikes }}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>@endforeach
            </div>
          @if( $category == 'facebook' )
<!-- 
            <a class="columns large-4 ban-games clearfix" href="#">
                <h3>Banner Juegos Milo</h3>
                <img src="http://dummyimage.com/300x350/fff/fff&text=+">
            </a> -->

          @else

            <div class="columns large-4"><a href="{{ URL::route('freetime_upload') }}" class="upload-meme-link">
                <div class="arrow upload-link-arrow-right"></div><span>Sube un meme</span>
                <div class="arrow upload-link-arrow-left"></div></a></div>        
            </div>


          @endif


        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>'memes-category'))
        <!-- FOOTER -->
      </div>
    </div>
    @include('site.login-model', array('page'=>'meme-category'))
    @include('site.register-model')
    <script>
      window.app = window.app || {};
      app.freetime_rank = "{{ URL::route('freetime_rank') }}";
      app.auth = '@if(Auth::check()) auth_true @endif';
      
    </script>
    <script data-main="{{ asset('static/site/scripts/site/fun') }}" src="{{ asset('static/site/scripts/libs/require.js') }}"></script>
  </body>
</html>
