<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRound extends Migration {


	public function up()
	{
		Schema::create('game_games_round', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->on_update('cascade')
                ->onDelete('cascade');
            $table->integer('score')
                ->unsigned()
                ->nullable();
            $table->integer('tries')
                ->unsigned(0)
                ->nullable()
                ->default(0);
            $table->boolean('guess')
                ->index()
                ->default(0);
            $table->boolean('enabled')
                ->index()
                ->default(0);
            $table->integer('was_anonymous')
                ->default(0);
            $table->integer('lifetime')
                ->unsigned();
            $table->integer('share')
                ->boolean()
                ->default(0);
            $table->timestamps();
        });
	}


	public function down()
    {
        Schema::dropIfExists('game_games_round');
	}
}
