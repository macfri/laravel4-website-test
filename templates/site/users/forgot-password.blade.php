    <!-- HEADER -->
    @include('site.header')
    <!-- HEADER -->
    <div class="page">
      <div class="header">
        <div class="frinje">
          <div class="left patt-left"></div>
          <div class="right patt-right"></div>
        </div>
        <div class="navs">
        <!-- MENU -->
        @include('site.users.menu', array('section'=>''))
        <!-- MENU -->
        </div>
      </div>
    </div>
    <div class="bg-home">
      <div class="content">
        <div id="login-page">
          <div class="row modal-session myform">
            <div class="columns large-12 large-centered rescue-pass">
              <h3>¿No puedes ingresar?</h3>
              <h4><b>No te preocupes, te ayudaremos.</b> Danos la dirección de correo electrónico con la que iniciaste sesión en MILO y te enviaremos las instrucciones para que restablezcas la contraseña.</h4>
              <form action="" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <label>Correo electrónico</label>
                <input type="text" name="email" id="email">@if(Session::has('message'))<label class="error">{{ Session::get('message') }}</label>@endif
                <button type="submit" class="btn-login">Enviar</button>
              </form>
              <div class="mb-50"></div>
            </div>
          </div>
          <div class="mb-50"></div>
        </div>
      </div>
      <div class="footer">
        <!-- FOOTER -->
        @include('site.footer', array('section'=>''))
        <!-- FOOTER -->
      </div>
    </div>
    <script src="{{ asset('static/site/scripts/libs/require.js') }}" data-main="{{ asset('static/site/scripts/site/login') }}"></script>
  </body>
</html>
