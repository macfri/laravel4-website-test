<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMemes extends Migration {

	public function up()
    {
		Schema::create('section_memes', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->string('slug', 100)->index();
            $table->text('description');
            $table->enum('status',
                array('active', 'inactive'))
                ->default('inactive')
                ->index();
            $table->integer('image_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');
            $table->integer('author_id')
                ->unsigned()
                ->index();
            $table->foreign('author_id')
                ->references('id')
                ->on('system_users')
                ->on_update('cascade')
                ->onDelete('cascade');


            $table->text('block_size', 20);
            $table->boolean('show_home')
                ->default(0);
            $table->integer('image_home_id')
                ->unsigned()
                ->nullable()
                ->index();
            $table->foreign('image_home_id')
                ->references('id')
                ->on('files')
                ->on_update('cascade');

            $table->timestamps();
        });
	}

	public function down()
    {
        Schema::dropIfExists('section_memes');
	}
}
