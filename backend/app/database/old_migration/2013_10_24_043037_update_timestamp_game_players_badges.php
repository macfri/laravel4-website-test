<?php

use Illuminate\Database\Migrations\Migration;

class UpdateTimestampGamePlayersBadges extends Migration {

	public function up()
	{
        Schema::table('game_players_badges', function($t) {
            $t->timestamps();
        });
	}

	public function down()
	{
       Schema::table('game_players_badges', function($t) {
        $t->dropColumn('timestamps');
       });
	}
}
