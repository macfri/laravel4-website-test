define([
  'jquery',
  '../libs/plugins/jquery.validate',
  '../libs/plugins/jquery.alphanumeric'
], function($){

  function setupCustomDisplayMessages () {
    if (jQuery.validator && jQuery.validator.setDefaults) {
      jQuery.validator.setDefaults({
        
        highlight: function(element, errorCls, validCls) {
          var el = $(element);
          var parent = el.parent();

          if (el.is('select, :file')) {
            parent.addClass(errorCls + '-custom').removeClass(validCls + '-custom');
          }

          else {
            el.addClass(errorCls).removeClass(validCls);
          }
        },
        errorPlacement : function (error, element) {
          if(element.attr('id') == "accept_tyc"){
            $('.tyc-error').html(error);
          } else {
            error.insertAfter(element);
          }
        },
        unhighlight: function(element, errorCls, validCls) {
          var el = $(element);
          var parent = el.parent();


          if (el.is('select, :file')) {
            parent.removeClass(errorCls + '-custom').addClass(validCls + '-custom');
          }

          else {
            el.addClass(validCls).removeClass(errorCls);
          }
        }   
        
      });
    }
  };
  
  function register($form){

    $('.text').alpha({allow: " "});
    $('.numeric').numeric();

    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 8
        },
        first_name: {
          required: true
        },
        last_name: {
          required: true
        },
        dni: {
          required: true
        },
        phone: {
          required: true,
          minlength: 7
        }
      },
      messages: {
        email: {
          required: 'Olvidaste  ingresar tu correo electrónico.',
          email: 'Necesitas un correo válido'
        },
        password: {
          required: 'Olvidaste ingresar tu contraseña',
          minlength: 'Más de 8 caracteres'
        },
        first_name: {
          required: 'Olvidaste ingresar tu nombre'
        },
        last_name: {
          required: 'Olvidaste ingresar tus apellidos'
        },
        dni: {
          required: 'Olvidaste ingresar el DNI'
        },
        phone: {
          required: 'Olvidaste ingresar tu teléfono'
        },        
        authorize_father: {
          required: 'campo requerido'
        }
      },
      submitHandler: function(form){
        form.submit();
      }
    });

    $form.find('#year').on('change', function(){
      $el = $(this);
      $authorize = $form.find('#authorize_father');
      var year = parseInt( $el.val(), 10 );
      var current_year = new Date().getFullYear();

      if( ( current_year - year ) < 18 ) {

        $authorize.rules('add', {required: true });

      } else {
        $authorize.rules('add',{required: false });

      }
    });
    $('.number').numeric();
  }
  return register;
});
