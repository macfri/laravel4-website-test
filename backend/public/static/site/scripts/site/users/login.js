require.config({
  paths: {
    'jquery':'../../libs/jquery',
    'auth': '../../modules/auth',

  },
  shim: {
    'auth': ['jquery']
  }
});

require([
  'auth'
], function (auth) {
  auth($('#login'));
});
