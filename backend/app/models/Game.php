<?php

class Game extends Eloquent {

    protected $table = 'game_games';
    protected $fillable = array(
        'title',
        'description',
        'slug',
        'brief',
        'show_home',
        'enabled',
        'author_id',
        'photo_id',
        'photo_home_id',
        'block_size',
        'path_swf'
    );

    public function author() {
        return $this->belongsTo('SystemUser');
    }

    public function image() {
        return $this->belongsTo('Photo');
    }

    public function imageShort() {
        return $this->belongsTo('Photo');
    }

    public function imageCover() {
        return $this->belongsTo('Photo');
    }

    public function imageHome() {
        return $this->belongsTo('Photo');
    }

    public function imageCtrl() {
        return $this->belongsTo('Photo');
    }

    public static function validate($input) {
        $rules = array(
            'title' => 'Required|Min:1|Max:100',
            'description' => 'Required|Min:1|Max:250',
            'photo' => 'mimes:jpeg,bmp,png'
        );
        return Validator::make($input, $rules);
    }

    public static function get_slug($title) {
        $slug = Str::slug($title);
        $slugCount = count(self::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function get_face() {
        $points = array(
            'face1' => rand(5, 20) * 1,
            'face2' => rand(2, 10) * 2,
            'face3' => rand(1, 70) * 3,
            'face4' => rand(15, 45) * 4,
            'face5' => rand(15, 87) * 5
        );

        $total = array_sum($points);
        $media = round(array_sum($points) / count($points));
        asort($points);

        $start = null;
        $end = null;
        foreach ($points as $key => $value) {
            if ($media < $value) {
                $end = $key;
                break;
            } else {
                $start = $key;
            }
        }

        $diff1 = $media - $points[$start];
        $diff2 = $points[$end] - $media;

        if ($diff1 < $diff2) {
            $face = $start;
        } else {
            $face = $end;
        }

        return $face;
    }

}
