define([
  'jquery',
], function($){
  function meme(image, canvas, top, bottom){
    top = top || '';
    bottom = bottom || '';

    if (!canvas)
      canvas = 0;

    if (canvas.toUpperCase)
      canvas = document.getElementById(canvas);

    if (($) && (canvas instanceof $))
      canvas = canvas[0];

    if (!(canvas instanceof HTMLCanvasElement))
      throw new Error('No canvas selected');

    var context = canvas.getContext('2d');

    if (!image)
      image = 0;

    if (image.toUpperCase) {
      var src = image;
      image = new Image();
      image.src = src;
    }

    var setCanvasDimensions = function(w, h) {
      canvas.width = w;
      canvas.height = h;
    };
    setCanvasDimensions(image.width, image.height);


    var drawText = function(text, topOrBottom, y) {

      topOrBottom = topOrBottom || 'top';
      var fontSize = (canvas.height / 8);
      var x = canvas.width / 2;
      if (typeof y === 'undefined') {
        y = fontSize;
        if (topOrBottom === 'bottom')
          y = canvas.height - 10;
      }

      if (context.measureText(text).width > (canvas.width * 1.1)) {

        var words = text.split(' '),
          wordsLength = words.length,
          justThis;

        if (topOrBottom === 'top') {
          var i = wordsLength;
          while (i --) {
            justThis = words.slice(0, i).join(' ');
            if (context.measureText(justThis).width < (canvas.width * 1.0)) {
              drawText(justThis, topOrBottom, y);
              drawText(words.slice(i, wordsLength).join(' '), topOrBottom, y + fontSize);
              return;
            }
          }
        }
        else if (topOrBottom === 'bottom') {
          for (var j = 0; j < wordsLength; j ++) {
            justThis = words.slice( j, wordsLength ).join(' ');
            if (context.measureText(justThis).width < (canvas.width * 1.0)) {
              drawText(justThis, topOrBottom, y);
              drawText(words.slice(0, j).join(' '), topOrBottom, y - fontSize);
              return;
            }
          }
        }

      }

      context.fillText(text, x, y, canvas.width * 0.9);
      context.strokeText(text, x, y, canvas.width * 0.9);

    };


    image.onload = function() {


      setCanvasDimensions(this.width, this.height);

      context.drawImage(image, 0, 0);

      context.fillStyle = 'white';
      context.strokeStyle = 'black';
      context.lineWidth = 2;
      var fontSize = (canvas.height / 8);
      context.font = fontSize + 'px Impact';
      context.textAlign = 'center';

      drawText(top, 'top');
      drawText(bottom, 'bottom');

    };

  }
  return meme;
});
