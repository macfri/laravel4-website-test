define([
  'jquery'
], function($){

  function posFixed($nav_fixed, class_fixed){
    var $window = $(window);
    $window.on('scroll', setFixed);
    var nav_pos = $nav_fixed.offset().top;

    function setFixed(){
      if ($window.scrollTop() > nav_pos) {
        $nav_fixed.addClass(class_fixed);
      }
      else {
        $nav_fixed.removeClass(class_fixed);
      }
    }
  }

  return posFixed;
});
