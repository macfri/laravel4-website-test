require.config({
  paths: {
    'jquery':'../libs/jquery',
    'foundation': '../libs/plugins/foundation',
    'modal': '../modules/modal',
    'register': '../modules/register',
    'meme': '../modules/meme',
    'auth': '../modules/auth',
    'swfobject' : '../libs/swfobject'

  },
  shim: {
    'meme': ['jquery']
  }
});

require([
  'jquery',
  'modal',
  'register',
  'auth',
  'meme',
  'swfobject'
], function ( $, modal, register, auth, meme, swf) {

  modal();

  register($('#register'));
  auth($('#login'));
  $(document).ready(function () {
    setupGame();
  });

  function setupGame() {
    var params = {},
        $meme  = $('.meme-generator-bg'),
        flashvars = {},
        defaultImg = $('.app-meme-img:first').attr('src');
    if($meme.data('default') !== ""){
      defaultImg = '';
    }
    flashvars = 'url_image=' + defaultImg;
    params.allowscriptaccess = "always";
    params.wmode = "transparent"; // direct
    params.flashvars = flashvars;

    swfobject.embedSWF($meme.data('static') + "/site/swf/meme.swf?v=2",
        "memegame",
        "220", "220", "10.0.0",
        "",
        flashvars, params);
  };

  window.getUploadDecode=function(){
    
    var defaultImg = '',
        $meme  = $('.meme-generator-bg');
    if($meme.data('default') !== ""){
      defaultImg = $meme.data('default');
    }
    return defaultImg
  }

  function getFlashMovieObject(movieName) {
    if (window.document[movieName]) {
        return window.document[movieName];
    }
    if (navigator.appName.indexOf("Microsoft Internet")==-1) {
      if (document.embeds && document.embeds[movieName])
        return document.embeds[movieName]; 
    }
    else {
      return document.getElementById(movieName);
    }
  }

  var img_src = $('.app-img-canvas').attr('data-meme-image');

  if ( $('.app-img-canvas').length !==0 ) {
    meme( img_src, 'meme-canvas', $('.top-line').val(), $('.bottom-line').val() );

  }

  $('.top-line, .bottom-line').on('focus', function(){
    var $el = $(this);
    var str = $el.val().toLowerCase();
    if (str === 'texto 1' || str === 'texto 2'){
      $el.val('');
    }
  });

  $('.top-line, .bottom-line').on('keyup', function(){
    var flash = getFlashMovieObject('memegame');
    flash.getUppertext($('.top-line').val().toUpperCase());
    flash.getLowerText($('.bottom-line').val().toUpperCase());
  });

  $('.btn-create-meme').on('click', function(event){
    var flash = getFlashMovieObject('memegame'),
        srcBase = flash.generateImage();
    $('#image_str').val(srcBase);
    $('.app-meme-form').submit();
  });

  $('.btn-search-meme').on('click', function(event){
    event.preventDefault();
    $('input#upload-file').trigger("click");
  });

  $('input#upload-file').on('change', function(event){
    $('input#file-root').val($(this).val());
    $('form#upload-image').submit();
  });

  $('.app-meme-img').on('click', function(event){
    event.preventDefault();
    var src = $(this).attr('src'),
        flash = getFlashMovieObject('memegame');

    flash.getUrlClick(src);

    $('.img-bg').attr('src', src);
    $('#image_str').val(src);
    $('.app-img-canvas').attr('data-meme-image', src);
    img_src = $('.app-img-canvas').attr('data-meme-image');
    if ( $('.app-img-canvas').length !==0 ) {
      meme(img_src, 'meme-canvas', $('.top-line').val(), $('.bottom-line').val());

    }
  });
});
