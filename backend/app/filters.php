<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

use Illuminate\Database\Eloquent\ModelNotFoundException;

App::error(function(ModelNotFoundException $e) {
    return Response::view('site.not-found', array(), 404);
});


/*
 * Method to strip tags globally.
 */

function globalXssClean() {
    // Recursive cleaning for array [] inputs, not just strings.
    $sanitized = arrayStripTags(Input::get());
    Input::merge($sanitized);
}

function arrayStripTags($array) {
    $result = array();
    foreach ($array as $key => $value) {
        // Don't allow tags on key either, maybe useful for dynamic forms.
        $key = strip_tags($key);

        // If the value is an array, we will just recurse back into the
        // function to keep stripping the tags out of the array,
        // otherwise we will set the stripped value.
        if (is_array($value)) {
            $result[$key] = arrayStripTags($value);
        } else {
            // I am using strip_tags(), you may use htmlentities(),
            // also I am doing trim() here, you may remove it, if you wish.
            $result[$key] = trim(strip_tags($value));
            //$result[$key] = Jinput($value);
        }
    }
    return $result;
}

App::before(function($request) {
    
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
    header('Access-Control-Allow-Credentials: true');
    
    if (!Request::is('pasa-el-rato/sube-tu-meme/me')) {
        globalXssClean();
    }

    if (!Request::is('iniciar-sesion-facebook') and ! Request::is('iniciar-sesion-google') and ! Request::is('iniciar-sesion') and ! Request::is('iniciar-sesion-game') and ! Request::is('pasa-el-rato/sube-tu-video')) {
        Session::forget('fun_video');
    }

    if (!Request::is('perfil/seleccionar-foto-facebook') and ! Request::is('iniciar-sesion-facebook') and ! Request::is('perfil') and ! Request::is('perfil/editar') and ! Request::is('perfil/editar/password')) {
        Session::forget('choose_photo');
    }

    if (Request::is('admin/*')) {
        if (!Request::is('admin/login') and ! Request::is('admin/logout')) {
            if (!Session::has('session_user')) {
                return Redirect::route('admin_login');
            }
        }
    }
});

App::after(function($request, $response) {
    
});

App::missing(function() {
    return Redirect::route('_404');
});


/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
    if (!Auth::check()) {
        return Redirect::route('user_login');
    }
});


Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (!Request::is('admin/*')) {
        if (Session::token() != Input::get('_token')) {
            return Response::make(
                            'You are not authorized', '403');
        }
    }
});
