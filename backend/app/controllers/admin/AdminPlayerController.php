<?php

class AdminPlayerController extends BaseController {

    private $viewpath = 'admin/player/';

    public function index() {
        $search_title = Input::get('search_title');
        $search_status = Input::get('search_status');

        $items = User::where('id', '>', 0);

        if (strlen($search_title)) {
            $items->where('title', 'LIKE', '%' . $search_title . '%');
        }

        if (strlen($search_status)) {
            $items->where('status', '=', $search_status);
        }

        $total = $items->count();
        $items = $items->paginate(10);

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                    'total' => $total,
                    'search_title' => $search_title,
                    'search_status' => $search_status,
                    'items2' => User::all()
                        )
        );
    }

    public function add() {
        return View::make(
                        $this->viewpath . __function__, array(
                        )
        );
    }

    public function edit($id) {
        $item = User::find($id);

        return View::make(
                        $this->viewpath . __function__, array(
                    'item' => $item,
                        )
        );
    }

    public function save() {
        $v = User::validate(Input::all());
        if ($v->passes()) {
            $item = new User();
            $item->title = Input::get('title');
            $item->slug = User::get_slug(Input::get('title'));
            $item->description = Input::get('description');
            $item->status = Input::get('status');
            $item->author_id = 1;
            $item->is_xtreme = (bool) Input::get('is_xtreme');
            $item->block_size = Input::get('block_size');
            $item->show_home = (bool) Input::get('show_home');

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_user_add')
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'users', $item->slug, 'photo', $ext
                );

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo', $ext)
                );

                $item->image_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_detail')) {
                $file = Input::file('photo_detail');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'users', $item->slug, 'photo_detail', $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo_detail', $ext)
                );

                $item->image_detail_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'users');


                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'home', 'users', $item->slug, $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                $item->image_home_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('photo_slider')) {
                $file = Input::file('photo_slider');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = new Photo();
                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->path = sprintf('%s/%s/%s/%s.%s', 'upload', 'users', $item->slug, 'photo_slider', $ext
                );

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo_slider', $ext)
                );

                $item->image_slider_id = $image->id;

                try {
                    $item->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_add')
                                    ->with('failure', $exception->getMessage());
                }
            }

            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug, 'gallery');

                        $image = new UserPhoto();
                        $image->title = $name;
                        $image->extension = $ext;
                        $image->description = $name;
                        $image->size = $size;
                        $image->mime = $mime;
                        $image->user_id = $item->id;
                        $image->save();

                        $image->path = sprintf('%s/%s/%s/%s/%s.%s', 'upload', 'users', $item->slug, 'gallery', $image->id, $ext
                        );

                        $image->save();

                        $file->move(
                                $upload_path, sprintf('%s.%s', $image->id, $ext)
                        );
                    }
                }
            }

            return Redirect::route('admin_user')
                            ->with('success', sprintf(
                                            'User %s Created Successfully.', $item->title));
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function update($id) {
        $v = User::validate(Input::all());
        if ($v->passes()) {

            $item = User::find($id);
            $item->title = Input::get('title');
            $item->description = Input::get('description');
            $item->show_home = Input::get('show_home');
            $item->block_size = Input::get('block_size');
            $item->is_xtreme = (bool) Input::get('is_xtreme');
            $item->status = Input::get('status');
            $item->author_id = 1;

            try {
                $item->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return Redirect::route('admin_user_edit', $id)
                                ->with('failure', $exception->getMessage());
            }

            if (Input::hasFile('photo')) {
                $file = Input::file('photo');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = Photo::find($item->image_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo', $ext)
                );


                if (isset($is_new)) {
                    $item->image_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_user_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_detail')) {
                $file = Input::file('photo_detail');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = Photo::find($item->image_detail_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%s.%s', 'photo_detail', $ext)
                );


                if (isset($is_new)) {
                    $item->image_detail_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_user_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_home')) {
                $file = Input::file('photo_home');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);


                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'home', 'users');

                $image = Photo::find($item->image_home_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $image->save();

                $file->move(
                        $upload_path, sprintf('%s.%s', $item->slug, $ext)
                );

                if (isset($is_new)) {
                    $item->image_home_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_user_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('photo_slider')) {
                $file = Input::file('photo_slider');
                $name = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $ext = pathinfo($name, PATHINFO_EXTENSION);

                $upload_path = sprintf(
                        '%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug);

                $image = Photo::find($item->image_slider_id);

                if (!$image) {
                    $image = new Photo;
                    $is_new = True;
                }

                $image->title = $name;
                $image->extension = $ext;
                $image->description = $name;
                $image->size = $size;
                $image->mime = $mime;
                $image->original_name = $name;

                try {
                    $image->save();
                } catch (Exception $exception) {
                    Log::error($exception);
                    return Redirect::route('admin_user_edit', $id)
                                    ->with('failure', $exception->getMessage());
                }

                $file->move(
                        $upload_path, sprintf('%d.%s', 'photo_slider', $ext)
                );


                if (isset($is_new)) {
                    $item->image_slider_id = $image->id;

                    try {
                        $item->save();
                    } catch (Exception $exception) {
                        Log::error($exception);
                        return Redirect::route('admin_user_edit', $id)
                                        ->with('failure', $exception->getMessage());
                    }
                }
            }

            if (Input::hasFile('attachment')) {
                $files = Input::file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $name = $file->getClientOriginalName();
                        $size = $file->getSize();
                        $mime = $file->getMimeType();
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $upload_path = sprintf(
                                '%s/%s/%s/%s/%s', public_path(), 'upload', 'users', $item->slug, 'gallery');

                        $image = new UserPhoto();
                        $image->title = $name;
                        $image->extension = $ext;
                        $image->description = $name;
                        $image->size = $size;
                        $image->mime = $mime;
                        $image->user_id = $item->id;
                        $image->save();

                        $image->path = sprintf('%s/%s/%s/%s/%s.%s', 'upload', 'users', $item->slug, 'gallery', $image->id, $ext
                        );

                        $image->save();

                        $file->move(
                                $upload_path, sprintf('%s.%s', $image->id, $ext)
                        );
                    }
                }
            }

            return Redirect::route('admin_user')
                            ->with('success', 'User Created Successfully.');
        } else {
            return Redirect::back()->withErrors($v)->withInput();
        }
    }

    public function destroy($id) {
        User::find($id)->delete();

        return Redirect::route('admin_user')
                        ->with('success', 'User Deleted Successfully.');
    }

    public function gallery_destroy() {
        $id = Input::get('id');
        UserPhoto::find($id)->delete();
        return Response::json(array(
                    'status_code' => 0,
                    'message' => 'delete photo'), 200
        );
    }

    public function report() {
        $items = User::all();

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                        )
        );
    }

    public function report2() {
        $sql = 'select
            name_rg,
            lastname_rg,
            email_rg,
            dni_rg,
            phone_rg,
            date_birth, 
            email_sent 
            from 
            user';

        $items = DB::select(DB::raw($sql));

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                        )
        );
    }

    public function report3() {
        $items = GameChulls::orderby('created_at')->get();

        return View::make(
                        $this->viewpath . __function__, array(
                    'items' => $items,
                        )
        );
    }

    public function export() {
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=say.csv');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $file = fopen("php://output", "w");
        $keys = array('first_name', 'last_name', 'email', 'created_at', 'dni', 'phone');
        $table = User::get($keys)->toArray();
        fputcsv($file, $keys);
        foreach ($table as $row) {
            fputcsv($file, $row);
        }
        fclose($file);
    }

}
